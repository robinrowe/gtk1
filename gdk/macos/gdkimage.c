/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include <Bitmap.h>

#include <config.h>

/* gcc -ansi -pedantic on GNU/Linux causes warnings and errors
 * unless this is defined:
 * warning: #warning "Files using this header must be compiled with _SVID_SOURCE or _XOPEN_SOURCE"
 */
#ifndef _XOPEN_SOURCE
#  define _XOPEN_SOURCE 1
#endif

#include <stdlib.h>
#include <sys/types.h>

#include "gdk/gdk.h"
#include "gdkprivate.h"


static void gdk_image_put_normal (GdkDrawable *drawable,
				  GdkGC       *gc,
				  GdkImage    *image,
				  gint         xsrc,
				  gint         ysrc,
				  gint         xdest,
				  gint         ydest,
				  gint         width,
				  gint         height);


static GList *image_list = NULL;


void
gdk_image_exit (void)
{
  GdkImage *image;

  while (image_list)
    {
      image = (GdkImage*) (image_list->data);
      gdk_image_destroy (image);
    }
}

GdkImage *
gdk_image_new_bitmap(GdkVisual *visual, gpointer data, gint w, gint h)
/*
 * Desc: create a new bitmap image
 */
{
#if 1
  GdkImage *image;
  GdkImagePrivate *priv;
  MacGWindRef macimg = NULL;
  int y;
  int bpr;
  char* srcP = NULL;
  char* dstP = NULL;
  
  priv = g_new (GdkImagePrivate, 1);
  image = (GdkImage *) priv;

  macimg = MacGWind_NewPixMap(w, h, 8+32); // +32 => gray
  
  dstP = MacGWind_GetBits(macimg);
  bpr = MacGWind_GetBytesPerRow(macimg);
  srcP = data;
  for (y = 0; y<h; y++) {
    BlockMoveData(srcP, dstP, w);
    dstP += bpr;
    srcP += w;
  }

  priv->macimg = macimg;
  priv->image_put = gdk_image_put_normal;

  image->type = GDK_IMAGE_NORMAL;
  image->visual = visual;
  image->byte_order = GDK_LSB_FIRST;
  image->width = w;
  image->height = h;
  image->depth = 1;
  image->bpp = 1;
  image->mem = MacGWind_GetBits(macimg);
  image->bpl = MacGWind_GetBytesPerRow(macimg);
  return(image);
  #endif
} /* gdk_image_new_bitmap() */

void
gdk_image_init (void)
{
}

GdkImage*
gdk_image_new (GdkImageType  type,
	       GdkVisual    *visual,
	       gint          width,
	       gint          height)
{
#if 1
  GdkImage *image;
  GdkImagePrivate *priv;
  MacGWindRef macimg = NULL;
  
  switch (type)
    {
    case GDK_IMAGE_SHARED:
      g_warning("gdk_image_new: GDK_IMAGE_SHARED not supported");
      return NULL; /* NOT SUPPORTED */
    case GDK_IMAGE_FASTEST:
      image = gdk_image_new (GDK_IMAGE_NORMAL, visual, width, height);
      break;
    default:
      priv = g_new (GdkImagePrivate, 1);
      image = (GdkImage*) priv;

      macimg = MacGWind_NewPixMap(width, height, 32);

      image->type = type;
      image->visual = visual;
      image->byte_order = GDK_MSB_FIRST;
      image->width = width;
      image->height = height;
      image->depth = 32;
      image->bpp = 4;
      image->bpl = MacGWind_GetBytesPerRow(macimg);
      image->mem = MacGWind_GetBits(macimg);

      priv->macimg = macimg;
      priv->image_put = gdk_image_put_normal;
    }

  return image;
  #endif
}

GdkImage*
gdk_image_bitmap_new (GdkImageType  type,
                      GdkVisual    *visual,
                      gint          width,
                      gint          height)
{
  g_warning("gdk_image_bitmap_new: not implemented!\n");
  return NULL;
}

GdkImage*
gdk_image_get (GdkWindow *window,
	       gint       xsrc,
	       gint       ysrc,
	       gint       width,
	       gint       height)
{
  GdkWindowPrivate *window_private = NULL;
  GdkImage* image = NULL;
  GdkImagePrivate* image_private = NULL;
  CGrafPtr src_port, dst_port;
  Rect src_rect, dst_rect;
  BitMap* src_bm = NULL;
  BitMap* dst_bm = NULL;
  
  g_return_val_if_fail (window != NULL, NULL);

  window_private = (GdkWindowPrivate*) window;

  image = gdk_image_new(GDK_IMAGE_NORMAL, (GdkVisual*)(NULL), width, height);
  g_return_val_if_fail (image != NULL, NULL);
  
  image_private = (GdkImagePrivate*) image;
  
  MacGWind_FocusPort(image_private->macimg);
  
  MacSetRect(&src_rect, xsrc, ysrc, xsrc+width, ysrc+height);
  MacSetRect(&dst_rect, 0, 0, 0+width, 0+height);
  
  src_bm = MacGWind_GetMacBitmap(window_private->macgwin);
  dst_bm = MacGWind_GetMacBitmap(image_private->macimg);

  
  PenNormal();
  ForeColor(blackColor);
  BackColor(whiteColor);
  CopyBits(src_bm, dst_bm, &src_rect, &dst_rect, srcCopy, NULL);


  
  return image;
}

guint32
gdk_image_get_pixel (GdkImage *image,
		     gint x,
		     gint y)
{
  guint32 *pixel;
  guint32 position;

  g_return_val_if_fail (image != NULL, 0);

  pixel = (guint32 *) ((char*)image->mem + y * image->bpl + x * image->bpp);

  return *pixel;
}

void
gdk_image_put_pixel (GdkImage *image,
		     gint x,
		     gint y,
		     guint32 pixel)
{
#if 1
  guint32 *pixelp;

  g_return_if_fail (image != NULL);
  g_return_if_fail (x >= 0 && 
		    x < image->width && 
  		    y >= 0 && 
		    y < image->height);

  pixelp = (guint32 *) ((char*)image->mem + y * image->bpl + x * image->bpp);

  *pixelp = pixel;
#endif
}

void
gdk_image_destroy (GdkImage *image)
{
#if 1
  GdkImagePrivate *priv;

  g_return_if_fail (image != NULL);

  priv = (GdkImagePrivate*) image;
  switch (image->type)
    {
    case GDK_IMAGE_NORMAL:
      {
        MacGWind_Release(&priv->macimg);
      }
      break;

    case GDK_IMAGE_SHARED:
      g_error ("trying to destroy shared memory image when gdk was compiled without shared memory support");
      break;

    default:
      g_assert_not_reached ();
    }

  g_free (image);
#endif
}

static void
gdk_image_put_normal (GdkDrawable *drawable,
		      GdkGC       *gc,
		      GdkImage    *image,
		      gint         xsrc,
		      gint         ysrc,
		      gint         xdest,
		      gint         ydest,
		      gint         width,
		      gint         height)
{
#if 1
  GdkWindowPrivate *drawable_private;
  GdkImagePrivate *image_private;
  GdkGCPrivate *gc_private;
  CGrafPtr src_port, dst_port;
  Rect src_rect, dst_rect;
  BitMap* src_bm = NULL;
  BitMap* dst_bm = NULL;

  g_return_if_fail (drawable != NULL);
  g_return_if_fail (image != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;

  image_private = (GdkImagePrivate*) image;
  gc_private = (GdkGCPrivate*) gc;

  g_return_if_fail (image->type == GDK_IMAGE_NORMAL);

  gdk_gc_predraw (drawable_private->macgwin, gc_private);

  MacSetRect(&src_rect, xsrc, ysrc, xsrc+width, ysrc+height);
  MacSetRect(&dst_rect, xdest, ydest, xdest+width, ydest+height);
  
  src_bm = MacGWind_GetMacBitmap(image_private->macimg);
  dst_bm = MacGWind_GetMacBitmap(drawable_private->macgwin);

  //LockPixels(src_bm);
  //LockPixels(dst_bm);

  ForeColor(blackColor);
  BackColor(whiteColor);
  CopyBits(src_bm, dst_bm, &src_rect, &dst_rect, srcCopy, NULL);
    
  //UnlockPixels(src_bm);
  //UnlockPixels(dst_bm);

  gdk_gc_postdraw (drawable_private->macgwin, gc_private);
 #endif
}
