/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include <Application.h>

#include "MacSetup.h"
#include "MacCarbonEvents.h"

#include <string.h>

#include "gdkconfig.h"
#include "gdk/gdk.h"
#include "gdkx.h"
#include "gdkprivate.h"
#include "gdkinput.h"
#include "gdkkeysyms.h"


typedef struct _GdkIOClosure GdkIOClosure;
typedef struct _GdkEventPrivate GdkEventPrivate;

#define DOUBLE_CLICK_TIME      250
#define TRIPLE_CLICK_TIME      500
#define DOUBLE_CLICK_DIST      5
#define TRIPLE_CLICK_DIST      5

typedef enum
{
  /* Following flag is set for events on the event queue during
   * translation and cleared afterwards.
   */
  GDK_EVENT_PENDING = 1 << 0
} GdkEventFlags;

struct _GdkIOClosure
{
  GdkInputFunction function;
  GdkInputCondition condition;
  GdkDestroyNotify notify;
  gpointer data;
};

struct _GdkEventPrivate
{
  GdkEvent event;
  guint    flags;
};

/* 
 * Private function declarations
 */

/*static*/ GdkEvent *gdk_event_new		(void);

/* moved declaration to gdkprivate.h */
/* static GdkEvent* gdk_event_unqueue      (void); */

static gboolean  gdk_event_prepare      (gpointer   source_data, 
				 	 GTimeVal  *current_time,
					 gint      *timeout,
					 gpointer  user_data);
static gboolean  gdk_event_check        (gpointer   source_data,
				 	 GTimeVal  *current_time,
				 	 gpointer  user_data);
static gboolean  gdk_event_dispatch     (gpointer   source_data,
					 GTimeVal  *current_time,
					 gpointer   user_data);

void	 gdk_synthesize_click	(GdkEvent  *event, 
					 gint	    nclicks);

GdkFilterReturn gdk_wm_protocols_filter (GdkXEvent *xev,
					 GdkEvent  *event,
					 gpointer   data);

/* Private variable declarations
 */

guint32 button_click_time[2];	    /* The last 2 button click times. Used
					     *	to determine if the latest button click
					     *	is part of a double or triple click.
					     */
GdkWindow *button_window[2];	    /* The last 2 windows to receive button presses.
					     *	Also used to determine if the latest button
					     *	click is part of a double or triple click.
					     */
guint button_number[2];		    /* The last 2 buttons to be pressed.
					     */
/*static*/ GdkEventFunc   event_func = NULL;    /* Callback for events */
/*static*/ gpointer       event_data = NULL;
static GDestroyNotify event_notify = NULL;

static GList *client_filters;	            /* Filters for client messages */

/* FIFO's for event queue, and for events put back using
 * gdk_event_put().
 */
static GList *queued_events = NULL;
static GList *queued_tail = NULL;

static GSourceFuncs event_funcs = {
  gdk_event_prepare,
  gdk_event_check,
  gdk_event_dispatch,
  (GDestroyNotify)g_free
};

/*********************************************
 * Functions for maintaining the event queue *
 *********************************************/

/*************************************************************
 * gdk_event_queue_find_first:
 *     Find the first event on the queue that is not still
 *     being filled in.
 *   arguments:
 *     
 *   results:
 *     Pointer to the list node for that event, or NULL
 *************************************************************/

static GList*
gdk_event_queue_find_first (void)
{
  GList *tmp_list = queued_events;
  /*
  GList *tmp_list;
  if (!queued_events)
      ProcessMacEvent();
  */
    
  tmp_list = queued_events;
  
  while (tmp_list)
    {
      GdkEventPrivate *event = (GdkEventPrivate*) tmp_list->data;
      g_return_val_if_fail(event != NULL, NULL);
      if (!(event->flags & GDK_EVENT_PENDING))
        {
	  return tmp_list;
        }

      tmp_list = g_list_next (tmp_list);
    }

  return NULL;
}

/*************************************************************
 * gdk_event_queue_remove_link:
 *     Remove a specified list node from the event queue.
 *   arguments:
 *     node: Node to remove.
 *   results:
 *************************************************************/

static void
gdk_event_queue_remove_link (GList *node)
{
  if (node->prev)
    node->prev->next = node->next;
  else
    queued_events = node->next;
  
  if (node->next)
    node->next->prev = node->prev;
  else
    queued_tail = node->prev;
}

/*************************************************************
 * gdk_event_queue_append:
 *     Append an event onto the tail of the event queue.
 *   arguments:
 *     event: Event to append.
 *   results:
 *************************************************************/

/*static*/ void
gdk_event_queue_append (GdkEvent *event)
{
  queued_tail = g_list_append (queued_tail, event);
  
  if (!queued_events)
    queued_events = queued_tail;
  else
    queued_tail = queued_tail->next;
    
  /* signal Mac Carbon event RunLoopSource that there is now event for dispatch */
  CFRunLoopSourceSignal (g_mac_process_gdk_event_queue_source);
  CFRunLoopWakeUp (g_mac_main_run_loop);
}

void 
gdk_events_init (void)
{
  setup_app_carbon_event_handlers ();
  button_click_time[0] = 0;
  button_click_time[1] = 0;
  button_window[0] = NULL;
  button_window[1] = NULL;
  button_number[0] = (guint) -1;
  button_number[1] = (guint) -1;
}

/*
 *--------------------------------------------------------------
 * gdk_events_pending
 *
 *   Returns if events are pending on the queue.
 *
 * Arguments:
 *
 * Results:
 *   Returns TRUE if events are pending
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

gboolean
gdk_events_pending (void)
{
  int ret;

  GDK_THREADS_ENTER ();
  ret = (gdk_event_queue_find_first() != NULL);
  GDK_THREADS_LEAVE ();
  
  return ret;
}

/*
 *--------------------------------------------------------------
 * gdk_event_get_graphics_expose
 *
 *   Waits for a GraphicsExpose or NoExpose event
 *
 * Arguments:
 *
 * Results: 
 *   For GraphicsExpose events, returns a pointer to the event
 *   converted into a GdkEvent Otherwise, returns NULL.
 *
 * Side effects:
 *
 *-------------------------------------------------------------- */

GdkEvent*
gdk_event_get_graphics_expose (GdkWindow *window)
{
  g_warning("gdk_event_get_graphics_expose: not implemented\n");
  
  return NULL;	
}

/*************************************************************
 * gdk_event_handler_set:
 *     
 *   arguments:
 *     func: Callback function to be called for each event.
 *     data: Data supplied to the function
 *     notify: function called when function is no longer needed
 * 
 *   results:
 *************************************************************/

void 
gdk_event_handler_set (GdkEventFunc   func,
		       gpointer       data,
		       GDestroyNotify notify)
{
  if (event_notify)
    (*event_notify) (event_data);

  event_func = func;
  event_data = data;
  event_notify = notify;
}

/*
 *--------------------------------------------------------------
 * gdk_event_get
 *
 *   Gets the next event.
 *
 * Arguments:
 *
 * Results:
 *   If an event is waiting that we care about, returns 
 *   a pointer to that event, to be freed with gdk_event_free.
 *   Otherwise, returns NULL.
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

GdkEvent*
gdk_event_get (void)
{
    return gdk_event_unqueue ();

}

/*
 *--------------------------------------------------------------
 * gdk_event_peek
 *
 *   Gets the next event.
 *
 * Arguments:
 *
 * Results:
 *   If an event is waiting that we care about, returns 
 *   a copy of that event, but does not remove it from
 *   the queue. The pointer is to be freed with gdk_event_free.
 *   Otherwise, returns NULL.
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

GdkEvent*
gdk_event_peek (void)
{
  GList *tmp_list;
  GdkEvent *event;

  GDK_THREADS_ENTER ();

  tmp_list = gdk_event_queue_find_first ();
  
  if (tmp_list)
    event = gdk_event_copy ((GdkEvent*) tmp_list->data);
  else
    event = NULL;

  GDK_THREADS_LEAVE ();

  return event;
}

void
gdk_event_put (GdkEvent *event)
{
  GdkEvent *new_event;

  g_return_if_fail (event != NULL);
  

  new_event = gdk_event_copy (event);

  GDK_THREADS_ENTER ();
  gdk_event_queue_append (new_event);
  GDK_THREADS_LEAVE ();
}

/*
 *--------------------------------------------------------------
 * gdk_event_copy
 *
 *   Copy a event structure into new storage.
 *
 * Arguments:
 *   "event" is the event struct to copy.
 *
 * Results:
 *   A new event structure.  Free it with gdk_event_free.
 *
 * Side effects:
 *   The reference count of the window in the event is increased.
 *
 *--------------------------------------------------------------
 */

static GMemChunk *event_chunk = NULL;

GdkEvent*
gdk_event_new (void)
{
  GdkEventPrivate *new_event;

  if (event_chunk == NULL)
    event_chunk = g_mem_chunk_new ("events",
				   sizeof (GdkEventPrivate),
				   4096,
				   G_ALLOC_AND_FREE);
  
  new_event = g_chunk_new (GdkEventPrivate, event_chunk);
  new_event->flags = 0;
  
  return (GdkEvent*) new_event;
}

GdkEvent*
gdk_event_copy (GdkEvent *event)
{
  GdkEvent *new_event;

  g_return_val_if_fail (event != NULL, NULL);
  g_return_val_if_fail (event->any.window != NULL, NULL);
  
  new_event = gdk_event_new ();
  
  *new_event = *event;
  gdk_window_ref (new_event->any.window);
  
  switch (event->any.type)
    {
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      new_event->key.string = g_strdup (event->key.string);
      break;
      
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      if (event->crossing.subwindow != NULL)
	gdk_window_ref (event->crossing.subwindow);
      break;
      
    case GDK_DRAG_ENTER:
    case GDK_DRAG_LEAVE:
    case GDK_DRAG_MOTION:
    case GDK_DRAG_STATUS:
    case GDK_DROP_START:
    case GDK_DROP_FINISHED:
      gdk_drag_context_ref (event->dnd.context);
      break;
      
    default:
      break;
    }
  
  return new_event;
}

/*
 *--------------------------------------------------------------
 * gdk_event_free
 *
 *   Free a event structure obtained from gdk_event_copy.  Do not use
 *   with other event structures.
 *
 * Arguments:
 *   "event" is the event struct to free.
 *
 * Results:
 *
 * Side effects:
 *   The reference count of the window in the event is decreased and
 *   might be freed, too.
 *
 *-------------------------------------------------------------- */

void
gdk_event_free (GdkEvent *event)
{
  g_return_if_fail (event != NULL);

  g_assert (event_chunk != NULL); /* paranoid */
  
  if (event->any.window)
    gdk_window_unref (event->any.window);
  
  switch (event->any.type)
    {
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      g_free (event->key.string);
      break;
      
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      if (event->crossing.subwindow != NULL)
	gdk_window_unref (event->crossing.subwindow);
      break;
      
    case GDK_DRAG_ENTER:
    case GDK_DRAG_LEAVE:
    case GDK_DRAG_MOTION:
    case GDK_DRAG_STATUS:
    case GDK_DROP_START:
    case GDK_DROP_FINISHED:
      gdk_drag_context_unref (event->dnd.context);
      break;
      
    default:
      break;
    }
  
  g_mem_chunk_free (event_chunk, event);
}

/*
 *--------------------------------------------------------------
 * gdk_event_get_time:
 *    Get the timestamp from an event.
 *   arguments:
 *     event:
 *   results:
 *    The event's time stamp, if it has one, otherwise
 *    GDK_CURRENT_TIME.
 *--------------------------------------------------------------
 */

guint32
gdk_event_get_time (GdkEvent *event)
{
  if (event)
    switch (event->type)
      {
      case GDK_MOTION_NOTIFY:
	return event->motion.time;
      case GDK_BUTTON_PRESS:
      case GDK_2BUTTON_PRESS:
      case GDK_3BUTTON_PRESS:
      case GDK_BUTTON_RELEASE:
      case GDK_SCROLL:
	return event->button.time;
      case GDK_KEY_PRESS:
      case GDK_KEY_RELEASE:
	return event->key.time;
      case GDK_ENTER_NOTIFY:
      case GDK_LEAVE_NOTIFY:
	return event->crossing.time;
      case GDK_PROPERTY_NOTIFY:
	return event->property.time;
      case GDK_SELECTION_CLEAR:
      case GDK_SELECTION_REQUEST:
      case GDK_SELECTION_NOTIFY:
	return event->selection.time;
      case GDK_PROXIMITY_IN:
      case GDK_PROXIMITY_OUT:
	return event->proximity.time;
      case GDK_DRAG_ENTER:
      case GDK_DRAG_LEAVE:
      case GDK_DRAG_MOTION:
      case GDK_DRAG_STATUS:
      case GDK_DROP_START:
      case GDK_DROP_FINISHED:
	return event->dnd.time;
      default:			/* use current time */
	break;
      }
  
  return GDK_CURRENT_TIME;
}

/*
 *--------------------------------------------------------------
 * gdk_set_show_events
 *
 *   Turns on/off the showing of events.
 *
 * Arguments:
 *   "show_events" is a boolean describing whether or
 *   not to show the events gdk receives.
 *
 * Results:
 *
 * Side effects:
 *   When "show_events" is TRUE, calls to "gdk_event_get"
 *   will output debugging informatin regarding the event
 *   received to stdout.
 *
 *--------------------------------------------------------------
 */

void
gdk_set_show_events (gint show_events)
{
  if (show_events)
    gdk_debug_flags |= GDK_DEBUG_EVENTS;
  else
    gdk_debug_flags &= ~GDK_DEBUG_EVENTS;
}

gint
gdk_get_show_events (void)
{
  return gdk_debug_flags & GDK_DEBUG_EVENTS;
}

static void
gdk_io_destroy (gpointer data)
{
  GdkIOClosure *closure = (GdkIOClosure*) data;

  if (closure->notify)
    closure->notify (closure->data);

  g_free (closure);
}

static gboolean  
gdk_io_invoke (GIOChannel   *source,
	       GIOCondition  condition,
	       gpointer      data)
{
  GdkIOClosure *closure = (GdkIOClosure*) data;
  GdkInputCondition gdk_cond = (GdkInputCondition) 0;

  if (condition & (G_IO_IN | G_IO_PRI))
    gdk_cond = (GdkInputCondition) (gdk_cond | GDK_INPUT_READ);
  if (condition & G_IO_OUT)
    gdk_cond = (GdkInputCondition) (gdk_cond | GDK_INPUT_WRITE);
  if (condition & (G_IO_ERR | G_IO_HUP | G_IO_NVAL))
    gdk_cond = (GdkInputCondition) (gdk_cond | GDK_INPUT_EXCEPTION);

  if (closure->condition & gdk_cond)
    closure->function (closure->data, g_io_channel_unix_get_fd (source), gdk_cond);

  return TRUE;
}

gint
gdk_input_add_full (gint	      source,
		    GdkInputCondition condition,
		    GdkInputFunction  function,
		    gpointer	      data,
		    GdkDestroyNotify  destroy)
{
  guint result;
  GdkIOClosure *closure = g_new (GdkIOClosure, 1);
  GIOChannel *channel;
  GIOCondition cond = (GIOCondition) 0;

  closure->function = function;
  closure->condition = condition;
  closure->notify = destroy;
  closure->data = data;

  if (condition & GDK_INPUT_READ)
    cond = (GIOCondition) (cond | (G_IO_IN | G_IO_PRI));
  if (condition & GDK_INPUT_WRITE)
    cond = (GIOCondition) (cond | G_IO_OUT);
  if (condition & GDK_INPUT_EXCEPTION)
    cond = (GIOCondition) (cond | G_IO_ERR|G_IO_HUP|G_IO_NVAL);

  channel = g_io_channel_unix_new (source);
  result = g_io_add_watch_full (channel, G_PRIORITY_DEFAULT, cond, 
				gdk_io_invoke,
				closure, gdk_io_destroy);
  g_io_channel_unref (channel);

  return result;
}

gint
gdk_input_add (gint		 source,
	       GdkInputCondition condition,
	       GdkInputFunction	 function,
	       gpointer		 data)
{
  return gdk_input_add_full (source, condition, function, data, NULL);
}

void
gdk_input_remove (gint tag)
{
  g_source_remove (tag);
}

void 
gdk_add_client_message_filter (GdkAtom       message_type,
			       GdkFilterFunc func,
			       gpointer      data)
{
  GdkClientFilter *filter = g_new (GdkClientFilter, 1);

  filter->type = message_type;
  filter->function = func;
  filter->data = data;
  
  client_filters = g_list_prepend (client_filters, filter);
}

GdkFilterReturn
gdk_wm_protocols_filter (GdkXEvent *xev,
		     GdkEvent  *event,
		     gpointer data)
{
  g_warning("gdk_wm_prorocols_filter: unimplmented");

  return GDK_FILTER_REMOVE;
}

static gboolean  
gdk_event_prepare (gpointer  source_data, 
		   GTimeVal *current_time,
		   gint     *timeout,
                   gpointer  user_data)
{
  gboolean retval;
  
  GDK_THREADS_ENTER ();

  *timeout = -1;

  retval = (gdk_event_queue_find_first () != NULL);

  GDK_THREADS_LEAVE ();

  return retval;
}

static gboolean  
gdk_event_check (gpointer  source_data,
		 GTimeVal *current_time,
                 gpointer  user_data)
{
  gboolean retval;

  GDK_THREADS_ENTER ();

  retval = (gdk_event_queue_find_first () != NULL);

  GDK_THREADS_LEAVE ();

  return retval;
}

GdkEvent*
gdk_event_unqueue (void)
{
  GdkEvent *event = NULL;
  GList *tmp_list;

  GDK_THREADS_ENTER ();

  tmp_list = gdk_event_queue_find_first ();

  if (tmp_list)
    {
      event = (GdkEvent *) tmp_list->data;
      gdk_event_queue_remove_link (tmp_list);
      g_list_free_1 (tmp_list);
    }

  GDK_THREADS_LEAVE ();
  
  return event;
}

static gboolean  
gdk_event_dispatch (gpointer  source_data,
		    GTimeVal *current_time,
		    gpointer  user_data)
{
  GdkEvent *event;

  GDK_THREADS_ENTER ();

  event = gdk_event_unqueue();

  if (event)
    {
      if (event_func)
	(*event_func) (event, event_data);
      
      gdk_event_free (event);
    }
  
  GDK_THREADS_LEAVE ();

  return TRUE;
}

void
gdk_synthesize_click (GdkEvent *event,
		      gint	nclicks)
{
  GdkEvent temp_event;

  g_return_if_fail (event != NULL);
  
  temp_event = *event;
  temp_event.type = (nclicks == 2) ? GDK_2BUTTON_PRESS : GDK_3BUTTON_PRESS;
  
  gdk_event_put (&temp_event);
}

void
gdk_event_button_generate (GdkEvent *event)
{
  if ((event->button.time < (button_click_time[1] + TRIPLE_CLICK_TIME)) &&
      (event->button.window == button_window[1]) &&
      (event->button.button == button_number[1]))
    {
      gdk_synthesize_click (event, 3);
      
      button_click_time[1] = 0;
      button_click_time[0] = 0;
      button_window[1] = NULL;
      button_window[0] = 0;
      button_number[1] = -1;
      button_number[0] = -1;
    }
  else if ((event->button.time < (button_click_time[0] + DOUBLE_CLICK_TIME)) &&
	   (event->button.window == button_window[0]) &&
	   (event->button.button == button_number[0]))
    {
      gdk_synthesize_click (event, 2);
      
      button_click_time[1] = button_click_time[0];
      button_click_time[0] = event->button.time;
      button_window[1] = button_window[0];
      button_window[0] = event->button.window;
      button_number[1] = button_number[0];
      button_number[0] = event->button.button;
    }
  else
    {
      button_click_time[1] = 0;
      button_click_time[0] = event->button.time;
      button_window[1] = NULL;
      button_window[0] = event->button.window;
      button_number[1] = -1;
      button_number[0] = event->button.button;

      MyPostEvent( event );
    }
}

/* Sends a ClientMessage to all toplevel client windows */
gboolean
gdk_event_send_client_message (GdkEvent *event, guint32 xid)
{
  g_return_val_if_fail(event != NULL, FALSE);
  
  /* Set up our event to send, with the exception of its target window */
  
  g_warning("gdk_event_send_client_message: not implemented!");  
  return 0;
}

void
gdk_event_send_clientmessage_toall (GdkEvent *event)
{
  g_return_if_fail(event != NULL);

  g_warning("gdk_event_send_client_message_toall: not implemented!");  
}

/*
 *--------------------------------------------------------------
 * gdk_flush
 *
 *   Flushes the Xlib output buffer and then waits
 *   until all requests have been received and processed
 *   by the X server. The only real use for this function
 *   is in dealing with XShm.
 *
 * Arguments:
 *
 * Results:
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

void
gdk_flush (void)
{
//!!!MACOS_TO_DO!!!
}



///// justin

void debug_event_print(GdkEvent* event) {
    
    if (!event) {
        printf("null event\n");
    }
    if (event->type == GDK_BUTTON_PRESS) {
        GdkEventButton* buttonEvent = (GdkEventButton*)event;
        printf("click at %i,%i!\n", buttonEvent->x, buttonEvent->y);

    }
    else if (event->type == GDK_BUTTON_RELEASE) {
        GdkEventButton* buttonEvent = (GdkEventButton*)event;
        printf("release at %i,%i!\n",(int) buttonEvent->x, (int)buttonEvent->y);
    }
    else if (event->type == GDK_KEY_PRESS) {
        GdkEventKey* keyEvent = (GdkEventKey*)event;
        if (keyEvent->length > 0)
            printf("key pressed = %s\n", keyEvent->string);
        printf("keysym name = %s\n", gdk_keyval_name(keyEvent->keyval));

    }
    else if (event->type == GDK_KEY_RELEASE) {
        GdkEventKey* keyEvent = (GdkEventKey*)event;
        if (keyEvent->length > 0)
            printf("key released = %s\n", keyEvent->string);
        printf("keysym name = %s\n", gdk_keyval_name(keyEvent->keyval));

    }
    else if (event->type == GDK_MOTION_NOTIFY) {
        GdkEventMotion* motionEvent = (GdkEventMotion*)event;
        printf("mouse moved at %i,%i!\n", (int)motionEvent->x, (int)motionEvent->y);

    }
    else if (event->type == GDK_ENTER_NOTIFY) {
        printf("gdk_enter_notify\n");

    }
    else if (event->type == GDK_LEAVE_NOTIFY) {
        printf("gdk_leave_notify\n");

    }
    else if (event->type == GDK_DRAG_ENTER) {
        printf("gdk_drag_enter\n");

    }
    else if (event->type == GDK_DRAG_LEAVE) {
        printf("gdk_drag_leave\n");

    }
    else if (event->type == GDK_DRAG_MOTION) {
        printf("gdk_drag_motion\n");

    }
    else if (event->type == GDK_DROP_START) {
        printf("gdk_drop_start\n");

    }
    else if (event->type == GDK_DROP_FINISHED) {
        printf("gdk_drop_finished\n");

    }
    else if (event->type == GDK_EXPOSE) {
        printf("gdk_expose\n");

    }
    else if (event->type == GDK_CONFIGURE) {
        printf("gdk_configure\n");

    }
    else
        printf("\nevent in %d is of type %d\n",((GdkEventAny*)event)->window, ((GdkEventAny*)event)->type);

}


