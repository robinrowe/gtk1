/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "config.h"


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <gdk/gdk.h>
#include "gdkconfig.h"
#include "gdkprivate.h"

//#include <QuickDraw.h>
//#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>


static GHashTable *font_name_hash = NULL;
static GHashTable *fontset_name_hash = NULL;

static void
gdk_font_hash_insert (GdkFontType type, GdkFont *font, const gchar *font_name)
{
	GdkFontPrivate *private = (GdkFontPrivate *)font;
	GHashTable **hashp = (type == GDK_FONT_FONT) ?
		&font_name_hash : &fontset_name_hash;

	if (!*hashp)
		*hashp = g_hash_table_new (g_str_hash, g_str_equal);

	private->names = g_slist_prepend (private->names, g_strdup (font_name));
	g_hash_table_insert (*hashp, private->names->data, font);
}

static void
gdk_font_hash_remove (GdkFontType type, GdkFont *font)
{
	GdkFontPrivate *private = (GdkFontPrivate *)font;
	GSList *tmp_list;
	GHashTable *hash = (type == GDK_FONT_FONT) ?
font_name_hash : fontset_name_hash;

	tmp_list = private->names;
	while (tmp_list)
    {
		g_hash_table_remove (hash, tmp_list->data);
		g_free (tmp_list->data);

		tmp_list = tmp_list->next;
    }

	g_slist_free (private->names);
	private->names = NULL;
}

static GdkFont *
gdk_font_hash_lookup (GdkFontType type, const gchar *font_name)
{
	GdkFont *result;
	GHashTable *hash = (type == GDK_FONT_FONT) ?
font_name_hash : fontset_name_hash;

	if (!hash)
		return NULL;
	else
    {
		result = g_hash_table_lookup (hash, font_name);
		if (result)
			gdk_font_ref (result);

		return result;
    }
}

GdkFont*
gdk_font_load (const gchar *font_name)
{
  GdkFont *font;
  GdkFontPrivate *priv;

  int numfields;
  int n1, n2, c;
  char *p;

  char foundry[32];
  char family[100];
  char weight[32];
  char slant[32];
  char set_width[32];
  char spacing[32];
  char registry[32];
  char encoding[32];
  char pixel_size[10];
  char point_size[10];
  char res_x[10];
  char res_y[10];
  char avg_width[10];

  const char *searchfont;

  int fnHeight, fnFace, fnEncoding, fnSpacing;
  short fnFontId = 0;
  Str255 pascal_font_name;
  guint *fontxid;
  fnHeight = 10;

  g_warning("gdk_font_load: CALLED with %s", font_name);
  g_return_val_if_fail (font_name != NULL, NULL);

  font = gdk_font_hash_lookup (GDK_FONT_FONT, font_name);
  if (font)
	  return font;
  
  numfields = sscanf (font_name,
                      "-%30[^-]-%100[^-]-%30[^-]-%30[^-]-%30[^-]-%n",
                      foundry,
                      family,
                      weight,
                      slant,
                      set_width,
                      &n1);


  if (numfields == 0)
  {
	  /* Assuming it's a regular font name */

	  // get font id from name
	  CopyCStringToPascal( font_name, pascal_font_name );
	  GetFNum( pascal_font_name, &fnFontId );

	  fnHeight = 10;
	  fnFace = normal;
  }
  else if (numfields != 5)
  {
	  g_warning ("gdk_font_load: font name %s illegal", font_name);
	  g_free (font);
	  return NULL;
  }
  else
  {
	  /* It must be a XLFD name */

      /* Check for hex escapes in the font family,
	  * put in there by gtkfontsel.
	  */
      p = family;
      while (*p)
	  {
          if (*p == '%' && isxdigit (p[1]) && isxdigit (p[2]))
		  {
              sscanf (p+1, "%2x", &c);
              *p = c;
              strcpy (p+1, p+3);
		  }
          p++;
	  }

      /* Skip add_style which often is empty in the requested font name */
      while (font_name[n1] && font_name[n1] != '-')
		  n1++;
      numfields++;

      numfields += sscanf (font_name + n1,
                           "-%8[^-]-%8[^-]-%8[^-]-%8[^-]-%30[^-]-%8[^-]-%30[^-]-%30[^-]%n",
                           pixel_size,
                           point_size,
                           res_x,
                           res_y,
                           spacing,
                           avg_width,
                           registry,
                           encoding,
                           &n2);

      if (numfields != 14 || font_name[n1 + n2] != '\0')
	  {
          g_warning ("gdk_font_load: font name %s illegal", font_name);
          g_free (font);
          return NULL;
	  }

      if (strcmp (pixel_size, "*") == 0)
	  {
		  if (strcmp (point_size, "*") == 0)
			  fnHeight = 10;
		  else
			  fnHeight = atoi(point_size) / 10;
	  }
      else
		  fnHeight = atoi (pixel_size);

      fnFace = 0;

      if (g_strcasecmp (weight, "thin") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "extralight") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "ultralight") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "light") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "normal") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "regular") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "medium") == 0)
		  fnFace |= normal;
      else if (g_strcasecmp (weight, "semibold") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "demibold") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "bold") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "extrabold") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "ultrabold") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "heavy") == 0)
		  fnFace |= bold;
      else if (g_strcasecmp (weight, "black") == 0)
		  fnFace |= bold;
      else
		  fnFace |= normal;

	  if (g_strcasecmp (slant, "italic") == 0
	   || g_strcasecmp (slant, "oblique") == 0
	   || g_strcasecmp (slant, "i") == 0
	   || g_strcasecmp (slant, "o") == 0)
	  {
          fnFace |= italic;
          fnFace &= ~normal;
	  }

	  // get font id from name
	  CopyCStringToPascal( family, pascal_font_name );
	  GetFNum( pascal_font_name, &fnFontId );

	  printf( "Found font: %s, %d pt (id: %d)\n", family, fnHeight, fnFontId );
  }	  

  priv = g_new (GdkFontPrivate, 1);
  priv->ref_count = 1;
  priv->font.type = GDK_FONT_FONT;
  priv->font.ascent = fnHeight;
  priv->font.descent = 4;	//MAC_OS_TODO
  //priv->xfont = 0;

  priv->fontnum = fnFontId;
  priv->face = fnFace;
  font = (GdkFont*) priv;

  gdk_font_hash_insert (GDK_FONT_FONT, font, font_name);
  
  return font;
}

GdkFont*
gdk_fontset_load (gchar *fontset_name)
{
  g_warning ("gdk_fontset_load: Not implemented");
  return NULL;
}

GdkFont*
gdk_font_ref (GdkFont *font)
{
  GdkFontPrivate *priv;

  g_return_val_if_fail (font != NULL, NULL);

  priv = (GdkFontPrivate*) font;
  priv->ref_count += 1;
  return font;
}

void
gdk_font_unref (GdkFont *font)
{
  GdkFontPrivate *priv;

  g_return_if_fail (font != NULL);
 
  priv = (GdkFontPrivate*) font;

  priv->ref_count -= 1;
  if (priv->ref_count == 0)
    {
      switch (font->type)
	{
	//case GDK_FONT_FONT:
	  //gdk_xid_table_remove ((void*) priv->xfont);
	  //delete priv->xfont;
	 // break;

	//default:
	//  g_assert_not_reached ();
	}
      g_free (font);
    }

}

gint
gdk_font_id (const GdkFont *font)
{
  const GdkFontPrivate *priv;

  g_return_val_if_fail (font != NULL, 0);

  priv = (const GdkFontPrivate*) font;

  if (font->type == GDK_FONT_FONT)
    return 0; //(gint) priv->xfont; //!!!MACOS_TO_DO!!!!

  g_assert_not_reached ();
  return 0;
}

gint
gdk_font_equal (const GdkFont *fonta,
                const GdkFont *fontb)
{
  const GdkFontPrivate *priva;
  const GdkFontPrivate *privb;

  g_return_val_if_fail (fonta != NULL, FALSE);
  g_return_val_if_fail (fontb != NULL, FALSE);

  priva = (const GdkFontPrivate*) fonta;
  privb = (const GdkFontPrivate*) fontb;

  if (fonta->type == GDK_FONT_FONT && fontb->type == GDK_FONT_FONT)
    return 0; //(priva->xfont == privb->xfont);  //!!!MACOS_TO_DO!!!

  g_assert_not_reached ();
  return 0;
}

gint
gdk_string_width (GdkFont     *font,
		  const gchar *string)
{
  return gdk_text_width (font, string, strlen (string));
}

extern GWorldPtr gDummyGWorld;


gint
gdk_text_width (GdkFont      *font,
		const gchar  *text,
		gint          text_length)
{
#if 1
  GdkFontPrivate *priv;
  gint width = 0;
  GWorldPtr savePort;
  GDHandle saveDev;
  
  g_return_val_if_fail (font != NULL, -1);
  g_return_val_if_fail (text != NULL, -1);

  priv = (GdkFontPrivate*) font;

//!!!MACOS_TO_DO!!!
   GetGWorld(&savePort, &saveDev);	//justin
   SetGWorld(gDummyGWorld, NULL);
  //MacInstallGFont(priv);

  TextFont(priv->fontnum);
  TextSize(priv->font.ascent);
  TextFace(priv->face);
   
  switch (font->type)
    {
    case GDK_FONT_FONT:
      width = TextWidth(text, 0, text_length);

      break;
    case GDK_FONT_FONTSET:
      g_warning("gdk_text_width: a GDK_FONT_FONTSET! eww");

    default:
      g_assert_not_reached ();
    }
    
//!!!MACOS_TO_DO!!!
   SetGWorld(savePort, saveDev);

  return width;
#endif
}

gint
gdk_text_width_wc (GdkFont	  *font,
		   const GdkWChar *text,
		   gint		   text_length)
{
  gchar *new_text;
  gint width;

  new_text = gdk_wcstombs(text);
  width = gdk_text_width(font, new_text, text_length);

  return width;
}

gint
gdk_char_width (GdkFont *font,
		gchar    character)
{
  return gdk_text_width (font, &character, 1);
}

gint
gdk_char_width_wc (GdkFont *font,
		   GdkWChar character)
{
  return gdk_text_width_wc (font, &character, 1);
}


gint
gdk_string_measure (GdkFont     *font,
                    const gchar *string)
{
  g_return_val_if_fail (font != NULL, -1);
  g_return_val_if_fail (string != NULL, -1);

  return gdk_text_measure (font, string, strlen (string));
}

void
gdk_text_extents (GdkFont     *font,
                  const gchar *text,
                  gint         text_length,
		  gint        *lbearing,
		  gint        *rbearing,
		  gint        *width,
		  gint        *ascent,
		  gint        *descent)
{
    GWorldPtr savePort;
    GDHandle saveDev;
    
    GetGWorld(&savePort, &saveDev);	//justin
    SetGWorld(gDummyGWorld, NULL);

    if (lbearing)
        *lbearing = 0;
    if (rbearing)
        *rbearing = 0;
    if (width)
        *width = TextWidth(text, 0, text_length);
    if (ascent)
        *ascent = 12;
    if (descent)
        *descent = 4;

#if 0
  GdkFontPrivate *priv;
  BFont *bfont;
  font_height bfontheight;

  g_return_if_fail (font != NULL);
  g_return_if_fail (text != NULL);

  priv = (GdkFontPrivate*) font;
  bfont = (BFont*) priv->xfont;

  bfont->GetHeight(&bfontheight);

  switch (font->type)
    {
    case GDK_FONT_FONT:
      /* XXX This is all quite bogus */
      if (lbearing)
	*lbearing = 0;
      if (rbearing)
	*rbearing = 0;
      if (width)
	*width = (gint) bfont->StringWidth(text, text_length);
      if (ascent)
	*ascent = (gint) bfontheight.ascent;
      if (descent)
	*descent = (gint) bfontheight.descent;
      break;

    default:
      g_assert_not_reached ();
    }
#endif
  
  SetGWorld(savePort, saveDev);

}

void
gdk_text_extents_wc (GdkFont        *font,
		     const GdkWChar *text,
		     gint            text_length,
		     gint           *lbearing,
		     gint           *rbearing,
		     gint           *width,
		     gint           *ascent,
		     gint           *descent)
{
  g_warning ("gdk_text_extents_wc: Not really implemented. Calling gdk_text_width");
  gdk_text_extents(font, (char*) text, text_length, lbearing, rbearing, width, ascent, descent);
}

void
gdk_string_extents (GdkFont     *font,
		    const gchar *string,
		    gint        *lbearing,
		    gint        *rbearing,
		    gint        *width,
		    gint        *ascent,
		    gint        *descent)
{
  g_return_if_fail (font != NULL);
  g_return_if_fail (string != NULL);

  gdk_text_extents (font, string, strlen (string),
		    lbearing, rbearing, width, ascent, descent);
}


gint
gdk_text_measure (GdkFont     *font,
                  const gchar *text,
                  gint         text_length)
{
  GdkFontPrivate *priv;

  g_return_val_if_fail (font != NULL, -1);
  g_return_val_if_fail (text != NULL, -1);

  priv = (GdkFontPrivate*) font;

  switch (font->type)
    {
    case GDK_FONT_FONT:
      return gdk_text_width (font, text, text_length); /* ??? */
      break;

    default:
      g_assert_not_reached ();
    }
  return 0;
}

gint
gdk_char_measure (GdkFont *font,
                  gchar    character)
{
  g_return_val_if_fail (font != NULL, -1);

  return gdk_text_measure (font, &character, 1);
}

gint
gdk_string_height (GdkFont     *font,
		   const gchar *string)
{
  g_return_val_if_fail (font != NULL, -1);
  g_return_val_if_fail (string != NULL, -1);

  return gdk_text_height (font, string, strlen (string));
}

gint
gdk_text_height (GdkFont     *font,
		 const gchar *text,
		 gint         text_length)
{
	return 20; //!!!MACOS_TO_DO!!!

}

gint
gdk_char_height (GdkFont *font,
		 gchar    character)
{
  g_return_val_if_fail (font != NULL, -1);

  return gdk_text_height (font, &character, 1);
}


#define MAX_FONTS 16384

void
gdk_font_get_list (char ***names, int *num)
{
	static char *fonts[MAX_FONTS];
	static int num_fonts = 0;
	int num_families;
	int num_styles;
	int size;
	FMFontFamilyIterator iterator;
	FMFontFamily family;
	Str255 pascalFontName;
	char *fontName;
	OSStatus rc;
	
	if (num_fonts > 0)
    {
		*names = fonts;
		*num = num_fonts;
    }

	rc = FMCreateFontFamilyIterator( NULL, NULL, kFMLocalIterationScope, &iterator );

	while ( rc != kFMIterationCompleted ) {
	
		rc = FMGetNextFontFamily( &iterator, &family );

		if ( rc != kFMIterationCompleted ) {

			rc = FMGetFontFamilyName( family, pascalFontName );

			if ( rc != noErr ) {
				continue;
			}

			if ( pascalFontName[1] == '#' || pascalFontName[1] == '.' ) {
				continue;
			}
			
			fontName = (char *) malloc( pascalFontName[0] + 1 );
			if ( fontName == NULL ) {
				break;
			}
			
			CopyPascalStringToC( pascalFontName, fontName );
			
			for ( size = 6; size < 20; size = size + 2 )
			{
				fonts[num_fonts+3] = g_strdup_printf ("-*-%s-normal-r-normal--*-*-*-*-p-*-*-*",
										fontName);
				fonts[num_fonts+2] = g_strdup_printf ("-*-%s-bold-r-normal--*-*-*-*-p-*-*-*",
										  fontName);
				fonts[num_fonts+1] = g_strdup_printf ("-*-%s-normal-i-normal--*-*-*-*-p-*-*-*",
										  fontName);
				fonts[num_fonts  ] = g_strdup_printf ("-*-%s-bold-i-normal--*-*-*-*-p-*-*-*",
										  fontName);
				num_fonts+=4;
			}

			if ( fontName != NULL ) {
				free( fontName );
			}
		}
	}

	FMDisposeFontFamilyIterator( &iterator );
	
	*names = fonts;
	*num = num_fonts;
}

