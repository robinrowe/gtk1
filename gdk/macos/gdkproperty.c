/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "config.h"

#include <string.h>
#include <gdk/gdk.h>
#include "gdkprivate.h"

GdkAtom
gdk_atom_intern (const gchar *atom_name,
		 gint         only_if_exists)
{
  GdkAtom retval;
  static GHashTable *atom_hash = NULL;
  static gint warned = 0;
  
  if (!atom_hash)
    atom_hash = g_hash_table_new (g_str_hash, g_str_equal);

  retval = GPOINTER_TO_UINT (g_hash_table_lookup (atom_hash, atom_name));
  if (!retval)
    {
      if (strcmp (atom_name, "PRIMARY") == 0)
	retval = GDK_SELECTION_PRIMARY;
      else if (strcmp (atom_name, "SECONDARY") == 0)
	retval = GDK_SELECTION_SECONDARY;
      else if (strcmp (atom_name, "ATOM") == 0)
	retval = GDK_SELECTION_TYPE_ATOM;
      else if (strcmp (atom_name, "BITMAP") == 0)
	retval = GDK_SELECTION_TYPE_BITMAP;
      else if (strcmp (atom_name, "COLORMAP") == 0)
	retval = GDK_SELECTION_TYPE_COLORMAP;
      else if (strcmp (atom_name, "DRAWABLE") == 0)
	retval = GDK_SELECTION_TYPE_DRAWABLE;
      else if (strcmp (atom_name, "INTEGER") == 0)
	retval = GDK_SELECTION_TYPE_INTEGER;
      else if (strcmp (atom_name, "PIXMAP") == 0)
	retval = GDK_SELECTION_TYPE_PIXMAP;
      else if (strcmp (atom_name, "WINDOW") == 0)
	retval = GDK_SELECTION_TYPE_WINDOW;
      else if (strcmp (atom_name, "STRING") == 0)
	retval = GDK_SELECTION_TYPE_STRING;
      else
	{
          if (!warned)
            {
              g_warning("gdk_atom_intern: unimplemented (warned once)");
              warned = 1;
            }
          /*g_warning("gdk_atom_intern: unimplemented atom %s", atom_name);*/
          retval = 0;
	}
      g_hash_table_insert (atom_hash, 
			   g_strdup (atom_name), 
			   GUINT_TO_POINTER (retval));
    }

  return retval;
}

gchar *
gdk_atom_name (GdkAtom atom)
{
  gchar name[256];

  switch (atom)
    {
    case GDK_SELECTION_PRIMARY: return g_strdup ("PRIMARY");
    case GDK_SELECTION_SECONDARY: return g_strdup ("SECONDARY");
    case GDK_SELECTION_TYPE_ATOM: return g_strdup ("ATOM");
    case GDK_SELECTION_TYPE_BITMAP: return g_strdup ("BITMAP");
    case GDK_SELECTION_TYPE_COLORMAP: return g_strdup ("COLORMAP");
    case GDK_SELECTION_TYPE_DRAWABLE: return g_strdup ("DRAWABLE");
    case GDK_SELECTION_TYPE_INTEGER: return g_strdup ("INTEGER");
    case GDK_SELECTION_TYPE_PIXMAP: return g_strdup ("PIXMAP");
    case GDK_SELECTION_TYPE_WINDOW: return g_strdup ("WINDOW");
    case GDK_SELECTION_TYPE_STRING: return g_strdup ("STRING");
    }

  g_warning("gdk_atom_name: not implemented");
  return g_strdup ("AN ATOM");
}

gint
gdk_property_get (GdkWindow   *window,
		  GdkAtom      property,
		  GdkAtom      type,
		  gulong       offset,
		  gulong       length,
		  gint         pdelete,
		  GdkAtom     *actual_property_type,
		  gint        *actual_format_type,
		  gint        *actual_length,
		  guchar     **data)
{
  g_warning ("gdk_property_get: Not implemented");

  return FALSE;
}

void
gdk_property_change (GdkWindow   *window,
		     GdkAtom      property,
		     GdkAtom      type,
		     gint         format,
		     GdkPropMode  mode,
		     guchar      *data,
		     gint         nelements)
{
  g_warning("gdk_property_change: not implemented");
}

void
gdk_property_delete (GdkWindow *window,
                     GdkAtom    property)
{ 
  g_warning("gdk_property_delete: not implemented");
}
