#pragma once

#include <Carbon/Carbon.h>
#include <CoreServices/CoreServices.h>
#include <glib.h>
#include "gdktypes.h"

#ifdef __cplusplus
extern "C" {
#endif

void MacGTK_WindowResized(WindowRef macWind);
void MacGTK_HandleGoAway(WindowRef macWind);
void MacGTK_SendExposeEvent(WindowRef macWind, RgnHandle rgnH);
void MacGTK_SendButtonPressEvent(WindowRef macWind, Point where);
void MacGTK_SendButtonReleaseEvent(WindowRef macWind, Point where);
guint MacToGTKMods(EventModifiers modifiers);
void MacGTK_SendKey(WindowRef focusedWindow, gchar theKey, EventModifiers modifiers, 
        GdkEventType eventType);
void MacGTK_Loop();

#ifdef __cplusplus
}
#endif
