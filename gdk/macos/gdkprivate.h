/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GDK_PRIVATE_H__
#define __GDK_PRIVATE_H__

#include <time.h>
#include <gdk/gdktypes.h>

#include <Carbon/Carbon.h>

#include "MacGWind.h"


struct _GdkHidden {
  gpointer dummy;
};

/* Define some of the X11 constants also here, again just for laziness */

#include <glib.h>

typedef gpointer XID;
typedef guint VisualID;
typedef int Status;
typedef guint32 KeySym;


/* null resource */
#define None 0

/* Error codes */
#define Success            0

/* Grabbing status */
#define GrabSuccess	   0
#define AlreadyGrabbed	   2

/* For CreateColormap */
#define AllocNone 0
#define AllocAll 1

/* Notify modes */
#define NotifyNormal 0
#define NotifyHint 1

/* Some structs are somewhat useful to emulate internally, just to
   keep the code less #ifdefed.  */
typedef struct {
  gpointer dummy;
} ColormapStruct, *Colormap;
  
typedef struct {
  gint map_entries;
  guint visualid;
  guint bitspixel;
} Visual;

typedef struct {
  Colormap colormap;
  unsigned long red_max;
  unsigned long red_mult;
  unsigned long green_max;
  unsigned long green_mult;
  unsigned long blue_max;
  unsigned long blue_mult;
  unsigned long base_pixel;
} XStandardColormap;

#define gdk_window_lookup(xid)	   ((GdkWindow*) gdk_xid_table_lookup (xid))
#define gdk_pixmap_lookup(xid)	   ((GdkPixmap*) gdk_xid_table_lookup (xid))

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _GdkWindowPrivate       GdkWindowPrivate;
typedef struct _GdkWindowPrivate       GdkPixmapPrivate;
typedef struct _GdkImagePrivate	       GdkImagePrivate;
typedef struct _GdkGCPrivate	       GdkGCPrivate;
typedef struct _GdkColormapPrivate     GdkColormapPrivate;
typedef struct _GdkColorInfo           GdkColorInfo;
typedef struct _GdkVisualPrivate       GdkVisualPrivate;
typedef struct _GdkFontPrivate	       GdkFontPrivate;
typedef struct _GdkCursorPrivate       GdkCursorPrivate;
typedef struct _GdkEventFilter	       GdkEventFilter;
typedef struct _GdkClientFilter	       GdkClientFilter;
typedef struct _GdkColorContextPrivate GdkColorContextPrivate;
typedef struct _GdkRegionPrivate       GdkRegionPrivate;


struct _GdkWindowPrivate
{
  GdkWindow window;
  GdkWindow *parent;
  MacGWindRef macgwin;
  gint16 x;
  gint16 y;
  guint16 width;
  guint16 height;
  guint8 resize_count;
  guint8 window_type;
  guint ref_count;
  guint destroyed : 2;
  guint mapped : 1;
  guint guffaw_gravity : 1;

  /* We must keep the event mask here to filter them ourselves */
  gint event_mask;

  /* Values for bg_type */
#define GDK_WIN32_BG_NORMAL 0
#define GDK_WIN32_BG_PIXEL 1
#define GDK_WIN32_BG_PIXMAP 2
#define GDK_WIN32_BG_PARENT_RELATIVE 3
#define GDK_WIN32_BG_TRANSPARENT 4

  /* We draw the background ourselves at WM_ERASEBKGND  */
  guchar bg_type;
  GdkColor bg_pixel;
  GdkPixmap *bg_pixmap;

  /* Window size hints */
  gint hint_flags;
  gint hint_x, hint_y;
  gint hint_min_width, hint_min_height;
  gint hint_max_width, hint_max_height;

  gint extension_events;
  gboolean extension_events_selected;

  GList *filters;
  GdkColormap *colormap;
  GList *children;
};

struct _GdkImagePrivate
{
  GdkImage image;
  MacGWindRef macimg;

  void (*image_put) (GdkDrawable *window,
		     GdkGC	 *gc,
		     GdkImage	 *image,
		     gint	  xsrc,
		     gint	  ysrc,
		     gint	  xdest,
		     gint	  ydest,
		     gint	  width,
		     gint	  height);
};

struct _GdkGCPrivate
{
  GdkGC gc;
  GdkGCValuesMask values_mask;
  GdkGCValues values;
  GdkRegion *clip_region;
  guint ref_count;
};

typedef enum {
  GDK_COLOR_WRITEABLE = 1 << 0
} GdkColorInfoFlags;

struct _GdkColorInfo
{
  GdkColorInfoFlags flags;
  guint ref_count;
};

struct _GdkColormapPrivate
{
  GdkColormap colormap;
  void *xcolormap; // !!!MACOS_TO_DO!!!
  GdkVisual *visual;
  gint private_val;

  GHashTable *hash;
  GdkColorInfo *info;
  time_t last_sync_time;
  
  guint ref_count;
};

struct _GdkVisualPrivate
{
  GdkVisual visual;
  Visual *xvisual;
};

struct _GdkFontPrivate
{
  GdkFont font;
  guint ref_count;
  short fontnum;
  short face;
  //!!!MACOS_TO_DO!!!

  GSList *names;
};

struct _GdkCursorPrivate
{
  GdkCursor cursor;
  gpointer xcursor;
};

struct _GdkEventFilter {
  GdkFilterFunc function;
  gpointer data;
};

struct _GdkClientFilter {
  GdkAtom       type;
  GdkFilterFunc function;
  gpointer      data;
};

#ifdef USE_XIM

typedef struct _GdkICPrivate GdkICPrivate;

struct _GdkICPrivate
{
  XIC xic;
  GdkICAttr *attr;
  GdkICAttributesType mask;
};

#endif /* USE_XIM */

struct _GdkColorContextPrivate
{
  GdkColorContext color_context;
  XStandardColormap std_cmap;
};

struct _GdkRegionPrivate
{
  GdkRegion region;
  RgnHandle macrgn;
};

typedef enum {
  GDK_DEBUG_MISC          = 1 << 0,
  GDK_DEBUG_EVENTS        = 1 << 1,
  GDK_DEBUG_DND           = 1 << 2,
  GDK_DEBUG_COLOR_CONTEXT = 1 << 3,
  GDK_DEBUG_XIM           = 1 << 4,
  GDK_DEBUG_SELECTION	  = 1 << 5
} GdkDebugFlag;

void gdk_events_init (void);
void gdk_window_init (void);
void gdk_visual_init (void);
void gdk_selection_init (void);
void gdk_dnd_init    (void);
void gdk_dnd_exit    (void);
void gdk_image_init  (void);
void gdk_image_exit  (void);

GdkColormap* gdk_colormap_lookup (Colormap  xcolormap);
GdkVisual*   gdk_visual_lookup	 (Visual   *xvisual);

void gdk_window_add_colormap_windows (GdkWindow *window);
void gdk_window_destroy_notify	     (GdkWindow *window);

void	 gdk_xid_table_insert (XID	*xid,
			       gpointer	 data);
void	 gdk_xid_table_remove (XID	 xid);
gpointer gdk_xid_table_lookup (XID	 xid);

/* Internal functions */

void    gdk_sel_prop_store (GdkWindow *owner,
			    GdkAtom    type,
			    gint       format,
			    guchar    *data,
			    gint       length);

void       gdk_event_queue_append (GdkEvent *event);

extern gint		 gdk_debug_level;
extern gint		 gdk_show_events;
extern gint		 gdk_stack_trace;
extern gchar		*gdk_display_name;
extern XID		 gdk_leader_window;
extern GdkWindowPrivate	 gdk_root_parent;
extern GdkAtom		 gdk_selection_property;
extern GdkWindow	*selection_owner[];
extern gchar		*gdk_progclass;
extern gint		 gdk_error_code;
extern gint		 gdk_error_warnings;
extern gint              gdk_null_window_warnings;
extern GList            *gdk_default_filters;

#ifdef USE_XIM
/* XIM support */
gint   gdk_im_open		 (void);
void   gdk_im_close		 (void);
void   gdk_ic_cleanup		 (void);

extern GdkICPrivate *gdk_xim_ic;		/* currently using IC */
extern GdkWindow *gdk_xim_window;	        /* currently using Window */
#endif /* USE_XIM */

/* Debugging support */

#ifdef G_ENABLE_DEBUG

#define GDK_NOTE(type,action)		     G_STMT_START { \
    if (gdk_debug_flags & GDK_DEBUG_##type)		    \
       { action; };			     } G_STMT_END

#else /* !G_ENABLE_DEBUG */

#define GDK_NOTE(type,action)
      
#endif /* G_ENABLE_DEBUG */

GDKVAR guint gdk_debug_flags;

/* Internal functions for debug output etc. */

char   *gdk_color_to_string (GdkColor *);

void gdk_gc_predraw (MacGWindRef   view, GdkGCPrivate     *gc_private);
void gdk_gc_postdraw (MacGWindRef  view, GdkGCPrivate     *gc_private);

void gdk_event_queue_append (GdkEvent *event);
static GdkEvent* gdk_event_new (void);
GdkEvent* gdk_event_unqueue (void);

void gdk_event_button_generate (GdkEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDK_PRIVATE_H__ */
