/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include <Screen.h>

#include <gdk/gdk.h>

#include "config.h"
#include "gdkx.h"

static void  gdk_visual_decompose_mask (gulong     mask,
					gint      *shift,
					gint      *prec);


static GdkVisualPrivate *system_visual;
static GdkVisualPrivate *visuals;
static gint nvisuals;

static gint available_depths[7];
static gint navailable_depths;

static GdkVisualType available_types[6];
static gint navailable_types;

void
gdk_visual_init (void)
{
//!!!MACOS_TO_DO!!!
#if 1
  Visual *default_xvisual = NULL;

  visuals = g_new (GdkVisualPrivate, 1);
  nvisuals = 1;

        default_xvisual = g_new (Visual, 1);
        visuals[0].xvisual = default_xvisual;
        visuals[0].xvisual->visualid = 0; 
        visuals[0].xvisual->bitspixel = 32;

        visuals[0].visual.type = GDK_VISUAL_TRUE_COLOR;
        visuals[0].visual.red_mask = 0x00FF0000;
        visuals[0].visual.green_mask = 0x0000FF00;
        visuals[0].visual.blue_mask = 0x000000FF;
        visuals[0].visual.depth = 32;
        visuals[0].visual.byte_order = GDK_LSB_FIRST;
        visuals[0].visual.bits_per_rgb = 42; /* Not used? */

        gdk_visual_decompose_mask (visuals[0].visual.red_mask,
                                   &visuals[0].visual.red_shift,
                                   &visuals[0].visual.red_prec);
        gdk_visual_decompose_mask (visuals[0].visual.green_mask,
                                   &visuals[0].visual.green_shift,
                                   &visuals[0].visual.green_prec);
        gdk_visual_decompose_mask (visuals[0].visual.blue_mask,
                                   &visuals[0].visual.blue_shift,
                                   &visuals[0].visual.blue_prec);
        visuals[0].xvisual->map_entries =
          1 << (MAX (visuals[nvisuals].visual.red_prec,
                MAX (visuals[nvisuals].visual.green_prec,
                     visuals[nvisuals].visual.blue_prec)));
 
  available_depths[0] = visuals[0].visual.depth;
  navailable_depths = 1;
  available_types[0] = visuals[0].visual.type;
  navailable_types = 1;

  system_visual = &(visuals[0]);
#endif
}

GdkVisual*
gdk_visual_ref (GdkVisual *visual)
{
  return visual;
}

void
gdk_visual_unref (GdkVisual *visual)
{
  return;
}

gint
gdk_visual_get_best_depth (void)
{
  return available_depths[0];
}

GdkVisualType
gdk_visual_get_best_type (void)
{
  return available_types[0];
}

GdkVisual*
gdk_visual_get_system (void)
{
  return ((GdkVisual*) system_visual);
}

GdkVisual*
gdk_visual_get_best (void)
{
  return ((GdkVisual*) &(visuals[0]));
}

GdkVisual*
gdk_visual_get_best_with_depth (gint depth)
{
  GdkVisual *return_val;
  int i;

  return_val = (GdkVisual*)NULL;
  for (i = 0; i < nvisuals; i++)
    if (depth == visuals[i].visual.depth)
      {
	return_val = (GdkVisual*) &(visuals[i]);
	break;
      }

  return return_val;
}

GdkVisual*
gdk_visual_get_best_with_type (GdkVisualType visual_type)
{
  GdkVisual *return_val;
  int i;

  return_val = (GdkVisual*)NULL;
  for (i = 0; i < nvisuals; i++)
    if (visual_type == visuals[i].visual.type)
      {
	return_val = (GdkVisual*) &(visuals[i]);
	break;
      }

  return return_val;
}

GdkVisual*
gdk_visual_get_best_with_both (gint          depth,
			       GdkVisualType visual_type)
{
  GdkVisual *return_val;
  int i;

  return_val = (GdkVisual*)NULL;
  for (i = 0; i < nvisuals; i++)
    if ((depth == visuals[i].visual.depth) &&
	(visual_type == visuals[i].visual.type))
      {
	return_val = (GdkVisual*) &(visuals[i]);
	break;
      }

  return return_val;
}

void
gdk_query_depths  (gint **depths,
		   gint  *count)
{
  *count = navailable_depths;
  *depths = available_depths;
}

void
gdk_query_visual_types (GdkVisualType **visual_types,
			gint           *count)
{
  *count = navailable_types;
  *visual_types = available_types;
}

GList*
gdk_list_visuals (void)
{
  GList *list;
  gint i;

  list = (GList*)NULL;
  for (i = 0; i < nvisuals; ++i)
    list = g_list_append (list, (gpointer) &visuals[i]);

  return list;
}

GdkVisual*
gdk_visual_lookup (Visual *xvisual)
{
  return &(visuals[0].visual);
}

GdkVisual*
gdkx_visual_get (VisualID xvisualid)
{
  return &(visuals[0].visual);
}

static void
gdk_visual_decompose_mask (gulong  mask,
			   gint   *shift,
			   gint   *prec)
{
  *shift = 0;
  *prec = 0;

  while (!(mask & 0x1))
    {
      (*shift)++;
      mask >>= 1;
    }

  while (mask & 0x1)
    {
      (*prec)++;
      mask >>= 1;
    }
}
