#include <stdio.h>

#include "MacToGTK.h"

void ProcessMacEvent();

GWorldPtr gDummyGWorld = NULL;

static void MyMouseDownHandler(EventRecord* evt) {

    Point where;
    Rect tempRect;


    //printf("--- MyMouseDownHandler ---\n");

    WindowRef	theWindow = NULL;
    SInt16		thePart = MacFindWindow(evt->where, &theWindow);

    switch (thePart) {

        case inMenuBar:
            //ClickMenuBar(evt);
            break;

        case inSysWindow:
            //SystemClick(evt, theWindow);
            break;

        case inDesk:
            break;

        case inGrow: {
            Rect newContentRect;
            ResizeWindow(theWindow, evt->where, NULL, &newContentRect);
            MacGTK_WindowResized(theWindow);
            break;
        }

        case inGoAway:
            if (TrackGoAway(theWindow, evt->where)) {
                MacGTK_HandleGoAway(theWindow);
            }
            break;

        case inContent:

            /*if (theWindow != FrontWindow()) {
            SelectWindow(theWindow);
            }
            else*/ {
                MacSetPort(GetWindowPort(theWindow));
                SetOrigin(0,0);
                where = evt->where;
                GlobalToLocal(&where);

                MacGTK_SendButtonPressEvent(theWindow, where);
            }
            break;

        case inDrag:
            GetRegionBounds(GetGrayRgn(), &tempRect);
            DragWindow(theWindow, evt->where, &tempRect);
            break;

        case inZoomIn:
        case inZoomOut:
        case inCollapseBox:
            break;
            
        }


}

static void MyMouseUpHandler(EventRecord* evt) {

    Point where;

    //printf("--- MyMouseUPHandler ---\n");

    WindowRef	theWindow = NULL;
    SInt16		thePart = MacFindWindow(evt->where, &theWindow);

    switch (thePart) {

        case inContent:

            MacSetPort(GetWindowPort(theWindow));
            SetOrigin(0,0);
            where = evt->where;
            GlobalToLocal(&where);

            MacGTK_SendButtonReleaseEvent(theWindow, where);
            break;

    }
}

static void MyKeyDownHandler(EventRecord* evt) {
	unsigned long state;
	long keyTrans;
	char theCharCode;
	Ptr kchr;

	int theKeyCode = (evt->message & keyCodeMask) >> 8;	
    WindowRef focusedWindow = FrontWindow();

	state = 0;
	kchr = (Ptr) GetScriptVariable(smCurrentScript, smKCHRCache);
	keyTrans = KeyTranslate(kchr, theKeyCode, &state);
	theCharCode = keyTrans;
	if (!theCharCode) theCharCode = (keyTrans>>16);
	
    printf("mac key pressed '%c' <%d>  front window=%d\n",theCharCode,theCharCode,focusedWindow);

    MacGTK_SendKey(focusedWindow, theCharCode, evt->modifiers, GDK_KEY_PRESS );
}


static void MyUpdateEvtHandler(EventRecord* evt) {

    Rect rgnBox;
    RgnHandle updateRgn = NULL;
    /*if (SIOUXHandleOneEvent(evt)) {
    }
    else {*/
        //printf("--- MyUpdateEvtHandler ---\n");
        WindowRef theWindow = (WindowRef)(evt->message);
        CGrafPtr windowPort = GetWindowPort(theWindow);

        // valide la region
        MacSetPort(windowPort);
        SetOrigin(0,0);

        updateRgn = NewRgn();

        BeginUpdate(theWindow);
        //CopyRgn(theWindow->visRgn, updateRgn);	TODO!!
        GetPortVisibleRegion(windowPort, updateRgn); //justin

#if DEBUG_UPDATE_EVENT
        ClipRect(&theWindow->portRect);
        ForeColor(redColor);
        PaintRect(&theWindow->portRect);
#endif


        EndUpdate(theWindow);

        MacGTK_SendExposeEvent(theWindow, updateRgn);
        DisposeRgn(updateRgn);
    //}
}
static void MyNullEvtHandler(EventRecord* evt) {

    MacGTK_Loop();
}


#if UNIVERSAL_INTERFACES_VERSION < 0x0400

enum {
	kQDDontChangeFlags                      = 0xFFFFFFFF,         // don't change anything
	kQDUseDefaultTextRendering      = 0,
	kQDUseTrueTypeScalerGlyphs      = (1 << 0),
	kQDUseCGTextRendering           = (1 << 1),
	kQDUseCGTextMetrics             = (1 << 2)
};

extern "C" {
	UInt32 SwapQDTextFlags( UInt32 flags );
};

#endif

void 
init_toolbox ()
{
  RegisterAppearanceClient ();
  InitCursor ();
}

void 
init_menu_bar()
{
  MenuRef windMenuRef;
  
  /* install system managed window menu */
  CreateStandardWindowMenu(0, &windMenuRef);
  InsertMenu(windMenuRef, 0);
  
  DrawMenuBar();
}


void GTKMacSetup() {
    /*
     printf("TEST GTK: simple.\n");
     GUSISetHook(GUSI_EventHook+mouseDown, (GUSIHook)MyMouseDownHandler);
     GUSISetHook(GUSI_EventHook+mouseUp, (GUSIHook)MyMouseUpHandler);
     GUSISetHook(GUSI_EventHook+updateEvt, (GUSIHook)MyUpdateEvtHandler);
     GUSISetHook(GUSI_EventHook+nullEvent, (GUSIHook)MyNullEvtHandler);
     */

    Rect gwRect;
    CGrafPtr savePort = NULL;
    GDHandle saveDev = NULL;

	// enable quartz font anti-aliasing
	long sysv_gestalt;

	Gestalt( gestaltSystemVersion, &sysv_gestalt );

	// only works on Mac OS X 10.1.5 and later
	if ( sysv_gestalt >= 0x1015 ) {
		UInt32 oldFlags = SwapQDTextFlags( kQDDontChangeFlags );
		SwapQDTextFlags( oldFlags | kQDUseTrueTypeScalerGlyphs | kQDUseCGTextMetrics | kQDUseCGTextRendering );
	}
   
    GetGWorld(&savePort, &saveDev);
    
    gwRect.left = 0;
    gwRect.top = 0;
    gwRect.right = 400;
    gwRect.bottom = 400;
    NewGWorld(&gDummyGWorld, 24, &gwRect,  NULL, NULL, pixelsLocked);
    SetGWorld(gDummyGWorld, NULL);
    TextSize(9);
    TextFont(1);
    SetGWorld(savePort, saveDev);
    
    init_toolbox();
    init_menu_bar();

    printf("GTKMacSetup()\n");
}


void DoEvent(EventRecord *event) {
    short			part;
    Boolean			hit;
    char			key;
    Rect			tempRect;

    switch (event->what)
    {
        case nullEvent:
            MyNullEvtHandler(event);
            break;
        case mouseDown:
            MyMouseDownHandler(event);
            break;
        case mouseUp:
            MyMouseUpHandler(event);
            break;
        case keyDown:
            MyKeyDownHandler(event);
            break;
        /*case autoKey:
            break;
        case activateEvt:
            break;
        case diskEvt:
            break;*/
        case updateEvt:
            MyUpdateEvtHandler(event);
            break;
        case kHighLevelEvent:
            AEProcessAppleEvent(event);
            MyNullEvtHandler(event); //oooh
            break;
        default:
            MyNullEvtHandler(event); //oooh

    }

}

void ProcessMacEvent() {
    Boolean gotEvent;
    EventRecord event;
    
    //printf("ProcessMacEvent()\n");

    gotEvent = WaitNextEvent(everyEvent, &event, 0 ,nil);
    if (gotEvent)
        DoEvent(&event);
    else MyNullEvtHandler(&event);

    //g_main_wakeup(); // ?????

}