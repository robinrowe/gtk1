/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include <Application.h>
//#include <Message.h>

#include <string.h>

#include "config.h"
#include <gdk/gdk.h>
#include "gdkx.h"

/* We emulate the GDK_SELECTION window properties by storing
 * it's data in a per-window hashtable.
 */

typedef struct {
  guchar *data;
  gint length;
  gint format;
  GdkAtom type;
} GdkSelProp;

static GHashTable *sel_prop_table = NULL;

void
gdk_selection_init (void)
{
  if (sel_prop_table == NULL)
    sel_prop_table = g_hash_table_new (g_int_hash, g_int_equal);
}

void
gdk_sel_prop_store (GdkWindow *owner,
		    GdkAtom    type,
		    gint       format,
		    guchar    *data,
		    gint       length)
{
 //!!!MACOS_TO_DO!!!
  g_warning ("gdk_sel_prop_store: not implemented on MacOS !"); 
}
  

gint
gdk_selection_owner_set (GdkWindow *owner,
			 GdkAtom    selection,
			 guint32    time,
			 gint       send_event)
{
 //!!!MACOS_TO_DO!!!
  g_warning ("gdk_selection_owner_set: not implemented on MacOS !"); 
  return TRUE;
}

GdkWindow*
gdk_selection_owner_get (GdkAtom selection)
{
  GdkWindow *window;

  window = g_new (GdkWindow, 1);
  g_warning ("gdk_selection_owner_get: not implemented on MacOS !");

  return NULL; /* Equivalent of None owner in X */
}

void
gdk_selection_convert (GdkWindow *requestor,
		       GdkAtom    selection,
		       GdkAtom    target,
		       guint32    time)
{
	//!!!MACOS_TO_DO!!!
    g_warning ("gdk_selection_convert: not implemented on MacOS !");

}

gint
gdk_selection_property_get (GdkWindow  *requestor,
			    guchar    **data,
			    GdkAtom    *ret_type,
			    gint       *ret_format)
{
    g_warning ("gdk_selection_property_get: not implemented on MacOS !");
    return 0; // !!!MACOS_TO_DO!!!

}

void
gdk_selection_property_delete (GdkWindowPrivate *priv)
{
    g_warning ("gdk_selection_property_delete: not implemented on MacOS !");
    return; // !!!MACOS_TO_DO!!!

}

void
gdk_selection_send_notify (guint32  requestor,
			   GdkAtom  selection,
			   GdkAtom  target,
			   GdkAtom  property,
			   guint32  time)
{
  g_warning ("gdk_selection_send_notify: not implemented");
}

gint
gdk_text_property_to_text_list (GdkAtom  encoding,
				gint     format, 
				guchar  *text,
				gint     length,
				gchar ***list)
{
  g_warning ("gdk_text_property_to_text_list: not implemented");
  
  return 0;
}

void
gdk_free_text_list (gchar **list)
{
  g_return_if_fail (list != NULL);

  /* ??? */
}

gint
gdk_string_to_compound_text (gchar   *str,
			     GdkAtom *encoding,
			     gint    *format,
			     guchar **ctext,
			     gint    *length)
{
  g_warning ("gdk_string_to_compound_text: Not implemented");

  return 0;
}

void
gdk_free_compound_text (guchar *ctext)
{
  g_warning ("gdk_free_compound_text: Not implemented");
}
