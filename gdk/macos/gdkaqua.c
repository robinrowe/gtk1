
#include "gdkaqua.h"

#include <Carbon/Carbon.h>
#include <gdkprivate.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void gdk_aqua_draw_pushbutton( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus )
{
	GdkWindowPrivate *window_private;
	GdkGCPrivate *gc_private;
    CGrafPtr savePort = NULL;
    GDHandle saveDev = NULL;
	ThemeButtonDrawInfo drawinfo;
	Rect rect;

	//printf( "draw button: %d, %d, %d, %d, p: %d\n", area->x, area->y, area->width, area->height, pressed );

	if ( pressed ) {
		drawinfo.state = kThemeStatePressed;
	} else {
		drawinfo.state = kThemeStateActive;
	}
	drawinfo.value = kThemeButtonOff;

	if ( focus ) {
		drawinfo.adornment = kThemeAdornmentFocus | kThemeAdornmentNone;
	} else {
		drawinfo.adornment = kThemeAdornmentNone;
	}

	window_private = (GdkWindowPrivate*) window;
	gc_private = (GdkGCPrivate*) gc;

	gdk_gc_predraw(window_private->macgwin, gc_private);

	SetRect( &rect, area->x + 2, area->y + 2, area->x + area->width - 2, area->y + area->height - 2 );

	DrawThemeButton( &rect, kThemePushButton, &drawinfo, NULL, NULL, NULL, 0 );

	gdk_gc_postdraw(window_private->macgwin, gc_private);
}


void gdk_aqua_draw_checkbox( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus, int selected )
{
	GdkWindowPrivate *window_private;
	GdkGCPrivate *gc_private;
    CGrafPtr savePort = NULL;
    GDHandle saveDev = NULL;
	ThemeButtonDrawInfo drawinfo;
	Rect rect;

	printf( "draw checkbox: %d, %d, %d, %d, p: %d, s: %d\n", area->x, area->y, area->width, area->height, pressed, selected );

	if ( pressed ) {
		drawinfo.state = kThemeStatePressed;
	} else {
		drawinfo.state = kThemeStateActive;
	}

	if ( false/*focus*/ ) {
		drawinfo.adornment = kThemeAdornmentFocus | kThemeAdornmentNone;
	} else {
		drawinfo.adornment = kThemeAdornmentNone;
	}

	if ( selected ) {
		drawinfo.value = kThemeButtonOn;
	} else {
		drawinfo.value = kThemeButtonOff;
	}
		
	window_private = (GdkWindowPrivate*) window;
	gc_private = (GdkGCPrivate*) gc;

	gdk_gc_predraw(window_private->macgwin, gc_private);

	SetRect( &rect, area->x - 4, area->y - 4, area->x + area->width, area->y + area->height );

	//EraseRect( &rect );
	MacGWind_Erase( window_private->macgwin, rect );
	DrawThemeButton( &rect, kThemeSmallCheckBox, &drawinfo, NULL, NULL, NULL, 0 );

	gdk_gc_postdraw(window_private->macgwin, gc_private);
}


void gdk_aqua_draw_bevelbutton( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus, int selected )
{
	GdkWindowPrivate *window_private;
	GdkGCPrivate *gc_private;
    CGrafPtr savePort = NULL;
    GDHandle saveDev = NULL;
	ThemeButtonDrawInfo drawinfo;
	Rect rect;

	printf( "draw bevelbutton: %d, %d, %d, %d, p: %d, s: %d\n", area->x, area->y, area->width, area->height, pressed, selected );

	if ( pressed ) {
		drawinfo.state = kThemeStatePressed;
	} else {
		drawinfo.state = kThemeStateActive;
	}

	if ( false/*focus*/ ) {
		drawinfo.adornment = kThemeAdornmentFocus | kThemeAdornmentNone;
	} else {
		drawinfo.adornment = kThemeAdornmentNone;
	}

	if ( selected ) {
		drawinfo.value = kThemeButtonOn;
	} else {
		drawinfo.value = kThemeButtonOff;
	}

	window_private = (GdkWindowPrivate*) window;
	gc_private = (GdkGCPrivate*) gc;

	gdk_gc_predraw(window_private->macgwin, gc_private);

	SetRect( &rect, area->x, area->y, area->x + area->width, area->y + area->height );

	//EraseRect( &rect );
	MacGWind_Erase( window_private->macgwin, rect );
	DrawThemeButton( &rect, kThemeMediumBevelButton, &drawinfo, NULL, NULL, NULL, 0 );

	gdk_gc_postdraw(window_private->macgwin, gc_private);
}
