/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>

#include "config.h"
#include "gdk/gdk.h"
#include "gdkx.h"
#include "gdkinput.h"
#include "gdkprivate.h"

#include "MacGWind.h"

/* Forward declarations */
static gboolean gdk_window_gravity_works (void);
static void     gdk_window_set_static_win_gravity (GdkWindow *window, 
						   gboolean   on);
static gboolean gdk_window_have_shape_ext (void);

extern gboolean do_motion_hints;

void
gdk_window_init (void)
{
  unsigned int width;
  unsigned int height;

  BitMap screenBits;
  Rect r;
  GetQDGlobalsScreenBits(&screenBits);

  width = screenBits.bounds.right - screenBits.bounds.left;
  height = screenBits.bounds.bottom - screenBits.bounds.top;		


  gdk_root_parent.window_type = GDK_WINDOW_ROOT;
  gdk_root_parent.window.user_data = NULL;
  gdk_root_parent.width = width;
  gdk_root_parent.height = height;
  gdk_root_parent.children = NULL;
  gdk_root_parent.colormap = NULL;
  gdk_root_parent.ref_count = 1;
}

GdkWindow*
gdk_window_new (GdkWindow     *parent,
		GdkWindowAttr *attributes,
		gint           attributes_mask)
{
#if 1
  GdkWindow *window;
  GdkWindowPrivate *priv;
  GdkWindowPrivate *parent_private;
  GdkVisual *visual;
  MacGWindRef macwin = NULL;
  gchar *title;
  gint x, y;
  
  g_return_val_if_fail (attributes != NULL, NULL);
  
  if (!parent)
    parent = (GdkWindow*) &gdk_root_parent;
  
  parent_private = (GdkWindowPrivate*) parent;
  if (parent_private->destroyed)
    return NULL;

  priv = g_new (GdkWindowPrivate, 1);
  window = (GdkWindow*) priv;
  
  priv->parent = parent;
  
  priv->destroyed = FALSE;
  priv->mapped = FALSE;
  priv->guffaw_gravity = FALSE;
  priv->resize_count = 0;
  priv->ref_count = 1;
  
  if (attributes_mask & GDK_WA_X)
    x = attributes->x;
  else
    x = 30;
  
  if (attributes_mask & GDK_WA_Y)
    y = attributes->y;
  else
    y = 55;
  
  priv->x = x;
  priv->y = y;
  priv->width = (attributes->width > 1) ? (attributes->width) : (1);
  priv->height = (attributes->height > 1) ? (attributes->height) : (1);
  priv->window_type = attributes->window_type;
  priv->extension_events = FALSE;
  
  priv->filters = NULL;
  priv->children = NULL;
  
  window->user_data = NULL;
  
  if (attributes_mask & GDK_WA_VISUAL)
    visual = attributes->visual;
  else
    visual = gdk_visual_get_system ();

  /* FOOBAR I moved this up here, because there was a case
     where the colormap was not set before gdk_colormap_ref 
     is called. This may be incorrect. JSM */

  priv->colormap = gdk_colormap_get_system ();
  
  if (attributes->wclass == GDK_INPUT_OUTPUT)
    {
      if (attributes_mask & GDK_WA_COLORMAP)
	priv->colormap = attributes->colormap;
      else
	priv->colormap = gdk_colormap_get_system ();
      
      switch (priv->window_type)
	{
	case GDK_WINDOW_TOPLEVEL:
	case GDK_WINDOW_CHILD:
	case GDK_WINDOW_DIALOG:
	case GDK_WINDOW_TEMP:
	  break;

	case GDK_WINDOW_ROOT:
	  g_error ("cannot make windows of type GDK_WINDOW_ROOT");
	  break;
	case GDK_WINDOW_PIXMAP:
	  g_error ("cannot make windows of type GDK_WINDOW_PIXMAP (use gdk_pixmap_new)");
	  break;
	}
    }
  else
    {
      priv->colormap = NULL;
    }

  if (attributes_mask & GDK_WA_TITLE)
    title = attributes->title;
  else
    title = g_get_prgname ();

  priv->event_mask = attributes->event_mask;

  switch (priv->window_type)
    {
    case GDK_WINDOW_TOPLEVEL:
    case GDK_WINDOW_DIALOG:
    case GDK_WINDOW_TEMP: 
	  macwin = MacGWind_NewTopLevel(
	    x,y,priv->width,priv->height,
	    priv->window_type,
	    title);
    break;
      
    case GDK_WINDOW_CHILD:
      macwin = MacGWind_NewChild(
      	parent_private->macgwin, x,y,priv->width,priv->height);
       if (attributes->wclass == GDK_INPUT_ONLY) {
          MacGWind_SetInputOnly(macwin);
       }
    break;
    
    default:
      macwin = NULL;
    break;
    }

  priv->macgwin = macwin;
  
  gdk_window_ref (window);

  gdk_xid_table_insert ((void**)(&priv->macgwin), window);
  
  if (priv->colormap)
    gdk_colormap_ref (priv->colormap);

  gdk_window_set_cursor (window, ((attributes_mask & GDK_WA_CURSOR) ?
				  (attributes->cursor) :
				  NULL));
  
  if (parent_private)
    parent_private->children = g_list_prepend (parent_private->children, window);
  
  return window;
#endif
}

GdkWindow *
gdk_window_foreign_new (guint32 anid)
{
  g_warning("gdk_window_foreign_new: not implemented!");
  
  return NULL;
}

/* Call this function when you want a window and all its children to
 * disappear.  When xdestroy is true, a request to destroy the XWindow
 * is sent out.  When it is false, it is assumed that the XWindow has
 * been or will be destroyed by destroying some ancestor of this
 * window.
 */
static void
gdk_window_internal_destroy (GdkWindow *window,
			     gboolean   xdestroy,
			     gboolean   our_destroy)
{
/*
  GdkWindowPrivate *priv;

  g_return_if_fail(window != NULL);

  priv = (GdkWindowPrivate*) window;

  if (!priv->destroyed)
    priv->destroyed = TRUE;
  
  //g_warning("gdk_window_internal_destroy: not implemented!");
  if (xdestroy) {
    MacGWind_Release(&priv->macgwin);
  }
  */
  
    GdkWindowPrivate *private;
  GdkWindowPrivate *temp_private;
  GdkWindow *temp_window;
  GList *children;
  GList *tmp;
  
  g_return_if_fail (window != NULL);
  
  private = (GdkWindowPrivate*) window;
  
  switch (private->window_type)
    {
    case GDK_WINDOW_TOPLEVEL:
    case GDK_WINDOW_CHILD:
    case GDK_WINDOW_DIALOG:
    case GDK_WINDOW_TEMP:
    case GDK_WINDOW_FOREIGN:
      if (!private->destroyed)
	{
	  if (private->parent)
	    {
	      GdkWindowPrivate *parent_private = (GdkWindowPrivate *)private->parent;
	      if (parent_private->children)
		parent_private->children = g_list_remove (parent_private->children, window);
	    }
	  
	  if (private->window_type != GDK_WINDOW_FOREIGN)
	    {
	      children = tmp = private->children;
	      private->children = NULL;
	      
	      while (tmp)
		{
		  temp_window = tmp->data;
		  tmp = tmp->next;
		  
		  temp_private = (GdkWindowPrivate*) temp_window;
		  if (temp_private)
		    gdk_window_internal_destroy (temp_window, FALSE,
						 our_destroy);
		}
	      
	      g_list_free (children);
	    }
	  
	  if (private->extension_events != 0)
	    gdk_input_window_destroy (window);
	  
	  if (private->filters)
	    {
	      tmp = private->filters;
	      
	      while (tmp)
		{
		  g_free (tmp->data);
		  tmp = tmp->next;
		}
	      
	      g_list_free (private->filters);
	      private->filters = NULL;
	    }
	  
	  if (private->window_type == GDK_WINDOW_FOREIGN)
	    {
	      if (our_destroy && (private->parent != NULL))
		{
      g_error ("called gdk_window_destroy on a GDK_WINDOW_FOREIGN (NOT IMPLEMENTED on MacOS)");
		  /* It's somebody elses window, but in our heirarchy,
		   * so reparent it to the root window, and then send
		   * it a delete event, as if we were a WM
		   */
		  /*
		  XClientMessageEvent xevent;

		  gdk_error_trap_push ();
		  gdk_window_hide (window);
		  gdk_window_reparent (window, NULL, 0, 0);
		  
		  xevent.type = ClientMessage;
		  xevent.window = private->xwindow;
		  xevent.message_type = gdk_wm_protocols;
		  xevent.format = 32;
		  xevent.data.l[0] = gdk_wm_delete_window;
		  xevent.data.l[1] = CurrentTime;

		  XSendEvent (private->xdisplay, private->xwindow,
			      False, 0, (XEvent *)&xevent);
		  gdk_flush ();
		  gdk_error_trap_pop ();
		  */
		}
	    }
	  else if (xdestroy) {
	    //XDestroyWindow (private->xdisplay, private->xwindow);
	    MacGWind_Release(&private->macgwin);
	  }
	  if (private->colormap)
	    gdk_colormap_unref (private->colormap);
	  
	  private->mapped = FALSE;
	  private->destroyed = TRUE;
	}
      break;
      
    case GDK_WINDOW_ROOT:
      g_error ("attempted to destroy root window");
      break;
      
    case GDK_WINDOW_PIXMAP:
      g_error ("called gdk_window_destroy on a pixmap (use gdk_pixmap_unref)");
      break;
    }
}

/* Like internal_destroy, but also destroys the reference created by
   gdk_window_new. */

void
gdk_window_destroy (GdkWindow *window)
{
  gdk_window_internal_destroy (window, TRUE, TRUE);
  gdk_window_unref (window);
}

/* This function is called when the XWindow is really gone.  */

void
gdk_window_destroy_notify (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed)
    {
      if (priv->window_type == GDK_WINDOW_FOREIGN)
	gdk_window_internal_destroy (window, FALSE, FALSE);
      else
	g_warning ("GdkWindow %#lx unexpectedly destroyed", priv->macgwin);
    }
  
  gdk_xid_table_remove (priv->macgwin);
  //gdk_xid_table_remove (priv->xwindow);
  //if (priv->bewindow)
  //  gdk_xid_table_remove (priv->bewindow);
  
  gdk_window_unref (window);
}

GdkWindow*
gdk_window_ref (GdkWindow *window)
{
  GdkWindowPrivate *priv= (GdkWindowPrivate *)window;
  
  g_return_val_if_fail (window != NULL, NULL);
  
  priv->ref_count += 1;
  return window;
}

void
gdk_window_unref (GdkWindow *window)
{
  static int warned = 0;
  GdkWindowPrivate *priv = (GdkWindowPrivate *)window;
  g_return_if_fail (window != NULL);
  
  priv->ref_count -= 1;
  if (priv->ref_count == 0)
    {
      if (!priv->destroyed)
	{
	  if (priv->window_type == GDK_WINDOW_FOREIGN)
            {
	      		gdk_xid_table_remove (priv->macgwin);
	      		//gdk_xid_table_remove (priv->xwindow);
          		//    if (priv->bewindow)
	      		//  gdk_xid_table_remove (priv->bewindow);
            }
	  else
            {
	      g_warning ("losing last reference to undestroyed window\n");
          warned = 1;
            }
	}
      g_dataset_destroy (window);
      g_free (window);
    }
}

void
gdk_window_show (GdkWindow *window)
{

#if 1
  GdkWindowPrivate *priv;
  MacGWindRef macGWin = NULL;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  macGWin = priv->macgwin;

  if (!priv->destroyed)
    {
      priv->mapped = TRUE;

      MacGWind_Show(macGWin, TRUE);
    }
 #endif
}

void
gdk_window_hide (GdkWindow *window)
{
#if 1
  GdkWindowPrivate *priv;
  MacGWindRef macGWin = NULL;

  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  macGWin = priv->macgwin;

  g_return_if_fail(priv->parent != NULL);

  if (!priv->destroyed)
    {
      priv->mapped = FALSE;

      MacGWind_Show(macGWin, FALSE);
    }
#endif
}

void
gdk_window_withdraw (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  if (!priv->destroyed)
    g_warning("gdk_window_withdrawn: not implemented!\n");
}

void
gdk_window_move (GdkWindow *window,
		 gint       x,
		 gint       y)
{

#if 1
  GdkWindowPrivate *priv = NULL;
  MacGWindRef macwin = NULL;
  
  g_return_if_fail (window != NULL);

  priv = (GdkWindowPrivate*) window;
  macwin = (MacGWindRef) (priv->macgwin);

  if (!priv->destroyed)
    {
      MacGWind_Move(macwin, x, y);
      if (priv->window_type == GDK_WINDOW_CHILD)
	{
	  priv->x = x;
	  priv->y = y;
	}
    }
#endif
}

void
gdk_window_resize (GdkWindow *window,
		   gint       width,
		   gint       height)
{
#if 1
  GdkWindowPrivate *priv = NULL;
  MacGWindRef macwin = NULL;
  
  g_return_if_fail (window != NULL);
  
  if (width < 1)
    width = 1;
  if (height < 1)
    height = 1;
  
  priv = (GdkWindowPrivate*) window;
  macwin = (MacGWindRef) (priv->macgwin);
  
  if (!priv->destroyed &&
      ((priv->resize_count > 0) ||
       (priv->width != (guint16) width) ||
       (priv->height != (guint16) height)))
    {
      
      MacGWind_Resize(macwin, width, height);
         
      priv->resize_count += 1;
      
      if (priv->window_type == GDK_WINDOW_CHILD)
	{
	  priv->width = width;
	  priv->height = height;
	}
    }
#endif
}

void
gdk_window_move_resize (GdkWindow *window,
			gint       x,
			gint       y,
			gint       width,
			gint       height)
{
  g_return_if_fail (window != NULL);
  
  gdk_window_move (window, x, y);
  gdk_window_resize (window, width, height);
}

void
gdk_window_reparent (GdkWindow *window,
		     GdkWindow *new_parent,
		     gint       x,
		     gint       y)
{

  GdkWindowPrivate *window_private;
  GdkWindowPrivate *parent_private;
  GdkWindowPrivate *old_parent_private;
  
  g_return_if_fail (window != NULL);

  if (!new_parent)
    new_parent = (GdkWindow*) &gdk_root_parent;
  
  window_private = (GdkWindowPrivate*) window;
  old_parent_private = (GdkWindowPrivate*)window_private->parent;
  parent_private = (GdkWindowPrivate*) new_parent;
  
  if (!window_private->destroyed && !parent_private->destroyed)
  {
      MacGWind_SetParent(window_private->macgwin, parent_private->macgwin);
      MacGWind_Move(window_private->macgwin, x,y);
  }
  window_private->parent = new_parent;
  
  if (old_parent_private)
    old_parent_private->children = g_list_remove (old_parent_private->children, window);
  
  parent_private->children = g_list_prepend (parent_private->children, window);

}

void
gdk_window_clear (GdkWindow *window)
{
#if 1
  GdkWindowPrivate *priv = NULL;

  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed)
    {      
      MacGWind_Erase(priv->macgwin, MacGWind_GetInteriorRect(priv->macgwin));
    }
#endif
}

void
gdk_window_clear_area (GdkWindow *window,
		       gint       x,
		       gint       y,
		       gint       width,
		       gint       height)
{

#if 1
  GdkWindowPrivate *priv = NULL;

  Rect tmpR;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed)
    {

      if (width <= 0)
        width = MacGWind_GetInteriorRect(priv->macgwin).right;

      if (height <= 0)
        height = MacGWind_GetInteriorRect(priv->macgwin).bottom;

      MacSetRect(&tmpR, x, y, x+width, y+height);
      
      MacGWind_Erase(priv->macgwin, tmpR);
    }
#endif
}

void
gdk_window_clear_area_e (GdkWindow *window,
		         gint       x,
		         gint       y,
		         gint       width,
		         gint       height)
{
  g_print("gdk_window_clear_area_e: ...\n");
  gdk_window_clear_area(window, x, y, width, height);
}

void
gdk_window_copy_area (

          GdkWindow    *dst,
		      GdkGC        *gc,
		      gint          xdst,
		      gint          ydst,
		      
		      GdkWindow    *src,
		      gint          xsrc,
		      gint          ysrc,
		      
		      gint          width,
		      gint          height
)
{
  // cf gdk_draw_pixmap
    
  GdkWindowPrivate *dst_private;
  GdkPixmapPrivate *src_private;
  GdkGCPrivate *gc_private;
  CGrafPtr src_port, dst_port;
  Rect src_rect, dst_rect;
  BitMap* src_bm = NULL;
  BitMap* dst_bm = NULL;
  
  g_return_if_fail (dst != NULL);
  g_return_if_fail (src != NULL);
  g_return_if_fail (gc != NULL);

  dst_private = (GdkWindowPrivate*) dst;
  src_private = (GdkPixmapPrivate*) src;
  if (dst_private->destroyed || src_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  if (width == -1)
    width = src_private->width;
  if (height == -1)
    height = src_private->height;

  gdk_gc_predraw (dst_private->macgwin, gc_private);
  
  MacSetRect(&src_rect, xsrc, ysrc, xsrc+width, ysrc+height);
  MacSetRect(&dst_rect, xdst, ydst, xdst+width, ydst+height);
  
  src_bm = MacGWind_GetMacBitmap(src_private->macgwin);
  dst_bm = MacGWind_GetMacBitmap(dst_private->macgwin);
  
  ForeColor(blackColor);
  BackColor(whiteColor);
  CopyBits(src_bm, dst_bm, &src_rect, &dst_rect, srcCopy, NULL);
  
  gdk_gc_postdraw (dst_private->macgwin, gc_private);
}

void
gdk_window_raise (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed) {
    MacGWind_Raise(priv->macgwin);
  }
}

void
gdk_window_lower (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed) {
    MacGWind_Lower(priv->macgwin);
  }
}

void
gdk_window_set_user_data (GdkWindow *window,
			  gpointer   user_data)
{
  g_return_if_fail (window != NULL);
  
  window->user_data = user_data;
}

void
gdk_window_set_hints (GdkWindow *window,
              gint       x,
              gint       y,
              gint       min_width,
              gint       min_height,
              gint       max_width,
              gint       max_height,
              gint       flags)
{
  GdkWindowPrivate *priv    = NULL;
  GdkGeometry        geom = { 0 };
  GdkWindowHints    hints    = 0;

  g_return_if_fail (window != NULL);

  priv = (GdkWindowPrivate*) window;
  if (!priv || priv->destroyed)
    return;

  geom.min_width = min_width;
  geom.min_height = min_height;
  geom.max_width = max_width;
  geom.max_height = max_height;
  geom.min_width = min_width;

  hints = GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE;

  MacGWind_SetGeometryHints(priv->macgwin, &geom, hints);
}

void
gdk_window_set_geometry_hints (GdkWindow      *window,
                   GdkGeometry    *geometry,
                   GdkWindowHints  geom_mask)
{
  GdkWindowPrivate *priv = NULL;

  g_return_if_fail (window != NULL);

  priv = (GdkWindowPrivate*) window;
  if (!priv || priv->destroyed)
    return;

  MacGWind_SetGeometryHints(priv->macgwin, geometry, geom_mask);
}

void
gdk_window_set_title (GdkWindow   *window,
		      const gchar *title)
{
  GdkWindowPrivate *priv;

  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  MacGWind_SetTitle(priv->macgwin, title);
}

void          
gdk_window_set_role (GdkWindow   *window,
		     const gchar *role)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  g_warning("gdk_window_set_role: not implemented!");
}

void          
gdk_window_set_transient_for (GdkWindow *window, 
			      GdkWindow *parent)
{
  GdkWindowPrivate *priv;
  GdkWindowPrivate *parent_private;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  parent_private = (GdkWindowPrivate*) parent;
  
  if (!priv->destroyed && !parent_private->destroyed)
    g_warning("gdk_window_set_transient_for: not implemented!");
}

void
gdk_window_set_background (GdkWindow *window,
			   GdkColor  *color)
{
#if 1
  GdkWindowPrivate *priv;
  RGBColor bkgCol;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;

  if (!priv->destroyed)
    {        
       bkgCol.red = color->red;
       bkgCol.green = color->green;
       bkgCol.blue = color->blue;
       
       MacGWind_SetBackground(priv->macgwin, bkgCol);
    }
#endif
}

void
gdk_window_set_back_pixmap (GdkWindow *window,
			    GdkPixmap *pixmap,
			    gint       parent_relative)
{
  GdkWindowPrivate *window_private;
  GdkPixmapPrivate *pixmap_private;
  
  g_return_if_fail (window != NULL);
  
  window_private = (GdkWindowPrivate*) window;
  pixmap_private = (GdkPixmapPrivate*) pixmap;

  if (!pixmap)
    return;
  
  g_warning("gdk_window_set_back_pixmap: not implemented!");

}

void
gdk_window_set_cursor (GdkWindow *window,
		       GdkCursor *cursor)
{
  GdkWindowPrivate *window_private;
  GdkCursorPrivate *cursor_private;
  static int warned = 0;
  
  g_return_if_fail (window != NULL);
  
  window_private = (GdkWindowPrivate*) window;
  cursor_private = (GdkCursorPrivate*) cursor;
  
  if (!window_private->destroyed)
    {
      if (!warned)
        {
          g_warning("gdk_window_set_cursor: not implemented! (only warned once)");
          warned = 1;
        }
    }
}

void
gdk_window_set_colormap (GdkWindow   *window,
			 GdkColormap *colormap)
{
  GdkWindowPrivate *window_private;
  GdkColormapPrivate *colormap_private;
  
  g_return_if_fail (window != NULL);
  g_return_if_fail (colormap != NULL);
  
  window_private = (GdkWindowPrivate*) window;
  colormap_private = (GdkColormapPrivate*) colormap;
  
  if (!window_private->destroyed)
    g_warning("gdk_window_set_colormap: not implemented!");
}

void
gdk_window_get_user_data (GdkWindow *window,
			  gpointer  *data)
{
  g_return_if_fail (window != NULL);
  
  *data = window->user_data;
}

void
gdk_window_get_geometry (GdkWindow *window,
			 gint      *x,
			 gint      *y,
			 gint      *width,
			 gint      *height,
			 gint      *depth)
{

  GdkWindowPrivate *window_private;
  Rect r;
  
  if (!window)
    {

      BitMap screenBits;
      Rect r;
      GetQDGlobalsScreenBits(&screenBits);
      
      r = screenBits.bounds;

      *x = r.left;
      *y = r.top;
      *width = r.right - r.left;
      *height = r.bottom - r.top;
      *depth = 32;

      return;
    }
  
  window_private = (GdkWindowPrivate*) window;
  
  if (!window_private->destroyed)
    {
      gdk_window_get_position(window, x, y);
      gdk_window_get_size(window, width, height);
      if (depth)
        *depth = 32;
    }

}

void
gdk_window_get_position (GdkWindow *window,
			 gint      *x,
			 gint      *y)
{

  Rect r;
  GdkWindowPrivate *window_private;
  
  g_return_if_fail (window != NULL);
  
  window_private = (GdkWindowPrivate*) window;

  if (!window_private->destroyed && window_private->macgwin != NULL)
    {

      r = MacGWind_GetPositionRect(window_private->macgwin);
      if (x)
        *x = r.left;
      if (y)
        *y = r.top;
    }

}

void
gdk_window_get_size (GdkWindow *window,
		     gint       *width,
		     gint       *height)
{

  GdkWindowPrivate *window_private;
  Rect rect;
  
  g_return_if_fail (window != NULL);
  
  window_private = (GdkWindowPrivate*) window;
  
  if (!window_private->destroyed && window_private->macgwin != NULL)
    {
        rect = MacGWind_GetPositionRect(window_private->macgwin);
        if (width)
            *width = (rect.right - rect.left);
        if (height)
            *height = (rect.bottom - rect.top);
    }

}

GdkVisual*
gdk_window_get_visual (GdkWindow *window)
{
  GdkWindowPrivate *window_private;
  
  g_return_val_if_fail (window != NULL, NULL);
  
  window_private = (GdkWindowPrivate*) window;

  if (window_private && !window_private->destroyed)
    return gdk_visual_get_system();
  
  return NULL;
}

GdkColormap*
gdk_window_get_colormap (GdkWindow *window)
{
  GdkWindowPrivate *window_private;
  
  g_return_val_if_fail (window != NULL, NULL);
  window_private = (GdkWindowPrivate*) window;
  
  g_return_val_if_fail (window_private->window_type != GDK_WINDOW_PIXMAP, NULL);

  if (!window_private->destroyed)
    return gdk_colormap_get_system();
  
  return NULL;
}

GdkWindowType
gdk_window_get_type (GdkWindow *window)
{
  GdkWindowPrivate *window_private;
  
  g_return_val_if_fail (window != NULL, (GdkWindowType) -1);
  
  window_private = (GdkWindowPrivate*) window;
  return (GdkWindowType) (window_private->window_type);
}

gint
gdk_window_get_origin (GdkWindow *window,
		       gint      *x,
		       gint      *y)
{
  Point where;
  GdkWindowPrivate *priv;
  
  where.h = where.v = 0;
  
  g_return_val_if_fail (window != NULL, 0);

  priv = (GdkWindowPrivate*) window;

  if (priv) {
  
    MacGWind_FocusPort(priv->macgwin);    
    LocalToGlobal(&where);
    
    if (x)
     *x = where.h;
    if (y)
      *y = where.v;

    return 1;
    
  }
  
  return 0;
}

gboolean
gdk_window_get_deskrelative_origin (GdkWindow *window,
				    gint      *x,
				    gint      *y)
{
  GdkWindowPrivate *priv;
  
  g_return_val_if_fail (window != NULL, 0);
  
  priv = (GdkWindowPrivate*) window;
  
  if (!priv->destroyed)
    g_warning("gdk_window_get_deskrelative_origin: not implemented!");
  
  return 0;
}

void
gdk_window_get_root_origin (GdkWindow *window,
			    gint      *x,
			    gint      *y)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;

  if (!priv->destroyed)
    g_warning("gdk_window_get_root_origin: not implemented!");
}

GdkWindow*
gdk_window_get_pointer (GdkWindow       *window,
			gint            *x,
			gint            *y,
			GdkModifierType *mask)
{
  GdkWindowPrivate *priv;
  Point where;
  
  if (window == NULL) { // global coords
  
    GetMouse(&where);
    LocalToGlobal(&where);
  }
  else { 
    priv = (GdkWindowPrivate*) window;

    MacGWind_FocusPort(priv->macgwin); 
    GetMouse(&where);
  }
  
  if (x)
    *x = where.h;
  if (y)
    *y = where.v;

  if (mask)
  {
      *mask = (GdkModifierType) MacGWind_GetMouseButtonMask();
  }
  
  return window; // ????
}

GdkWindow*
gdk_window_at_pointer (gint *win_x,
		       gint *win_y)
{
  GdkWindow *window;
  
  g_warning("gdk_window_at_pointer: not implemented!");

  window = NULL;
  
  return window;
}

GdkWindow*
gdk_window_get_parent (GdkWindow *window)
{
  g_return_val_if_fail (window != NULL, NULL);
  
  return ((GdkWindowPrivate*) window)->parent;
}

GdkWindow*
gdk_window_get_toplevel (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_val_if_fail (window != NULL, NULL);
  
  priv = (GdkWindowPrivate*) window;
  
  while (priv->window_type == GDK_WINDOW_CHILD)
    {
      window = ((GdkWindowPrivate*) window)->parent;
      priv = (GdkWindowPrivate*) window;
    }
  
  return window;
}

GList*
gdk_window_get_children (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_val_if_fail (window != NULL, NULL);
  
  priv = (GdkWindowPrivate*) window;
  if (priv->destroyed)
    return NULL;

  g_warning("gdk_window_get_children: not implemented!");
  
  return NULL;
}

GdkEventMask  
gdk_window_get_events (GdkWindow *window)
{
  GdkWindowPrivate *priv;
  
  g_return_val_if_fail (window != NULL, (GdkEventMask) 0);
  
  priv = (GdkWindowPrivate*) window;
  if (priv->destroyed)
    return (GdkEventMask) 0;
  
  return (GdkEventMask) priv->event_mask;
}

void          
gdk_window_set_events (GdkWindow       *window,
		       GdkEventMask     event_mask)
{
  GdkWindowPrivate *priv;
  
  g_return_if_fail (window != NULL);
  
  priv = (GdkWindowPrivate*) window;
  if (priv->destroyed)
    return;
  
  priv->event_mask = event_mask;
}

void
gdk_window_add_colormap_windows (GdkWindow *window)
{
  g_warning("gdk_window_add_colormap_windows: not implemented!");
}

/*
 * This needs the X11 shape extension.
 * If not available, shaped windows will look
 * ugly, but programs still work.    Stefan Wille
 */
void
gdk_window_shape_combine_mask (GdkWindow *window,
			       GdkBitmap *mask,
			       gint x, gint y)
{
  g_warning("gdk_window_shape_combine_mask: not implemented!");
}

void          
gdk_window_add_filter (GdkWindow     *window,
		       GdkFilterFunc  function,
		       gpointer       data)
{
  GdkWindowPrivate *priv;
  GList *tmp_list;
  GdkEventFilter *filter;
  
  priv = (GdkWindowPrivate*) window;
  if (priv && priv->destroyed)
    return;
  
  if (priv)
    tmp_list = priv->filters;
  else
    tmp_list = gdk_default_filters;
  
  while (tmp_list)
    {
      filter = (GdkEventFilter *)tmp_list->data;
      if ((filter->function == function) && (filter->data == data))
	return;
      tmp_list = tmp_list->next;
    }
  
  filter = g_new (GdkEventFilter, 1);
  filter->function = function;
  filter->data = data;
  
  if (priv)
    priv->filters = g_list_append (priv->filters, filter);
  else
    gdk_default_filters = g_list_append (gdk_default_filters, filter);
}

void
gdk_window_remove_filter (GdkWindow     *window,
			  GdkFilterFunc  function,
			  gpointer       data)
{
  GdkWindowPrivate *priv;
  GList *tmp_list, *node;
  GdkEventFilter *filter;
  
  priv = (GdkWindowPrivate*) window;
  
  if (priv)
    tmp_list = priv->filters;
  else
    tmp_list = gdk_default_filters;
  
  while (tmp_list)
    {
      filter = (GdkEventFilter *)tmp_list->data;
      node = tmp_list;
      tmp_list = tmp_list->next;
      
      if ((filter->function == function) && (filter->data == data))
	{
	  if (priv)
	    priv->filters = g_list_remove_link (priv->filters, node);
	  else
	    gdk_default_filters = g_list_remove_link (gdk_default_filters, node);
	  g_list_free_1 (node);
	  g_free (filter);
	  
	  return;
	}
    }
}

void
gdk_window_set_override_redirect (GdkWindow *window,
				  gboolean override_redirect)
{
  g_warning("gdk_window_set_override_redirect: not implemented!");
}

void          
gdk_window_set_icon (GdkWindow *window, 
		     GdkWindow *icon_window,
		     GdkPixmap *pixmap,
		     GdkBitmap *mask)
{
  g_warning("gdk_window_set_icon: not implemented!");
}

void          
gdk_window_set_icon_name (GdkWindow *window, 
			  gchar *    name)
{
  g_warning("gdk_window_set_icon_name: not implemented!");
}

void          
gdk_window_set_group (GdkWindow *window, 
		      GdkWindow *leader)
{
  g_warning("gdk_window_set_group: not implemented!");
}

void
gdk_window_set_decorations (GdkWindow      *window,
			    GdkWMDecoration decorations)
{
  g_warning("gdk_window_set_decorations: not implemented!");
}

void
gdk_window_set_functions (GdkWindow    *window,
			  GdkWMFunction functions)
{
  g_warning("gdk_window_set_functions: not implemented!");
}

GList *
gdk_window_get_toplevels (void)
{
  GList *new_list = NULL;
  GList *tmp_list;
  
  tmp_list = gdk_root_parent.children;
  while (tmp_list)
    {
      new_list = g_list_prepend (new_list, tmp_list->data);
      tmp_list = tmp_list->next;
    }
  
  return new_list;
}

void
gdk_window_set_child_shapes (GdkWindow *window)
{
  g_warning("gdk_window_set_child_shapes: not implemented!");
}

void
gdk_window_merge_child_shapes (GdkWindow *window)
{
  g_warning("gdk_window_merge_child_shapes: not implemented!");
}

/*************************************************************
 * gdk_window_is_visible:
 *     Check if the given window is mapped.
 *   arguments:
 *     window: 
 *   results:
 *     is the window mapped
 *************************************************************/

gboolean 
gdk_window_is_visible (GdkWindow *window)
{
  GdkWindowPrivate *priv = (GdkWindowPrivate *)window;
  
  g_return_val_if_fail (window != NULL, FALSE);
  
  return priv->mapped;
}

/*************************************************************
 * gdk_window_is_viewable:
 *     Check if the window and all ancestors of the window
 *     are mapped. (This is not necessarily "viewable" in
 *     the X sense, since we only check as far as we have
 *     GDK window parents, not to the root window)
 *   arguments:
 *     window:
 *   results:
 *     is the window viewable
 *************************************************************/

gboolean 
gdk_window_is_viewable (GdkWindow *window)
{
  GdkWindowPrivate *priv = (GdkWindowPrivate *)window;
  
  g_return_val_if_fail (window != NULL, FALSE);
  
  while (priv && 
	 (priv != &gdk_root_parent) &&
	 (priv->window_type != GDK_WINDOW_FOREIGN))
    {
      if (!priv->mapped)
	return FALSE;
      
      priv = (GdkWindowPrivate *)priv->parent;
    }
  
  return TRUE;
}

void          
gdk_drawable_set_data (GdkDrawable   *drawable,
		       const gchar   *key,
		       gpointer	      data,
		       GDestroyNotify destroy_func)
{
  g_dataset_set_data_full (drawable, key, data, destroy_func);
}


/*************************************************************
 * gdk_window_set_static_gravities:
 *     Set the bit gravity of the given window to static,
 *     and flag it so all children get static subwindow
 *     gravity.
 *   arguments:
 *     window: window for which to set static gravity
 *     use_static: Whether to turn static gravity on or off.
 *   results:
 *     Does the XServer support static gravity?
 *************************************************************/

gboolean 
gdk_window_set_static_gravities (GdkWindow *window,
				 gboolean   use_static)
{
  g_warning("gdk_window_set_static_gravities: not implemented!");
  return FALSE;
}
