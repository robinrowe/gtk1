/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1999 Peter Mattis, Spencer Kimball and Josh MacDonald
 * Copyright (C) 1998-1999 Tor Lillqvist
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "config.h"

#include <string.h>

#define INITGUID

#include "gdkconfig.h"
#include "gdkx.h"
#include <gdk/gdk.h>

typedef struct _GdkDragContextPrivate GdkDragContextPrivate;

typedef enum {
  GDK_DRAG_STATUS_DRAG,
  GDK_DRAG_STATUS_MOTION_WAIT,
  GDK_DRAG_STATUS_ACTION_WAIT,
  GDK_DRAG_STATUS_DROP
} GtkDragStatus;

typedef enum {
  GDK_DRAG_SOURCE,
  GDK_DRAG_TARGET
} GdkDragKind;

/* Structure that holds information about a drag in progress.
 * this is used on both source and destination sides.
 */
struct _GdkDragContextPrivate {
  GdkDragContext context;

  guint   ref_count;

  guint16 last_x;		/* Coordinates from last event */
  guint16 last_y;
  XID dest_xid;
  guint drag_status;		/* Current status of drag */
};

GdkDragContext *current_dest_drag = NULL;

/* Drag Contexts */

static GList *contexts;

GdkDragContext *
gdk_drag_context_new        (void)
{
  GdkDragContextPrivate *result;

  result = g_new0 (GdkDragContextPrivate, 1);

  result->ref_count = 1;

  contexts = g_list_prepend (contexts, result);

  return (GdkDragContext *)result;
}

void
gdk_drag_context_ref (GdkDragContext *context)
{
  g_return_if_fail (context != NULL);

  ((GdkDragContextPrivate *)context)->ref_count++;
}

void
gdk_drag_context_unref (GdkDragContext *context)
{
  GdkDragContextPrivate *priv = (GdkDragContextPrivate *)context;

  g_return_if_fail (context != NULL);

  priv->ref_count--;

  GDK_NOTE (DND, g_print ("gdk_drag_context_unref: %d%s\n",
			  priv->ref_count,
			  (priv->ref_count == 0 ? " freeing" : "")));

  if (priv->ref_count == 0)
    {
      g_dataset_destroy (priv);
      
      g_list_free (context->targets);

      if (context->source_window)
	gdk_window_unref (context->source_window);

      if (context->dest_window)
	gdk_window_unref (context->dest_window);

      contexts = g_list_remove (contexts, priv);
      g_free (priv);
    }
}

#define resolve_link(hWnd, lpszLinkName, lpszPath, lpszDescription) FALSE

static GdkFilterReturn
gdk_dropfiles_filter (GdkXEvent *xev,
		      GdkEvent  *event,
		      gpointer   data)
{
  /* XXX */
}

/*************************************************************
 ************************** Public API ***********************
 *************************************************************/

void
gdk_dnd_init (void)
{
  g_warning ("gdk_drag_init: not implemented\n");
}      

void
gdk_dnd_exit (void)
{
  g_warning ("gdk_drag_exit: not implemented\n");
}

GdkDragContext * 
gdk_drag_begin (GdkWindow     *window,
		GList         *targets)
{
  g_warning ("gdk_drag_begin: not implemented\n");
}

guint32
gdk_drag_get_protocol (guint32          xid,
		       GdkDragProtocol *protocol)
{
  /* This isn't used */
  return 0;
}

void
gdk_drag_find_window (GdkDragContext  *context,
		      GdkWindow       *drag_window,
		      gint             x_root,
		      gint             y_root,
		      GdkWindow      **dest_window,
		      GdkDragProtocol *protocol)
{
  g_warning ("gdk_drag_find_window: not implemented\n");
}

gboolean
gdk_drag_motion (GdkDragContext *context,
		 GdkWindow      *dest_window,
		 GdkDragProtocol protocol,
		 gint            x_root, 
		 gint            y_root,
		 GdkDragAction   suggested_action,
		 GdkDragAction   possible_actions,
		 guint32         time)
{
  return FALSE;
}

void
gdk_drag_drop (GdkDragContext *context,
	       guint32         time)
{
  g_return_if_fail (context != NULL);

  g_warning ("gdk_drag_drop: not implemented\n");
}

void
gdk_drag_abort (GdkDragContext *context,
		guint32         time)
{
  g_return_if_fail (context != NULL);

  g_warning ("gdk_drag_abort: not implemented\n");
}

/* Destination side */

void
gdk_drag_status (GdkDragContext   *context,
		 GdkDragAction     action,
		 guint32           time)
{
  GDK_NOTE (DND, g_print ("gdk_drag_status\n"));
}

void 
gdk_drop_reply (GdkDragContext   *context,
		gboolean          ok,
		guint32           time)
{
  g_warning("gdk_drop_reply: not implemented!");
}

void
gdk_drop_finish (GdkDragContext   *context,
		 gboolean          success,
		 guint32           time)
{
  g_warning("gdk_drop_finish: not implemented!");
}

void
gdk_window_register_dnd (GdkWindow      *window)
{
  g_warning ("gdk_window_register_dnd: not implemented\n");
}

/*************************************************************
 * gdk_drag_get_selection:
 *     Returns the selection atom for the current source window
 *   arguments:
 *
 *   results:
 *************************************************************/

GdkAtom
gdk_drag_get_selection (GdkDragContext *context)
{
  g_warning ("gdk_drag_get_selection: not implemented\n");
  return GDK_NONE;
}
