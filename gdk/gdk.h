/* GDK - Gimp Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GDK_H__
#define __GDK_H__

//#include <unistd.h>
#include "glib-config.h"
#include "glib/gtypes.h"
#include "gdk-os.h"
#include "gdkgc.h"
#include "gdkcc.h"
#include "gdkcolor.h"
#include "gdkcursor.h"
#include "gdkdnd.h"
#include "gdkdrawable.h"
#include "gdkevents.h"
#include "gdkfont.h"
#include "gdkim.h"
#include "gdkimage.h"
#include "gdkinput.h"
#include "gdkpixmap.h"
#include "gdkproperty.h"
#include "gdkregion.h"
#include "gdkrgb.h"
#include "gdkselection.h"
#include "gdktypes.h"
#include "gdkvisual.h"
#include "gdkwindow.h"
#include "gdkcompat.h"
#include "glib/gthread.h"
#include "glib/gmain.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Initialization, exit and events
 */
#define	  GDK_PRIORITY_EVENTS		(G_PRIORITY_DEFAULT)
GDK_DLL void 	  gdk_init		   	(gint	   	*argc,
					 gchar        ***argv);
GDK_DLL gboolean  gdk_init_check   	        (gint	   	*argc,
					 gchar        ***argv);
GDK_DLL void  	  gdk_exit		   	(gint	    	 error_code);
GDK_DLL gchar*	  gdk_set_locale	   	(void);

/* Push and pop error handlers for X errors
 */
GDK_DLL void      gdk_error_trap_push           (void);
GDK_DLL gint      gdk_error_trap_pop            (void);


GDK_DLL void	  gdk_set_use_xshm		(gboolean	 use_xshm);

GDK_DLL gboolean  gdk_get_use_xshm		(void);
GDK_DLL gchar*	  gdk_get_display		(void);

GDK_DLL gsize gdk_input_add_full	  (gint		     source,
			   GdkInputCondition condition,
			   GdkInputFunction  function,
			   gpointer	     data,
			   GdkDestroyNotify  destroy);
GDK_DLL gint gdk_input_add	  (gint		     source,
			   GdkInputCondition condition,
			   GdkInputFunction  function,
			   gpointer	     data);
GDK_DLL void gdk_input_remove	  (gint		     tag);

GDK_DLL gint     gdk_pointer_grab       (GdkWindow    *window,
				 gboolean      owner_events,
				 GdkEventMask  event_mask,
				 GdkWindow    *confine_to,
				 GdkCursor    *cursor,
				 guint32       time);
GDK_DLL void     gdk_pointer_ungrab     (guint32       time);
GDK_DLL gint     gdk_keyboard_grab      (GdkWindow    *window,
				 gboolean      owner_events,
				 guint32       time);
GDK_DLL void     gdk_keyboard_ungrab    (guint32       time);
GDK_DLL gboolean gdk_pointer_is_grabbed (void);

GDK_DLL gint gdk_screen_width  (void);
GDK_DLL gint gdk_screen_height (void);

GDK_DLL gint gdk_screen_width_mm  (void);
GDK_DLL gint gdk_screen_height_mm (void);

GDK_DLL void gdk_flush (void);
GDK_DLL void gdk_beep (void);

GDK_DLL void gdk_key_repeat_disable (void);
GDK_DLL void gdk_key_repeat_restore (void);

/* Rectangle utilities
 */
GDK_DLL gboolean gdk_rectangle_intersect (GdkRectangle *src1,
				  GdkRectangle *src2,
				  GdkRectangle *dest);
GDK_DLL void     gdk_rectangle_union     (GdkRectangle *src1,
				  GdkRectangle *src2,
				  GdkRectangle *dest);

/* Conversion functions between wide char and multibyte strings. 
 */
GDK_DLL gchar     *gdk_wcstombs          (const GdkWChar   *src);
GDK_DLL gint       gdk_mbstowcs          (GdkWChar         *dest,
				  const gchar      *src,
				  gint              dest_max);

/* Miscellaneous */
GDK_DLL void     gdk_event_send_clientmessage_toall (GdkEvent    *event);
GDK_DLL gboolean gdk_event_send_client_message (GdkEvent    *event,
					guint32      xid);

/* Key values
 */
GDK_DLL gchar*   gdk_keyval_name         (guint        keyval);
GDK_DLL guint    gdk_keyval_from_name    (const gchar *keyval_name);
GDK_DLL void     gdk_keyval_convert_case (guint        symbol,
				  guint       *lower,
				  guint       *upper);
GDK_DLL guint    gdk_keyval_to_upper     (guint        keyval);
GDK_DLL guint    gdk_keyval_to_lower     (guint        keyval);
GDK_DLL gboolean gdk_keyval_is_upper     (guint        keyval);
GDK_DLL gboolean gdk_keyval_is_lower     (guint        keyval);

#ifndef _WIN32
GdkWindow*    gdk_window_ref	     (GdkWindow	    *window);
void	      gdk_window_unref	     (GdkWindow	    *window);
GdkPixmap *gdk_pixmap_ref		(GdkPixmap  *pixmap);
void	   gdk_pixmap_unref		(GdkPixmap  *pixmap);
GdkBitmap *gdk_bitmap_ref		(GdkBitmap  *pixmap);
void	   gdk_bitmap_unref		(GdkBitmap  *pixmap);
void	      gdk_window_get_size	 (GdkWindow	  *window,
					  gint		  *width,
					  gint		  *height);
GdkVisual*    gdk_window_get_visual	 (GdkWindow	  *window);
GdkColormap*  gdk_window_get_colormap	 (GdkWindow	  *window);
GdkWindowType gdk_window_get_type	 (GdkWindow	  *window);
void gdk_draw_pixmap	 (GdkDrawable  *drawable,
			  GdkGC	       *gc,
			  GdkDrawable  *src,
			  gint		xsrc,
			  gint		ysrc,
			  gint		xdest,
			  gint		ydest,
			  gint		width,
			  gint		height);
void gdk_draw_image	 (GdkDrawable  *drawable,
			  GdkGC	       *gc,
			  GdkImage     *image,
			  gint		xsrc,
			  gint		ysrc,
			  gint		xdest,
			  gint		ydest,
			  gint		width,
			  gint		height);
#endif
/* Threading
 */

extern GMutex *gdk_threads_mutex;

GDK_DLL void     gdk_threads_enter                (void);
GDK_DLL void     gdk_threads_leave                (void);

#ifdef	G_THREADS_ENABLED
#  define GDK_THREADS_ENTER()	G_STMT_START {	\
      if (gdk_threads_mutex)                 	\
        g_mutex_lock (gdk_threads_mutex);   	\
   } G_STMT_END
#  define GDK_THREADS_LEAVE()	G_STMT_START { 	\
      if (gdk_threads_mutex)                 	\
        g_mutex_unlock (gdk_threads_mutex); 	\
   } G_STMT_END
#else	/* !G_THREADS_ENABLED */
#  define GDK_THREADS_ENTER()
#  define GDK_THREADS_LEAVE()
#endif	/* !G_THREADS_ENABLED */

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDK_H__ */
