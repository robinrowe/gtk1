#ifndef __GDK_INPUT_H__
#define __GDK_INPUT_H__

#include "gdktypes.h"
#include "gdkprivate.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum
{
  GDK_EXTENSION_EVENTS_NONE,
  GDK_EXTENSION_EVENTS_ALL,
  GDK_EXTENSION_EVENTS_CURSOR
} GdkExtensionMode;

typedef enum
{
  GDK_MODE_DISABLED,
  GDK_MODE_SCREEN,
  GDK_MODE_WINDOW
} GdkInputMode;

typedef enum
{
  GDK_AXIS_IGNORE,
  GDK_AXIS_X,
  GDK_AXIS_Y,
  GDK_AXIS_PRESSURE,
  GDK_AXIS_XTILT,
  GDK_AXIS_YTILT,
  GDK_AXIS_LAST
} GdkAxisUse;

typedef struct GdkDeviceKey
{
	guint keyval;
	GdkModifierType modifiers;
} GdkDeviceKey;

 /* information about a device axis */
typedef struct GdkAxisInfo
{
	/* reported x resolution */
	gint xresolution;
	/* reported x minimum/maximum values */
	gint xmin_value, xmax_value;
	/* calibrated resolution (for aspect ration) - only relative values
	   between axes used */
	gint resolution;
	/* calibrated minimum/maximum values */
	gint min_value, max_value;
} GdkAxisInfo;

typedef struct GdkDeviceInfo
{
  guint32 deviceid;
  gchar *name;
  GdkInputSource source;
  GdkInputMode mode;
  gint has_cursor;	/* TRUE if the X pointer follows device motion */
  gint num_axes;
  gint num_keys;
  GdkDeviceKey *keys;
  /* information about the axes */
#if 0
//def _WIN32
  const GdkAxisInfo* axes;
#else
  GdkAxisUse* axes;    /* Specifies use for each axis */
#endif
#ifdef _WIN32
  /* reverse lookup on axis use type */
  gint axis_for_use[GDK_AXIS_LAST];
  /* true if we need to select a different set of events, but
   * can't because this is the core pointer
   */
  gint needs_update;
  /* State of buttons */
  gint button_state;
  gint* last_axis_data;
  gint last_buttons;
#ifdef HAVE_WINTAB
  /* WINTAB stuff: */
  HCTX hctx;
  /* Cursor number */
  UINT cursor;
  /* The cursor's CSR_PKTDATA */
  WTPKT pktdata;
  /* CSR_NPBTNMARKS */
  UINT npbtnmarks[2];
  /* Azimuth and altitude axis */
  AXIS orientation_axes[2];
#endif
#endif
} GdkDeviceInfo;

typedef struct GdkTimeCoord
{
  guint32 time;
  gdouble x;
  gdouble y;
  gdouble pressure;
  gdouble xtilt;
  gdouble ytilt;
} GdkTimeCoord;

GDK_DLL GList *       gdk_input_list_devices         (void);
GDK_DLL void          gdk_input_set_extension_events (GdkWindow        *window,
					      gint              mask,
					      GdkExtensionMode  mode);
GDK_DLL void          gdk_input_set_source           (guint32           deviceid,
					      GdkInputSource    source);
GDK_DLL gboolean      gdk_input_set_mode             (guint32           deviceid,
					      GdkInputMode      mode);
GDK_DLL void          gdk_input_set_axes             (guint32           deviceid,
					      GdkAxisUse       *axes);
GDK_DLL void          gdk_input_set_key              (guint32           deviceid,
					      guint             index,
					      guint             keyval,
					      GdkModifierType   modifiers);
GDK_DLL void          gdk_input_window_get_pointer   (GdkWindow        *window,
					      guint32           deviceid,
					      gdouble          *x,
					      gdouble          *y,
					      gdouble          *pressure,
					      gdouble          *xtilt,
					      gdouble          *ytilt,
					      GdkModifierType  *mask);
GDK_DLL GdkTimeCoord *gdk_input_motion_events        (GdkWindow        *window,
					      guint32           deviceid,
					      guint32           start,
					      guint32           stop,
					      gint             *nevents_return);

void gdk_input_window_destroy (GdkWindow *window);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GDK_INPUT_H__ */
