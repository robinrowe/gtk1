// gdkspeech.h
// Copyright Robin.Rowe@CinePaint.org 2021/05/14
// License open source MIT

#ifndef gdkspeech_h
#define gdkspeech_h

#ifdef __cplusplus
extern "C" {
#endif

struct GdkSpeech* GdkSpeechOpen();
int GdkSpeechSay(struct GdkSpeech* gs,const char* text);
void GdkSpeechClose(struct GdkSpeech* gs);

#ifdef __cplusplus
}
#endif

#endif