// wintab.h 2021/1/25

#ifndef wintab_h
#define wintab_h

#error

typedef struct HCTX
{	int junk;
} HCTX;


typedef struct WTPKT
{
	int junk;
} WTPKT;

typedef struct AXIS
{
	int junk;
} AXIS;

// If using UWP Windows Runtime then it's quite straightforward. The PointerEventArgs event seems to have all necessary data.
// Modified Core App(C++ / WinRT) template project snippet from Visual Studio 2019:

#if 0

void OnPointerMoved(IInspectable const&, PointerEventArgs const& args)
{
    if (m_selected)
    {
        float2 const point = args.CurrentPoint().Position();

        m_selected.Offset(
            {
                point.x + m_offset.x,
                point.y + m_offset.y,
                0.0f
            });

        // (new!) Change sprite color based on pen pressure and tilt
        auto sprite = m_selected.as<SpriteVisual>();

        auto const props = args.CurrentPoint().Properties();
        auto const pressure = props.Pressure();
        auto const orientation = props.Orientation() / 360.0f;
        auto const tiltx = (props.XTilt() + 90) / 180.0f;
        auto const tilty = (props.YTilt() + 90) / 180.0f;

        Compositor compositor = m_visuals.Compositor();
        sprite.Brush(compositor.CreateColorBrush({
            (uint8_t)(pressure * 0xFF),
            (uint8_t)(tiltx * 0xFF),
            (uint8_t)(tilty * 0xFF),
            (uint8_t)(orientation * 0xFF)
            }));
    }
}

void SetWindow(CoreWindow const& window)
{
    window.PointerCursor(Windows::UI::Core::CoreCursor{ CoreCursorType::Arrow, 0 });

    window.PointerPressed({ this, &App::OnPointerPressed });
    window.PointerReleased({ this, &App::OnPointerReleased });
    window.PointerMoved({ this, &App::OnPointerMoved });
}

// Pointer event data in PointerEventArgs.
void OnPointerPressed(Windows::UI::Core::CoreWindow const& /* sender */,
    Windows::UI::Core::PointerEventArgs const& /* args */) {}

void OnPointerReleased(Windows::UI::Core::CoreWindow const& /* sender */,
    Windows::UI::Core::PointerEventArgs const& /* args */) {}

void OnPointerMoved(Windows::UI::Core::CoreWindow const& /* sender */,
    Windows::UI::Core::PointerEventArgs const& /* args */) {}

#endif

#endif
