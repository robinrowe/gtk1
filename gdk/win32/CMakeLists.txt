# gtk1/win32/CMakeLists.txt
# Created by Robin Rowe 2019-12-14
# License: License GNU Lesser General Public License (GNU LGPL)

cmake_minimum_required(VERSION 3.8)
project(gdk-os)
message("Configuring ${PROJECT_NAME}...")
enable_testing()
file(STRINGS sources.cmake SOURCES)
add_definitions(-DDLL_EXPORTS)
add_library(${PROJECT_NAME} OBJECT ${SOURCES})
