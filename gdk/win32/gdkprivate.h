/* GDK - Gimp Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GDK_PRIVATE_H__
#define __GDK_PRIVATE_H__

#include "gdk/gdktypes.h"
#include "gdk/gdkevents.h"
#include "gdk/gdkfont.h"
#include "gdk/gdkgc.h"
#include "gdk/gdkim.h"
#include "gdk/gdkimage.h"
#include "gdk/gdkregion.h"
#include "gdk/gdkvisual.h"
#include "gdk/gdkwindow.h"
#include "gdk/gdkcursor.h"
#include "glib/garray.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define gdk_window_lookup(xid)	   ((GdkWindow*) gdk_xid_table_lookup ((HANDLE) xid))
#define gdk_pixmap_lookup(xid)	   ((GdkPixmap*) gdk_xid_table_lookup (xid))
#define gdk_font_lookup(xid)	   ((GdkFont*) gdk_xid_table_lookup (xid))

#ifdef INSIDE_OUT

#define GdkDrawable GdkDrawableInside
#define GdkDrawablePrivate GdkDrawablePrivateInside
#define GdkFontPrivate GdkFontPrivateInside
#include "gdkprivate-win32.h"
/*
#define _GdkDrawablePrivate _GdkDrawable

#define _GdkImagePrivate _GdkImage
#define GdkImage GdkImagePublic
*/
#endif

typedef struct GdkDrawablePrivate
{
  GdkDrawable drawable;
  GdkDrawableClass *klass;
  gpointer klass_data;

  guint8 window_type;
  guint ref_count;

  guint16 width;
  guint16 height;

  GdkColormap *colormap;

  guint destroyed : 2;
} GdkDrawablePrivate;

typedef struct GdkWindowPrivate
{
  GdkDrawablePrivate drawable;
  
  GdkWindow *parent;
  gint16 x;
  gint16 y;
  guint8 resize_count;
  guint mapped : 1;
  guint guffaw_gravity : 1;

  gint extension_events;

  GList *filters;
  GList *children;
} GdkWindowPrivate;

typedef struct GdkImageClass 
{
  void (*destroy)   (GdkImage    *image);
  void (*image_put) (GdkImage	 *image,
		     GdkDrawable *window,
		     GdkGC	 *gc,
		     gint	  xsrc,
		     gint	  ysrc,
		     gint	  xdest,
		     gint	  ydest,
		     gint	  width,
		     gint	  height);
} GdkImageClass;

typedef struct GdkImagePrivate
{
  GdkImage image;
  guint ref_count;
  GdkImageClass *klass;
} GdkImagePrivate;

typedef struct GdkFontPrivate
{
  GdkFont font;
  guint ref_count;
} GdkFontPrivate;

typedef struct GdkGCPrivate
{
  guint ref_count;
  GdkGCClass *klass;
  gpointer klass_data;
} GdkGCPrivate;

typedef enum {
  GDK_COLOR_WRITEABLE = 1 << 0
} GdkColorInfoFlags;

typedef struct GdkColorInfo
{
  GdkColorInfoFlags flags;
  guint ref_count;
} GdkColorInfo;

typedef struct GdkColormapPrivate
{
  GdkColormap colormap;

  GdkVisual *visual;

  guint ref_count;
} GdkColormapPrivate;

typedef struct GdkEventFilter {
  GdkFilterFunc function;
  gpointer data;
} GdkEventFilter;

typedef struct GdkClientFilter {
  GdkAtom       type;
  GdkFilterFunc function;
  gpointer      data;
} GdkClientFilter;

typedef enum 
{
  GDK_ARG_STRING,
  GDK_ARG_INT,
  GDK_ARG_BOOL,
  GDK_ARG_NOBOOL,
  GDK_ARG_CALLBACK
} GdkArgType;

typedef void (*GdkArgFunc) (const char *name, const char *arg, gpointer data);

typedef struct GdkArgContext
{
  GPtrArray *tables;
  gpointer cb_data;
} GdkArgContext;

typedef struct GdkArgDesc
{
  const char *name;
  GdkArgType type;
  gpointer location;
  GdkArgFunc callback;
} GdkArgDesc;

void gdk_event_button_generate (GdkEvent *event);

/* FIFO's for event queue, and for events put back using
 * gdk_event_put().
 */
extern GList *gdk_queued_events;
extern GList *gdk_queued_tail;
extern GdkEventFunc   gdk_event_func;    /* Callback for events */
extern gpointer       gdk_event_data;
extern GDestroyNotify gdk_event_notify;
GdkEvent* gdk_event_new (void);
void      gdk_events_init   (void);
void      gdk_events_queue  (void);
GdkEvent* gdk_event_unqueue (void);
GList* gdk_event_queue_find_first  (void);
void   gdk_event_queue_remove_link (GList    *node);
void   gdk_event_queue_append      (GdkEvent *event);

void gdk_window_init (void);
void gdk_visual_init (void);
void gdk_dnd_init    (void);

void gdk_image_init  (void);
void gdk_image_exit  (void);

void gdk_input_init  (void);
void gdk_input_exit  (void);

void gdk_windowing_exit (void);

void gdk_window_add_colormap_windows (GdkWindow *window);
void gdk_window_destroy_notify	     (GdkWindow *window);

/* If you pass x = y = -1, it queries the pointer
   to find out where it currently is.
   If you pass x = y = -2, it does anything necessary
   to know that the drag is ending.
*/
void gdk_dnd_display_drag_cursor(gint x,
				 gint y,
				 gboolean drag_ok,
				 gboolean change_made);

extern gboolean		 gdk_show_events;
extern gint		 gdk_screen;
extern GdkWindow  	*gdk_parent_root;
extern gint		 gdk_error_code;
extern gint		 gdk_error_warnings;
extern GList            *gdk_default_filters;

GdkWindow* _gdk_window_alloc (void);

/* Font/string functions implemented in module-specific code */
gint _gdk_font_strlen (GdkFont *font, const char *str);
void _gdk_font_destroy (GdkFont *font);
void _gdk_colormap_real_destroy (GdkColormap *colormap);
void _gdk_cursor_destroy (GdkCursor *cursor);

/* Initialization */

extern GdkArgDesc _gdk_windowing_args[];
gboolean _gdk_windowing_init_check (int argc, char **argv);

#ifdef USE_XIM
/* XIM support */
gint   gdk_im_open		 (void);
void   gdk_im_close		 (void);
void   gdk_ic_cleanup		 (void);
#endif /* USE_XIM */

/* Debugging support */

#ifdef G_ENABLE_DEBUG
#define GDK_BUFFER

#ifdef EXTRA_GDK_DEBUG
#define GDK_NOTE(type,action)		     G_STMT_START { \
       { action; };			     } G_STMT_END

#else

#define GDK_NOTE(type,action)		     G_STMT_START { \
    if (gdk_debug_flags & GDK_DEBUG_##type)		    \
       { action; };			     } G_STMT_END

#endif

#else /* !G_ENABLE_DEBUG */
#define GDK_BUFFER //
#define GDK_NOTE(type,action)
      
#endif /* G_ENABLE_DEBUG */

extern guint gdk_debug_flags;

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDK_PRIVATE_H__ */
