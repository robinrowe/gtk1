// gdkspeech.h
// Copyright Robin.Rowe@Gimp.org 2021/05/14
// License open source MIT

#ifndef gdkspeech_h
#define gdkspeech_h

#include <gdktypes.h>

typedef struct GdkPen
{	int is_inverted;
	float pressure;
	long tilt_x;
	long tilt_y;
	long mouse_x;
	long mouse_y;
} GdkPen;

extern GdkPen gdkpen;

#ifdef __cplusplus
extern "C" {
#endif

GDK_DLL struct GdkTablet* GdkTabletOpen();
GDK_DLL int GdkTabletStart(struct GdkTablet* gt,GdkWindow* gw);
GDK_DLL void GdkTabletClose(struct GdkTablet* gt);
GDK_DLL GdkPen* GdkGetPen();
GDK_DLL void GdkPenPrint();

#ifdef __cplusplus
}
#endif

#endif