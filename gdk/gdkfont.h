#ifndef __GDK_FONT_H__
#define __GDK_FONT_H__

#include <gdk/gdktypes.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Types of font.
 *   GDK_FONT_FONT: the font is an XFontStruct.
 *   GDK_FONT_FONTSET: the font is an XFontSet used for I18N.
 */
typedef enum
{
  GDK_FONT_FONT,
  GDK_FONT_FONTSET
} GdkFontType;

typedef struct GdkFont
{
  GdkFontType type;
  gint ascent;
  gint descent;
} GdkFont;

GDK_DLL gchar**  gdk_font_list_new  (const gchar    *font_pattern, gint *n_returned);
GDK_DLL void     gdk_font_list_free (gchar **font_list);
GDK_DLL GdkFont* gdk_font_load	    (const gchar    *font_name);
GDK_DLL GdkFont* gdk_fontset_load   (const gchar    *fontset_name);
GDK_DLL GdkFont* gdk_font_ref	    (GdkFont        *font);
GDK_DLL void	 gdk_font_unref	    (GdkFont        *font);
GDK_DLL uintptr_t	 gdk_font_id	    (const GdkFont  *font);
GDK_DLL gboolean gdk_font_equal	    (const GdkFont  *fonta,
			     const GdkFont  *fontb);

GDK_DLL gint	 gdk_string_width   (GdkFont        *font,
			     const gchar    *string);
GDK_DLL gint	 gdk_text_width	    (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDK_DLL gint	 gdk_text_width_wc  (GdkFont        *font,
			     const GdkWChar *text,
			     gint            text_length);
GDK_DLL gint	 gdk_char_width	    (GdkFont        *font,
			     gchar           character);
GDK_DLL gint	 gdk_char_width_wc  (GdkFont        *font,
			     GdkWChar        character);
GDK_DLL gint	 gdk_string_measure (GdkFont        *font,
			     const gchar    *string);
GDK_DLL gint	 gdk_text_measure   (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDK_DLL gint	 gdk_char_measure   (GdkFont        *font,
			     gchar           character);
GDK_DLL gint	 gdk_string_height  (GdkFont        *font,
			     const gchar    *string);
GDK_DLL gint	 gdk_text_height    (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDK_DLL gint	 gdk_char_height    (GdkFont        *font,
			     gchar           character);

GDK_DLL void     gdk_text_extents   (GdkFont     *font,
			     const gchar *text,
			     gint         text_length,
			     gint        *lbearing,
			     gint        *rbearing,
			     gint        *width,
			     gint        *ascent,
			     gint        *descent);
GDK_DLL void    gdk_text_extents_wc (GdkFont        *font,
			     const GdkWChar *text,
			     gint            text_length,
			     gint           *lbearing,
			     gint           *rbearing,
			     gint           *width,
			     gint           *ascent,
			     gint           *descent);
GDK_DLL void     gdk_string_extents (GdkFont     *font,
			     const gchar *string,
			     gint        *lbearing,
			     gint        *rbearing,
			     gint        *width,
			     gint        *ascent,
			     gint        *descent);

GDK_DLL gchar*   gdk_font_full_name_get (GdkFont *font);
GDK_DLL void	 gdk_font_full_name_free (gchar *name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GDK_FONT_H__ */
