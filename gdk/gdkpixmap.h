#ifndef __GDK_PIXMAP_H__
#define __GDK_PIXMAP_H__

#include <gdk/gdktypes.h>
#include <gdk/gdkcolor.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Pixmaps
 */
GDK_DLL GdkPixmap* gdk_pixmap_new		(GdkWindow  *window,
					 gsize	     width,
					 gsize	     height,
					 gsize	     depth);
GDK_DLL GdkBitmap* gdk_bitmap_create_from_data	(GdkWindow   *window,
					 const gchar *data,
					 gsize	      width,
					 gsize	      height);
GDK_DLL GdkPixmap* gdk_pixmap_create_from_data	(GdkWindow   *window,
					 const gchar *data,
	gsize	      width,
	gsize	      height,
	gsize	      depth,
					 GdkColor    *fg,
					 GdkColor    *bg);
GDK_DLL GdkPixmap* gdk_pixmap_create_from_xpm	(GdkWindow  *window,
					 GdkBitmap **mask,
					 GdkColor   *transparent_color,
					 const gchar *filename);
GDK_DLL GdkPixmap* gdk_pixmap_colormap_create_from_xpm 
                                        (GdkWindow   *window,
					 GdkColormap *colormap,
					 GdkBitmap  **mask,
					 GdkColor    *transparent_color,
					 const gchar *filename);
GDK_DLL GdkPixmap* gdk_pixmap_create_from_xpm_d (GdkWindow  *window,
					 GdkBitmap **mask,
					 GdkColor   *transparent_color,
					 gchar	   **data);
GDK_DLL GdkPixmap* gdk_pixmap_colormap_create_from_xpm_d 
                                        (GdkWindow   *window,
					 GdkColormap *colormap,
					 GdkBitmap  **mask,
					 GdkColor    *transparent_color,
					 gchar     **data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GDK_PIXMAP_H__ */
