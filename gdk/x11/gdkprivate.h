/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GDK_PRIVATE_H__
#define __GDK_PRIVATE_H__

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <linux/limits.h>
#include "gdktypes.h"
#include "gdkdrawable.h"
#include "gdkimage.h"
#include "gdkvisual.h"
#include "gdkfont.h"
#include "gdkcursor.h"
#include "gdkevents.h"
#include "gdkcc.h"
#include "gdkregion.h"
#include "gdkwindow.h"

#define gdk_window_lookup(xid)	   ((GdkWindow*) gdk_xid_table_lookup (xid))
#define gdk_pixmap_lookup(xid)	   ((GdkPixmap*) gdk_xid_table_lookup (xid))
#define gdk_font_lookup(xid)	   ((GdkFont*) gdk_xid_table_lookup (xid))


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if 0
typedef struct _GdkDrawablePrivate     GdkDrawablePrivate;
typedef struct _GdkWindowPrivate       GdkWindowPrivate;
typedef struct _GdkWindowPrivate       GdkPixmapPrivate;
typedef struct _GdkImagePrivate	       GdkImagePrivate;
typedef struct _GdkGCPrivate	       GdkGCPrivate;
typedef struct _GdkColormapPrivate     GdkColormapPrivate;
typedef struct _GdkColorInfo           GdkColorInfo;
typedef struct _GdkVisualPrivate       GdkVisualPrivate;
typedef struct _GdkFontPrivate	       GdkFontPrivate;
typedef struct _GdkCursorPrivate       GdkCursorPrivate;
typedef struct _GdkEventFilter	       GdkEventFilter;
typedef struct _GdkClientFilter	       GdkClientFilter;
typedef struct _GdkColorContextPrivate GdkColorContextPrivate;
typedef struct _GdkRegionPrivate       GdkRegionPrivate;
#endif

#define GDK_IS_WINDOW(d) (GDK_DRAWABLE_TYPE(d) <= GDK_WINDOW_TEMP || \
                          GDK_DRAWABLE_TYPE(d) == GDK_WINDOW_FOREIGN)

/*
#define GDK_DRAWABLE_TYPE(d) (((GdkWindowPrivate *)d)->window_type)
#define GDK_DRAWABLE_DESTROYED(d) (((GdkDrawablePrivate *)d)->destroyed)

#define GDK_IS_PIXMAP(d) (GDK_DRAWABLE_TYPE(d) == GDK_DRAWABLE_PIXMAP)
#define gdk_window_lookup(xid)	   ((GdkWindow*) gdk_xid_table_lookup ((HANDLE) xid))
#define gdk_pixmap_lookup(xid)	   ((GdkPixmap*) gdk_xid_table_lookup (xid))
#define gdk_font_lookup(xid)	   ((GdkFont*) gdk_xid_table_lookup (xid))
*/


typedef struct GdkDrawablePrivate
{
  GdkDrawable drawable;
  GdkDrawableClass *klass;
  gpointer klass_data;

  guint8 window_type;
  guint ref_count;

  guint16 width;
  guint16 height;

  GdkColormap *colormap;

  guint destroyed : 2;
} GdkDrawablePrivate;

typedef struct GdkWindowPrivate
{
  GdkWindow window;
  GdkWindow *parent;
  Window xwindow;
  Display *xdisplay;
  gint16 x;
  gint16 y;
  guint16 width;
  guint16 height;
  guint8 resize_count;
  guint8 window_type;
  guint ref_count;
  guint destroyed : 2;
  guint mapped : 1;
  guint guffaw_gravity : 1;

  gint extension_events;

  GList *filters;
  GdkColormap *colormap;
  GList *children;
} GdkWindowPrivate;

typedef struct GdkImagePrivate
{
  GdkImage image;
  XImage *ximage;
  Display *xdisplay;
  gpointer x_shm_info;

  void (*image_put) (GdkDrawable *window,
		     GdkGC	 *gc,
		     GdkImage	 *image,
		     gint	  xsrc,
		     gint	  ysrc,
		     gint	  xdest,
		     gint	  ydest,
		     gint	  width,
		     gint	  height);
} GdkImagePrivate;

typedef struct GdkGCPrivate
{
  GdkGC gc;
  GC xgc;
  Display *xdisplay;
  guint ref_count;
} GdkGCPrivate;

typedef enum {
  GDK_COLOR_WRITEABLE = 1 << 0
} GdkColorInfoFlags;

typedef struct GdkColorInfo
{
  GdkColorInfoFlags flags;
  guint ref_count;
} GdkColorInfo;

typedef struct GdkColormapPrivate
{
  GdkColormap colormap;
  Colormap xcolormap;
  Display *xdisplay;
  GdkVisual *visual;
  gint private_val;

  GHashTable *hash;
  GdkColorInfo *info;
  time_t last_sync_time;
  
  guint ref_count;
} GdkColormapPrivate;

typedef struct GdkVisualPrivate
{
  GdkVisual visual;
  Visual *xvisual;
} GdkVisualPrivate;

typedef struct GdkFontPrivate
{
  GdkFont font;
  /* XFontStruct *xfont; */
  /* generic pointer point to XFontStruct or XFontSet */
  gpointer xfont;
  Display *xdisplay;
  guint ref_count;

  GSList *names;
} GdkFontPrivate;

typedef struct GdkCursorPrivate
{
  GdkCursor cursor;
  Cursor xcursor;
  Display *xdisplay;
} GdkCursorPrivate;

typedef struct GdkDndCursorInfo {
  Cursor	  gdk_cursor_dragdefault, gdk_cursor_dragok;
  GdkWindow	 *drag_pm_default, *drag_pm_ok;
  GdkPoint	  default_hotspot, ok_hotspot;
  GList *xids;
} GdkDndCursorInfo;

//typedef struct _GdkDndCursorInfo GdkDndCursorInfo;

typedef struct GdkDndGlobals {
  GdkAtom	     gdk_XdeEnter, gdk_XdeLeave, gdk_XdeRequest;
  GdkAtom	     gdk_XdeDataAvailable, gdk_XdeDataShow, gdk_XdeCancel;
  GdkAtom	     gdk_XdeTypelist;

  GdkDndCursorInfo  *c;
  GdkWindow	**drag_startwindows;
  guint		  drag_numwindows;
  gboolean	  drag_really, drag_perhaps, dnd_grabbed;
  Window	  dnd_drag_target;
  GdkPoint	  drag_dropcoords;

  GdkPoint dnd_drag_start, dnd_drag_oldpos;
  GdkRectangle dnd_drag_dropzone;
  GdkWindowPrivate *real_sw;
  Window dnd_drag_curwin;
  Time last_drop_time; /* An incredible hack, sosumi miguel */
} GdkDndGlobals;

//typedef struct _GdkDndGlobals GdkDndGlobals;

typedef struct GdkEventFilter {
  GdkFilterFunc function;
  gpointer data;
} GdkEventFilter;

typedef struct GdkClientFilter {
  GdkAtom       type;
  GdkFilterFunc function;
  gpointer      data;
} GdkClientFilter;

typedef enum 
{
  GDK_ARG_STRING,
  GDK_ARG_INT,
  GDK_ARG_BOOL,
  GDK_ARG_NOBOOL,
  GDK_ARG_CALLBACK
} GdkArgType;

typedef void (*GdkArgFunc) (const char *name, const char *arg, gpointer data);

typedef struct GdkArgContext
{
  GPtrArray *tables;
  gpointer cb_data;
} GdkArgContext;

typedef struct GdkArgDesc
{
  const char *name;
  GdkArgType type;
  gpointer location;
  GdkArgFunc callback;
} GdkArgDesc;

extern GdkArgDesc _gdk_windowing_args[];
gboolean _gdk_windowing_init_check (int argc, char **argv);

#ifdef USE_XIM

typedef struct _GdkICPrivate GdkICPrivate;

struct _GdkICPrivate
{
  XIC xic;
  GdkICAttr *attr;
  GdkICAttributesType mask;
};

#endif /* USE_XIM */

typedef struct GdkColorContextPrivate
{
  GdkColorContext color_context;
  Display *xdisplay;
  XStandardColormap std_cmap;
} GdkColorContextPrivate;

typedef struct GdkRegionPrivate
{
  GdkRegion region;
  Region xregion;
} GdkRegionPrivate;

extern GList *gdk_queued_events;
extern GList *gdk_queued_tail;
extern GdkEventFunc   gdk_event_func;    /* Callback for events */
extern gpointer       gdk_event_data;
extern GDestroyNotify gdk_event_notify;
static GdkEvent* gdk_event_new (void);
void      gdk_events_init   (void);
void      gdk_events_queue  (void);
GdkEvent* gdk_event_unqueue (void);
GList* gdk_event_queue_find_first  (void);
void   gdk_event_queue_remove_link (GList    *node);
void   gdk_event_queue_append      (GdkEvent *event);

void gdk_events_init (void);
void gdk_window_init (void);
void gdk_visual_init (void);
void gdk_dnd_init    (void);

void gdk_image_init  (void);
void gdk_image_exit (void);

void gdk_input_init  (void);
void gdk_input_exit  (void);

GdkColormap* gdk_colormap_lookup (Colormap  xcolormap);
GdkVisual*   gdk_visual_lookup	 (Visual   *xvisual);

void gdk_window_add_colormap_windows (GdkWindow *window);
void gdk_window_destroy_notify	     (GdkWindow *window);

void	 gdk_xid_table_insert (XID	*xid,
			       gpointer	 data);
void	 gdk_xid_table_remove (XID	 xid);
gpointer gdk_xid_table_lookup (XID	 xid);

gint gdk_send_xevent (Window window, gboolean propagate, glong event_mask,
		      XEvent *event_send);

/* If you pass x = y = -1, it queries the pointer
   to find out where it currently is.
   If you pass x = y = -2, it does anything necessary
   to know that the drag is ending.
*/
void gdk_dnd_display_drag_cursor(gint x,
				 gint y,
				 gboolean drag_ok,
				 gboolean change_made);

gint _gdk_font_strlen (GdkFont *font, const char *str);
void _gdk_font_destroy (GdkFont *font);
void _gdk_colormap_real_destroy (GdkColormap *colormap);
void _gdk_cursor_destroy (GdkCursor *cursor);

/* Please see gdkwindow.c for comments on how to use */ 
Window gdk_window_xid_at(Window base, gint bx, gint by, gint x, gint y, GList *excludes, gboolean excl_child);
Window gdk_window_xid_at_coords(gint x, gint y, GList *excludes, gboolean excl_child);

extern gint		 gdk_debug_level;
extern gint		 gdk_show_events;
extern gint		 gdk_use_xshm;
extern gint		 gdk_stack_trace;
extern gchar		*gdk_display_name;
extern Display		*gdk_display;
extern gint		 gdk_screen;
extern Window		 gdk_root_window;
extern Window		 gdk_leader_window;
extern GdkWindowPrivate	 gdk_root_parent;
extern Atom		 gdk_wm_delete_window;
extern Atom		 gdk_wm_take_focus;
extern Atom		 gdk_wm_protocols;
extern Atom		 gdk_wm_window_protocols[];
extern Atom		 gdk_selection_property;
extern GdkDndGlobals	 gdk_dnd;
extern GdkWindow	*selection_owner[];
extern gchar		*gdk_progclass;
extern gint		 gdk_error_code;
extern gint		 gdk_error_warnings;
extern gint              gdk_null_window_warnings;
extern GList            *gdk_default_filters;
extern const int         gdk_nevent_masks;
extern const int         gdk_event_mask_table[];

extern GdkWindowPrivate *gdk_xgrab_window;  /* Window that currently holds the
					     * x pointer grab
					     */

#ifdef USE_XIM
/* XIM support */
gint   gdk_im_open		 (void);
void   gdk_im_close		 (void);
void   gdk_ic_cleanup		 (void);

extern GdkICPrivate *gdk_xim_ic;		/* currently using IC */
extern GdkWindow *gdk_xim_window;	        /* currently using Window */
#endif /* USE_XIM */

/* Debugging support */

#ifdef G_ENABLE_DEBUG

#define GDK_NOTE(type,action)		     G_STMT_START { \
    if (gdk_debug_flags & GDK_DEBUG_##type)		    \
       { action; };			     } G_STMT_END

#else /* !G_ENABLE_DEBUG */

#define GDK_NOTE(type,action)
      
#endif /* G_ENABLE_DEBUG */

extern guint gdk_debug_flags;


gboolean _gdk_font_wc_to_glyphs (GdkFont         *font,
				 const GdkWChar  *text,
				 gint             text_length,
				 gchar          **result,
				 gint            *result_length);
gchar *_gdk_wcstombs_len      (const GdkWChar  *src,
			       int              length);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDK_PRIVATE_H__ */
