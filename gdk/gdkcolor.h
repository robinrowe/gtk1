#ifndef __GDK_COLOR_H__
#define __GDK_COLOR_H__

#include <gdk/gdktypes.h>
#include <gdk/gdkvisual.h>
#include <gdk/gdkcolor.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* The color type.
 *   A color consists of red, green and blue values in the
 *    range 0-65535 and a pixel value. The pixel value is highly
 *    dependent on the depth and colormap which this color will
 *    be used to draw into. Therefore, sharing colors between
 *    colormaps is a bad idea.
 */
typedef struct GdkColor
{
  gulong  pixel;
  gushort red;
  gushort green;
  gushort blue;
} GdkColor;

/* The colormap type.
 */
typedef struct GdkColormap
{
  gint      size;
  GdkColor *colors;
} GdkColormap;


GDK_DLL GdkColormap* gdk_colormap_new	  (GdkVisual   *visual,
				   gboolean	allocate);
GDK_DLL GdkColormap* gdk_colormap_ref	  (GdkColormap *cmap);
GDK_DLL void	     gdk_colormap_unref	  (GdkColormap *cmap);

GDK_DLL GdkColormap* gdk_colormap_get_system	   (void);
GDK_DLL gint	     gdk_colormap_get_system_size  (void);

GDK_DLL void gdk_colormap_change (GdkColormap	*colormap,
			  gint		 ncolors);


GDK_DLL gint  gdk_colormap_alloc_colors   (GdkColormap *colormap,
				   GdkColor    *colors,
				   gint         ncolors,
				   gboolean     writeable,
				   gboolean     best_match,
				   gboolean    *success);
GDK_DLL gboolean gdk_colormap_alloc_color (GdkColormap *colormap,
				   GdkColor    *color,
				   gboolean     writeable,
				   gboolean     best_match);
GDK_DLL void     gdk_colormap_free_colors (GdkColormap *colormap,
				   GdkColor    *colors,
				   gint         ncolors);

GDK_DLL GdkVisual *gdk_colormap_get_visual (GdkColormap *colormap);
     
GDK_DLL GdkColor *gdk_color_copy  (const GdkColor *color);
GDK_DLL void      gdk_color_free  (GdkColor       *color);
GDK_DLL gboolean      gdk_color_parse (const gchar    *spec,
			   GdkColor       *color);
GDK_DLL guint     gdk_color_hash  (const GdkColor *colora);
GDK_DLL gboolean  gdk_color_equal (const GdkColor *colora,
			   const GdkColor *colorb);

/* The following functions are deprecated */
GDK_DLL void gdk_colors_store	 (GdkColormap	*colormap,
			  GdkColor	*colors,
			  gint		 ncolors);
GDK_DLL gboolean gdk_colors_alloc	 (GdkColormap	*colormap,
			  gboolean	 contiguous,
			  gulong	*planes,
			  gint		 nplanes,
			  gulong	*pixels,
			  gint		 npixels);
GDK_DLL void gdk_colors_free	 (GdkColormap	*colormap,
			  gulong	*pixels,
			  gint		 npixels,
			  gulong	 planes);
GDK_DLL gboolean gdk_color_white	 (GdkColormap	*colormap,
			  GdkColor	*color);
GDK_DLL gboolean gdk_color_black	 (GdkColormap	*colormap,
			  GdkColor	*color);
GDK_DLL gboolean gdk_color_alloc	 (GdkColormap	*colormap,
			  GdkColor	*color);
GDK_DLL gboolean gdk_color_change	 (GdkColormap	*colormap,
			  GdkColor	*color);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GDK_COLOR_H__ */
