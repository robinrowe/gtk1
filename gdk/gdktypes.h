/* GDK - Gimp Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GDK_TYPES_H__
#define __GDK_TYPES_H__

//#include "glib.h"
#include "glib/gtypes.h"

#if 0
#ifdef G_OS_WIN32
#  ifdef GDK_COMPILATION
#    define GDK_DLL __declspec(dllexport)
#  else
#    define GDK_DLL extern __declspec(dllimport)
#  endif
#else
#  define GDK_DLL extern
#endif
#endif

#ifndef CAST
#define CAST(x) (x)
#endif

#ifdef _WIN32

#ifdef DLL_EXPORT
#define GDK_DLL __declspec(dllexport)
#else
#define GDK_DLL __declspec(dllimport)
#endif
#else
#	define GDK_DLL 
	typedef int SOCKET;
#endif

/* The system specific file glib-config.h contains such configuration
 * settings that are needed not only when compiling GDK (or GTK)
 * itself, but also occasionally when compiling programs that use GDK
 * (or GTK). One such setting is what windowing API backend is in use.
 */
#include <glib-config.h>

/* some common magic values */
#define GDK_NONE	     0L
#define GDK_CURRENT_TIME     0L
#define GDK_PARENT_RELATIVE  1L

/* special deviceid for core pointer events */
#define GDK_CORE_POINTER 0xfedc


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Type definitions for the basic structures.
 */

typedef guint32		GdkWChar;
typedef uintptr_t	GdkAtom;
typedef struct GdkDrawable		 GdkWindow;
typedef struct GdkDrawable       GdkBitmap;
typedef struct GdkDrawable       GdkDrawable;
typedef struct GdkDrawable       GdkPixmap;
typedef struct GdkDrawable       GdkWindow;
typedef struct GdkGC             GdkGC;

typedef enum
{
  GDK_LSB_FIRST,
  GDK_MSB_FIRST
} GdkByteOrder;

/* Types of modifiers.
 */
typedef enum
{
  GDK_SHIFT_MASK    = 1 << 0,
  GDK_LOCK_MASK	    = 1 << 1,
  GDK_CONTROL_MASK  = 1 << 2,
  GDK_MOD1_MASK	    = 1 << 3,
  GDK_MOD2_MASK	    = 1 << 4,
  GDK_MOD3_MASK	    = 1 << 5,
  GDK_MOD4_MASK	    = 1 << 6,
  GDK_MOD5_MASK	    = 1 << 7,
  GDK_BUTTON1_MASK  = 1 << 8,
  GDK_BUTTON2_MASK  = 1 << 9,
  GDK_BUTTON3_MASK  = 1 << 10,
  GDK_BUTTON4_MASK  = 1 << 11,
  GDK_BUTTON5_MASK  = 1 << 12,
  GDK_RELEASE_MASK  = 1 << 13,
  GDK_MODIFIER_MASK = 0x3fff
} GdkModifierType;

typedef enum
{
  GDK_INPUT_READ       = 1 << 0,
  GDK_INPUT_WRITE      = 1 << 1,
  GDK_INPUT_EXCEPTION  = 1 << 2
} GdkInputCondition;

typedef enum
{
  GDK_OK	  = 0,
  GDK_ERROR	  = -1,
  GDK_ERROR_PARAM = -2,
  GDK_ERROR_FILE  = -3,
  GDK_ERROR_MEM	  = -4
} GdkStatus;

typedef void (*GdkInputFunction) (gpointer	    data,
				  gint		    source,
				  GdkInputCondition condition);

typedef void (*GdkDestroyNotify) (gpointer data);

typedef struct GdkPoint
{
  gint16 x;
  gint16 y;
} GdkPoint;

typedef struct GdkRectangle
{
  gint16 x;
  gint16 y;
  guint16 width;
  guint16 height;
} GdkRectangle;

typedef struct GdkSegment
{
  gint16 x1;
  gint16 y1;
  gint16 x2;
  gint16 y2;
} GdkSegment;

typedef enum {
  GDK_DEBUG_MISC          = 1 << 0,
  GDK_DEBUG_EVENTS        = 1 << 1,
  GDK_DEBUG_DND           = 1 << 2,
  GDK_DEBUG_COLOR_CONTEXT = 1 << 3,
  GDK_DEBUG_XIM           = 1 << 4,
  GDK_DEBUG_COLORMAP	  = 1 << 5,
  GDK_DEBUG_GDKRGB	  = 1 << 6,
  GDK_DEBUG_GC		  = 1 << 7,
  GDK_DEBUG_PIXMAP	  = 1 << 8,
  GDK_DEBUG_IMAGE	  = 1 << 9,
  GDK_DEBUG_INPUT	  = 1 <<10,
  GDK_DEBUG_CURSOR	  = 1 <<11,
  /* Some combinations */
  GDK_DEBUG_EVENTS_AND_COLORMAP = (GDK_DEBUG_EVENTS|GDK_DEBUG_COLORMAP)
} GdkDebugFlag;

#define GDK_DRAWABLE_TYPE(d) (((GdkDrawablePrivate *)d)->window_type)
#define GDK_DRAWABLE_DESTROYED(d) (((GdkDrawablePrivate *)d)->destroyed)

#ifdef _WIN32
#include <WinSock2.h>
#else
typedef int SOCKET;
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDK_TYPES_H__ */
