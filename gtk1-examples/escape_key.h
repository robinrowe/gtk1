// escape_key.h
// Copyright 2021/03/17 Robin.Rowe@CinePaint.org
// License MIT open source

#ifndef escape_key_h
#define escape_key_h

#include <gdk/gdkkeysyms.h>

gboolean escape_key(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{	switch (event->keyval)
	{	case GDK_Escape:
			puts("Goodbye");
			gtk_main_quit();
			break;
		default:
			return FALSE;
	}
	return FALSE;
}

#endif