#! /bin/bash
if [ -e "widget.xml" ];
then
echo "File: widget.xml exists";
echo "Delete it manually and invoke me";
exit 1
fi
FILE_NAME="widget.xml"
echo "<!-- XML version=\"1.2\"-->" > $FILE_NAME
echo "<BOOK NAME=\"GtkBook\">" >> $FILE_NAME
DATE=`date +%d-%h-%Y\ \ %H:%M` #string with [day-month-year + hour:minute]
for i in `cat widgets`;
do
echo "<WIDGET NAME=\"$i\" STATUS=\"incomplete\" DATE=\"$DATE\" AUTHOR=\"NONE\">$i</WIDGET>" >> $FILE_NAME
done
echo "</BOOK>" >> $FILE_NAME