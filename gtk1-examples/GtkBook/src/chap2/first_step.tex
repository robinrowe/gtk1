%<!-- ***************************************************************** -->
\section{Getting Started}
%<!-- ***************************************************************** -->


The first thing to do, of course, is download the GTK source and
install it. You can always get the latest version from ftp.gtk.org in
/pub/gtk. You can also view other sources of GTK information on
url="http://www.gtk.org/" GTK uses GNU autoconf for configuration. 
Once untar'ed, type ./configure --help to see a list of options.

The GTK source distribution also contains the complete source to all
of the examples used in this tutorial, along with Makefiles to aid
compilation.

To begin our introduction to GTK, we'll start with the simplest
program possible. This program will create a 200x200 pixel window and
has no way of exiting except to be killed by using the shell.

\begin{verbatim}
/* example-start base base.c */

#include <gtk/gtk.h>

int main( int   argc,
          char *argv[] )
{
    GtkWidget *window;
    
    gtk_init (&amp;argc, &amp;argv);
    
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_show  (window);
    
    gtk_main ();
    
    return(0);
}
/* example-end */
\end{verbatim}

You can compile the above program with gcc using:
\begin{verbatim}
gcc base.c -o base `gtk-config --cflags --libs`
\end{verbatim}


The meaning of the unusual compilation options is explained below in
"sec\_compiling" name="Compiling Hello World".

All programs will of course include gtk/gtk.h which declares the
variables, functions, structures, etc. that will be used in your GTK
application.

The next line:

\begin{verbatim}
gtk_init (&amp;argc, &amp;argv);
\end{verbatim}

calls the function gtk\_init(gint *argc, gchar ***argv) which will be
called in all GTK applications. This sets up a few things for us such
as the default visual and color map and then proceeds to call
gdk\_init(gint *argc, gchar ***argv). This function initializes the
library for use, sets up default signal handlers, and checks the
arguments passed to your application on the command line, looking for
one of the following:

\itemize
\item <tt/--gtk-module/
\item <tt/--g-fatal-warnings/
\item <tt/--gtk-debug/
\item <tt/--gtk-no-debug/
\item <tt/--gdk-debug/
\item <tt/--gdk-no-debug/
\item <tt/--display/
\item <tt/--sync/
\item <tt/--no-xshm/
\item <tt/--name/
\item <tt/--class/
\end{itemize}

It removes these from the argument list, leaving anything it does not
recognize for your application to parse or ignore. This creates a set
of standard arguments accepted by all GTK applications.

The next two lines of code create and display a window.

\begin{verbatim}
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_show (window);
\end{verbatim}

The \begin{textit}GTK\_WINDOW\_TOPLEVEL} argument specifies that we want the
window to undergo window manager decoration and placement. Rather than
create a window of 0x0 size, a window without children is set to
200x200 by default so you can still manipulate it.

The gtk\_widget\_show() function lets GTK know that we are done setting
the attributes of this widget, and that it can display it.

The last line enters the GTK main processing loop.

\begin{verbatim}
  gtk_main ();
\end{verbatim}

gtk\_main() is another call you will see in every GTK application.
When control reaches this point, GTK will sleep waiting for X events
(such as button or key presses), timeouts, or file IO notifications to
occur. In our simple example, however, events are ignored.

<!-- ----------------------------------------------------------------- -->
<sect1>Hello World in GTK
<p>
Now for a program with a widget (a button).  It's the classic
hello world a la GTK.

\begin{verbatim}
/* example-start helloworld helloworld.c */

#include <gtk/gtk.h>

/* This is a callback function. The data arguments are ignored
 * in this example. More on callbacks below. */
void hello( GtkWidget *widget,
            gpointer   data )
{
    g_print ("Hello World\n");
}

gint delete_event( GtkWidget *widget,
                   GdkEvent  *event,
		   gpointer   data )
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

    g_print ("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return(TRUE);
}

/* Another callback */
void destroy( GtkWidget *widget,
              gpointer   data )
{
    gtk_main_quit();
}

int main( int   argc,
          char *argv[] )
{
    /* GtkWidget is the storage type for widgets */
    GtkWidget *window;
    GtkWidget *button;
    
    /* This is called in all GTK applications. Arguments are parsed
     * from the command line and are returned to the application. */
    gtk_init(&amp;argc, &amp;argv);
    
    /* create a new window */
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    
    /* When the window is given the "delete_event" signal (this is given
     * by the window manager, usually by the "close" option, or on the
     * titlebar), we ask it to call the delete_event () function
     * as defined above. The data passed to the callback
     * function is NULL and is ignored in the callback function. */
    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
			GTK_SIGNAL_FUNC (delete_event), NULL);
    
    /* Here we connect the "destroy" event to a signal handler.  
     * This event occurs when we call gtk_widget_destroy() on the window,
     * or if we return FALSE in the "delete_event" callback. */
    gtk_signal_connect (GTK_OBJECT (window), "destroy",
			GTK_SIGNAL_FUNC (destroy), NULL);
    
    /* Sets the border width of the window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    
    /* Creates a new button with the label "Hello World". */
    button = gtk_button_new_with_label ("Hello World");
    
    /* When the button receives the "clicked" signal, it will call the
     * function hello() passing it NULL as its argument.  The hello()
     * function is defined above. */
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (hello), NULL);
    
    /* This will cause the window to be destroyed by calling
     * gtk_widget_destroy(window) when "clicked".  Again, the destroy
     * signal could come from here, or the window manager. */
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			       GTK_SIGNAL_FUNC (gtk_widget_destroy),
			       GTK_OBJECT (window));
    
    /* This packs the button into the window (a gtk container). */
    gtk_container_add (GTK_CONTAINER (window), button);
    
    /* The final step is to display this newly created widget. */
    gtk_widget_show (button);
    
    /* and the window */
    gtk_widget_show (window);
    
    /* All GTK applications must have a gtk_main(). Control ends here
     * and waits for an event to occur (like a key press or
     * mouse event). */
    gtk_main ();
    
    return(0);
}
/* example-end */
\end{verbatim}

<!-- ----------------------------------------------------------------- -->
<sect1>Compiling Hello World <label id="sec_compiling">
<p>
To compile use:

\begin{verbatim}
gcc -Wall -g helloworld.c -o helloworld `gtk-config --cflags` \
    `gtk-config --libs`
\end{verbatim}

This uses the program <tt/gtk-config/, which comes with GTK. This
program "knows" what compiler switches are needed to compile programs
that use GTK. <tt/gtk-config --cflags/ will output a list of include
directories for the compiler to look in, and <tt>gtk-config --libs</>
will output the list of libraries for the compiler to link with and
the directories to find them in. In the above example they could have
been combined into a single instance, such as
<tt/`gtk-config --cflags --libs`/.

Note that the type of single quote used in the compile command above
is significant.

The libraries that are usually linked in are:
\begin{itemize}
\itemThe GTK library (-lgtk), the widget library, based on top of GDK.
\itemThe GDK library (-lgdk), the Xlib wrapper.
\itemThe gmodule library (-lgmodule), which is used to load run time
extensions.
\itemThe GLib library (-lglib), containing miscellaneous functions;
only g_print() is used in this particular example. GTK is built on top
of glib so you will always require this library. See the section on
<ref id="sec_glib" name="GLib"> for details.
\itemThe Xlib library (-lX11) which is used by GDK.
\itemThe Xext library (-lXext). This contains code for shared memory
pixmaps and other X extensions.
\itemThe math library (-lm). This is used by GTK for various purposes.
\end{itemize}

<!-- ----------------------------------------------------------------- -->
<sect1>Theory of Signals and Callbacks
<p>
Before we look in detail at <em>helloworld</em>, we'll discuss signals
and callbacks. GTK is an event driven toolkit, which means it will
sleep in gtk_main until an event occurs and control is passed to the
appropriate function.

This passing of control is done using the idea of "signals". (Note
that these signals are not the same as the Unix system signals, and
are not implemented using them, although the terminology is almost
identical.) When an event occurs, such as the press of a mouse button,
the appropriate signal will be "emitted" by the widget that was
pressed.  This is how GTK does most of its useful work. There are
signals that all widgets inherit, such as "destroy", and there are
signals that are widget specific, such as "toggled" on a toggle
button.

To make a button perform an action, we set up a signal handler to
catch these signals and call the appropriate function. This is done by
using a function such as:

\begin{verbatim}
gint gtk_signal_connect( GtkObject     *object,
                         gchar         *name,
                         GtkSignalFunc  func,
                         gpointer       func_data );
\end{verbatim}

where the first argument is the widget which will be emitting the
signal, and the second the name of the signal you wish to catch. The
third is the function you wish to be called when it is caught, and the
fourth, the data you wish to have passed to this function.

The function specified in the third argument is called a "callback
function", and should generally be of the form

\begin{verbatim}
void callback_func( GtkWidget *widget,
                    gpointer   callback_data );
\end{verbatim}

where the first argument will be a pointer to the widget that emitted
the signal, and the second a pointer to the data given as the last
argument to the gtk_signal_connect() function as shown above.

Note that the above form for a signal callback function declaration is
only a general guide, as some widget specific signals generate
different calling parameters. For example, the CList "select_row"
signal provides both row and column parameters.

Another call used in the <em>helloworld</em> example, is:

\begin{verbatim}
gint gtk_signal_connect_object( GtkObject     *object,
                                gchar         *name,
                                GtkSignalFunc  func,
                                GtkObject     *slot_object );
\end{verbatim}

gtk_signal_connect_object() is the same as gtk_signal_connect() except
that the callback function only uses one argument, a pointer to a GTK
object. So when using this function to connect signals, the callback
should be of the form

\begin{verbatim}
void callback_func( GtkObject *object );
\end{verbatim}

where the object is usually a widget. We usually don't setup callbacks
for gtk_signal_connect_object however. They are usually used to call a
GTK function that accepts a single widget or object as an argument, as
is the case in our <em>helloworld</em> example.

The purpose of having two functions to connect signals is simply to
allow the callbacks to have a different number of arguments. Many
functions in the GTK library accept only a single GtkWidget pointer as
an argument, so you want to use the gtk_signal_connect_object() for
these, whereas for your functions, you may need to have additional
data supplied to the callbacks.

<!-- ----------------------------------------------------------------- -->
<sect1>Events
<p>
In addition to the signal mechanism described above, there is a set
of <em>events</em> that reflect the X event mechanism. Callbacks may
also be attached to these events. These events are:

\begin{itemize}
\item event
\item button\_press\_event
\item button\_release\_event
\item motion\_notify\_event
\item delete\_event
\item destroy\_event
\item expose\_event
\item key\_press\_event
\item key\_release\_event
\item enter\_notify\_event
\item leave\_notify\_event
\item configure\_event
\item focus\_in\_event
\item focus\_out\_event
\item map\_event
\item unmap\_event
\item property\_notify\_event
\item selection\_clear\_event
\item selection\_request\_event
\item selection\_notify\_event
\item proximity\_in\_event
\item proximity\_out\_event
\item drag\_begin\_event
\item drag\_request\_event
\item drag\_end\_event
\item drop\_enter\_event
\item drop\_leave\_event
\item drop\_data\_available\_event
\item other\_event
\end{itemize}


In order to connect a callback function to one of these events, you
use the function gtk\_signal\_connect, as described above, using one of
the above event names as the <tt/name/ parameter. The callback
function for events has a slightly different form than that for
signals:

\begin{verbatim}
void callback_func( GtkWidget *widget,
                    GdkEvent  *event,
                    gpointer   callback\_data );
\end{verbatim}

GdkEvent is a C <tt/union/ structure whose type will depend upon which
of the above events has occurred. In order for us to tell which event
has been issued each of the possible alternatives has a <tt/type/
parameter which reflects the event being issued. The other components
of the event structure will depend upon the type of the
event. Possible values for the type are:

\begin{verbatim}
  GDK\_NOTHING
  GDK\_DELETE
  GDK\_DESTROY
  GDK\_EXPOSE
  GDK\_MOTION\_NOTIFY
  GDK\_BUTTON\_PRESS
  GDK\_2BUTTON\_PRESS
  GDK\_3BUTTON\_PRESS
  GDK\_BUTTON\_RELEASE
  GDK\_KEY\_PRESS
  GDK\_KEY\_RELEASE
  GDK\_ENTER\_NOTIFY
  GDK\_LEAVE\_NOTIFY
  GDK\_FOCUS\_CHANGE
  GDK\_CONFIGURE
  GDK\_MAP
  GDK\_UNMAP
  GDK\_PROPERTY\_NOTIFY
  GDK\_SELECTION\_CLEAR
  GDK\_SELECTION\_REQUEST
  GDK\_SELECTION\_NOTIFY
  GDK\_PROXIMITY\_IN
  GDK\_PROXIMITY\_OUT
  GDK\_DRAG\_BEGIN
  GDK\_DRAG\_REQUEST
  GDK\_DROP\_ENTER
  GDK\_DROP\_LEAVE
  GDK\_DROP\_DATA\_AVAIL
  GDK\_CLIENT\_EVENT
  GDK\_VISIBILITY\_NOTIFY
  GDK\_NO\_EXPOSE
  GDK\_OTHER\_EVENT	/* Deprecated, use filters instead */
\end{verbatim}

So, to connect a callback function to one of these events we would use
something like:

\begin{verbatim}
gtk_signal_connect( GTK_OBJECT(button), "button_press_event",
                    GTK_SIGNAL_FUNC(button_press_callback), 
		    NULL);
\end{verbatim}

This assumes that <tt/button/ is a Button widget. Now, when the
mouse is over the button and a mouse button is pressed, the function
<tt/button_press_callback/ will be called. This function may be
declared as:

\begin{verbatim}
static gint button_press_callback( GtkWidget      *widget, 
                                   GdkEventButton *event,
                                   gpointer        data );
\end{verbatim}

Note that we can declare the second argument as type
<tt/GdkEventButton/ as we know what type of event will occur for this
function to be called.

The value returned from this function indicates whether the event
should be propagated further by the GTK event handling
mechanism. Returning TRUE indicates that the event has been handled,
and that it should not propagate further. Returning FALSE continues
the normal event handling.  See the section on
<ref id="sec_Adv_Events_and_Signals"
name="Advanced Event and Signal Handling"> for more details on this
propagation process.

For details on the GdkEvent data types, see the appendix entitled
<ref id="sec_GDK_Event_Types" name="GDK Event Types">.

<!-- ----------------------------------------------------------------- -->
<sect1>Stepping Through Hello World
<p>
Now that we know the theory behind this, let's clarify by walking
through the example <em>helloworld</em> program.

Here is the callback function that will be called when the button is
"clicked". We ignore both the widget and the data in this example, but
it is not hard to do things with them. The next example will use the
data argument to tell us which button was pressed.

\begin{verbatim}
void hello( GtkWidget *widget,
            gpointer   data )
{
    g_print ("Hello World\n");
}
\end{verbatim}

The next callback is a bit special. The "delete_event" occurs when the
window manager sends this event to the application. We have a choice
here as to what to do about these events. We can ignore them, make
some sort of response, or simply quit the application.

The value you return in this callback lets GTK know what action to
take.  By returning TRUE, we let it know that we don't want to have
the "destroy" signal emitted, keeping our application running. By
returning FALSE, we ask that "destroy" be emitted, which in turn will
call our "destroy" signal handler.

\begin{verbatim}
gint delete_event( GtkWidget *widget,
                   GdkEvent  *event,
		   gpointer   data )
{
    g_print ("delete event occurred\n");

    return (TRUE); 
}
\end{verbatim}

Here is another callback function which causes the program to quit by
calling gtk_main_quit(). This function tells GTK that it is to exit
from gtk_main when control is returned to it.

\begin{verbatim}
void destroy( GtkWidget *widget,
              gpointer   data )
{
    gtk_main_quit ();
}
\end{verbatim}

I assume you know about the main() function... yes, as with other
applications, all GTK applications will also have one of these.

\begin{verbatim}
int main( int   argc,
          char *argv[] )
{
\end{verbatim}

This next part declares pointers to a structure of type
GtkWidget. These are used below to create a window and a button.

\begin{verbatim}
    GtkWidget *window;
    GtkWidget *button;
\end{verbatim}

Here is our gtk_init again. As before, this initializes the toolkit,
and parses the arguments found on the command line. Any argument it
recognizes from the command line, it removes from the list, and
modifies argc and argv to make it look like they never existed,
allowing your application to parse the remaining arguments.

\begin{verbatim}
    gtk_init (&amp;argc, &amp;argv);
\end{verbatim}

Create a new window. This is fairly straightforward. Memory is
allocated for the GtkWidget *window structure so it now points to a
valid structure. It sets up a new window, but it is not displayed
until we call gtk_widget_show(window) near the end of our program.

\begin{verbatim}
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
\end{verbatim}

Here are two examples of connecting a signal handler to an object, in
this case, the window. Here, the "delete_event" and "destroy" signals
are caught. The first is emitted when we use the window manager to
kill the window, or when we use the gtk_widget_destroy() call passing
in the window widget as the object to destroy. The second is emitted
when, in the "delete\_event" handler, we return FALSE.
 
The GTK\_OBJECT and GTK\_SIGNAL\_FUNC are macros that perform
type casting and checking for us, as well as aid the readability of
the code.

\begin{verbatim}
    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                        GTK_SIGNAL_FUNC (delete_event), NULL);
    gtk_signal_connect (GTK_OBJECT (window), "destroy",
                        GTK_SIGNAL_FUNC (destroy), NULL);
\end{verbatim}

This next function is used to set an attribute of a container object.
This just sets the window so it has a blank area along the inside of
it 10 pixels wide where no widgets will go. There are other similar
functions which we will look at in the section on
<ref id="sec_setting_widget_attributes" name="Setting Widget Attributes">

And again, <tt/GTK_CONTAINER/ is a macro to perform type casting.

\begin{verbatim}
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
\end{verbatim}

This call creates a new button. It allocates space for a new GtkWidget
structure in memory, initializes it, and makes the button pointer
point to it. It will have the label "Hello World" on it when
displayed.

\begin{verbatim}
    button = gtk_button_new_with_label ("Hello World");
\end{verbatim}

Here, we take this button, and make it do something useful. We attach
a signal handler to it so when it emits the "clicked" signal, our
hello() function is called. The data is ignored, so we simply pass in
NULL to the hello() callback function. Obviously, the "clicked" signal
is emitted when we click the button with our mouse pointer.

\begin{verbatim}
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
                        GTK_SIGNAL_FUNC (hello), NULL);
\end{verbatim}

We are also going to use this button to exit our program. This will
illustrate how the "destroy" signal may come from either the window
manager, or our program. When the button is "clicked", same as above,
it calls the first hello() callback function, and then this one in the
order they are set up. You may have as many callback functions as you
need, and all will be executed in the order you connected
them. Because the gtk_widget_destroy() function accepts only a
GtkWidget *widget as an argument, we use the
gtk_signal_connect_object() function here instead of straight
gtk_signal_connect().

\begin{verbatim}
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                               GTK_SIGNAL_FUNC (gtk_widget_destroy),
                               GTK_OBJECT (window));
\end{verbatim}

This is a packing call, which will be explained in depth later on in
<ref id="sec_packing_widgets" name="Packing Widgets">. But it is
fairly easy to understand. It simply tells GTK that the button is to
be placed in the window where it will be displayed. Note that a GTK
container can only contain one widget. There are other widgets, that
are described later, which are designed to layout multiple widgets in
various ways.
 
\begin{verbatim}
    gtk_container_add (GTK_CONTAINER (window), button);
\end{verbatim}

Now we have everything set up the way we want it to be. With all the
signal handlers in place, and the button placed in the window where it
should be, we ask GTK to "show" the widgets on the screen. The window
widget is shown last so the whole window will pop up at once rather
than seeing the window pop up, and then the button form inside of
it. Although with such a simple example, you'd never notice.

\begin{verbatim}
    gtk_widget_show (button);

    gtk_widget_show (window);
\end{verbatim}

And of course, we call gtk_main() which waits for events to come from
the X server and will call on the widgets to emit signals when these
events come.

\begin{verbatim}
    gtk_main ();
\end{verbatim}

And the final return. Control returns here after gtk_quit() is called.

\begin{verbatim}
    return (0);
\end{verbatim}

Now, when we click the mouse button on a GTK button, the widget emits
a "clicked" signal. In order for us to use this information, our
program sets up a signal handler to catch that signal, which
dispatches the function of our choice. In our example, when the button
we created is "clicked", the hello() function is called with a NULL
argument, and then the next handler for this signal is called. This
calls the gtk\_widget\_destroy() function, passing it the window widget
as its argument, destroying the window widget. This causes the window
to emit the "destroy" signal, which is caught, and calls our destroy()
callback function, which simply exits GTK.

Another course of events is to use the window manager to kill the
window, which will cause the "delete\_event" to be emitted. This will
call our "delete_event" handler. If we return TRUE here, the window
will be left as is and nothing will happen. Returning FALSE will cause
GTK to emit the "destroy" signal which of course calls the "destroy"
callback, exiting GTK.
