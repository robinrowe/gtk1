#! /bin/bash

#configuration files
FILELIST="FileList";
GENERIC_HEADER="Header";
GENERIC_BODY="Body";
GENERIC_ERROR="Error";#informational


#program config
GENERIC_VERSION="0.1"
GENERIC_STYLESHEET="gtkdoc.css"

#progam xml -> html generator
GENERIC_TRANSLATEPROGAM="cat" #later the files are to be xmlised and we'll use a xml->html converter here.

#node configurations
NODE_HOME="index.html";
NODE_NEXT=""
NODE_PREV=""
NODE_UP=""



#html header
HTML_START_HEADER="<HTML><HEAD>"
HTML_END_HEADER="</HEAD>"

HTML_START_BODY="<BODY><HR />"
HTML_END_BODY="<HR /></BODY></HTML>"

HTML_NAVIGATE="<CENTER><A HREF=\"./\$NODE_PREV\"> Prev </A>&nbsp;<A HREF=\"./$NODE_HOME\"> HOME </A>&nbsp;<A HREF=\"./$NODE_UP\"> Prev </A></CENTER>"

#
# remove the older files and 
# create newer documents
#
function init()
{

    if [ -e $FILELIST ];
	then
	echo "$FILELIST found"
    else
	echo "FATAL ERROR: Cannot find $FILELIST. Exiting"
	exit -1;
    fi
    echo "Starting Build: `date`" >> $GENERIC_ERROR
}

function clean()
{
    for i in `cat $FILELIST`;
    do
      if [ -e $i.html ];
	  then
	  /bin/rm $i.html
	  touch $i.html
	  else
	  touch $i.html
      fi
    done
}


#mundane cloning.
function generate_header()
{
    FILE=$1;
    echo $HTML_START_HEADER >> $FILE
    cat $GENERIC_HEADER >> $FILE
    echo "<STYLE TYPE=\"text/css\" NAME=\"$GENERIC_STYLESHEET\">" >> $FILE
    echo '<!--Last Modified:' `date` $FILE "-->"  >> $FILE
    echo '<!--(C) $GENERIC_YEAR GtkBook $GENERIC_VERSION  -->' >> $FILE
    echo $HTML_END_HEADER >> $FILE
}


function generate_body()
{
    FILE=$1;
    SOURCE=$2
    echo $HTML_START_BODY >> $FILE
    cat $GENERIC_BODY >> $FILE
    echo "<CENTER><A HREF=\"./$NODE_PREV\"> Prev </A>&nbsp;<A HREF=\"./$NODE_HOME\"> HOME </A>&nbsp;<A HREF=\"./$NODE_UP\"> Up </A></CENTER>" >> $FILE


    #real nuts go in here.
    $GENERIC_TRANSLATEPROGAM  $SOURCE >> $FILE

    echo "<CENTER><A HREF=\"./$NODE_PREV\"> Prev </A>&nbsp;<A HREF=\"./$NODE_HOME\"> HOME </A>&nbsp;<A HREF=\"./$NODE_UP\"> Up </A></CENTER>" >> $FILE
    echo $HTML_END_BODY >> $FILE
}

#
#generate a HTML book in the form:
#<prev>  <home>  //cant find out how! <next>
#

function generate()
{


    NODE_UP="Contents.html";
    NODE_PREV="Contents.html";

    for i in `cat $FILELIST`;
    do
      if [ -e $i.html ];
	  then
	  generate_header $i.html
	  generate_body $i.html $i
	  echo "Generated file" $i.html
      else
	  echo "Cannot Find Document!",$i.html
      fi
      NODE_PREV=$i
    done
    
    /bin/rm $NODE_HOME
    #generate the index file
    generate_header $NODE_HOME
    cat $GENERIC_BODY >> $FILE
    echo "<TABLE ALIGN=\"center\" WIDTH=\"100%\">" >> $FILE    
    echo "<TBODY>" >> $FILE
    for i in `cat $FILELIST`;
    do
      echo "<TR><TD><A HREF=\"$i.html\">$i</A></TD></TR>" >> $NODE_HOME
    done
    echo "</TBODY>" >> $FILE
    echo "</TABLE>" >> $FILE
    
}


# int main(){
# start here.
clean
generate