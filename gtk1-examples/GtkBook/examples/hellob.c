#include<gtk/gtk.h>

gboolean print_hello(GtkWidget *w, gpointer data)
{
  g_message("Hello World\n");
  return TRUE;
}

int main(int argc,char *argv[])
{
  GtkWidget *win;
  GtkWidget *btn1,*btn2;
  GtkWidget *box;
  gtk_init(&argc,&argv);

  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(TRUE,TRUE);

  btn1=gtk_button_new_with_label("Hello World");
  gtk_box_pack_start_defaults(GTK_BOX(box),btn1);

  btn2=gtk_button_new_with_label("Quit");
  gtk_box_pack_start_defaults(GTK_BOX(box),btn2);
  
  gtk_window_set_title(GTK_WINDOW(win),"  Hello World  ");
  gtk_window_set_position(GTK_WINDOW(win),GTK_WIN_POS_MOUSE);
  gtk_container_add(GTK_CONTAINER(win),box);

  g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_signal_connect(G_OBJECT(btn1),"clicked",G_CALLBACK(print_hello),NULL);
  g_signal_connect(G_OBJECT(btn2),"clicked",G_CALLBACK(gtk_main_quit),NULL);


  gtk_widget_show_all(win);

  gtk_main();
  return 0;
}
//gcc -Wall hellob.c -o bhello `pkg-config gtk+-2.0 --cflags --libs`
