#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

static GList *str;

gboolean print_string(GtkList *l,GtkWidget *chd)
{
  GtkLabel *child;

  child=GTK_BIN(chd)->child;
  
  if(GTK_IS_LABEL(child))
    {
      g_printf("%s\n",gtk_label_get_text(GTK_LABEL(child)));
    }
  else
    g_printf("Not a label\n");
  return FALSE;
}


gboolean add_string(GtkWidget *w, gpointer data)
{
  gchar *stri;
  GtkCombo *b=(GtkCombo *)data;

  stri=gtk_entry_get_text(GTK_ENTRY(b->entry));
  
  g_list_append(str,g_strdup(stri));    
  gtk_combo_set_popdown_strings(GTK_COMBO(b),str);

  return TRUE;
}

GtkWidget* combo_demo(void)
{
  GtkWidget *b,*cb,*add;
  static gchar *names[]={"GNU/Linux","GNU Hurd","GNU Darwin","GNU Mach",NULL};
  gchar **ptr;

  ptr=NULL;
  b=gtk_hbox_new(FALSE,FALSE);  

  
  //populate the list.
  ptr=names;
  str=g_list_alloc();
  while(*ptr!=NULL)
    {
      g_list_append(str,g_strdup(*ptr));
      ptr++;
    }

  cb=gtk_combo_new();
  gtk_combo_set_value_in_list (GTK_COMBO(cb),TRUE,FALSE);
  gtk_combo_set_popdown_strings(GTK_COMBO(cb),str);

  add=gtk_button_new_with_mnemonic("_Add");
  gtk_box_pack_start_defaults(GTK_BOX(b),cb);
  gtk_box_pack_start_defaults(GTK_BOX(b),add);

  gtk_combo_set_use_arrows_always (GTK_COMBO(cb),
				   FALSE);


  gtk_signal_connect(GTK_OBJECT(add),"clicked",GTK_SIGNAL_FUNC(add_string),cb);
  g_signal_connect(G_OBJECT(GTK_COMBO(cb)->list),
		   "select_child",
		   G_CALLBACK(print_string),cb);
  return b;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;
  //  int i=0;

  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);


  gtk_container_add(GTK_CONTAINER(w),
		    combo_demo());

  gtk_signal_connect(GTK_OBJECT(w),
		     "destroy",
		     GTK_SIGNAL_FUNC(gtk_main_quit),
		     NULL);

  gtk_window_set_title(GTK_WINDOW(w),
		       "Combo Button[s]");

  gtk_window_set_default_size(GTK_WINDOW(w),
			      200,
			      40);
  gtk_widget_show_all(w);  
  gtk_main();
  return 0;
}




//gcc -Wall combo_ex.c `pkg-config libgnomeui-2.0 --cflags --libs`
//export PKG_CONFIG_PATH="/usr/lib/pkgconfig/"


gboolean
combo_toggled(GtkWidget *w,
	      gpointer data)
{
  GtkRadioButton *rb;
  rb=(GtkRadioButton *)w;
  
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb))==TRUE)
    {
      g_printf("Active: %s\n",gtk_button_get_label(GTK_BUTTON(rb)));
    }
  return TRUE;
}


/*
  Works only with version GTK 2.4+
*/

GtkWidget* combo_demo_2_3(void)
{
  GtkTreeStore *ts;
  GtkTreeIter ti;
  GtkWidget *b,*cb;
  static gchar *names[]={"GNU/Linux","GNU Hurd","GNU Darwin","GNU Mach",NULL};
  gchar **ptr;

  ptr=NULL;
  b=gtk_hbox_new(FALSE,FALSE);  

  ts=gtk_tree_store_new(1,
			G_TYPE_STRING,
			-1);
  
  //populate the list.
  ptr=names;
  while(*ptr!=NULL)
    {

      gtk_tree_store_append(GTK_TREE_STORE(ts),
			    &ti,
			    NULL);      //a list!
      ptr++;			       
    }

  // cb=gtk_combo_box_new_with_model(GTK_TREE_MODEL(ts));
  
  gtk_box_pack_start_defaults(GTK_BOX(b),cb);
			      
  return b;
}
