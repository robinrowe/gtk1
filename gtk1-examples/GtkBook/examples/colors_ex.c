#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

GtkWidget * make_color(void)
{
  GtkWidget *w,*l;
  GtkTooltips *tt;
  GdkColor clr;
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  l=gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(l),"Black or White?");

  gtk_container_add(GTK_CONTAINER(w),l);
  

  gdk_color_parse("black",&clr);
  gtk_widget_modify_bg(w,GTK_STATE_NORMAL,&clr);

  gtk_widget_set_app_paintable(l,TRUE);
  gdk_color_parse("white",&clr);
  gtk_widget_modify_fg(l,GTK_STATE_NORMAL,&clr);

  gdk_color_parse("blue",&clr);
  gtk_widget_modify_base(l,GTK_STATE_NORMAL,&clr);
  

  tt=gtk_tooltips_new();
  gtk_tooltips_set_tip(tt,l,"Click to Exit",NULL);

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(w),"key_press_event",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Great Tips!");
  gtk_window_resize(GTK_WINDOW(w),200,100);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);
  w=make_color();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}
//gcc -Wall colors_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o colors
