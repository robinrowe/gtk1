#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

GtkWidget * make_font_preview(void)
{
  GtkWidget *w,*box,*l;
  PangoFontDescription *font_desc;
  static gchar *font_str="Kadambi 24"; //some font names.
  static gchar *font_str2="Densmore 13";
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);

  font_desc=pango_font_description_from_string(font_str);


  gtk_container_add(GTK_CONTAINER(w),box);
  
  l=gtk_label_new("Hello World");
  gtk_box_pack_start_defaults(GTK_BOX(box),l);
  gtk_widget_modify_font(l,font_desc);
  pango_font_description_free(font_desc);

  font_desc=pango_font_description_from_string(font_str2);
  l=gtk_label_new("Welcome Heroes!");
  gtk_box_pack_start_defaults(GTK_BOX(box),l);
  gtk_widget_modify_font(l,font_desc);
  pango_font_description_free(font_desc);


  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Great FOnts!");
  gtk_window_set_default_size(GTK_WINDOW(w),200,100);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);
  w=make_font_preview();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}




//gcc -Wall x.c `pkg-config libgnomeui-2.0 --cflags --libs`

/*
  z=gtk_dialog_new();
  gtk_window_set_modal(GTK_WINDOW(z),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(z),TRUE);

  b=gtk_button_new_from_stock(GTK_MUTHU_DUCK);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),b);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_hseparator_new());
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_label_new(s));
 
  gtk_dialog_set_response_sensitive(GTK_DIALOG(z),
  GTK_RESPONSE_DELETE_EVENT,
  TRUE);
  gtk_widget_show_all(z);
  gtk_dialog_run(GTK_DIALOG(z));
  gtk_widget_destroy(z);
  GtkWidget *z,*b;
*/
