#include<glib.h>

int main(int argc,char *argv[])
{
  GIOChannel *reader,*writer;
  char *data;
  int length=0,index=0,flag=0,rval=0;

  g_return_if_fail(argc>1);

  /* No need type system init */
  reader=g_io_channel_new_file(argv[1],"r",NULL);
  writer=g_io_channel_new_file("hello","w",NULL);
  g_return_val_if_fail(reader!=NULL || writer!=NULL,-2);

  if(g_io_channel_read_to_end(reader,&data,&length,NULL)==G_IO_STATUS_ERROR ||
     data==NULL){
    g_message("Cannot Read from Channel!");
    return -1;
  }
  
  index=0;
  flag=1;
  rval=0;
  while(index < length){    
    if(flag) {
      g_printf("%c",data[index]);
      g_io_channel_write(writer,(char *)data+index,1,&rval);
    }
    index++;

    if(flag==1 && data[index]==':'){      
      flag=0;
    }

    if(flag==0 && data[index]=='\n')
      flag=1;
  }
  g_io_channel_close(reader);
  g_io_channel_close(writer);
  g_free(data);
  return -1;
}
/* gcc testchnl.c `pkg-config glib-2.0 --cflags --libs` */
