#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<string.h>
#include<math.h>

/*
  Gaussian Curve:
  f(z) = exp(-(x^2 + y^2)/2)/(sqrt(2*M_PI*1)
*/



gboolean paint_function(GtkCurve *im,gdouble valx,gdouble valy)
{
  
  static float gauss_vec[1000]={};
  double val=0,r1,r2;
  int i=0;

  val=-valx;
  while(val<valx)
    {
      r1=(val*val + valy*valy);
      r2=exp(-r1);
      r2=r2*1000;

      gauss_vec[i++]=(float)r2;
      val+=valx/500.00;
    }

  gtk_curve_set_vector(im,1000,gauss_vec);
  
  gtk_widget_queue_draw(GTK_WIDGET(im));
  return TRUE;
}


gboolean yadjustment_cb(GtkAdjustment *w,gpointer data)
{
  GtkCurve *im;
  gdouble val;
  gdouble *valx,*valy;

  im=(GtkCurve *)data;
  val=gtk_adjustment_get_value(w);

  
  valx=(gdouble *)g_object_get_data(G_OBJECT(im),"x");
  valy=(gdouble *)g_object_get_data(G_OBJECT(im),"y");


  //  g_printf("Valy=%g\n",val);
  *valy=val;

  paint_function(im,*valx,*valy);
  return TRUE;
}

gboolean xadjustment_cb(GtkAdjustment *w,gpointer data)
{
  GtkCurve *im;
  gdouble val;
  gdouble *valx,*valy;

  im=(GtkCurve *)data;
  val=gtk_adjustment_get_value(w);
  
  valx=(gdouble *)g_object_get_data(G_OBJECT(im),"x");
  valy=(gdouble *)g_object_get_data(G_OBJECT(im),"y");
  
  //  g_printf("Valx=%g\n",val);
  *valx=val;

  paint_function(im,*valx,*valy);
  return TRUE;
}


int main(int argc,char *argv[])
{
  GtkWidget *w,*box,*r;
  GtkWidget *im;
  GtkAdjustment *xaj,*yaj;
  static gdouble valx,valy;

  gtk_init(&argc,&argv);  
    
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  r=gtk_hruler_new();
  gtk_ruler_set_range(GTK_RULER(r),0,360,0,10);

  im=gtk_curve_new();
  gtk_widget_show(im);
  gtk_curve_set_range(GTK_CURVE(im),0,500,0,1000);
  gtk_curve_set_curve_type(GTK_CURVE(im),GTK_CURVE_TYPE_SPLINE);

  box=gtk_vbox_new(FALSE,FALSE);

  gtk_box_pack_start_defaults(GTK_BOX(box),im);
  gtk_box_pack_start_defaults(GTK_BOX(box),r);

  xaj=GTK_ADJUSTMENT(gtk_adjustment_new(0,0,10,0.1,1,0.01));
  yaj=GTK_ADJUSTMENT(gtk_adjustment_new(0,0,10,0.1,1,0.01));


  //attach property to the Image widgets.
  g_object_set_data(G_OBJECT(im),"x",&valx);
  g_object_set_data(G_OBJECT(im),"y",&valy);


  r=gtk_hscrollbar_new(xaj);
  gtk_box_pack_start_defaults(GTK_BOX(box),r);
  gtk_signal_connect(GTK_OBJECT(xaj),"value_changed",GTK_SIGNAL_FUNC(xadjustment_cb),im);//pass the image

  r=gtk_hscrollbar_new(yaj);
  gtk_box_pack_start_defaults(GTK_BOX(box),r);
  gtk_signal_connect(GTK_OBJECT(yaj),"value_changed",GTK_SIGNAL_FUNC(yadjustment_cb),im);//pass the image

  gtk_container_add(GTK_CONTAINER(w),box);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);


  gtk_window_set_title(GTK_WINDOW(w),"gAuSsIaN CuRvE");
  gtk_window_set_default_size(GTK_WINDOW(w),200,40);
  gtk_widget_show_all(w);
  //  gtk_adjustment_set_value(aj,50.00);
  gtk_main();
  return 0;
}
//gcc -Wall gaussian_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o gaussian_ex -lm

#ifdef GROW_OLD
void gaussian_picture(GdkPixbuf *pb,double valx, double valy) //a theta degree degree turn
{
  int ht,wt;

  double i=0;
  int j=0;
  int l=0,m=0;

  int bpp=3;
  int rowstride=0;
  double r1,r2;
  int prevx,prevy;
    

  gchar *pixel;

  ht=gdk_pixbuf_get_height(pb);
  wt=gdk_pixbuf_get_width(pb);
  bpp=3;

  pixel=gdk_pixbuf_get_pixels(pb);

  rowstride=bpp*wt;

  gdk_pixbuf_fill(pb,0x00); //fillup with black.


  m=(int)valy;
  i=-valx;

  while(i<valx)
    {
      r1=(i*i + valy*valy);
      r2=exp(-r1);
      r2=r2*wt;
      //g_printf("j=%g\n",r2);
      j=(int)r2;
     
      if(j<0)
	j=-j;

      l=i+(int)(valx);
      l=(l/(2.0*valx))*rowstride;
       
      //make that pixel white
      j=ht-j;
      pixel[j*rowstride + l + 0]=0xff;
      pixel[j*rowstride + l + 1]=0xff;
      pixel[j*rowstride + l + 2]=0xff;
      i+=0.1;

      gdk_draw_line(

      prevx=j;
      prevy=l;
    }
  

  return;
}
#endif
