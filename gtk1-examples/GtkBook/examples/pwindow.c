#include <gtk/gtk.h>

int
main (int argc, char **argv)
{
  GtkWidget     *win, *box,*tb;
  GtkIconSize sz;
  gtk_init( &argc, &argv );

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(1,1);
  
  tb=gtk_toolbar_new();
  sz=gtk_icon_size_register("muthus-new-size",50,50);
  //  gtk_toolbar_set_show_arrow(GTK_TOOLBAR(tb),TRUE);
  gtk_toolbar_set_tooltips(GTK_TOOLBAR(tb),TRUE);
  //gtk_toolbar_set_style(GTK_TOOLBAR(tb),  GTK_TOOLBAR_SPACE_LINE);
  gtk_toolbar_set_orientation(GTK_TOOLBAR(tb),  GTK_ORIENTATION_HORIZONTAL);
  gtk_toolbar_set_icon_size(GTK_TOOLBAR(tb),sz);
  {
    int i=-1;
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_NEW,"Create New File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_OPEN,"Open a File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_SAVE,"Save File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_EXECUTE,"run File",NULL, NULL, NULL,i);
  }

  //  gtk_box_pack_end_defaults(GTK_BOX(box),tb);
  gtk_box_pack_end_defaults(GTK_BOX(box),gtk_label_new("Last Line"));
			     
  gtk_container_add (GTK_CONTAINER (win), tb);
  g_signal_connect (GTK_OBJECT(win), "destroy",G_CALLBACK (gtk_main_quit), NULL);

  gtk_container_set_border_width(GTK_CONTAINER(win),10);
  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}
//gcc -Wall pwindow.c -o pwin `pkg-config gtk+-2.0 --cflags --libs`
/*
  g_signal_connect (GTK_OBJECT(win), "destroy", 
		    G_CALLBACK (gtk_main_quit), NULL);

  but = gtk_button_new_with_label ("None");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_NONE);
  gtk_button_set_use_underline(GTK_BUTTON(but),TRUE);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);

  but = gtk_button_new_with_label ("Half");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_HALF);
  gtk_button_set_use_underline(GTK_BUTTON(but),TRUE);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);

  but = gtk_button_new_with_label ("Normal");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_NORMAL);
  gtk_button_set_use_underline(GTK_BUTTON(but),TRUE);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);


  but = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(but),"<span foreground=\"red\"><i>Italics</i> <b>Bold</b><u> Underline</u></span><span variant=\"smallcaps\">I Love India</span>");
  gtk_box_pack_start_defaults(GTK_BOX(box),but);

*/
