#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

#define PT 11

void expose_event(GtkWidget *w)
{

  GdkPoint pts[PT]={{-10,1},{-4,-2},{-3,3},
		    {-2,4},{-1,5},{0,6},
		    {2,4},{1,5},{5,1},
		    {4,2},{3,3}};

  GdkColor c;
  int i=0;
  int Xmin,Xmax;
  int Ymin,Ymax;
  int x,y;
  GdkGC *pen;
  int wt,ht;
  
  Xmin=-10;
  Xmax=5;
  
  Ymin=1;
  Ymax=6;

  x=0,y=0;



  pen=gdk_gc_new(w->window);
  gdk_drawable_get_size(w->window,
			&wt,&ht);
  g_printf("ht,wt: (%d, %d)\n",ht,wt);

  Xmax=Xmax+((Xmin < 0)? -Xmin: Xmin);
  Ymax=Ymax+((Ymin < 0)? -Ymin: Ymin);


  gdk_color_parse("HotPink",&c);
  gdk_gc_set_background(pen,&c);

  for(i=0;i<30;i++)
    {
      gdk_draw_line(w->window,
		    pen,
		    (i*wt)/30,0,
		    (i*wt)/30,ht
		    );

      gdk_draw_line(w->window,
		    pen,
		    0,(i*ht)/30,
		    wt,(i*ht)/30
		    );
    }



  gdk_color_parse("yellow",&c);
  gdk_gc_set_foreground(pen,&c);
  gdk_gc_set_line_attributes(pen,
			     10,GDK_LINE_DOUBLE_DASH,
			     GDK_CAP_BUTT,
			     GDK_JOIN_BEVEL);

  for(i=0;i<PT;i++)
     {
      x=pts[i].x;
      y=pts[i].y;
      
      g_printf("orig: (%d, %d)\t",x,y);
      //SHIFT
      x=x+((Xmin < 0)? -Xmin: Xmin);
      y=y+((Ymin < 0)? -Ymin: Ymin);

      g_printf("shift: (%d, %d)\t",x,y);
      //SCALE                 --sometimes even DSP helps!
      x=x*((double )wt/Xmax);
      y=y*((double )ht/Ymax);

      g_printf("scale: (%d, %d)\n",x,ht-y);
      gdk_draw_line(w->window,
		    pen,
		    x,ht-((Ymin < 0)? -Ymin: Ymin)*ht/(double)Ymax,
		    x,ht-y
		    );

      pts[i].x=x;
      pts[i].y=ht-y;
    }
  

   gdk_color_parse("green",&c);
   gdk_gc_set_foreground(pen,&c);

  gdk_gc_set_line_attributes(pen,
			     1,GDK_LINE_DOUBLE_DASH,
			     GDK_CAP_BUTT,
			     GDK_JOIN_BEVEL);
  gdk_draw_line(w->window,
		pen,
		0,ht-((Ymin < 0)? -Ymin: Ymin)*ht/(double)Ymax,
		wt,ht-((Ymin < 0)? -Ymin: Ymin)*ht/(double)Ymax);

  gdk_draw_line(w->window,
		pen,
		wt/2,0,
		wt/2,ht);




}

GtkWidget* graph_widget(void)
{
  GtkWidget *w;  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_size_request(w,200,200);
  g_signal_connect(G_OBJECT(w),"expose_event",G_CALLBACK(expose_event),
		   NULL);
  gtk_widget_show(w);
  return w;
}

int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=graph_widget();

  gtk_main();
  return 0;
}

//export PKG_CONFIG_PATH=/usr/lib/pkgconfig
//gcc -Wall graph.c `pkg-config libgnomeui-2.0 --cflags --libs` -o graph
