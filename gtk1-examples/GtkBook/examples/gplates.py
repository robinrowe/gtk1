#! /usr/bin/python
# A simple program to generate GObject templates.

def boiler_plate(Name,Parent):
    if( not (type(Name) is types.StringType) and  not( type(Parent)is types.StringType)):
        print "pass string types only";
        return;

    NameClass=Name+"Class";
    ParentClass=Parent+"Class";

    #morph Name from Mixed caps to _ separated.
    x=[]
    last=0
    for i in range(0,len(Name)):
        if(Name[i].isupper()):
            if(last==i):
                continue
            x.append(Name[last:i].lower())
            last=i
    if(i!=last):
        x.append(Name[last:].lower())
    KName="_".join(x)
    del x,last,i
    
    KIs=KName.split("_");
    KIs=KIs[0]+"_IS_"+"_".join(KIs[1:])
    
    KType=KName.split("_");
    KType=KType[0]+"_TYPE_"+"_".join(KType[1:])
    KType=KType.upper()
    
    #generate Macros
    print "#define "+KType+"  ("+KName.lower()+"_get_type())"
    print "#define "+KName.upper()+"(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),"+KType+","+Name+"))"
    print "#define "+KName.upper()+"_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass),"+KType+","+NameClass+"))"
    print "#define "+KIs.upper()+"(obj)"+"   (G_TYPE_CHECK_INSTANCE_TYPE((obj),"+KType+"))"
    print "#define "+KIs.upper()+"_CLASS(klass)"+"   (G_TYPE_CHECK_CLASS_TYPE((klass),"+KType+"))"
    print "#define "+KName.upper()+"_GET_CLASS(obj)"+"   (G_TYPE_INSTANCE_GET_CLASS((obj),"+KType+","+NameClass+"))"


    #type definitions
    print "typedef \t struct _"+Name+"  "+Name+";"
    print "typedef \t struct _"+NameClass+" "+NameClass+";"
    
    #generate the GObject structure.
    print "struct _"+Name+" {"
    print "\t"+Parent+"  _"+Parent+";"
    print "\t /* Fill Your Member Data  Here*/"
    print "};"

    #generate the GObjectClass structure.
    print "struct _"+NameClass+"{"
    print "\t"+ParentClass+" klass;"
    print "\t /* your custom class functions  */"
    print "};"
    
    #generate class_init, class_finalize functions.
    print "void "+KName+"_class_init("+NameClass+"* klass,gpointer data)"
    print "{"
    print "/* Do Class Init Stuff Here: set member fcns etal*/ "
    print "}"

    print "void "+KName+"_class_finalize("+NameClass+"* klass,gpointer data)"
    print "{"
    print "/* delete class specific data which you may have dynamically allocated*/"
    print ""
    print "}"

    #Generate  type_init functions
    print "int "+KName+"_get_type(){"
    print "static type_id=0;"
    print "if (!type_id){"
    print "\tstatic GTypeInfo "+Name+"_type= {"
    print "\t/*Class Size */ \n\t sizeof("+NameClass +"),"
    print "\t/* base init */ \n\t NULL,"
    print "\t/* base finalize */\n\t NULL,"
    print " "
    print "\t/* class init*/\n\t (GClassInitFunc)"+KName+"_class_init,"
    print "\t/* class finalize*/\n\t (GClassFinalizeFunc)"+KName+"_class_finalize,"
    print "\t/* const static data */\n\t NULL,"
    print "\t/* instance types */"
    print "\t/* instance size */\n\t sizeof("+Name+"),"
    print "\t/* n_preallocs   */\n\t 0,"
    print "\t/* InstanceInit  */\n\t (GInstanceInitFunc)",KName+"_init,"
    print "\t/* value table! */ \n\t NULL"
    print "};"
    print "type_id=g_type_register_static(G_OBJECT_TYPE("+Parent+"),"
    print "\t\t\""+Name+"\","
    print "\t\t&"+Name+"_type,"
    print "\t\t  G_TYPE_FLAG_DERIVABLE);"
    print "/* Comment it if you need to. Good for beginners like me */"
    print "g_message(\"registered type: %s with id %d\","+Name+",type_id);"
    print "}"
    print ""
    print "return type_id;"
    print "}"
    
    pass
   

if __name__=="__main__":
    import types;
    import sys;
    if(len(sys.argv)!=3):
        print "Usage: $gplates.py Object Parent"
        sys.exit(1)
    boiler_plate(sys.argv[1],sys.argv[2]);
    sys.exit(0)
