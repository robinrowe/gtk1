#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


gboolean  configure_event(GtkWidget *w,gpointer data)
{  
  //make a shaped mask
  GdkPixmap *pix;
  GdkBitmap *bit;
  GdkColor c={0};

  pix=gdk_pixmap_create_from_xpm(w->window,&bit,&c,"circle.xpm");
  gdk_window_shape_combine_mask(GDK_WINDOW(w->window),bit,100,100);  
  gtk_window_set_decorated(GTK_WINDOW(w),FALSE);
  return TRUE;
}

GtkWidget * make_shapedwidget(void)
{
  GtkWidget *w;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  
  g_signal_connect(G_OBJECT(w),"configure_event",G_CALLBACK(configure_event),NULL);
  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);

   gtk_container_add(GTK_CONTAINER(w),gtk_color_selection_new());
  return w;
}

int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=make_shapedwidget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall nonrectangular_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o nr
