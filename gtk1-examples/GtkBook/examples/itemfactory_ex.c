#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

#define GTK_MUTHU_DUCK "gtk-muthu-duck"

int call_me(gpointer *d,guint x,GtkWidget *w);

int main(int argc,char *argv[])
{
  GtkWidget *w,*bar,*box;
  GtkItemFactory *fac;
  GtkAccelGroup *ag;

  GdkPixbuf *f;

  GtkIconFactory *iff;
  GtkIconSet *is;
  GtkStockItem duck_stock={GTK_MUTHU_DUCK,NULL,GDK_MODIFIER_MASK,"icon"};


  static GtkItemFactoryEntry x[]=
    {
      {"/_Soup","<ALT>s",NULL,0,"<Branch>"},
      {"/Soup/_Cassava","<alt>c",(GtkItemFactoryCallback1)call_me,0,""},
      {"/Soup/_Tomato","<control>t",(GtkItemFactoryCallback1)call_me,0,""} ,      
      {"/Soup/_ChickenClear","<control><shift>c",(GtkItemFactoryCallback1)call_me,0,""} ,
      {"/Soup/","",NULL,0,"<Separator>"} ,
      {"/Soup/_Chicken","",(GtkItemFactoryCallback1)call_me,0,"<StockItem>",(const gchar *)GTK_MUTHU_DUCK} ,

      {"/_Tiffin","<alt>d",NULL,0,"<Branch>"},
      {"/Tiffin/Appam","<control>A",(GtkItemFactoryCallback1)call_me,0,""},
      {"/Tiffin/","",NULL,0,"<Separator>"} ,
      {"/Tiffin/Dosai","<control>D",(GtkItemFactoryCallback1)call_me,0,""},

      {"/_Help","<alt>h",(GtkItemFactoryCallback1)call_me,0,""},
    };


  gtk_init(&argc,&argv);

  
  f=gdk_pixbuf_new_from_file("./Toy.jpg",NULL);
  iff=gtk_icon_factory_new();
  is=gtk_icon_set_new_from_pixbuf(f);
  
  gtk_icon_factory_add(iff,GTK_MUTHU_DUCK,is);
  gdk_pixbuf_unref(f);
  gtk_icon_factory_add_default(iff);      
  gtk_stock_add(&duck_stock,1);


  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  bar=gtk_statusbar_new();
  box=gtk_vbox_new(FALSE,FALSE);
  ag=gtk_accel_group_new();


  gtk_window_add_accel_group(GTK_WINDOW(w),ag);
  fac=gtk_item_factory_new(GTK_TYPE_MENU_BAR,"<main>",ag);
  
  gtk_item_factory_create_items(fac,
				sizeof(x)/sizeof(GtkItemFactoryEntry),
				x,
				NULL);

  gtk_statusbar_push(GTK_STATUSBAR(bar),0,"Hello World");

  gtk_object_set_data(GTK_OBJECT(fac),"statusbar",bar);//for handling in callbacks.

  gtk_container_add(GTK_CONTAINER(w),box);
  gtk_box_pack_start(GTK_BOX(box),gtk_item_factory_get_widget(fac,"<main>"),FALSE,FALSE,0);
  gtk_box_pack_start(GTK_BOX(box),gtk_hseparator_new(),!FALSE,!FALSE,0);
  gtk_box_pack_start(GTK_BOX(box),bar,FALSE,FALSE,0);
  

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Item Factory");
  
  gtk_window_set_default_size(GTK_WINDOW(w),100,100);
  gtk_widget_show_all(w);
  
  gtk_main();
  return 0;
}



int call_me(gpointer *d,guint x,GtkWidget *w)
{
  gchar *s;
  GtkStatusbar *bar;
  GtkItemFactory *fac;

  s=g_strdup_printf("%s",gtk_item_factory_path_from_widget(w));
  fac=gtk_item_factory_from_widget(w);

  bar=(GtkStatusbar *)gtk_object_get_data(GTK_OBJECT(fac),"statusbar");
  
  gtk_statusbar_pop(bar,0);
  gtk_statusbar_push(bar,0,s);
  
  g_free(s);		

  return FALSE;
}

//gcc -Wall itemfactory_ex..c `pkg-config libgnomeui-2.0 --cflags --libs`

/*
  z=gtk_dialog_new();
  gtk_window_set_modal(GTK_WINDOW(z),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(z),TRUE);

  b=gtk_button_new_from_stock(GTK_MUTHU_DUCK);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),b);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_hseparator_new());
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_label_new(s));
 
  gtk_dialog_set_response_sensitive(GTK_DIALOG(z),
				    GTK_RESPONSE_DELETE_EVENT,
				    TRUE);
  gtk_widget_show_all(z);
  gtk_dialog_run(GTK_DIALOG(z));
  gtk_widget_destroy(z);
  GtkWidget *z,*b;
*/
