#include<glib.h>
#include<glib-object.h>


/* Lists all of the fundamental types. */
void GTypesTest(void)
{

	int i=0;
	GType t;
	g_type_init();
	g_type_init();
	
	g_message("TypeId | TypeName");
	for(i=0;i<21;i++){
		t=G_TYPE_MAKE_FUNDAMENTAL(i);
		g_message(" %d | %s ",t,g_type_name(t));
	}
	
}
int main()
{	
	GTypesTest();
	return 0;
}
