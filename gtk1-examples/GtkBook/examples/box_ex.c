/*
  Show Vbox, Hbox.
  Show VButtonBox, HButtonBox.
*/
#include<gtk/gtk.h>

gboolean box_list(GtkButton *b,gpointer data)
{
  GtkBox *box=GTK_BOX(data);
  int i;
  if(box!=NULL)
    {
      for(i=0;i<g_list_length(box->children);i++)
	{
	  GtkBoxChild *w;
	  w=g_list_nth_data(box->children,i);
	  g_message("%s",GTK_BUTTON(w->widget)->label_text);
	}
    }
  return TRUE;
}

int main() //(int argc,char *argv[])
{
  GtkWidget *win;
  GtkWidget *box,*but;

  gtk_init(NULL,NULL);

  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(1,1);

  but = gtk_button_new_with_label ("None");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_NONE);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);


  but = gtk_button_new_with_label ("List -contents- of the box.");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_HALF);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);
  g_signal_connect(G_OBJECT(but),"clicked",G_CALLBACK(box_list),box);

  but = gtk_button_new_with_label ("Normal");
  gtk_button_set_relief(GTK_BUTTON(but),GTK_RELIEF_NORMAL);
  gtk_box_pack_start_defaults(GTK_BOX(box),but);


  gtk_window_set_title(GTK_WINDOW(win),"  Box World  ");
  gtk_window_set_position(GTK_WINDOW(win),GTK_WIN_POS_MOUSE);
  gtk_container_add(GTK_CONTAINER(win),box);  

  g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(gtk_main_quit),NULL);


  gtk_widget_show_all(win);

  gtk_main();
  return 0;
}
//gcc -Wall box_ex.c -o box `pkg-config gtk+-2.0 --cflags --libs`
