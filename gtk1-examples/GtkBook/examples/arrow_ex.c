#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>




GtkWidget * arrow_widget(void)
{
  GtkWidget *w;
  GtkWidget *box,*arrow,*button;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  box=gtk_hbox_new(FALSE,FALSE);

  gtk_container_add(GTK_CONTAINER(w),box);

  /* Type of Arrow */
  /* Type of Shadow */
  arrow=gtk_arrow_new( GTK_ARROW_LEFT, GTK_SHADOW_ETCHED_OUT);

  gtk_box_pack_start_defaults(GTK_BOX(box),arrow);

  arrow=gtk_arrow_new( GTK_ARROW_UP, /* Type of Arrow */
		       GTK_SHADOW_ETCHED_IN /* Type of Shadow */);
  gtk_box_pack_start_defaults(GTK_BOX(box),arrow);

  arrow=gtk_arrow_new( GTK_ARROW_LEFT, /* Type of Arrow */
		       GTK_SHADOW_OUT /* Type of Shadow */);
  gtk_box_pack_start_defaults(GTK_BOX(box),arrow);

  arrow=gtk_arrow_new( GTK_ARROW_DOWN, /* Type of Arrow */
		       GTK_SHADOW_NONE /* Type of Shadow */);
  button=gtk_button_new();
  gtk_container_add(GTK_CONTAINER(button),arrow);
  
  gtk_box_pack_start_defaults(GTK_BOX(box),button);

  gtk_widget_show_all(box);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(button),"clicked",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_window_set_title(GTK_WINDOW(w),"Quiver Full of Arrows");
  gtk_window_resize(GTK_WINDOW(w),300,40);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=arrow_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall arrow_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o arrow_ex
