#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<string.h>
#include<math.h>

#if GTK_CHECK_VERSION(2,2,1) >= 0

void  range_cb(GtkRange *r)
{
  g_printf("Range=>%g\n",gtk_range_get_value(r));
}

/*
  Mathematically transform this co-ordinates through
  the given matrix multiplication to get the values of 
  rotated (X',Y') from (X,Y)
  | X'|      | cos(Q)  sin(Q) | | X |
  |   |   =  | 	       	      | |   |
  | Y'|      | -sin(Q) cos(Q) |	| Y |
*/


void rotate_picture(GdkPixbuf *pb,double theta) //a theta degree degree turn
{
  int ht,wt;
  int i=0,j=0;
  int l=0,m=0;
  int delx=0, dely=0;
  int bpp=3;
  int rowstride=0;
  double sin_val,cos_val; //they dont change, once we've calculated for 1 theta.
    
  gchar *dest;
  gchar *pixel;

  ht=gdk_pixbuf_get_height(pb);
  wt=gdk_pixbuf_get_width(pb);
  bpp=3;

  //pd=gdk_pixbuf_new(GDK_COLORSPACE_RGB,FALSE,8,wt,ht); //a buffer image.
  
  pixel=gdk_pixbuf_get_pixels(pb);
  // dest=gdk_pixbuf_get_pixels(pd);

  sin_val=sin(M_PI*theta/180.0);
  cos_val=cos(M_PI*theta/180.0);

  //  g_printf("W=%d H=%d\n",wt,ht);
  rowstride=bpp*wt;

  dest=g_malloc(sizeof(gchar)*rowstride*ht);
  if(dest == NULL)
    {
      g_printf("Cannot Allocate Memory!\n");     
      return;
    }
  else
    {
      // g_printf("Allocated Memory\n");
    }

  delx=0;
  dely=0;

  delx=ht*sin_val;
  dely=ht*cos_val; 

  for(i=0;i<ht;i++)
    {
      for(j=0;j<rowstride;j+=bpp)
	{       
	  l=(int)(cos_val*i+ sin_val*(j/3));
	  m=(int)(-sin_val*i+cos_val *(j/3));
	  	 	

	  if(m<0)
	    m=m+wt;
	  if(l<0)
	    l=l+ht;

	  m+=delx;
	  l+=dely;

	  m=m%wt;
	  l=l%ht;

	  m=(m<0) ? -m : m;
	  l=(l<0) ? -l : l;
	  
	  //g_printf("[%d,%d] => [%d,%d]\n",i,j/3,l,m/3);
	  m=m*3;

	  dest[l*rowstride + m + 0]=pixel[i*rowstride + j+ 0];
	  dest[l*rowstride + m + 1]=pixel[i*rowstride + j+ 1];
	  dest[l*rowstride + m + 2]=pixel[i*rowstride + j+ 2];		  
	}
    }  

  memcpy(pixel,dest,ht*rowstride);
  g_free(dest);

  return;
}

gboolean adjustment_cb(GtkAdjustment *w,gpointer data)
{
  static GdkPixbuf *p=NULL;
  GtkImage *im;
  GdkPixbuf *pb;
  gdouble val;
  gchar *pixel,*dest;
  guint ht,wt;
  
  val=gtk_adjustment_get_value(w);
  g_printf("Val=%g\n",val);

  im=(GtkImage *)data;
  pb=gtk_image_get_pixbuf(im);
  
  if(p==NULL)
    {
      p=gdk_pixbuf_copy(pb);
    }

  ht=gdk_pixbuf_get_height(pb);
  wt=gdk_pixbuf_get_width(pb);

  pixel=gdk_pixbuf_get_pixels(pb);
  dest=gdk_pixbuf_get_pixels(p);

  memcpy(pixel,dest,ht*wt*3);

  rotate_picture(pb,val);
  
  gtk_widget_queue_draw(GTK_WIDGET(im));

  return TRUE;
}


int main(int argc,char *argv[])
{
  GtkWidget *w,*box,*r;
  GdkPixbuf *pb;
  GtkWidget *im;
  GtkAdjustment *aj;

  gtk_init(&argc,&argv);
  
    
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  r=gtk_hruler_new();
  gtk_ruler_set_range(GTK_RULER(r),0,360,0,10);

  pb=gdk_pixbuf_new_from_file("tux.bmp",NULL);
  im=gtk_image_new_from_pixbuf(pb);
  g_object_unref(pb);

  box=gtk_vbox_new(FALSE,FALSE);

  gtk_box_pack_start_defaults(GTK_BOX(box),im);
  gtk_box_pack_start_defaults(GTK_BOX(box),r);

  r=gtk_hscrollbar_new(NULL);
  gtk_range_set_range(GTK_RANGE(r),0,360);
  aj=gtk_range_get_adjustment(GTK_RANGE(r));

  gtk_box_pack_start_defaults(GTK_BOX(box),r);



  gtk_container_add(GTK_CONTAINER(w),box);

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(aj),"value_changed",GTK_SIGNAL_FUNC(adjustment_cb),im);//pass the image


  gtk_window_set_title(GTK_WINDOW(w),"Lets Spin!");
  gtk_window_set_default_size(GTK_WINDOW(w),200,40);
  gtk_widget_show_all(w);

  //  gtk_adjustment_set_value(aj,50.00);
  gtk_main();
  return 0;
}
//gcc -Wall rotate_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o rotate_ex -lm

#else
#error "Use Gtk2.2.4 or later. Error, cant continue"
#endif
