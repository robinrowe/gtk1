#include "marquee.h"
//#include<gtk/gtk.h>
//#include<gtk/gtkitemfactory.h>
//#include<glib/gprintf.h>
//#include<string.h>

//default sizes.

/*
struct Marquee
{
  GtkWidget *w;//eventbox;
  GdkPixmap *pix; //blit the pixmap to the window, on expose.
  PangoLayout *ly;
  gchar *text;
  int rate;
  int count;
  gboolean flag;
  GtkTextDirection type;
  GdkPixbuf *bg_pb;
  GdkPixmap *bg_map;
  GdkBitmap *bg_mask;
};
*/

static gboolean
marquee_draw(struct Marquee *marq)
{
  int wt,ht;
  int x,y;
  GdkRectangle area;
  GtkWidget *w;


  g_return_val_if_fail(marq!=NULL,FALSE);

  w=marq->w;
  g_assert(marq != NULL && w!=NULL && marq->text!=NULL);
  //  g_message("Enter Draw");

  //the first expose event only, comes into this.
  if(marq->pix==NULL)
    {
      return TRUE;
    }

#if 0
  if(marq->bg_pb==NULL)
    {
      //      g_message("RGBA built.\n");
      marq->bg_pb= gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8,w->allocation.width,w->allocation.height);
      g_assert(marq->bg_pb!=NULL);
      gdk_pixbuf_fill(marq->bg_pb,0x00000040); /* color is [RGBA] */            
      gdk_pixbuf_render_pixmap_and_mask_for_colormap(marq->bg_pb,
						     gtk_widget_get_colormap(marq->w),
						     &marq->bg_map,
						     &marq->bg_mask,
						     0x40-1);
      marq->w->style->bg_pixmap[GTK_STATE_NORMAL]=marq->bg_map;
    }
#endif 

  if(marq->ly==NULL)
    {
      PangoFontDescription *pfd;
      PangoContext *pc=NULL;
      
      pfd=pango_font_description_from_string("Sans 16");
      pc=gtk_widget_create_pango_context(marq->w);
      marq->ly=pango_layout_new(pc);
      if(marq->text == NULL)
	marq->text=g_strdup("NULL");
      //      g_message("set_text: %s",marq->text);
      pango_layout_set_markup(marq->ly,marq->text,-1);
      pango_layout_set_font_description(marq->ly,pfd);

      pango_font_description_free(pfd);
      g_object_unref(pc);
      
      //      return TRUE;
    }

  //check for the size of the widget, and the pixmap, they ought to be in sync.
  //wt & ht hold the size of the pixmap.
  wt=0;
  ht=0;
  gdk_drawable_get_size(marq->pix,&wt,&ht);
  if(marq->w->allocation.width!=wt || marq->w->allocation.height!=ht)
    {
      //      g_message("Allocation Changed\n");
      g_object_unref(marq->pix);
      marq->pix=gdk_pixmap_new(marq->w->window,
			       marq->w->allocation.width,
	 		       marq->w->allocation.height,
			       -1);
#if 0
      if(marq->bg_pb!=NULL)
	g_object_unref(marq->bg_pb);
      marq->bg_pb=NULL;
#endif

      g_assert(marq->pix!=NULL);
    }

  if(marq->text!=NULL)
    pango_layout_set_markup(marq->ly,marq->text,-1);


  area=w->allocation;
  pango_layout_get_pixel_size(marq->ly,&wt,&ht);

  if(ht>area.height)
     ht=area.height-10;
  if(wt>area.width)  
    wt=area.width-50;

  x=(area.width-wt)/2 ;
  y=(area.height-ht)/2 ;
  //  g_message("draw: text(%d %d) area(%d,%d) x,y(%d,%d)",
  //	    wt,ht,area.width,area.height,x,y);
  
  /* clear the old contents of the pixmap */
  gdk_draw_rectangle(marq->pix,marq->w->style->bg_gc[GTK_STATE_NORMAL],
  		     TRUE,0,0,area.width,area.height);  

  gdk_draw_layout(marq->pix,marq->w->style->fg_gc[GTK_STATE_NORMAL], x,y,marq->ly);
  //  g_message("layout draw: text(%d %d) area(%d,%d) x,y(%d,%d)",wt,ht,area.width,area.height,x,y);

  //hide the rest
  x=area.width-x;
  area.width=x;
  gdk_draw_rectangle(marq->pix,marq->w->style->bg_gc[GTK_STATE_NORMAL],
  		     TRUE,x,y,area.width,area.height);


  //  g_message("Leave Draw");  
  return TRUE;
}

static gboolean
marquee_scroll(gpointer data)
{
  struct Marquee *marq;
  GtkWidget *w;
  gchar *text;
  gchar y;
  int x,i=0;

  g_return_val_if_fail(data!=NULL,FALSE);

  //  g_message("Enter marquee_scroll");
  marq=(struct Marquee *)data;

  if(marq->text==NULL  || marq->flag==FALSE)
    return FALSE;

  w=GTK_WIDGET(marq->w);
  text=marq->text;

  //  g_message("1 timeout:  %d",x);

  
  x=strlen(text);
  
  for(marq->count=0;marq->count<marq->rate;marq->count++)
    {
      if(marq->type==GTK_TEXT_DIR_RTL)
	{
	  y=text[0];
	  for(i=0;i<(x-1);i++)      
	    {
	      text[i]=text[i+1];
	    }
	  text[x-1]=y;
	  text[x]='\0';
	}
      else if(marq->type==GTK_TEXT_DIR_LTR)
	{
	  y=text[x-1];
	  for(i=x-1;i>0;i--)
	    {
	      text[i]=text[i-1];
	    }
	  text[0]=y;
	  text[x]='\0';
	}
	  
    }
  
  marquee_draw(marq);
  //  g_message("2 timeout: %s %d",text,x);
  gtk_widget_queue_draw(w);

  return marq->flag;
}

static void
marquee_size_request (GtkWidget      *widget,
		      GtkRequisition *requisition)
{
  //  g_message("Enter marquee_size_request");
  requisition->width = 400;
  requisition->height = 50;
}

static void 
marquee_size_allocate (GtkWidget     *widget,
		       GtkAllocation *allocation)
{
  //  g_message("Allocate: %d %d",  allocation->width, allocation->height);
  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);
      gtk_widget_queue_draw(widget);
    }
  //  g_message("Leave allocate");
}


static gboolean
marquee_configure_event (GtkWidget      *widget,
			 GdkEventConfigure *event,gpointer data)
{
  struct Marquee *marq;
  marq=(struct Marquee *)data;
  //  g_message("Configure:");
  if(marq->pix!=NULL)
    g_object_unref(marq->pix);
  marq->pix=gdk_pixmap_new(marq->w->window,
			   event->width,
			   event->height,
			   -1);
  return TRUE;//handled .
}


static void
marquee_set_text(struct Marquee *marq,const gchar *txt)
{
  
  marq->text=g_strdup_printf("%s            ",txt);
  //  g_message("set_text: %s",marq->text);
}



gboolean marquee_expose(GtkWidget *w,GdkEventExpose *event,gpointer data)
{
  /*  int wt,ht;
      int x,y;  */
  struct Marquee *marq=NULL;
  GdkRectangle area;

  marq=(struct Marquee *)data;
  g_assert(marq != NULL && w!=NULL);
  //  g_message("Enter marquee_expose");


  area=w->allocation;
  if(marq->pix==NULL)
    {
      int id=0;
      id++;
      marq->pix=gdk_pixmap_new(marq->w->window,
			       area.width,
			       area.height,
			       -1);
      //      g_message("Fun None.\n");
      gdk_draw_rectangle(marq->pix,marq->w->style->fg_gc[GTK_STATE_NORMAL],
			 TRUE,1,1,area.width-1,area.height-1);
      
      if(g_object_get_data(G_OBJECT(marq->w),"timeout-id")!=NULL)
	{
	  id=(int)g_object_get_data(G_OBJECT(marq->w),"timeout-id");
	  gtk_timeout_remove(id);
	}

      id=gtk_timeout_add(1050,
			 (GtkFunction) marquee_scroll,marq);
      g_object_set_data(G_OBJECT(marq->w),
			"timeout-id",(gpointer)id);
      marq->flag=TRUE;
      marquee_draw(marq);
    }


  if(marq->w->window)
    {
      //      g_message("Paint Expose src(%d,%d)",event->area.x,event->area.y);

      /*
	if(marq->bg_mask != NULL)
	{
	gdk_window_shape_combine_mask(marq->w->window,marq->bg_mask,0,0);
	}
      */

      gdk_draw_drawable(marq->w->window,
			marq->w->style->bg_gc[GTK_STATE_NORMAL],
			marq->pix,
			event->area.x,event->area.y,0,0,//0,0,//
			event->area.width,event->area.height);
      gtk_style_set_background(marq->w->style,marq->w->window,GTK_STATE_NORMAL);
    }
  //  g_message("Fun After.");
  return TRUE;

  /*
    area=w->allocation;
    g_assert(marq->ly != NULL);
    
    if(marq->text!=NULL)
      pango_layout_set_markup(marq->ly,marq->text,-1);

   pango_layout_get_pixel_size(marq->ly,&wt,&ht);

  //  g_message("expose: text(%d %d) area(%d,%d)",wt,ht,area.width,area.height);
   x=(area.width-wt)/2;
   y=(area.height-ht)/2;
  */
  
  //box to contain the text and two diamonds beside the start & end of text,
  //start

  /*
  gtk_paint_box(w->style,w->window,GTK_STATE_NORMAL,GTK_SHADOW_ETCHED_OUT,&area,w,"marquee",
		x-2,y-2,wt+2,ht+2);  
  gtk_paint_diamond(w->style,w->window,GTK_STATE_NORMAL,GTK_SHADOW_IN,&area,w,"marquee",
		    x-2-ht,y-2,ht+2,ht+2);    
  gtk_paint_diamond(w->style,w->window,GTK_STATE_NORMAL,GTK_SHADOW_OUT,&area,w,"marquee",
		    x+wt-2,y-2,ht+2,ht+2);  
   gtk_paint_layout(w->style,w->window, GTK_STATE_NORMAL, FALSE,
		   &area,w,"marquee",x,y, marq->ly);  
   gtk_paint_layout(w->style,w->window, GTK_STATE_NORMAL, TRUE,
		   &area,w,"marquee",x,y, marq->ly);
  */
  		  
  //  g_message("Leave Expose");

  //  return TRUE; 
}

void
marquee_custom_destroy(GtkWidget *w,gpointer data)
{
  struct Marquee *marq;

  marq=(struct Marquee *)data;
  //  g_message("Enter marquee_custom_destroy");

  g_object_unref(marq->pix);
  marq->pix=NULL;
  marq->flag=FALSE;
  /*
    int id;
    id=(int)g_object_get_data(G_OBJECT(marq->w),"timeout-id");
    gtk_timeout_remove(id);
  */
  gtk_widget_destroy(w);
  g_free(marq->text);
  g_free(marq);
  marq=NULL;
  return;
}


GtkWidget* marquee_widget(const gchar *txt,GtkTextDirection type)
{
  struct Marquee *marq;
  marq=g_malloc(sizeof(struct Marquee)*1);
  
  //  g_message("Enter Marquee Widget");
    
  marq->w=NULL;
  marq->text=NULL;
  marq->ly=NULL;
  marq->pix=NULL;
  marq->rate=5;
  marq->count=0;
  marq->flag=TRUE;
  marq->type=GTK_TEXT_DIR_RTL;//DD New style of ticker by default for all marquees.
  marq->bg_pb=NULL;

  switch(type)
    {
    case GTK_TEXT_DIR_RTL:
      marq->type=type;
      marq->rate=5;
	marq->count=0;
      break;
    case GTK_TEXT_DIR_LTR:
      marq->rate=5;
      marq->count=0;
      marq->type=type;
    default:
           g_message("<wrong type of text direction. default set to RTL>");
    }
       

  //  g_message("<creating widgets: marquee>");
  marq->w=gtk_event_box_new();
  marquee_set_text(marq,txt);

  
  //  g_message("<making connections: marquee>");

  g_signal_connect(G_OBJECT(marq->w),"configure_event",G_CALLBACK(marquee_configure_event),marq);
  g_signal_connect(G_OBJECT(marq->w),"size_allocate",G_CALLBACK(marquee_size_allocate),NULL);
  g_signal_connect(G_OBJECT(marq->w),"size_request",G_CALLBACK(marquee_size_request),NULL);
  g_signal_connect(G_OBJECT(marq->w),"expose_event",G_CALLBACK(marquee_expose),marq);
  g_signal_connect(G_OBJECT(marq->w),"destroy",G_CALLBACK(marquee_custom_destroy),marq);
  gtk_widget_show(marq->w);
  //  g_message("[Leave]");
  return marq->w;
}

//gcc -Wall marquee.c `pkg-config libgnomeui-2.0 --cflags --libs` -o marquee
/**
   messed it again!
*/
