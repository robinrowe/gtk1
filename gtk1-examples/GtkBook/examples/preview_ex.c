#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


gboolean track_me (GtkWidget	     *widget,
		   GdkEventMotion      *event)
{
  GtkPreview *pv;
  GdkModifierType state;
  guint x,y;
  guint i,j,oldx;

  pv=(GtkPreview *)widget;
  
  //Trace the path with black!
  //g_printf("Event Motion CB\n");
  
  gdk_window_get_pointer (event->window, &x, &y, &state);

 /*
 // g_printf("Event Motion CB %d %d\n",x,y);
 */
  
  if(x>=180)
     x=180;

  if(y>=180)
     y=180;
  
  i=y+2;
  oldx=x;
  j=x+2;

  while( y<i)    
    {
      x=oldx;
      while(x<j)
	{
	  pv->buffer[200*3*(int)y + (int)x*3+0]=0x00;//R
	  pv->buffer[200*3*(int)y + (int)x*3+1]=0x00;//G
	  pv->buffer[200*3*(int)y + (int)x*3+2]=0x00;//B
	  x++;
	}
      y++;
    }

 gtk_widget_queue_draw(widget);

 return TRUE;
}


gboolean key_cb(GtkWidget	     *widget,
		GdkEventKey	     *event)
{
  g_printf("KEY_CB\n");
  return FALSE;
}

GtkWidget * make_preview(void)
{
  GtkWidget *w,*box,*pv;
  gint i=0,j=0;
  gint bpp=3;
  guchar *row;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);

  pv=gtk_preview_new(GTK_PREVIEW_COLOR);
  gtk_preview_size(GTK_PREVIEW(pv),200,200);


  gtk_container_add(GTK_CONTAINER(w),box);
  gtk_box_pack_start_defaults(GTK_BOX(box),pv);
  
  /*
    Paint the preview in a strange design.
  */
  row=g_malloc(sizeof(gchar)*bpp*200);
  for(i=0;i<200;i++)
    {
      //ROW is eaxctly only one row!
      for(j=0;j<200;j++)
	{
	  row[j*bpp + 0]=i+j;   //R
	  row[j*bpp + 1]=2*i+2*j; //G
	  row[j*bpp + 2]=i-j; //B
	}     
      /*
	x-> 0 //offset in  a row.
	y-> i //row number
	w-> 200 // width of a buffer['row' variable].
      */     
      gtk_preview_draw_row(GTK_PREVIEW(pv), row,0,i,200);
    }
  g_free(row);


  gtk_widget_set_events(pv,gtk_widget_get_events(pv) | GDK_KEY_PRESS | GDK_POINTER_MOTION_MASK);

  gtk_object_set_data(GTK_OBJECT(w),"preview",pv); //for later callbacks.
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_signal_connect(GTK_OBJECT(pv),"motion_notify_event",GTK_SIGNAL_FUNC(track_me),NULL);
  gtk_signal_connect(GTK_OBJECT(w),"key_press_event",GTK_SIGNAL_FUNC(key_cb),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Strange Designs");
  gtk_window_set_default_size(GTK_WINDOW(w),200,200);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);
  w=make_preview();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}




//gcc -Wall x.c `pkg-config libgnomeui-2.0 --cflags --libs`

/*
  z=gtk_dialog_new();
  gtk_window_set_modal(GTK_WINDOW(z),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(z),TRUE);

  b=gtk_button_new_from_stock(GTK_MUTHU_DUCK);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),b);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_hseparator_new());
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_label_new(s));
 
  gtk_dialog_set_response_sensitive(GTK_DIALOG(z),
  GTK_RESPONSE_DELETE_EVENT,
  TRUE);
  gtk_widget_show_all(z);
  gtk_dialog_run(GTK_DIALOG(z));
  gtk_widget_destroy(z);
  GtkWidget *z,*b;
*/
