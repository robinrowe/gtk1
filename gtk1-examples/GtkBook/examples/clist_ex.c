#include <gtk/gtk.h>

void change_lbl(GtkWidget *w,gpointer data)
{
  GtkButton *b;
  GtkEntry *e;
  gchar *str;
  
  b=(GtkButton *)w;
  e=(GtkEntry *)data;

  str=g_strdup(gtk_entry_get_text(e));
  gtk_button_set_label(b,
		       str);
  g_free(str);  
}

int main(int argc,char *argv[])
{  
  GtkWidget *btn,*entry,*box,*w;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  btn= gtk_button_new_with_label("Change Label");
  entry= gtk_entry_new();
  box= gtk_vbox_new(FALSE,FALSE);

  gtk_box_pack_start_defaults(GTK_BOX(box),
			      entry);

  gtk_box_pack_start_defaults(GTK_BOX(box),
			      btn);

  gtk_container_add(GTK_CONTAINER(w), box);

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(btn),"clicked",GTK_SIGNAL_FUNC(change_lbl),entry);

  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}
