#include<gtk/gtk.h>
#include<libxml/parser.h>

typedef struct _Record Record;
typedef struct _Form Form;
typedef int (*NotifyFunc) (Record *);

struct _Record
{
  gchar *Title;
  gchar *FirstName;
  gchar *MiddleName;
  gchar *LastName;
  gchar *Address1;
  gchar *Address2;
  gchar *City;
  gchar *State;
  gchar *Pin;
  gchar *Country;
};

struct _Form
{
  GtkWidget *container; /* Main Widget */
  GtkWidget *titleLabel,*titleEntry;
  GtkWidget *fLabel,*fEntry;
  GtkWidget  *mLabel,*mEntry;
  GtkWidget *lLabel,*lEntry;
  GtkWidget *addr1Label,*addr1Entry;
  GtkWidget *addr2Label,*addr2Entry;
  GtkWidget *cityLabel,*cityEntry;
  GtkWidget *stateLabel,*stateEntry;
  GtkWidget *pinLabel,*pinEntry;
  GtkWidget *countryLabel,*countryEntry;
};

/* API for the Address Book */
Form * make_form(Record *rec,NotifyFunc);
void   update_form(Form *form,Record *rec);

/* XML file to Record list */
GSList *get_records(gchar *filename);

/* XML Record output */
void print_rec(Record *rec)
{
  g_message("%s %s %s from %s",rec->Title,rec->FirstName,rec->LastName,rec->Country);
}

/* Record to XML file */
void save_records(GSList *reclist,gchar *filename);

/* Internal Window Factory */
GtkWidget * make_window(GSList *reclist);

/* Display Manager Callback */
void display_mgr(GtkButton *btn);

/* Cleanup Function */
void cleanup_fcn(GtkWidget *w);

/* TODO
   1: Input Saving, into the same XML file.
   2: Update Records/Delete Records.
   3: Some Searching Based on Fields.
   4: Viewing All records as a tree.
*/


int main(int argc,char *argv[])
{ 
  GSList *reclist;
  gtk_init(&argc,&argv);
  reclist=get_records((argc==2)?argv[1]:"addr.xml");
  /* g_message("Gottit\n"); */
  g_return_val_if_fail(make_window(reclist)!=NULL,-1);
  gtk_main();
  return 0;
}


/* Internal Window Factory */

#define SET_DATA(gob,desc,data) \
           g_object_set_data(G_OBJECT(gob),desc,(gpointer)data)
#define GET_DATA(gob,desc) \
           g_object_get_data(G_OBJECT(gob),desc)
GtkWidget *make_window(GSList *reclist)
{ 
  GtkWidget *win;
  GtkWidget *box,*hbox,*fw,*bw,*last,*first;
  Form *form;
  

  if(reclist==NULL || g_slist_length(reclist)<=0)
    {
      g_message("Records List [NULL|0]");
      return NULL;
    }

  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(win),"GtkAddress Book");

  /*  g_message("Form[ing]"); */
  form=make_form(g_slist_nth_data(reclist,0),NULL);
  g_return_val_if_fail(form!=NULL,NULL);

  hbox=gtk_hbox_new(FALSE,0);
  box=gtk_vbox_new(TRUE,1);

  fw=gtk_button_new_from_stock(GTK_STOCK_GO_FORWARD);
  bw=gtk_button_new_from_stock(GTK_STOCK_GO_BACK);
  last=gtk_button_new_from_stock(GTK_STOCK_GOTO_LAST);
  first=gtk_button_new_from_stock(GTK_STOCK_GOTO_FIRST);

  
  gtk_box_pack_start_defaults(GTK_BOX(hbox),first);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),bw);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),fw);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),last);

  gtk_widget_set_name(first,"first");
  gtk_widget_set_name(bw,"bw");
  gtk_widget_set_name(fw,"fw");
  gtk_widget_set_name(last,"last");

  SET_DATA(first,"form",form);
  SET_DATA(bw,"form",form);
  SET_DATA(fw,"form",form);
  SET_DATA(last,"form",form);


  SET_DATA(first,"reclist",reclist);
  SET_DATA(bw,"reclist",reclist);
  SET_DATA(fw,"reclist",reclist);
  SET_DATA(last,"reclist",reclist);
  SET_DATA(win,"reclist",reclist); /*for cleanup*/


  g_signal_connect(G_OBJECT(first),"clicked",G_CALLBACK(display_mgr),NULL);
  g_signal_connect(G_OBJECT(bw),"clicked",G_CALLBACK(display_mgr),NULL);
  g_signal_connect(G_OBJECT(fw),"clicked",G_CALLBACK(display_mgr),NULL);
  g_signal_connect(G_OBJECT(last),"clicked",G_CALLBACK(display_mgr),NULL);

  gtk_widget_grab_focus(box);

  gtk_box_set_spacing(GTK_BOX(box),0);
  gtk_box_set_homogeneous(GTK_BOX(box),FALSE);

  gtk_box_pack_start_defaults(GTK_BOX(box),form->container);
  gtk_box_pack_start_defaults(GTK_BOX(box),hbox);
  gtk_container_add(GTK_CONTAINER(win),box);

  g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(cleanup_fcn),NULL);
  gtk_widget_show_all(win);
  return win;
}


/* Cleanup Function 
   Free the reclist.
*/
void cleanup_fcn(GtkWidget *w)
{
  GSList *reclist,*ptr;
  reclist=(GSList *)GET_DATA(w,"relist");
  for(ptr=reclist;ptr!=NULL;ptr=ptr->next)
    {
      g_free(ptr->data);      
    }
  g_slist_free(reclist);
  gtk_main_quit();/* Finally Say Bye */
}



/* Display Manager Callback 
   Show various pages, by using 
   different forms!
*/
void display_mgr(GtkButton *btn)
{
  static int current_page=0;
  gchar const *ptr;
  Record *rec;
  GSList *reclist;
  Form *form;

  reclist=(GSList *)GET_DATA(btn,"reclist");
  ptr=gtk_widget_get_name(GTK_WIDGET(btn));


  switch(ptr[0])
    {
    case 'b'://goBack()
      if(current_page<=0)
	break;
      current_page--;
      break;
    case 'l'://goLast()
      current_page=g_slist_length(reclist)-1;
      break;
    case 'f':
      if(ptr[1]=='w') //gdForward()	
	{
	  if((current_page+1) < g_slist_length(reclist))
	    current_page++;
	}
	else
	  current_page=0; //goFirst()
      break;
    }      
  
  /* Get the New Form */
  form=(Form *)GET_DATA(btn,"form");
  /* Get the record */
  rec=g_slist_nth_data(reclist,current_page);
  if(rec)
    update_form(form,rec);
  else
    g_message("REC==NULL");
  g_message("Current Page: %d, %s %d",current_page,ptr,g_slist_length(reclist));      
}
  
/* API for the Address Book 
   Make a Record structure, into a 
   UI Form. Notify the function,
   on any Changes by passing the 
   Record struct to it.
*/
#define MAKE_WIDGET(struc,obj,tag,strtag)\
                {\
                  struc->obj##Entry=gtk_entry_new();\
                  gtk_entry_set_text(GTK_ENTRY(struc->obj##Entry),(rec->tag)?(rec->tag):" ");\
                  struc->obj##Label=gtk_label_new(strtag);\
                }

#define PACK_BOX(struc,box,obj)\
                {\
		  GtkWidget *hbox=gtk_hbox_new(TRUE,2);\
		  gtk_box_pack_start_defaults(GTK_BOX(hbox),struc->obj##Label);\
		  gtk_box_pack_start_defaults(GTK_BOX(hbox),struc->obj##Entry);\
		  gtk_box_pack_start_defaults(GTK_BOX(struc->box),hbox);\
		}

Form* make_form(Record *rec,NotifyFunc fcn)
{
  Form *f=(Form *)g_malloc0(sizeof(Form)*1);
  f->container=gtk_vbox_new(TRUE,1);

  g_return_val_if_fail(rec!=NULL,NULL);
  /* Making */

  MAKE_WIDGET(f, title,Title,"Title");
  MAKE_WIDGET(f, f,FirstName,"FirstName");
  MAKE_WIDGET(f, m,MiddleName,"MiddleName");
  MAKE_WIDGET(f, l,LastName,"LastName");
  MAKE_WIDGET(f, addr1,Address1,"Address1");
  MAKE_WIDGET(f, addr2,Address2,"Address2)");
  MAKE_WIDGET(f, city,City,"City");
  MAKE_WIDGET(f, state,State,"State");
  MAKE_WIDGET(f, pin,Pin,"Pin");
  MAKE_WIDGET(f, country,Country,"Country");

  /* Packing */
  
  PACK_BOX(f,container,title);
  PACK_BOX(f,container,f);
  PACK_BOX(f,container,m);
  PACK_BOX(f,container,l);
  PACK_BOX(f,container,addr1);
  PACK_BOX(f,container,addr2);
  PACK_BOX(f,container,city);
  PACK_BOX(f,container,state);
  PACK_BOX(f,container,pin);
  PACK_BOX(f,container,country);

  return f;
}


/* Update an already created form */
#define UPDATE_WIDGET(struc,obj,tag,title)\
          gtk_entry_set_text(GTK_ENTRY(struc->obj##Entry),rec->tag)

void   update_form(Form *f,Record *rec)
{
  UPDATE_WIDGET(f, title,Title,"Title");
  UPDATE_WIDGET(f, f,FirstName,"FirstName");
  UPDATE_WIDGET(f, m,MiddleName,"MiddleName");
  UPDATE_WIDGET(f, l,LastName,"LastName");
  UPDATE_WIDGET(f, addr1,Address1,"Address1");
  UPDATE_WIDGET(f, addr2,Address2,"Address2)");
  UPDATE_WIDGET(f, city,City,"City");
  UPDATE_WIDGET(f, state,State,"State");
  UPDATE_WIDGET(f, pin,Pin,"Pin");
  UPDATE_WIDGET(f, country,Country,"Country");
  gtk_widget_queue_draw(f->container); /* Redraw yourself + Children */
  return;
}



/* XML file to Record list 
   Parse a given XML file, and get
   the record list, as an array of Records,
   NULL terminated.
*/
GSList *get_records(gchar *filename)
{
  Record *rec;
  GSList* reclist;
  xmlDocPtr doc;
  xmlNodePtr node;
  
  doc=xmlParseFile(filename);
  g_return_val_if_fail(doc!=NULL,NULL);
  
  node=xmlDocGetRootElement(doc);
  g_return_val_if_fail(node!=NULL,NULL);

  reclist=g_slist_alloc();
  reclist->data=NULL;

  for(node=xmlDocGetRootElement(doc)->children;node!=NULL;node=node->next)
    {
      if(!xmlStrcmp(node->name,"Card"))
	{
	  xmlNodePtr temp;
	  temp=node->children;
	  rec=g_malloc0(sizeof(Record)*1);	  
	  for(;temp!=NULL;temp=temp->next)
	    {
	      switch(temp->name[0])
		{
		case 'T'://Title
		  rec->Title=xmlGetProp(temp,"val");
		  break;
		case 'F': //First Name
		  rec->FirstName=xmlGetProp(temp,"val");
		  break;
		case 'M': //Middle Name
		  rec->MiddleName=xmlGetProp(temp,"val");
		  break;
		case 'L'://LastName
		  rec->LastName=xmlGetProp(temp,"val");
		  break;
		case 'A'://Address
		  if( temp->name[7]=='1')
		    {
		      rec->Address1=xmlGetProp(temp,"val");
		    }
		  else
		      rec->Address2=xmlGetProp(temp,"val");		  
		  break;
		case 'C'://City
		  if(temp->name[3]=='y')
		    {
		      rec->City=xmlGetProp(temp,"val");
		    }
		  else
		    {  //Country
		       rec->Country=xmlGetProp(temp,"val");
		    }		
		  break;
		case 'S'://State
		  rec->State=xmlGetProp(temp,"val");
		  break;
		case 'P'://Pin
		  rec->Pin=xmlGetProp(temp,"val");
		  break;
		}
	    }

	  print_rec(rec);
	  if(!reclist->data)
	    reclist->data=rec;
	  else
	    g_slist_append(reclist,rec);
	}
    }
  xmlFree(node);
  xmlFree(doc);
  return reclist;
}

/* Record to XML file 
   Append the record list to the file.
   Record is NULL terminated.
*/
void save_records(GSList *list,gchar *filename)
{ 
  xmlDocPtr doc;
  xmlNodePtr rnode,cnode,wnode;
  xmlNsPtr ns=NULL;
  GSList *tmp;
  Record *rec;
  
  doc=xmlNewDoc("1.2");
 
  rnode=xmlNewDocNode(doc,NULL,"AddressBook","\n"); /* Root Node */
  xmlDocSetRootElement(doc,rnode);
  

  for(tmp=list;tmp!=NULL;tmp=tmp->next) /*Naviagte the List of [Cards]*/
    {
      
      rec=tmp->data;

      /* Card is child of AddressBook */
      cnode=xmlNewChild(rnode,ns,"Card","\n");

      /* All rest are children of Card */
      wnode=xmlNewNode(ns,"Title");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->Title);

      wnode=xmlNewNode(ns,"FirstName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->FirstName);

      wnode=xmlNewNode(ns,"MiddleName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->MiddleName);

      wnode=xmlNewNode(ns,"LastName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->LastName);

      wnode=xmlNewNode(ns,"Address1");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Address1);
            
      wnode=xmlNewNode(ns,"Address2");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Address2);

      wnode=xmlNewNode(ns,"City");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->City);

      wnode=xmlNewNode(ns,"State");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->State);

      wnode=xmlNewNode(ns,"Pin");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Pin);		

      wnode=xmlNewNode(ns,"Country");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Country);
    }

  {
    char buffer[256]="Created on ";
    time_t t=time(NULL);
    asctime_r(localtime(&t),buffer+strlen(buffer));
    cnode=xmlNewComment(buffer);
    xmlAddChild(rnode,cnode);
  }

  xmlSaveFile(filename,doc);
  xmlFreeNode(rnode);
  xmlFreeDoc(doc);
}

/* gcc -Wall address.c `pkg-config libxml-2.0  --cflags --libs` `pkg-config gtk+-2.0 --cflags --libs`
*/

