#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>

void btn_drag_end(GtkWidget *widget,
		  GdkDragContext *context)
{
  g_printf("Drag Ends here\n");
}

void btn_drag_begin(GtkWidget      *widget,
		    GdkDragContext *context,
		    gpointer        data)
{
  GdkPixbuf *pixbuf;
  g_printf("Drag SetIcon\n");
  pixbuf=gdk_pixbuf_new(GDK_COLORSPACE_RGB,FALSE,8,48,32);
  gdk_pixbuf_fill(pixbuf,0xcc00ffee);
  gtk_drag_set_icon_pixbuf (context, pixbuf, -2, -2);
}

void btn_drag_handle (GtkWidget        *widget,
		      GdkDragContext   *context,
		      GtkSelectionData *selection_data,
		      guint             info,
		      guint             time,
		      gpointer          data)
{
  const guchar *str;
  g_printf("Drag Handle\n");
  str=gtk_button_get_label(GTK_BUTTON(widget));
  
  gtk_selection_data_set (selection_data,
			  gdk_atom_intern ("STRING", FALSE),
			  8,(guchar *)str,strlen(str));

  /*
    gtk_selection_data_set          (GtkSelectionData *selection_data,
    GdkAtom type,
    gint format,
    const guchar *data,
    gint length);
  */
  
}

gboolean  entry_drop_handle (GtkWidget        *widget,
			     GdkDragContext   *context,
			     gint              x,
			     gint              y,
			     GtkSelectionData *selection_data,
			     guint             info,
			     guint             time,
			     gpointer          data)
{
  g_printf("Drop Handle\n");
  gchar *str;
  str=(gchar *)selection_data->data;

  gtk_drag_finish(context,TRUE,FALSE,time);//indicate success.

  gtk_entry_set_text(GTK_ENTRY(widget),str);
  g_printf("%s\n",str);
  return TRUE;
}


GtkWidget *dnd_widget()
{
  GtkWidget *entry,*w,*box,*button;

  static const GtkTargetEntry targets[] = {
    { "UTF8_STRING", 0, 0 },
    { "STRING", 0, 0 },
    { "TEXT",   0, 0 }, 
    { "COMPOUND_TEXT", 0, 0 },
    { "text/plain",0,0},
    { "text/uri-list",0,1}};
    
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);

  entry=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(entry),200);
  button=gtk_button_new_with_label("Drag this. Select."); 

  gtk_container_add(GTK_CONTAINER(w),box); 
  gtk_box_pack_start_defaults(GTK_BOX(box),entry);
  gtk_box_pack_start(GTK_BOX(box),  button,FALSE,FALSE,0);
    

  gtk_widget_add_events (button,
                         GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK |
			 GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK|
			 GDK_BUTTON1_MASK | GDK_BUTTON3_MASK);
    
  gtk_drag_source_set(  button,
		      GDK_BUTTON1_MASK | GDK_BUTTON3_MASK,
		      targets,6,
		      GDK_ACTION_COPY);

  gtk_drag_dest_set (entry,
		     GTK_DEST_DEFAULT_HIGHLIGHT |
		     GTK_DEST_DEFAULT_MOTION |
		     GTK_DEST_DEFAULT_DROP,
		     targets, 6,
		     GDK_ACTION_COPY);

  g_signal_connect (button,
		    "drag_begin",
		    G_CALLBACK (btn_drag_begin),
		    NULL);

  g_signal_connect (button,
		    "drag_end",
		    G_CALLBACK (btn_drag_end),
		    NULL);


  g_signal_connect (button,
		    "drag_data_set",
		    G_CALLBACK (btn_drag_handle),
		    NULL);


  g_signal_connect (entry,
		    "drag_data_received",
		    G_CALLBACK (entry_drop_handle),
		    NULL);
    
  //no need for other draggers.

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_window_set_title(GTK_WINDOW(w),"Drag & Drop");
  gtk_window_resize(GTK_WINDOW(w),200,40);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=dnd_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}



//gcc -Wall dnd_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o dnd
