#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>


const gchar* font_select(void)
{
  GtkWidget* fs;
  gchar *fontname;
  fs=gtk_font_selection_dialog_new("Choose A Font");
  if(gtk_dialog_run(GTK_DIALOG(fs))==GTK_RESPONSE_OK)
    {
      fontname=gtk_font_selection_dialog_get_font_name(GTK_FONT_SELECTION_DIALOG(fs));
      g_message(fontname);
    }
  else
    fontname=NULL;

  gtk_widget_destroy(fs);
  return fontname;
}

gboolean change_font(GtkWidget *w,gpointer data)
{
  PangoFontDescription *font_desc;
  const gchar *font_str=NULL;
  GtkWidget *text=(GtkWidget *)data;
  
  font_str=font_select();//get font name.
  if(font_str!=NULL)
    {
      font_desc=pango_font_description_from_string(font_str);
      gtk_widget_modify_font(text,font_desc);
      pango_font_description_free(font_desc);
    }

  return TRUE;
}

GtkWidget *font_widget()
{
  GtkWidget *text,*w,*box,*button;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);
  button=gtk_button_new_with_label("Change Font");

  text=gtk_text_view_new();

  gtk_container_add(GTK_CONTAINER(w),box);

  text->allocation.width=200;
  text->allocation.height=200;

  button=gtk_button_new_with_label("Font Select");  
  gtk_box_pack_start_defaults(GTK_BOX(box),text);
  gtk_box_pack_start(GTK_BOX(box),button,FALSE,FALSE,0);

  gtk_widget_show_all(box);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(button),"clicked",GTK_SIGNAL_FUNC(change_font),text);
  gtk_window_set_title(GTK_WINDOW(w),"Font Select");
  gtk_window_resize(GTK_WINDOW(w),200,100);

  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=font_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}


//gcc -Wall fontdialog_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o fontdialog
