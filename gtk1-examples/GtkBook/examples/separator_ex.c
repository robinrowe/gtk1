#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


GtkWidget* separator_demo(void)
{
  GtkWidget *menu;
  GtkWidget *it,*sm;
  GtkWidget *b;

  
  menu=gtk_menu_bar_new();

  sm=gtk_menu_new();
  it=gtk_menu_item_new_with_mnemonic("_Open");
  gtk_menu_shell_append(GTK_MENU_SHELL(sm),it);

  it=gtk_separator_menu_item_new();
  gtk_menu_shell_append(GTK_MENU_SHELL(sm),it);

  it=gtk_menu_item_new_with_mnemonic("_Exit");
  gtk_menu_shell_append(GTK_MENU_SHELL(sm),it);
  gtk_signal_connect(GTK_OBJECT(it),"activate",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  it=gtk_menu_item_new_with_mnemonic("_File"); 
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(it),(sm)); 
  gtk_menu_shell_append(GTK_MENU_SHELL(menu),it);
  b=gtk_hbox_new(FALSE,FALSE);
  gtk_box_pack_start_defaults(GTK_BOX(b),menu);
  
  gtk_box_pack_start_defaults(GTK_BOX(b),gtk_vseparator_new());
  gtk_box_pack_start_defaults(GTK_BOX(b),gtk_calendar_new()); //yikes!
  
  return b;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;
  //  int i=0;

  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);


  gtk_container_add(GTK_CONTAINER(w),
		    separator_demo());

  gtk_signal_connect(GTK_OBJECT(w),
		     "destroy",
		     GTK_SIGNAL_FUNC(gtk_main_quit),
		     NULL);


  gtk_window_set_title(GTK_WINDOW(w),
		       "Hscale Vscale Hruler Vruler.!");

  gtk_window_set_default_size(GTK_WINDOW(w),
			      200,
			      40);
  gtk_widget_show_all(w);
  
  gtk_main();

  return 0;
}




//gcc -Wall separator_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o separator
//export PKG_CONFIG_PATH="/usr/lib/pkgconfig/"
