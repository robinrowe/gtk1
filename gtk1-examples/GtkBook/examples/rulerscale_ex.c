#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


GtkWidget* ruler_scale_demo(void)
{
  GtkWidget *r,*s,*b;
  GtkObject *adj;

  b=gtk_hbox_new(FALSE,FALSE);

  //make a ruler

  s=gtk_hruler_new();
  gtk_ruler_set_range(GTK_RULER(s),
		      0,
		      1.0,
		      0,
		      0.01);


  //scale
  adj=gtk_adjustment_new(0,0,1,0.1,0.2,0);
  r=gtk_vscale_new(GTK_ADJUSTMENT(adj));

  gtk_box_pack_start_defaults(GTK_BOX(b),s);
  gtk_box_pack_start_defaults(GTK_BOX(b),r);
  
  return b;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;
  //  int i=0;

  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_container_add(GTK_CONTAINER(w),
		    ruler_scale_demo());

  gtk_signal_connect(GTK_OBJECT(w),
		     "destroy",
		     GTK_SIGNAL_FUNC(gtk_main_quit),
		     NULL);


  gtk_window_set_title(GTK_WINDOW(w),
		       " Hruler Vscale");

  gtk_window_set_default_size(GTK_WINDOW(w),
			      200,
			      40);
  gtk_widget_show_all(w);
  
  gtk_main();

  return 0;
}




//gcc -Wall rulerscale_ex.c `pkg-config libgnomeui-2.0 --cflags --libs`
//export PKG_CONFIG_PATH="/usr/lib/pkgconfig/"
