#include <gtk/gtk.h>

int
main (int argc, char **argv)
{
  GtkWidget     *win, *box,*tb;
  GtkIconSize sz;
  gtk_init( &argc, &argv );

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(0,0);
  
  tb=gtk_toolbar_new();
  sz=gtk_icon_size_register("muthus-new-size",50,50);
  gtk_toolbar_set_tooltips(GTK_TOOLBAR(tb),TRUE);
  //gtk_toolbar_set_style(GTK_TOOLBAR(tb),  GTK_TOOLBAR_SPACE_LINE);
  gtk_toolbar_set_orientation(GTK_TOOLBAR(tb),  GTK_ORIENTATION_HORIZONTAL);
  gtk_toolbar_set_icon_size(GTK_TOOLBAR(tb),sz);
  {
    int i=-1;
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_NEW,"Create New File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_OPEN,"Open a File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_SAVE,"Save File",NULL, NULL, NULL,i);
    gtk_toolbar_insert_stock    (GTK_TOOLBAR(tb),
				 GTK_STOCK_EXECUTE,"run File",NULL, NULL, NULL,i);
  }


  gtk_box_pack_end_defaults(GTK_BOX(box),gtk_label_new("Last Line"));
  gtk_box_pack_end_defaults(GTK_BOX(box),tb);
			     
  gtk_container_add (GTK_CONTAINER (win), box);
  g_signal_connect (GTK_OBJECT(win), "destroy",G_CALLBACK (gtk_main_quit), NULL);

  gtk_container_set_border_width(GTK_CONTAINER(win),10);
  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}
//gcc -Wall toolbar_ex.c -o toolb `pkg-config gtk+-2.0 --cflags --libs`
