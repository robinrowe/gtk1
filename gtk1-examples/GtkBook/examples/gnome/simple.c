#include<libgtkhtml/gtkhtml.h>
#include<curl/curl.h>


#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>

struct MemoryStruct {
  char *memory;
  size_t size;
};

size_t
WriteMemoryCallback(void *ptr, size_t size, size_t nmemb, void *data)
{
  register int realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)data;
  
  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory) {
    memcpy(&(mem->memory[mem->size]), ptr, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
  }
  return realsize;
}


void load_url(HtmlDocument *doc,const gchar *url, HtmlStream *stream);
void click_url(HtmlDocument *doc,const gchar *url);

  GtkWidget *html_view;
  HtmlDocument *html_doc;


int main(int argc,char **argv)
{
  GtkWidget *win;

  gtk_init(NULL,NULL);

  html_doc=html_document_new();
  html_document_open_stream(html_doc,"text/html");
  html_document_write_stream(html_doc, "<html><head></head><body>Hello, Girls!<br><a href=\"http://www.nitt.edu/\">click me</a></body></html>",-1);
  html_document_close_stream(html_doc);


  g_signal_connect(html_doc,"request_url",G_CALLBACK(load_url),NULL);
  g_signal_connect(html_doc,"link_clicked",G_CALLBACK(click_url),NULL);
  

  html_view=html_view_new();  
  html_view_set_document(HTML_VIEW(html_view),html_doc);
  


  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(win),"  HTML rendered  ");
  gtk_window_set_position(GTK_WINDOW(win),GTK_WIN_POS_MOUSE);
  gtk_container_add(GTK_CONTAINER(win),html_view);  

  g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  gtk_widget_show_all(win);

  g_message("Complied on "  __TIME__ " and  " __DATE__ "\n");

  gtk_main();
  return 0;
}

void load_url(HtmlDocument *doc,const gchar *url, HtmlStream *stream)
{
  g_message("Load url %s",url);
}
void click_url(HtmlDocument *doc,const gchar *url)
{

  g_message("Clicked url %s",url);
  

  html_document_open_stream(doc,"text/html");
  html_document_write_stream(doc,"<html><head><title>Muthus Crack</title></head><body>\
<h1>Hello World!</h1></body></html>",87);
  html_document_close_stream(doc);

  html_view_set_document(HTML_VIEW(html_view),html_doc);

  g_assert(doc!=NULL);
  
  g_message("html_document written!");
}

/*

*/

/* gcc -Wall simple.c `pkg-config --cflags --libs libgnomeui-2.0 libgtkhtml-2.0` */
