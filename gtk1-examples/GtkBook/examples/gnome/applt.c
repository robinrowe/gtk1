#include<gnome.h>
#include<panel-2.0/panel-applet.h>

gboolean create_applet_fcn(PanelApplet *applet,
			   const char *iid,
			   gpointer data)
{
  GtkWidget *l;

  if(!strcmp(iid,"OAFIID:MyApplet"))
    {
      g_message("Activated ...Panel Applet!\n");
      l=gtk_label_new("Hello World");
      gtk_container_add(GTK_CONTAINER(applet),l);
      return TRUE;
    }
  else
    g_message("Passed me with this :%s !",iid);

    return FALSE;
}


PANEL_APPLET_BONOBO_FACTORY("OAFIID:MyApplet",PANEL_TYPE_APPLET ,"MyApplet","1.2",create_applet_fcn,NULL);
/* gcc applt.c `pkg-config --cflags --libs libgnomeui-2.0 libpanelapplet-2.0` -o applt .*/
/* ./applt --oaf-activate-iid=OAFIID:MyApplet --oaf-ior-fd=1  */

