#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>



void paint_picture(GdkPixbuf *pb,gboolean random_flag)
{ 
  int temp=0;
  int ht,wt;
  int i=0,j=0;
  int rowstride=0;  
  int bpp=0;
  gchar *pixel;


  if(gdk_pixbuf_get_bits_per_sample(pb)!=8)   //we handle only 24 bit images.
  	return;                               //look at 3 bytes per pixel.

  bpp=3;	         	  //getting attributes of height,
  ht=gdk_pixbuf_get_height(pb);   //width, and bpp.Also get pointer
  wt=gdk_pixbuf_get_width(pb);	  //to pixels.
  pixel=gdk_pixbuf_get_pixels(pb);
  rowstride=wt*bpp;
  

  srand(time(NULL));

  for(i=0;i<ht;i++)		//iterate over the height of image.
    for(j=0;j<rowstride;j+=bpp)   //read every pixel in the row.skip
				//bpp bytes 
      {	
      
      //access pixel[i][j] as
      // pixel[i*rowstride + j]

      //access red,green and blue as
	if(random_flag == FALSE)
	  {
	    pixel[i*rowstride+ j + 0]=((float)(j+i)/200.0)*255; //R
	    pixel[i*rowstride+ j + 1]=((float)((i)/200.0)*255); //G
	    pixel[i*rowstride+ j + 2]=((float)(i-j)/400.0)*255; //B
	  }
	else
	  {
	    temp=random()%255;
	    pixel[i*rowstride+ j + 0]=temp;
	    pixel[i*rowstride+ j + 1]=temp^(temp+1);
	    pixel[i*rowstride+ j + 2]=temp%random();
	  }
      }  
  return;
}

GtkWidget * make_bgwidget(void)
{
  GtkWidget *w;
  GdkPixbuf *pic;
  GdkPixmap *pix;
  GdkColormap *cmap;
  guint wt=0,ht=0;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  pic=gdk_pixbuf_new_from_file("/usr/share/wallpapers/triplegears.jpg",NULL);
  

  if(pic==NULL)
    {
      pic=gdk_pixbuf_new(GDK_COLORSPACE_RGB,FALSE,8,200,200);
      paint_picture(pic,FALSE);
    }

  wt=gdk_pixbuf_get_width(pic);
  ht=gdk_pixbuf_get_height(pic);

  gtk_widget_show(w);
  cmap=gdk_colormap_get_system();

  pix=gdk_pixmap_new(NULL,wt,ht,cmap->visual->depth);
  gdk_drawable_set_colormap(pix,cmap);
  gdk_draw_pixbuf(pix,w->style->fg_gc[GTK_STATE_NORMAL],
		   pic,0,0,0,0,wt,ht,
		   GDK_RGB_DITHER_NONE,0,0);

  w->style->bg_pixmap[GTK_STATE_NORMAL]=pix;

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_window_set_title(GTK_WINDOW(w),"BackGround Picture.");
  gtk_container_add(GTK_CONTAINER(w),gtk_color_selection_new());

  gtk_window_set_default_size(GTK_WINDOW(w),wt,ht);  
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=make_bgwidget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall bgpicture_ex.c  `pkg-config libgnomeui-2.0 --cflags --libs` -o bg
