#include<gtk/gtk.h>

int main() //(int argc,char *argv[])
{
  GtkWidget *win;
  GtkWidget *btn;

  gtk_init(NULL,NULL);

  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  btn=gtk_button_new_with_label("   Hello World      ");
  gtk_button_set_use_underline(GTK_BUTTON(btn),TRUE);

  gtk_window_set_title(GTK_WINDOW(win),"  Hello World  ");
  gtk_window_set_position(GTK_WINDOW(win),GTK_WIN_POS_MOUSE);
  gtk_container_add(GTK_CONTAINER(win),btn);  

  g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_signal_connect(G_OBJECT(btn),"clicked",G_CALLBACK(gtk_main_quit),NULL);

  gtk_widget_show_all(win);

  gtk_main();
  return 0;
}
//gcc -Wall hello.c -o hello `pkg-config gtk+-2.0 --cflags --libs`
