#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>




GtkWidget * icon_widget(void)
{
  GtkWidget *w;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_window_set_icon_from_file(GTK_WINDOW(w),
				"/usr/share/pixmaps/gnome-emacs.png",
				NULL);

  gtk_container_add(GTK_CONTAINER(w),gtk_color_selection_new());
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=icon_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall miniicon_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o miniicon
