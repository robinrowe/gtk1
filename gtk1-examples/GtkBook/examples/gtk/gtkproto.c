#include<gtk/gtk.h>

int menu_for_pop(GtkWidget *w,GdkEventButton *e)
{
  g_print("Im called! at %g %g\n",e->x,e->y);
  switch(e->type)
    {
    case GDK_3BUTTON_PRESS://3ple click
      g_print(" GDK_3BUTTON_PRESS:\n");
      break;
    case GDK_2BUTTON_PRESS://double click
      g_print(" GDK_2BUTTON_PRESS:\n");
      break;
    case GDK_BUTTON_PRESS://single click
      g_print(" GDK_BUTTON_PRESS:\n");
      break;
    }
  return 0;
}

int main(int argc,char *argv[])
{
  GtkWidget *w,*x,*tv,*togglebtn;
  GtkTextBuffer *tb;
  GtkWidget *c;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  x=gtk_vbox_new(TRUE,TRUE);
  tb=gtk_text_buffer_new(NULL);
  tv=gtk_text_view_new_with_buffer(tb);
  c=gtk_event_box_new();
  togglebtn=gtk_toggle_button_new_with_mnemonic("<control>X");
  
  gtk_container_add(GTK_CONTAINER(w),x);
  gtk_box_pack_start_defaults(GTK_BOX(x),tv);
  gtk_box_pack_start_defaults(GTK_BOX(x),c);
  gtk_box_pack_start_defaults(GTK_BOX(x),togglebtn);
  gtk_widget_set_events(c,GDK_2BUTTON_PRESS );

  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_signal_connect(G_OBJECT(c),"button_press_event",G_CALLBACK(menu_for_pop),NULL);

  gtk_window_resize(GTK_WINDOW(w),100,100);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}
//gcc gtkproto.c `pkg-config --cflags --libs gtk+-2.0`
