#include <gtk/gtk.h>
#include "gtkgraf.h"

int
main(int argc,char **argv)
{
  GtkWidget *w;
  GtkWidget *g;

  gtk_init(&argc,&argv);

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g=gtk_graf_new();
  gtk_widget_set_size_request(g,400,400);
  
  gtk_container_add(GTK_CONTAINER(w),g);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}
