#include<gtk/gtk.h>

void  get_screen_shot(GtkWidget *w);

int main(int argc,char *argv[])
{
  GtkWidget *w;
  GtkWidget *cal,*vb,*tv;
  //  GdkColor red={0xff0000,0xff,0xff,0xff},green={0xff,0,0,0xff};

  gtk_init(&argc,&argv);

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  cal=gtk_calendar_new();
  vb=gtk_vbox_new(TRUE,TRUE);

  tv=gtk_entry_new();
  gtk_box_pack_start_defaults(GTK_BOX(vb),cal);
  gtk_box_pack_start_defaults(GTK_BOX(vb),tv);
  
  gtk_entry_append_text(GTK_ENTRY(tv),
		  "Muthiah Annmalai is an Egoist!" );
  gtk_container_add(GTK_CONTAINER(w),vb);
  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_signal_connect(G_OBJECT(w),"key_press_event",G_CALLBACK(get_screen_shot),NULL);
  gtk_widget_set_size_request(w,200,200);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}


void  get_screen_shot(GtkWidget *w)
{ 
  GdkPixbuf *p;
  GtkWidget *d,*i;
  
  p=gdk_pixbuf_get_from_drawable(NULL,
				 w->window,
				 gtk_widget_get_colormap(w),
				 0,0,
				 0,0,
				 200,
				 200);
  if(p==NULL)
    return;

  gdk_pixbuf_save(p,
		  "ps2.png",
		  "png",
		  NULL,NULL);

  d=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  i=gtk_image_new_from_pixbuf(p);
  gtk_container_add(GTK_CONTAINER(d),i);
  gtk_widget_show_all(d);

  gdk_pixbuf_unref(p);
  return;
}

//gcc -Wall -o gn1 gn1.c `pkg-config gtk+-2.0 --cflags --libs`
//Anachronisms..
//We are working with source incompatible GNOME & GTK libraries..
//Why the hell is it?
