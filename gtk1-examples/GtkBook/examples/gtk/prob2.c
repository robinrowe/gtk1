#include<gtk/gtk.h>

int main(int argc,char *argv[])
{
  GtkWidget *w,*t;
  
  GtkTreeIter ti,top;
  GtkTreeStore *ts;
  GtkTreeView *tv;
  GtkTreeViewColumn *tvc;
  GtkCellRenderer *cl;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  ts=gtk_tree_store_new(2,G_TYPE_STRING,G_TYPE_STRING);

  gtk_tree_store_append(ts,&top,NULL);
  gtk_tree_store_set(ts,&top, 0,"Hello",1,"World",-1);

  gtk_tree_store_append(ts,&ti,&top);
  gtk_tree_store_set(ts,&ti, 0,"C ",1,"C++",-1);

  gtk_tree_store_append(ts,&ti,&top);
  gtk_tree_store_set(ts,&top, 0,"Python ",1,"Lisp",-1);

  gtk_tree_store_append(ts,&top,&top);
  gtk_tree_store_set(ts,&top, 0,"Perl ",1,"LOGO",-1);

  tv=GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(ts)));

  cl=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Col 1",
					       cl,
					       "text",
					       0,
					       NULL);

  gtk_tree_view_append_column(tv,tvc);


  cl=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Col 2",
	 				       cl,
					       "text",
 					       1,
					       NULL);
  gtk_tree_view_append_column(tv,tvc);


  gtk_container_add(GTK_CONTAINER(w),GTK_WIDGET(tv));  

  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}

//gcc prob2.c `pkg-config --cflags --libs gtk+-2.0` 

