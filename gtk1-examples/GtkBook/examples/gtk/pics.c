#include<gtk/gtk.h>


gboolean  load_next_pic(GtkWidget *w,GdkEventButton data);

int main(int argc,char *argv[])
{
  GtkWidget *w,*i,*img;
  GdkBitmap *bmap=NULL;
  GdkPixmap *pm;
  GdkPixbuf *pb;
  int wt,ht;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  wt=0;ht=0;

  pb=gdk_pixbuf_new_from_file("/usr/share/pixmaps/emacs.png",NULL);
  wt=gdk_pixbuf_get_width(pb);
  ht=gdk_pixbuf_get_height(pb);


  bmap=gdk_pixmap_new(w->window,wt,ht,1);
  pm=gdk_pixmap_new(w->window,wt,ht,-1);

  gdk_pixbuf_render_threshold_alpha(pb,bmap,0,0,0,0,wt,ht,0xff);
  gdk_pixbuf_render_to_drawable_alpha(pb,pm,0,0,0,0,wt,ht,
				      GDK_PIXBUF_ALPHA_FULL,0xff,0,0,0);

  if(gdk_pixbuf_get_has_alpha(pb))
    g_message("Has Alpha");

  img=gtk_image_new_from_pixmap(pm,bmap);
  gtk_widget_show(w);
  gtk_container_add(GTK_CONTAINER(w),img);

  /*
    gdk_window_shape_combine_mask(img->window,
				bmap,0,0);
  */

  gtk_widget_show(img);

  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_object_unref(pb);


  //  gtk_widget_set_size_request(w,wt,ht);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}
//gcc pics.c -o pics `pkg-config --cflags --libs gtk+-2.0`

