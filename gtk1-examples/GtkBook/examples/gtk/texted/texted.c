/*
    Texted a Gtk Demonstration of Textediting.
    (C) 2003 Muthiah Annamalai.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

//    But not from the software engineering perspective,
//    is it good.


#include<gtk/gtk.h>
#include<string.h>
#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<unistd.h>
#include<errno.h>

GtkWidget *text;		//The Text Area.
GtkWidget *textbuffer;
GtkWidget *sw;			//scrolled window
GtkTextIter *ti;
int result;
char zero_char = 0;

// Code for a single threaded Text Editor:
//Open,Close,Save.New,Exit.


gboolean fileopen;		//Status of a file
gboolean newfile;		//Status of Newness
gboolean filesaved;		//Staus of Saved
gchar filename[256];		//filename
int err;
int count;
char buffer[1024];
char temp[1024];		//never depend on the states of temp.


int save_cb (GtkWidget * w, gpointer data);
void
print_status (void)
{
  g_print ("\nIn function: print_status\n");

  g_print ("File Open :");
  (fileopen == TRUE) ? g_print ("TRUE") : g_print ("FALSE");

  g_print ("\tFile New :");
  (newfile == TRUE) ? g_print ("TRUE") : g_print ("FALSE");

  g_print ("\tFile Saved :");
  (filesaved == TRUE) ? g_print ("TRUE\n") : g_print ("FALSE\n");
}

int
text_insert (GtkTextView * tb, gchar * s)
{
  print_status ();
  g_print ("Text Inserted");
  filesaved = FALSE;
  return TRUE;
}

int
edited (GtkTextView * tv, GdkEventKey * event)
{
  //  print_status();
  //  g_print("In the function: edited\n");
  //  g_print("The character string was %s \n",event->string);
  //  g_print("Hardware Kay Code=%d   Key Val=%d\n",event->hardware_keycode,event->keyval);
  filesaved = FALSE;
  return TRUE;
}

int
exit_last_cb (GtkWidget * w, gpointer data)
{
  print_status ();
  g_print ("In function:exit_last_cb\n");
  gtk_main_quit ();		//Bye Dear ;.....
  return FALSE;
}

int
save_last_cb (GtkWidget * w, gpointer data)
{
  print_status ();
  g_print ("in function : save_last_cb\n");
  save_cb (w, data);
  gtk_widget_destroy (w);
  return TRUE;
}

int
cancel_last_cb (GtkWidget * w, gpointer data)
{
  print_status ();
  g_print ("in function cance_last_cb\n");
  gtk_widget_destroy (w);
  return TRUE;			//do nothing ?
}


void
popup_dialog_question (char *q1, char *q2, char *q3)
{
  print_status ();
  GtkWidget *w;
  GtkWidget *button;
  GtkWidget *box, *vbox;


  g_print ("In function:popup_dialog_question \n");

  w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  box = gtk_hbox_new (TRUE, TRUE);
  vbox = gtk_vbox_new (TRUE, TRUE);

  button = gtk_button_new_from_stock (GTK_STOCK_SAVE);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (save_last_cb), GTK_OBJECT (w));
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);

  button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (cancel_last_cb),
			     GTK_OBJECT (w));
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);

  button = gtk_button_new_from_stock (GTK_STOCK_QUIT);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (exit_last_cb), GTK_OBJECT (w));
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);

  gtk_container_add (GTK_CONTAINER (vbox), box);

  box = gtk_hbox_new (TRUE, TRUE);
  button = gtk_label_new (q1);
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);
  button = gtk_label_new (q2);
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);
  button = gtk_label_new (q3);
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 5);
  gtk_container_add (GTK_CONTAINER (vbox), box);

  gtk_container_add (GTK_CONTAINER (w), vbox);
  gtk_window_set_modal (GTK_WINDOW (w), TRUE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (w), TRUE);
  gtk_window_set_position (GTK_WINDOW (w), GTK_WIN_POS_CENTER_ON_PARENT);
  gtk_widget_show_all (w);
}

int
kill_popup (GtkWidget * w, gpointer data)
{
  print_status ();
  g_print ("In Function: kill_popup\n");
  gtk_widget_destroy (w);
  return TRUE;
}


void
popup_dialog (char *data)
{
  print_status ();
  GtkWidget *w;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *table;

  w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  table = gtk_table_new (4, 4, TRUE);
  button = gtk_button_new_from_stock (GTK_STOCK_DIALOG_WARNING);
  label = gtk_label_new (data);


  gtk_window_set_modal (GTK_WINDOW (w), TRUE);
  gtk_window_set_position (GTK_WINDOW (w), GTK_WIN_POS_CENTER_ALWAYS);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 4, 0, 2);
  gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 4, 2, 4);
  gtk_container_add (GTK_CONTAINER (w), table);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (w));
  gtk_widget_show_all (w);
  g_print ("This is Function: popup_dialog\n");
}

int
get_filename_open (GtkWidget * w, gpointer data)
{
  gchar **name;
  int fp;
  g_print ("In Function: get_filename\n");
  print_status ();
  name = gtk_file_selection_get_selections (GTK_FILE_SELECTION (w));
  strcpy (filename, *name);
  g_print ("Selected Filename was %s\n", filename);
  gtk_widget_destroy (w);

  fp = open (filename, O_RDONLY);

  if (fp != -1)
    {
      gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (textbuffer), ti);
      while ((err = read (fp, buffer, 1023)) > 0)
	{
	  count =
	    gtk_text_buffer_get_char_count (GTK_TEXT_BUFFER (textbuffer));
	  count = (count > 0) ? count : -count;
	  g_print ("Text Length Present=%d Read Length=%d\n", count, err);

	  //g_print("The data was %s  \n",buffer);
	  //gtk_text_buffer_set_text(GTK_TEXT_BUFFER(textbuffer),
	  //buffer,err);//deletes old!!

	  memcpy (temp, &zero_char, 1023);
	  strncpy (temp, buffer, err);
	  //      gtk_text_iter_set_offset(ti,count);

	  gtk_text_buffer_insert (GTK_TEXT_BUFFER (textbuffer), ti, temp,
				  err);
	}
      gtk_text_view_set_buffer (GTK_TEXT_VIEW (text),
				GTK_TEXT_BUFFER (textbuffer));
      close (fp);

      //check error status.
      if (errno != 0)
	{
	  g_print ("Error Opening A File\n");
	  popup_dialog ("Cannot Read/Open  File:Permission Denied");
	}
      else
	{
	  g_print ("Opened & Read the File \n");

	  //Change State:
	  filesaved = TRUE;
	  newfile = FALSE;
	  fileopen = TRUE;
	}
    }
  else
    {
      popup_dialog ("File Not Found: File does not exist");
    }
  return TRUE;
}


void
ask_last_status (void)
{
  //popup a dialog box and ask him if we got to save
  //it or discard
  //file we have edited.  
  g_print ("This is Function: ask_last_status\n");
  print_status ();
  if ((fileopen == TRUE) && (filesaved != TRUE))
    {
      popup_dialog_question ("Save", "Cancel", "Exit Anyway");
    }
  else
    gtk_main_quit ();		//because the file is closed dear!
  //continue with the proceedings
}

int
get_filename_save (GtkWidget * w, gpointer data)
{
  char **name;
  int fp;
  GtkTextIter *tiend;

  tiend = g_malloc (sizeof (GtkTextIter));

  g_print ("This is Function: get_filename_save\n");
  print_status ();
  name = gtk_file_selection_get_selections (GTK_FILE_SELECTION (w));
  //  gtk_window_set_title(GTK_WINDOW(&GTK_FILE_SELECTION(name)->parent_instance.window)\
  //                   ,"Save your File Under The Name");
  strcpy (filename, *name);
  g_free (*name);

  g_print ("File Name To be Saved Was %s\n", filename);
  fp = open (filename, O_CREAT | O_WRONLY, S_IWRITE | S_IREAD | S_IEXEC);	//WE ARE COPYING DATA FROM THE BUFFER To FILE  

  g_print ("Got the values of \"open\" call as %d\n", fp);
  if (fp != -1)			//a valid file descriptor
    {
      g_print ("Created The File %s Successfully!\n", filename);
      //      textbuffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
      gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (textbuffer), ti);
      gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (textbuffer), tiend);
      *name =
	gtk_text_buffer_get_text (GTK_TEXT_BUFFER (textbuffer), ti,
				  tiend, TRUE);
      //Now all our data is in *name; We have to dump it into a single file.
      //      count=gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(textbuffer));
      count = strlen (*name);
      g_print ("Data to write was %s \n Length = %d\n", *name, count);
      err = 0;
      while (count > 0)
	{
	  err = write (fp, *name, count);
	  if (err <= 0)
	    break;
	  *name = *name + err;
	  g_print ("written %d bytes;count =%d \n", err, count);
	  count -= err;
	}
      close (fp);

      //Change the state
      fileopen = TRUE;
      filesaved = TRUE;
      newfile = FALSE;		//'cos weve saved it dude!
      popup_dialog ("File Saved");
    }
  else
    {
      perror (strerror (errno));
      popup_dialog ("Cannot Open A File to Write!\n");
      g_print ("Error in function: get_filename_save\n");
      fileopen = TRUE;
      filesaved = FALSE;
      newfile = newfile;	//earlier status
    }
  gtk_widget_destroy (w);
  return TRUE;
}

int
open_cb (GtkWidget * w, gpointer data)
{
  GtkWidget *filsel;

  g_print ("This is Function: open_cb\n");
  print_status ();
  if (fileopen != TRUE)
    {
      //then "open" some file
      filsel = gtk_file_selection_new ("Select a File to Open");
      gtk_signal_connect_object (GTK_OBJECT
				 (GTK_FILE_SELECTION (filsel)->ok_button),
				 "clicked",
				 GTK_SIGNAL_FUNC (get_filename_open),
				 GTK_OBJECT (filsel));
      gtk_signal_connect_object (GTK_OBJECT
				 (GTK_FILE_SELECTION (filsel)->
				  cancel_button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (filsel));
      gtk_window_set_modal (GTK_WINDOW
			    (&
			     (GTK_FILE_SELECTION (filsel)->parent_instance.
			      window)), TRUE);
      gtk_window_set_destroy_with_parent (GTK_WINDOW
					  (&
					   (GTK_FILE_SELECTION (filsel)->
					    parent_instance.window)), TRUE);
      gtk_widget_show (filsel);
      g_print ("Exiting :open_cb\n");
    }
  else
    {
      popup_dialog ("Close this file First!");
    }
  return TRUE;
}

int
close_cb (GtkWidget * w, gpointer data)
{
  g_print ("This is Function: close_cb\n");
  print_status ();
  if (fileopen == TRUE)
    {
      if (filesaved == TRUE)
	{
	  //close the file if it is saved:Simply clean the buffer.
	  memcpy (temp, &zero_char, 1024);
	  temp[0] = ' ';
	  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (textbuffer), temp, 1);
	  gtk_text_view_set_buffer (GTK_TEXT_VIEW (text),
				    GTK_TEXT_BUFFER (textbuffer));

	  //change the status.
	  fileopen = FALSE;	//no file is open!!
	  filesaved = FALSE;	//The file willnot be saved !!
	  newfile = FALSE;	//The file is not new either!!
	}
      else
	{
	  popup_dialog ("Save the File Before Closing");
	}
    }
  else
    {
      popup_dialog ("There's Nothing Else to Close!!");
    }
  return TRUE;
}

int
new_cb (GtkWidget * w, gpointer data)
{
  g_print ("This is Function: new_cb\n");
  print_status ();
  if (fileopen != TRUE)
    {
      fileopen = TRUE;		//All the data will simply go to the dialogs
      popup_dialog ("A New File has been Created");

      memcpy (temp, &zero_char, 1024);
      temp[0] = ' ';
      gtk_text_buffer_set_text (GTK_TEXT_BUFFER (textbuffer), temp, 1);
      gtk_text_view_set_buffer (GTK_TEXT_VIEW (text),
				GTK_TEXT_BUFFER (textbuffer));
    }
  else
    {
      popup_dialog ("Close this file First!");
    }
  return TRUE;
}

int
save_cb (GtkWidget * w, gpointer data)
{
  GtkWidget *filsel;
  g_print ("This is Function: save_cb\n");
  print_status ();
  if ((fileopen == TRUE) && (filesaved != TRUE))
    {
      //ask for the filename & then save it
      //then "open" some file
      filsel = gtk_file_selection_new ("Save As");
      gtk_signal_connect_object (GTK_OBJECT
				 (GTK_FILE_SELECTION (filsel)->ok_button),
				 "clicked",
				 GTK_SIGNAL_FUNC (get_filename_save),
				 GTK_OBJECT (filsel));
      gtk_signal_connect_object (GTK_OBJECT
				 (GTK_FILE_SELECTION (filsel)->
				  cancel_button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (filsel));
      gtk_window_set_modal (GTK_WINDOW
			    (&
			     (GTK_FILE_SELECTION (filsel)->parent_instance.
			      window)), TRUE);
      //      gtk_window_set_destroy_with_parent(GTK_WINDOW( &(GTK_FILE_SELECTION(filsel)->parent_instance.window)),TRUE);
      gtk_widget_show_all (filsel);
      g_print ("Exiting :save_cb\n");
    }
  else
    {
      popup_dialog ("Buffer UnChanged :Nothing to save");
    }
  return TRUE;
}

int
exit_program (GtkWidget * w, gpointer data)
{

  g_print ("This is Function: exit_program\n");
  print_status ();
  if (filesaved != TRUE)
    {
      ask_last_status ();	//Incase the guy has forgotten
    }
  else
    {
      gtk_main_quit ();
    }
  return FALSE;
}

int
exitbtn_cb (GtkWidget * w, gpointer data)
{
  g_print ("This is Function: exitbtn_cb\n");
  print_status ();
  exit_program (w, data);
  return TRUE;
}





int
main (int argc, char *argv[])
{
  GtkWidget *w;			//This is the Main Window.
  GtkWidget *save;		//Save Button.
  GtkWidget *open;		//Open Button.
  GtkWidget *new;		//New Button
  GtkWidget *close;		//Close Button
  GtkWidget *exitbtn;		//Exit Button
  GtkWidget *vbox;		//To hold the "FULL" setup...

//GtkWidget *table;//to hold the whole setup
  GtkWidget *hbox;		//to hold the buttons alone.

  ti = g_malloc (sizeof (GtkTextIter));

  //Initialising GTK
  gtk_init (&argc, &argv);

  g_print ("Initialisng GTK .....Please Wait\n");
  //Initialising the Buttons & Text Area.
  vbox = gtk_vbox_new (FALSE, FALSE);

  new = gtk_button_new_from_stock (GTK_STOCK_NEW);
  open = gtk_button_new_from_stock (GTK_STOCK_OPEN);
  close = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
  save = gtk_button_new_from_stock (GTK_STOCK_SAVE);
  exitbtn = gtk_button_new_from_stock (GTK_STOCK_QUIT);

  textbuffer = GTK_TEXT_BUFFER (gtk_text_buffer_new (NULL));
  text = gtk_text_view_new_with_buffer (GTK_TEXT_BUFFER (textbuffer));

  gtk_signal_connect_object (GTK_OBJECT (text), "key_release_event",
			     GTK_SIGNAL_FUNC (edited), NULL);
  // gtk_signal_connect_object(GTK_OBJECT(textbuffer),"paste_clipboard",GTK_SIGNAL_FUNC(edited),NULL);
  // gtk_signal_connect_object(GTK_OBJECT(textbuffer),"cut_clipboard",GTK_SIGNAL_FUNC(edited),NULL);
  // gtk_signal_connect_object(GTK_OBJECT(text),"insert_at_cursor",GTK_SIGNAL_FUNC(text_insert),NULL);
  // gtk_signal_connect_object(GTK_OBJECT(text),"move_cursor",GTK_SIGNAL_FUNC(text_insert),NULL);

  gtk_text_view_set_editable (GTK_TEXT_VIEW (text), TRUE);
  g_print ("Creating Buttons.....\n");

  //Initialising the window & scrolled window
  w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  g_print ("Creating Windows...\n");

  //Defaulting all the STATUS FLAGS to FALSE
  fileopen = FALSE;
  newfile = FALSE;
  filesaved = FALSE;
  memcpy (filename, &zero_char, 256);	//zero filled
  g_print ("Initialising Global variables\n");

  hbox = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), new, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (hbox), open, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (hbox), save, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (hbox), close, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (hbox), exitbtn, TRUE, TRUE, 5);

  g_print ("Building the Boxes..\n");

  // table=gtk_table_new(10,10,TRUE);
  //gtk_table_attach_defaults(GTK_TABLE(table),hbox,0,10,0,1);

  g_print ("Attaching Boxes & Text Areas to tables...\n");

  gtk_container_set_border_width (GTK_CONTAINER (w), 10);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), text);

  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);
  gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 5);

  gtk_container_add (GTK_CONTAINER (w), vbox);
  g_print ("Adding Table to windows\n");

  gtk_window_set_position (GTK_WINDOW (w), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size (GTK_WINDOW (w), 400, 400);
  g_print ("Positioning  the windows.......\n");

  // GTK_TEXT_VIEW(text)->hadjustment=gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(sw));
  // GTK_TEXT_VIEW(text)->vadjustment=gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(sw));
  gtk_signal_connect (GTK_OBJECT (w), "delete_event",
		      GTK_SIGNAL_FUNC (ask_last_status), NULL);
  gtk_signal_connect_object (GTK_OBJECT (open), "clicked",
			     GTK_SIGNAL_FUNC (open_cb), NULL);
  gtk_signal_connect_object (GTK_OBJECT (close), "clicked",
			     GTK_SIGNAL_FUNC (close_cb), NULL);
  gtk_signal_connect_object (GTK_OBJECT (save), "clicked",
			     GTK_SIGNAL_FUNC (save_cb), NULL);
  gtk_signal_connect_object (GTK_OBJECT (exitbtn), "clicked",
			     GTK_SIGNAL_FUNC (exitbtn_cb), NULL);
  gtk_signal_connect_object (GTK_OBJECT (new), "clicked",
			     GTK_SIGNAL_FUNC (new_cb), NULL);

//When will the geese stop cackling ? OCTA looks more like a ...bird sanctuary.

  g_print ("Creating Callbacks..........\n");
  gtk_widget_show_all (w);
  g_print ("Everything over.\n Waiting for orders....\n");
  print_status ();
  gtk_main ();
  return TRUE;

}
