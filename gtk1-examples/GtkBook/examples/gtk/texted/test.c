#include<gtk/gtk.h>

void popup_dialog_question(char *q1,char *q2,char *q3);

int exit_last_cb(GtkWidget *w,gpointer data)
{
  g_print("In function:exit_last_cb\n");
  gtk_main_quit();
  return FALSE;
}

int save_last_cb(GtkWidget *w,gpointer data)
{
  g_print("in function : save_last_cb\n");
  return TRUE;
}

int cancel_last_cb(GtkWidget *w,gpointer data)
{
  g_print("in function cance_last_cb\n");
  return TRUE; //do nothing ?
}


int main(int argc,char *argv[])
{
  gtk_init(&argc,&argv);
  popup_dialog_question("Ok","Cancel","Exit");
  gtk_main();
  return 0;
}
  

void popup_dialog_question(char *q1,char *q2,char *q3)
{
  GtkWidget *w;
  GtkWidget *b1,*b2,*b3,*l1,*l2,*l3;  
  GtkWidget *box,*vbox;
  

  g_print("In function:popup_dialog_question \n");

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_hbox_new(TRUE,TRUE);  
  vbox=gtk_vbox_new(TRUE,TRUE);

  b1=gtk_button_new_from_stock(GTK_STOCK_SAVE);
  gtk_signal_connect_object(GTK_OBJECT(b1),"clicked",GTK_SIGNAL_FUNC(save_last_cb),GTK_OBJECT(w));
  gtk_box_pack_start(GTK_BOX(box),b1,TRUE,TRUE,5);
  g_print("Making Save Button \n");

  b2=gtk_button_new_from_stock(GTK_STOCK_CANCEL);
  gtk_signal_connect_object(GTK_OBJECT(b2),"clicked",GTK_SIGNAL_FUNC(cancel_last_cb),GTK_OBJECT(w)); 
  gtk_box_pack_start(GTK_BOX(box),b2,TRUE,TRUE,5);
  g_print("Making Cancel Button \n");

  b3=gtk_button_new_from_stock(GTK_STOCK_QUIT);
  gtk_signal_connect_object(GTK_OBJECT(b3),"clicked",GTK_SIGNAL_FUNC(exit_last_cb),GTK_OBJECT(w));
  gtk_box_pack_start(GTK_BOX(box),b3,TRUE,TRUE,5);
  g_print("Making Quit Button \n");

  gtk_container_add(GTK_CONTAINER(vbox),box);
  
  box=gtk_hbox_new(TRUE,TRUE);
  l1=gtk_label_new(q1);
  gtk_box_pack_start(GTK_BOX(box),l1,TRUE,TRUE,5);
  l2=gtk_label_new(q2);
  gtk_box_pack_start(GTK_BOX(box),l2,TRUE,TRUE,5);
  l3=gtk_label_new(q3);
  gtk_box_pack_start(GTK_BOX(box),l3,TRUE,TRUE,5);

  gtk_container_add(GTK_CONTAINER(vbox),box);
  
  gtk_container_add(GTK_CONTAINER(w),vbox);
  gtk_window_set_modal(GTK_WINDOW(w),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(w),TRUE);
  gtk_widget_show_all(w);
}

//gcc -o test -Wall test.c `pkg-config --cflags --libs gtk+-2.0`
