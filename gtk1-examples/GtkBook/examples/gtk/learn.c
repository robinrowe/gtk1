#include<gtk/gtk.h>



int main(int argc,char *argv[])
{
  GtkWidget *w,*x,*tv,*togglebtn;
  GtkTextBuffer *tb;
  GtkTextIter ti;
  GtkWidget *c;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  x=gtk_vbox_new(TRUE,TRUE);


  /*
    Powers of TextTags
  */
  tb=gtk_text_buffer_new(NULL);
  gtk_text_buffer_create_tag(tb,"blue","background","#0fc0f0",NULL);  
  gtk_text_buffer_create_tag(tb,"green","background","green",NULL);  
  gtk_text_buffer_create_tag(tb,"red","background","red",NULL);  
  gtk_text_buffer_create_tag(tb,"rom","editable",FALSE,NULL);  
  gtk_text_buffer_create_tag(tb,"sup","rise",10*PANGO_SCALE,NULL);  
  tv=gtk_text_view_new_with_buffer(tb);

  gtk_text_buffer_get_start_iter(tb,&ti);
  gtk_text_buffer_insert_with_tags_by_name(tb,&ti,"Red World",9,"blue",NULL);
  gtk_text_buffer_insert_with_tags_by_name(tb,&ti,"Green World",11,"green",NULL);
  gtk_text_buffer_insert_with_tags_by_name(tb,&ti,"Cant KillMe",11,"red",NULL);
  gtk_text_buffer_insert_with_tags_by_name(tb,&ti,"super scripts",-1,"sup",NULL);
  gtk_text_buffer_insert(tb,&ti,"Hello World ",-1);
  c=gtk_event_box_new();
  togglebtn=gtk_toggle_button_new_with_mnemonic("&X");
  

  gtk_container_add(GTK_CONTAINER(w),x);
  gtk_box_pack_start_defaults(GTK_BOX(x),tv);
  gtk_box_pack_start_defaults(GTK_BOX(x),c);
  gtk_box_pack_start_defaults(GTK_BOX(x),togglebtn);
  gtk_widget_set_events(c,GDK_2BUTTON_PRESS );

  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  gtk_window_resize(GTK_WINDOW(w),100,100);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}
//gcc learn.c `pkg-config --cflags --libs gtk+-2.0`


/*
  Pango Text Attributes:

<span> attributes
font_desc

A font description string, such as "Sans Italic 12"; note that any other span attributes will override this description. So if you have "Sans Italic" and also a style="normal" attribute, you will get Sans normal, not italic.

font_family

A font family name
face

Synonym for font_family

size

Font size in 1000ths of a point, one of the absolute sizes 'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large', or one of the relative sizes 'smaller' or 'larger'.
style

One of 'normal', 'oblique', 'italic'

weight

One of 'ultralight', 'light', 'normal', 'bold', 'ultrabold', 'heavy', or a numeric weight

variant

'normal' or 'smallcaps'

stretch

One of 'ultracondensed', 'extracondensed', 'condensed', 'semicondensed', 'normal', 'semiexpanded', 'expanded', 'extraexpanded', 'ultraexpanded'

foreground

An RGB color specification such as '#00FF00' or a color name such as 'red'

background

An RGB color specification such as '#00FF00' or a color name such as 'red'

underline
One of 'single', 'double', 'low', 'none'

rise
Vertical displacement, in 10000ths of an em. Can be negative for subscript, positive for superscript.

strikethrough
'true' or 'false' whether to strike through the text 
strikethrough'true'or'false'whethertostrikethroughthetextstrikethrough'true'or'false'whethertostrikethroughthetext
lang



*/
