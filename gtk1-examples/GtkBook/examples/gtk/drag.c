#include<gtk/gtk.h>


int main(int argc,char *argv[])
{
  GtkWidget *w,*c;
  GtkTargetEntry target;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  c=gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(w),c);
    

  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  g_signal_connect(G_OBJECT(c),"event",G_CALLBACK(my_dnd),NULL);

  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}

//gcc drag.c `pkg-config --cflags --libs gtk+-2.0`

