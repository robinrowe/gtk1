#include<gtk/gtk.h>
#include<unistd.h>

// Cookie Cutter.
/*
void	   gtk_box_pack_start	       (GtkBox	     *box,
					GtkWidget    *child,
					gboolean      expand,
					gboolean      fill,
					guint	      padding);
guint  gtk_signal_connect		  (GtkObject	       *object,
					   const gchar	       *name,
					   GtkSignalFunc	func,
					   gpointer		func_data);
guint  gtk_signal_connect_object	  (GtkObject	       *object,
					   const gchar	       *name,
					   GtkSignalFunc	func,
					   GtkObject	       *slot_object);
*/

void scene(GtkWidget *w,gpointer data);

GtkWidget *pbar;
int count=0;  

int
main(int argc,char **argv)
{
  GtkWidget *w;
  GtkWidget  *hbox;
  GtkWidget *hit;
  GtkWidget *label1;

  gtk_init(&argc,&argv);

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  hbox=gtk_hbox_new(TRUE /* Homogeneous */ ,5 /*spacing */ );
  hit=gtk_button_new();
  pbar=gtk_progress_bar_new();
  label1=gtk_label_new(NULL);
  

  gtk_window_set_default_size(GTK_WINDOW(w),400,400);
  gtk_window_set_title(GTK_WINDOW(w),"Muthu's New Teacher: GTK");
  
  gtk_label_set_text(GTK_LABEL(label1),"First Label");
  gtk_container_add(GTK_CONTAINER(hit),label1 );

  gtk_progress_configure(GTK_PROGRESS(pbar),1,0,100);
  gtk_progress_bar_set_orientation(GTK_PROGRESS_BAR(pbar),GTK_PROGRESS_TOP_TO_BOTTOM);
  //  gtk_progress_bar_set_activity_step(GTK_PROGRESS_BAR(pbar),count);


  gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(hit),FALSE,FALSE,5);
  gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(pbar),FALSE,FALSE,5);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect_object(GTK_OBJECT(hit),"clicked",GTK_SIGNAL_FUNC(scene),GTK_OBJECT(w));

  gtk_container_add(GTK_CONTAINER(w),hbox );
  gtk_widget_show_all(w);  
  gtk_progress_set_show_text(GTK_PROGRESS(pbar),1);
  gtk_main();

  return 0;
}

void scene(GtkWidget *w,gpointer data)
{
  gtk_progress_bar_update(GTK_PROGRESS_BAR(pbar),(float)0);
  while(1)
    {
      //      gtk_progress_set_show_text(GTK_PROGRESS(pbar),count);     
      //      gtk_progress_set_show_text(GTK_PROGESS(pbar),count);
      count++;
      if ((count & 0x65 )== 0x65) break;
      //      gtk_progress_set_value(GTK_PROGESS(pbar),(float)count);
      //gtk_progress_set_value(GTK_PROGRESS(pbar),(float)count);
      gtk_progress_bar_update(GTK_PROGRESS_BAR(pbar),(float)count/100);
      g_print("counting...\n");
    }
  sleep(10);
  gtk_progress_bar_update(GTK_PROGRESS_BAR(pbar),(float)0);
  g_print("Coutning Over!!\n");
  count=0;
}
