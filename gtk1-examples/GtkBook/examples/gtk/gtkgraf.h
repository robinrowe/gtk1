#include<gtk/gtk.h>
#include<gdk/gdk.h>

#ifndef GTK_GRAF_H
#define GTK_GRAF_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_GRAF(obj) GTK_CHECK_CAST(obj,gtk_graf_get_type(),GtkGraf)
#define GTK_GRAF_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_dial_get_type(),GtkGrafClass)
#define GTK_IS_GRAF(obj) GTK_CHECK_TYPE( obj,gtk_graf_get_type())


typedef struct _GtkGraf GtkGraf;
typedef struct _GtkGrafClass GtkGrafClass;

struct _GtkGraf
{
  GtkWidget widget;

  /* Private */
  GdkPixmap *pixmap; //offscreen map
  GdkGC     *gc;   //pen used for drawing  
  int height;
  int width;
};

struct _GtkGrafClass
{
  GtkWidgetClass parent_class;
};

GtkWidget* gtk_graf_new();
GType gtk_graf_get_type();
void  gtk_graf_set_size(GdkRectangle *rect);
void  gtk_graf_set_color(gchar *color);
void  gtk_graf_plot(GdkPoint *p, int length);

   

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
