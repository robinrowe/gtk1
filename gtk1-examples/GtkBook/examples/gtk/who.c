#include<stdio.h>
#include<string.h>
#include<gtk/gtk.h>

void get_user_info(GtkTreeSelection *ts,gpointer data);
void disp_user_info(const char *username);
GtkTreeView *get_user_list(void);

int main(int argc,char *argv[])
{
  GtkWidget *w,*m;
  GtkTreeSelection *ts;
  GtkWidget *scroll;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  m=GTK_WIDGET(get_user_list());
  scroll=gtk_scrolled_window_new(NULL,NULL);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scroll),
				   m);
  gtk_container_add(GTK_CONTAINER(w),scroll);
  ts=gtk_tree_view_get_selection(GTK_TREE_VIEW(m));
  gtk_widget_show_all(w);
  g_signal_connect(G_OBJECT(ts),"changed",G_CALLBACK(get_user_info),w);
  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  gtk_main();
  return 0;
}

/*
  get_user_list(void): function returns a "treeview" widget which has
  the various fields of the `who` command in the GUI mode shown as fields
  of a row in the tree.
*/

GtkTreeView *get_user_list(void)
{ 
   static char line[512];
   char uname[80];
   char stationno[80];
   char month[80];
   char day[80];
   char time[80];
   char ipaddress[80];
   FILE *fp ;
   
   GtkListStore *list;
   GtkTreeIter  ti;
   GtkTreeViewColumn *tvc;
   GtkTreeView *tv;
   GtkCellRenderer *crt;
   
   fp = popen("who","r"); //Exec the `who` command

  if(fp == NULL)
    return NULL;

  /*
    who file format is like:
    [uname] [stationno] [month] [day] [time] [ipaddress]
  */

  list=gtk_list_store_new(6,
			  G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING,
			  G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING);
  do
    {

      uname[0]='\0';
      stationno[0]='\0';
      month[0]='\0';
      day[0]='\0';
      time[0]='\0';
      ipaddress[0]='\0';

      line[0]='\0';
      fgets(line,512,fp);
      
      /*
	who file format is like:
	[uname] [stationno] [month] [day] [time] [ipaddress]
      */
      if(sscanf(line,"%s %s %s %s %s %s",
		uname,stationno,month,day,time,ipaddress) <= 1)
	break;

      /*
	printf("%s* %s* %s* %s* %s* %s\n",
	uname,stationno,month,day,time,ipaddress);
	printf("%s\t %s\t %s\t %s\t %s\t %s\n",
	uname,stationno,month,day,time,ipaddress);
      */
      
      gtk_list_store_append(list,&ti);
      gtk_list_store_set(list,&ti,
			 0,g_strdup(uname),
			 1,g_strdup(stationno),
			 2,g_strdup(month),
			 3,g_strdup(day),
			 4,g_strdup(time),
			 5,g_strdup(ipaddress),
			 -1);
    } while(!feof(fp));
  

  tv=GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(list)));

  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("User Name",
					       crt,
					       "text",
					       0,
					       NULL);
  g_object_set(crt,"foreground","red",NULL);

  gtk_tree_view_append_column(tv,
			      tvc);

  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Station #",
					       crt,
					       "text",
					       1,
					       NULL);
  g_object_set(crt,"foreground","blue",NULL);
  //  g_object_set(crt,"strikethrough",TRUE,NULL);
  gtk_tree_view_append_column(tv,
			      tvc);


  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Month",
					       crt,
					       "text",
					       2,
					       NULL);
  g_object_set(crt,"foreground","green",NULL);
  //  g_object_set(crt,"background","yellow",NULL);
  gtk_tree_view_append_column(tv,
			      tvc);


  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Day",
					       crt,
					       "text",
					       3,
					       NULL);
  g_object_set(crt,"foreground","orange",NULL);
  gtk_tree_view_append_column(tv,
			      tvc);


  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("Time [hh:mm]",
					       crt,
					       "text",
					       4,
					       NULL);
  g_object_set(crt,"foreground","violet",NULL);
  gtk_tree_view_append_column(tv,
			      tvc);


  crt=gtk_cell_renderer_text_new();
  tvc=gtk_tree_view_column_new_with_attributes("IP Address",
					       crt,
					       "text",
					       5,
					       NULL);
  g_object_set(crt,"foreground","#cc33ff",NULL);
  g_object_set(crt,"underline",TRUE,NULL);
  gtk_tree_view_append_column(tv,
			      tvc);

  pclose(fp);
  return tv;
}


void get_user_info (GtkTreeSelection *selection, gpointer data)
{
        GtkTreeIter iter;
        GtkTreeModel *model;
	gchar *username;

        if (gtk_tree_selection_get_selected (selection, &model, &iter))
        {
	  gtk_tree_model_get (model, &iter,0, &username, -1);
	  g_printf("Youve chosen to finger: %s\n",username);
	}
	return;
}

void disp_user_info(const char *username)
{
	GtkTextBuffer *tb;
	GtkTextIter *ti;
	GtkWidget *d,*tv;

	FILE *fp;
	static char line[512];

	printf("Thread Created!\n UserName %s\n",username);	
	strcpy(line,"finger ");
	strcat(line,username);
	

	if(1)
	  {

	  fp=popen(line,"r");
	  if(fp==NULL)
	    return;

	  tb=gtk_text_buffer_new(NULL);
	  gtk_text_buffer_get_start_iter(tb,ti);
	  
	  do
	    {
	      line[0]='\0';
	      fgets(line,512,fp);
	      gtk_text_buffer_insert(tb,ti,line,-1);
	    }
	  while(!feof(fp));
	  pclose(fp);

	  d=gtk_dialog_new_with_buttons("Zen Master",
				NULL,
				GTK_DIALOG_MODAL |  GTK_DIALOG_DESTROY_WITH_PARENT |  GTK_DIALOG_NO_SEPARATOR,
				"Ok",
				0,
				NULL
				);

	  tv=gtk_text_view_new_with_buffer(tb);
	  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(d)->vbox),tv);
 	  gtk_window_set_modal(GTK_WINDOW(d),TRUE);
	  gtk_widget_show_all(d);
	  gtk_dialog_run(GTK_DIALOG(d));
	  gtk_widget_destroy(d);
	  }
}

/*
  compilation lines:
  gcc -Wall -o who who.c `pkg-config gtk+-2.0 --cflags --libs`
*/
