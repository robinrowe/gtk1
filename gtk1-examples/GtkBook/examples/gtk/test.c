
#include<stdio.h>
#include<gtk/gtk.h>

int dialog_who(char *s);

int main(int argc,char *argv[])
{
  gtk_init(&argc,&argv);
  dialog_who("finger ");
  dialog_who("who ");
  return 0;
}

int dialog_who (char *s)
{
  GtkWidget *d,*tv;
  GtkTextBuffer *tb;
  GtkTextIter ti;


  FILE *fp ;
  static char line[512];


  d=gtk_dialog_new_with_buttons("Zen Master",
				NULL,
				GTK_DIALOG_MODAL |  GTK_DIALOG_DESTROY_WITH_PARENT |  GTK_DIALOG_NO_SEPARATOR,
				"Ok",
				0,
				"Cancel",
				1,
				"Exit",
				2,
				NULL
				);

  tv=gtk_text_view_new();
  tb=gtk_text_view_get_buffer(GTK_TEXT_VIEW(tv));
  gtk_text_buffer_get_start_iter(tb,&ti);
  fp = popen(s,"r"); //Exec the `who` command
  do
    {
      line[0]='\0';
      fgets(line,512,fp);
      gtk_text_buffer_insert(tb,&ti,line,-1);
    }
  while(!feof(fp));
  pclose(fp);

  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(d)->vbox),tv);
  gtk_window_set_modal(GTK_WINDOW(d),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(d),TRUE);
  gtk_widget_show_all(d);
  gtk_dialog_run(GTK_DIALOG(d));
  gtk_widget_destroy(d);
  return 0;
}

/*
  Compilation Lines
  gcc  -Wall -o test test.c `pkg-config --cflags --libs gtk+-2.0`
*/
