#include<gtk/gtk.h>



int main(int argc,char *argv[])
{
  GtkWidget *w;
  GtkWidget *b;

  static const GtkTargetEntry targets[] = {
    { "UTF8_STRING", 0, 0 },
    { "STRING", 0, 0 },
    { "TEXT",   0, 0 }, 
    { "COMPOUND_TEXT", 0, 0 },
    { "text/plain",0,0},
    { "text/uri-list",0,1}};

  gtk_init(&argc,&argv);

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  b=gtk_button_new_with_label("Drag me");    
  
  gtk_container_add(GTK_CONTAINER(w),b);
  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);
  

  gtk_drag_source_set(b,
		      GDK_SHIFT_MASK,
		      targets,6,
		      GDK_ACTION_ASK);

  gtk_drag_source_set_icon_stock(b,GTK_STOCK_OPEN);
  //gdk_pixbuf_new_from_file("/usr/share/pixmaps/gnome-emacs.png",NULL));


  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}


//gcc -Wall -o prog prog.c `pkg-config gtk+-2.0 --cflags --libs`

