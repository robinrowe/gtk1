#include<stdio.h>
#include<assert.h>
#include<string.h>
#include<gtk/gtk.h>
#include<unistd.h>

#define CLR 1

//This HEX_VAL returns a #RRGGBB value from a GdkColor structure
#define HEX_VAL(x) ((x.red&0xff00)<<8)+ (x.green&0xff00)+((x.blue&0xff00)>>8)

//This HEX_VAL returns a #RR0000 value from a GdkColor structure
#define RED(x) ((x&0xff0000)>>16)
//This HEX_VAL returns a #00GG00 value from a GdkColor structure
#define GREEN(x) ((x&0x00ff00)>>8)
//This HEX_VAL returns a #0000BB value from a GdkColor structure
#define BLUE(x) (x&0x0000ff)

void change_color(GtkWidget *w,gpointer data);
int generate_html(GtkWidget *w,gpointer data);
void generate_html_stupidway(GtkWidget *w,gpointer data);
GdkColor start_color,end_color;

int
main(int argc,char **argv)
{
  GtkWidget *w,*b1,*b2,*start,*entry,*box;  
  gtk_init(&argc,&argv);

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  //cal=gtk_calendar_new();
  box=gtk_vbox_new(FALSE,FALSE);

  entry=gtk_entry_new();
  b1=gtk_button_new_with_label("Starting Color");
  b2=gtk_button_new_with_label("Ending Color");
  start=gtk_button_new_with_label("Convert Text");

  gtk_box_pack_start(GTK_BOX(box),entry,FALSE,FALSE,10);
  gtk_box_pack_start(GTK_BOX(box),b1,FALSE,FALSE,10);
  gtk_box_pack_start(GTK_BOX(box),b2,FALSE,FALSE,10);
  gtk_box_pack_start(GTK_BOX(box),start,FALSE,FALSE,10);

  //  gtk_box_pack_start(GTK_BOX(box),csel,TRUE,TRUE,10);


  gtk_window_set_default_size(GTK_WINDOW(w),100,200);
  gtk_window_set_title(GTK_WINDOW(w),"Muthu's");

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(b1),"clicked",GTK_SIGNAL_FUNC(change_color),w);
  gtk_signal_connect(GTK_OBJECT(b2),"clicked",GTK_SIGNAL_FUNC(change_color),w);
  gtk_signal_connect(GTK_OBJECT(start),"clicked",GTK_SIGNAL_FUNC(generate_html),entry);

  gtk_container_add(GTK_CONTAINER(w),box);
  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}


void change_color(GtkWidget *w,gpointer a)
{
  GtkWidget *csel,*data;
  GdkColor c;
  csel=gtk_color_selection_dialog_new("Choose Bg Color");
  data=(GtkWidget *)a;
  if(gtk_dialog_run(&GTK_COLOR_SELECTION_DIALOG(csel)->parent_instance)==GTK_RESPONSE_OK)
    {
      gtk_color_selection_get_current_color(
				  GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(csel)->colorsel)
				  ,&c);
      printf("Color was  #%02x%02x%02x \n",(c.red&0xff00)>>8,\
(c.green&0xff00)>>8,(c.blue&0xff00)>>8); 
      if (strncmp(gtk_button_get_label(GTK_BUTTON(w)),"S",1)==0)
	{
	  //This is a "Start button , so set the start color;
	  start_color=c;
	  printf("start_color set\n");
	}
      else
	{
	  end_color=c;
	  printf("end_color set\n");
	}
      gtk_widget_modify_bg(GTK_WIDGET(data),GTK_STATE_NORMAL,&c);
      gtk_widget_modify_bg(GTK_WIDGET(w),GTK_STATE_NORMAL,&c);
      gtk_widget_destroy(csel);
    }
}

void generate_html_stupidway(GtkWidget *w,gpointer data)
{
  GtkWidget *entry;
  extern GdkColor start_color,end_color;
  int start_val,end_val,step;
  gchar buffer[80],*ptr;

  entry=(GtkWidget *)data;
  ptr=gtk_entry_get_text(GTK_ENTRY(entry));
  printf("%s\n",ptr); //gtk_entry_get_text(GTK_ENTRY(entry)));
  
  start_val=HEX_VAL(start_color);
  end_val=HEX_VAL(end_color);
  printf("Chosen colors were start=#%06x end=#%06x\n",start_val,end_val);

  step=(start_val-end_val);
  step=(step>0) ? step:-step;
  step=step/strlen(ptr);
  printf("step size=%d\n",step);

  if (start_val > end_val)
    {
      while(start_val > end_val)
	{	  
	  start_val -= step;
	  /*	  sprintf(buffer,"#%02x%02x%02x",(start_color.red&0xff00)>>8,\
	      (start_color.green&0xff00)>>8,(start_color.blue&0xff00)>>8);*/
	  sprintf(buffer,"%02x%02x%02x",RED(start_val),GREEN(start_val),BLUE(start_val));
	  printf("<font color=\"#%s\">%c</font>",buffer,*ptr);
	  ptr++;
	}
    }
  else
    {
      while(start_val < end_val)
	{
	  start_val += step;
	  /*sprintf(buffer,"#%02x%02x%02x",(start_color.red&0xff00)>>8,\
	    (start_colo.green&0xff00)>>8,(start_color.blue&0xff00)>>8);*/
	  sprintf(buffer,"%02x%02x%02x",RED(start_val),GREEN(start_val),BLUE(start_val));
	  printf("<font color=\"#%s\">%c</font>",buffer,*ptr);
	  ptr++;
	}
    }
  printf("\n");
}

int generate_html(GtkWidget *w,gpointer data)
{
  GtkWidget *entry;
  extern GdkColor start_color,end_color;
  unsigned int start_val,end_val,step;
  unsigned int dr,dg,db,temp=0;

  gchar buffer[80],*ptr;

  entry=(GtkWidget *)data;
  ptr=gtk_entry_get_text(GTK_ENTRY(entry));
  //sets the ptr to the start of the string.
  printf("%s\n",ptr); //gtk_entry_get_text(GTK_ENTRY(entry)));
  
  start_val=HEX_VAL(start_color);
  end_val=HEX_VAL(end_color);
  printf("Chosen colors were start=#%06x end=#%06x\n",start_val,end_val);

  step=strlen(ptr);
  if (step<=0)
    return -1;

  //Now you have the color[s]
  //you have the the text too.

  step--;

  dr=RED(start_val)-RED(end_val);
  dr=dr/3;
  dr=dr/strlen(ptr);

  dg=GREEN(start_val)-GREEN(end_val);
  dg=dg/3;
  dg=dg/strlen(ptr);

  db=BLUE(start_val)-BLUE(end_val);
  db=db/3;
  db=db/strlen(ptr);
  temp=start_val;

  printf("STEP r=%02x  g=%02x b=%02x\n",dr,dg,db);


  printf("\n<h2>");
  do {
      //printf("Step size=%d\n",step);

      if (step<=(strlen(ptr)/3))
	{
	  temp+=(dr<<16);
	  sprintf(buffer,"#%02x%02x%02x",RED(temp),GREEN(temp),BLUE(temp));
	}
      else if(step<=(2*strlen(ptr)/3))
	{
	  //green
	  temp+=(dg<<8);
	  sprintf(buffer,"#%02x%02x%02x",RED(temp),GREEN(temp),BLUE(temp));
	}
      else
	{
	  temp+=(db);
	  sprintf(buffer,"#%02x%02x%02x",RED(temp),GREEN(temp),BLUE(temp));
	  //blue
	}
      printf("<font color=\"%s\">%c</font>",buffer,*ptr);
      ptr++;
      step--;
    }  while(step!=0); //!oof I used an unsigned man.
 return printf("</h2>\n");
}
