#include<gtk/gtk.h>

void activate_cb(GtkMenuItem *mitem)
{
  GtkWidget *d;
  gchar *ptr=gtk_widget_get_name(GTK_WIDGET(mitem));
  gchar **o;
  if(ptr[1]=='2')
    {
      g_print("Im here with %s\n",ptr);
      d=gtk_file_selection_new("Choose a File");
      gtk_dialog_run(GTK_DIALOG(d));
      o=gtk_file_selection_get_selections(GTK_FILE_SELECTION(d));
      g_print("File chosen was %s\n",*o);
      gtk_widget_destroy(d);
    }
  
}

int main(int argc,char *argv[])
{
  GtkWidget *window,*vbox,*mbar,*mitem,*temp,*m;

  gtk_init(&argc,&argv);

  window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  mbar=gtk_menu_bar_new();

  vbox=gtk_vbox_new(TRUE,TRUE);
  mitem=gtk_menu_item_new_with_label("File");
  m=gtk_menu_new();

  temp=gtk_menu_item_new_with_label("F1");
  gtk_widget_set_name(GTK_WIDGET(temp),"F1");
  g_signal_connect(G_OBJECT(temp),"activate",G_CALLBACK(activate_cb),NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(m),temp);

  temp=gtk_menu_item_new_with_label("F2");
  gtk_widget_set_name(GTK_WIDGET(temp),"F2");
  g_signal_connect(G_OBJECT(temp),"activate",G_CALLBACK(activate_cb),NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(m),temp);

  temp=gtk_menu_item_new_with_label("F3");
  gtk_widget_set_name(GTK_WIDGET(temp),"F3");
  g_signal_connect(G_OBJECT(temp),"activate",G_CALLBACK(activate_cb),NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(m),temp);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(mitem),m);

  g_signal_connect(G_OBJECT(mitem),"activate",G_CALLBACK(activate_cb),NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(mbar),mitem);

  mitem=gtk_menu_item_new_with_mnemonic("Edit");
  gtk_menu_shell_append(GTK_MENU_SHELL(mbar),mitem);

  gtk_container_add(GTK_CONTAINER(window),vbox);
  gtk_box_pack_start_defaults(GTK_BOX(vbox),mbar);

  g_signal_connect(G_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  //  gtk_window_resize(GTK_WINDOW(window),100,100);
  gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_MOUSE);
  gtk_widget_show_all(window);
  gtk_main();
  return 0;
}
//gcc gtkproto.c `pkg-config --cflags --libs gtk+-2.0`
