#include<gtk/gtk.h>

int main(int argc,char *argv[])
{
  GtkWidget *w,*tv;
  GtkTextBuffer *tb;
  GtkTextIter start,end;
  GtkWidget *c;

  gtk_init(&argc,&argv);
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  tb=gtk_text_buffer_new(NULL);
  gtk_text_buffer_create_tag(tb,"wrap","foreground","green",NULL);

  gtk_text_buffer_set_text(tb,"HelloWorldIamReallyFrustratedHelloWorldIamReallyFrustratedHelloWorldIamReallyFrustrated",-1);

  gtk_text_buffer_get_bounds (tb, &start, &end);
  gtk_text_buffer_apply_tag_by_name (tb, "wrap", &start, &end);
  tv=gtk_text_view_new_with_buffer(tb);

  c=gtk_scrolled_window_new(NULL,NULL);
  gtk_scrolled_window_add_with_viewport(
					GTK_SCROLLED_WINDOW(c),
					GTK_WIDGET(tv));
  gtk_container_add(GTK_CONTAINER(w),c);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(c),
				 GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
				 
  g_signal_connect(G_OBJECT(w),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  gtk_widget_show_all(w);
  gtk_main();
  return 0;
}

//gcc prob.c `pkg-config --cflags --libs gtk+-2.0`

