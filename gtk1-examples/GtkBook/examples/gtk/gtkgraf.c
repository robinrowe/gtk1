#include <math.h>
#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include "gtkgraf.h"

static void gtk_graf_class_init(GtkGrafClass *klass);
static void gtk_graf_init(GtkGraf *graf);

static gboolean  
gtk_graf_configure(GtkWidget	     *widget,
		   GdkEventConfigure   *event);

static gboolean gtk_graf_expose(GtkWidget *widget,
				GdkEventExpose *event);
static void
gtk_graf_realize(GtkWidget *widget)
{
  GtkGraf *graf;
  GdkWindowAttr attributes;
  
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_GRAF(widget));
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  graf=GTK_GRAF(widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;

  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;

  attributes.event_mask = gtk_widget_get_events (widget) | 
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
    GDK_POINTER_MOTION_HINT_MASK;

  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  
  widget->window = gdk_window_new (widget->parent->window, 
				   &attributes, 
				    GDK_WA_X | GDK_WA_Y | 
				   GDK_WA_VISUAL | GDK_WA_COLORMAP);

  widget->style = gtk_style_attach (widget->style, 
				    widget->window);

  gtk_style_set_background (widget->style,
			    widget->window,
			    GTK_STATE_ACTIVE);
}		 

static GtkWidgetClass *parent_class = NULL;


GType
gtk_graf_get_type ()
{
  static GType graf_type = 0;

  if (!graf_type)
    {
      g_printf("type register once\n");
      static const GTypeInfo graf_info =
      {
	sizeof (GtkGrafClass),
	NULL,
	NULL,
	(GClassInitFunc) gtk_graf_class_init,
	NULL,
	NULL,
	sizeof (GtkGraf),
        0,
	(GInstanceInitFunc) gtk_graf_init,
      };

      graf_type = g_type_register_static (GTK_TYPE_WIDGET, 
					  "GtkGraf", 
					  &graf_info, 
					  0);
    }
  g_printf("graf type=%d\n",graf_type);
  return graf_type;
}


static void gtk_graf_class_init(GtkGrafClass *class)
{
  GtkWidgetClass *widget_class;
  g_printf("Graf class [init]\n"); 
  widget_class = (GtkWidgetClass*) class;
  parent_class = gtk_type_class (gtk_widget_get_type ());
  if(GTK_IS_WIDGET_CLASS(parent_class))
    g_printf("Is A widget class!\n");
  else
    g_printf("Not a widget\n");
  widget_class->realize=gtk_graf_realize;
  widget_class->expose_event=gtk_graf_expose;
  widget_class->configure_event=gtk_graf_configure;
}

static void gtk_graf_init(GtkGraf *graf)
{
  g_printf("Graf Object being Instantiated\n");
  graf->gc=NULL;
  graf->height=0;
  graf->width=0;
}

static gboolean
gtk_graf_configure(GtkWidget	     *widget,
		   GdkEventConfigure   *event)
{
  GtkGraf *graf;

  g_return_val_if_fail(widget!=NULL,TRUE);
  g_return_val_if_fail(GTK_IS_GRAF(widget),TRUE);

  graf=(GtkGraf *)widget;
  graf->gc=gdk_gc_new(NULL);
  graf->width=widget->allocation.width;
  graf->height=widget->allocation.width;

  return TRUE;
}

static gboolean gtk_graf_expose(GtkWidget *widget,
				GdkEventExpose *event)
{
  GtkGraf *graf;
  graf=(GtkGraf *)widget;
  g_printf("gtk graf Expose Event!\n");

  if(graf->gc==NULL)
    {
      graf->gc=gdk_gc_new(NULL);
    }

  return TRUE;
}


GtkWidget * gtk_graf_new(void)
{
  GtkGraf *graf;
  g_printf("Graf Object [New]\n");
  graf=g_object_new(gtk_graf_get_type(),NULL);
  return GTK_WIDGET(graf);
}

void  gtk_graf_set_size(GdkRectangle *rect)
{

}

void  gtk_graf_set_color(gchar *color)
{
  
}

void  gtk_graf_plot(GdkPoint *p, int length)
{
  
}
