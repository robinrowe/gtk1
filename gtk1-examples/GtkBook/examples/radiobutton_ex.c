#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


gboolean 
radio_toggled2(GtkWidget *w,
	      gpointer data)
{
  GtkRadioButton *rb;
  GSList *list;


  rb=(GtkRadioButton *)w;
  list=gtk_radio_button_get_group(rb);
  while(list != NULL)
    {
      rb=(GtkRadioButton *)list->data;
      if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb))==TRUE)
	{
	  g_printf("Active: %s\n",gtk_button_get_label(GTK_BUTTON(rb)));
	  break;
	}
      list=list->next;
    }
  return TRUE;
}

gboolean 
radio_toggled(GtkWidget *w,
	      gpointer data)
{
  GtkRadioButton *rb;
  rb=(GtkRadioButton *)w;
  
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb))==TRUE)
    {
      g_printf("Toggled: selected: %s\n",gtk_button_get_label(GTK_BUTTON(rb)));
    }
  return TRUE;
}

GtkWidget* radio_demo(void)
{
  GSList *list;
  GtkWidget *rb,*b;
  static gchar *names[]={"GNU/Linux","GNU Hurd","GNU Darwin","GNU Mach",NULL};
  gchar **ptr;

  ptr=NULL;
  list=NULL;
  b=gtk_hbox_new(FALSE,FALSE);  

  ptr=names;

  while(*ptr!=NULL)
    {
      rb=gtk_radio_button_new_with_label(list,g_strdup(*ptr));
      list=gtk_radio_button_get_group(GTK_RADIO_BUTTON(rb));

      gtk_box_pack_start_defaults(GTK_BOX(b),rb);      
      gtk_signal_connect(GTK_OBJECT(rb),"toggled",GTK_SIGNAL_FUNC(radio_toggled),NULL);	
      ptr++;
    }
  return b;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;
  //  int i=0;

  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);


  gtk_container_add(GTK_CONTAINER(w),
		    radio_demo());

  gtk_signal_connect(GTK_OBJECT(w),
		     "destroy",
		     GTK_SIGNAL_FUNC(gtk_main_quit),
		     NULL);

  gtk_window_set_title(GTK_WINDOW(w),
		       "Radio Button[s]");

  gtk_window_set_default_size(GTK_WINDOW(w),
			      200,
			      40);
  gtk_widget_show_all(w);  
  gtk_main();
  return 0;
}




//gcc -Wall radiobutton_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o radiobutton
//export PKG_CONFIG_PATH="/usr/lib/pkgconfig/"
