#include<stdio.h>
#include<string.h>
#include<time.h>
#include<libxml/parser.h>
#include<gtk/gtk.h>

typedef struct _Record Record;
struct _Record
{
  gchar *Title;
  gchar *FirstName;
  gchar *MiddleName;
  gchar *LastName;
  gchar *Address1;
  gchar *Address2;
  gchar *City;
  gchar *State;
  gchar *Pin;
  gchar *Country;
};

void print_rec(Record *rec)
{
  g_message("%s %s %s from %s",rec->Title,rec->FirstName,rec->LastName,rec->Country);
}

/* Writes Records into XML file */
void save_records(GSList *list,gchar *filename)
{ 
  xmlDocPtr doc;
  xmlNodePtr rnode,cnode,wnode;
  xmlNsPtr ns=NULL;
  GSList *tmp;
  Record *rec;
  
  doc=xmlNewDoc("1.2");
 
  rnode=xmlNewDocNode(doc,NULL,"AddressBook","\n"); /* Root Node */
  xmlDocSetRootElement(doc,rnode);
  

  for(tmp=list;tmp!=NULL;tmp=tmp->next) /*Naviagte the List of [Cards]*/
    {
      
      rec=tmp->data;

      /* Card is child of AddressBook */
      cnode=xmlNewChild(rnode,ns,"Card","\n");

      /* All rest are children of Card */
      wnode=xmlNewNode(ns,"Title");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->Title);

      wnode=xmlNewNode(ns,"FirstName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->FirstName);

      wnode=xmlNewNode(ns,"MiddleName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val",rec->MiddleName);

      wnode=xmlNewNode(ns,"LastName");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->LastName);

      wnode=xmlNewNode(ns,"Address1");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Address1);
            
      wnode=xmlNewNode(ns,"Address2");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Address2);

      wnode=xmlNewNode(ns,"City");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->City);

      wnode=xmlNewNode(ns,"State");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->State);

      wnode=xmlNewNode(ns,"Pin");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Pin);		

      wnode=xmlNewNode(ns,"Country");
      xmlAddChild(cnode,wnode);
      xmlSetProp(wnode,"val", rec->Country);
    }

  {
    char buffer[256]="Created on ";
    time_t t=time(NULL);
    asctime_r(localtime(&t),buffer+strlen(buffer));
    cnode=xmlNewComment(buffer);
    xmlAddChild(rnode,cnode);
  }

  xmlSaveFile(filename,doc);
  xmlFreeNode(rnode);
  xmlFreeDoc(doc);
}

GSList *get_records(gchar *filename)
{
  Record *rec;
  GSList* reclist;
  xmlDocPtr doc;
  xmlNodePtr node;
  
  doc=xmlParseFile(filename);
  g_return_val_if_fail(doc!=NULL,NULL);
  
  node=xmlDocGetRootElement(doc);
  g_return_val_if_fail(node!=NULL,NULL);

  reclist=g_slist_alloc();
  reclist->data=NULL;

  for(node=xmlDocGetRootElement(doc)->children;node!=NULL;node=node->next)
    {
      if(!xmlStrcmp(node->name,"Card"))
	{
	  xmlNodePtr temp;
	  temp=node->children;
	  rec=g_malloc0(sizeof(Record)*1);	  
	  for(;temp!=NULL;temp=temp->next)
	    {
	      switch(temp->name[0])
		{
		case 'T'://Title
		  rec->Title=xmlGetProp(temp,"val");
		  break;
		case 'F': //First Name
		  rec->FirstName=xmlGetProp(temp,"val");
		  break;
		case 'M': //Middle Name
		  rec->MiddleName=xmlGetProp(temp,"val");
		  break;
		case 'L'://LastName
		  rec->LastName=xmlGetProp(temp,"val");
		  break;
		case 'A'://Address
		  if( temp->name[7]=='1')
		    {
		      rec->Address1=xmlGetProp(temp,"val");
		    }
		  else
		      rec->Address2=xmlGetProp(temp,"val");		  
		  break;
		case 'C'://City
		  if(temp->name[3]=='y')
		    {
		      rec->City=xmlGetProp(temp,"val");
		    }
		  else
		    {  //Country
		       rec->Country=xmlGetProp(temp,"val");
		    }		
		  break;
		case 'S'://State
		  rec->State=xmlGetProp(temp,"val");
		  break;
		case 'P'://Pin
		  rec->Pin=xmlGetProp(temp,"val");
		  break;
		}
	    }
	  print_rec(rec);

	  if(!reclist->data)
	    reclist->data=rec;
	  else
	    g_slist_append(reclist,rec);
	}
    }
  xmlFree(node);
  xmlFree(doc);
  return reclist;
}

#if 0
int main()
{

  /*  GSList* reclist=get_records("addr.xml");
      save_records(reclist,"rdda.xml");
  */

  //  g_slist_foreach(reclist,print_rec);
#if 0
  while(*rec!=NULL)
    {
      print_rec(rec);
      rec++;
    }
#endif

  return 0;
}
#endif 

/* Recursive Descent */
void rP(xmlNodePtr node)
{
  static int nesting=1;
  
  if(node!=NULL)
    {
      xmlNodePtr tmp;
      tmp=node->children;
      if(tmp==NULL)
	{	
	  xmlAttrPtr prop;
	  int spc=0;


	  /* Iterate through all the attributes */
	  printf("\n");
	  for(spc=0;spc<nesting;spc++ && printf("\t"));

	  printf("Node %s \n",(node->next!=NULL)?node->next->name:"");

	  for(spc=0;spc<nesting;spc++ && printf("\t"));
	  
	  /*  if(xmlNodeGetContent(node))
	      printf("%s",xmlNodeGetContent(node)); */

	  for(prop=node->properties;prop!=NULL;prop=prop->next)	    
	    printf("|_\t%s,",prop->name);

 	  return;


	}      
      /* Have some Children ?? */
      for(;tmp!=NULL;tmp=tmp->next)
	{
	  /* One Level Deeper */
	  nesting++;	  	  
	  rP(tmp);

	  nesting--;
	}
    }          
}


#if 0
int main(int argc,char *argv[])
{ 
  xmlDocPtr doc;
  xmlNodePtr node;

  if(argc!=2)
    return -1;
  doc = xmlParseFile(argv[1]);  
  
  node=doc->children;
  
  while(node!=NULL)
    {
      rP(node);
      node=node->next;
    }

#if 0
  while(node!=NULL)
    {
      if(!xmlStrcmp(node->name,"Book"))
	{
	  xmlNodePtr tmp;
	  tmp=node->children;
	  while(tmp!=NULL)
	    {
	 
	      if(!xmlStrcmp(tmp->name,"CHAP"))
		{
		  printf("CHAPTER [%s]",xmlGetProp(tmp,"name"));		
		  printf("\\= \n\t{%s}\n",xmlNodeGetContent(tmp));
		}
	      tmp=tmp->next;
	    }
	}
      node=node->next;
    }
#endif /* 0 */

  xmlFreeNode(node);
  xmlFreeDoc(doc);

  return 0;
}
#endif

int main(int argc,char *argv[])
{ 
  xmlDocPtr doc;
  xmlNodePtr node;

  if(argc!=2)
    return -1;
  doc = xmlParseFile(argv[1]);
  node=doc->children;
  
  while(node!=NULL)
    {
      rP(node);
      node=node->next;
    }
  return 0;
}

/*gcc -Wall txml.c `pkg-config libxml-2.0  --cflags --libs` `pkg-config gtk+-2.0 --cflags --libs`*/
