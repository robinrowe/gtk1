#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>


void file_select(GtkWidget *btn,gpointer data)
{
  GtkWidget* fs;
  fs=gtk_file_selection_new("Multiple Files Minus FileOps");
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION(fs));
  
  //list only  C files.
  gtk_file_selection_complete(GTK_FILE_SELECTION(fs),"*.c");
			      
  //set a default file
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(fs),"./");
  
  //allow multiple selections
  gtk_file_selection_set_select_multiple (GTK_FILE_SELECTION(fs),TRUE);
  
  if(gtk_dialog_run(GTK_DIALOG(fs)) == GTK_RESPONSE_OK)
    {
      gchar **x;
      x=gtk_file_selection_get_selections(GTK_FILE_SELECTION(fs));
      
      //list the files.
      while(*x != NULL)
	{ 	
	  g_printf("Chosen: %s\n",*x);
	  x++;
	}
    }
  gtk_widget_destroy(fs);
}

GtkWidget * file_widget(void)
{
  GtkWidget *w;
  GtkWidget *box,*button;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  box=gtk_hbox_new(FALSE,FALSE);

  gtk_container_add(GTK_CONTAINER(w),box);

  button=gtk_button_new_with_label("File Select");  
  gtk_box_pack_start_defaults(GTK_BOX(box),button);

  gtk_widget_show_all(box);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(button),"clicked",GTK_SIGNAL_FUNC(file_select),NULL);
  gtk_window_set_title(GTK_WINDOW(w),"File Select");
  gtk_window_resize(GTK_WINDOW(w),200,40);

  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=file_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall filesel_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o filesel
