#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

static GList *list;
static char *clist[752];

void cleanup(void)
{
  char **p;
  p=clist;
  while((p-clist) < 752)
    {
      g_free(*p);
      p++;
    }
  g_list_free(list);
  gtk_main_quit();
}

gboolean print_string(GtkList *l,GtkWidget *chd,gpointer data)
{
  static int last;
  int cur;
  GtkWidget *child;
  GtkImage *im;
  GdkPixbuf *pb;
  GtkStatusbar *sb;
  int r,g,b;
  long int x;
  gchar *str;


  child=GTK_BIN(chd)->child;
  if(GTK_IS_LABEL(child))
    {
      // g_printf("%s\n",gtk_label_get_text(GTK_LABEL(child)));
    }

  // g_printf("%d\n",gtk_list_child_position(GTK_LIST(l), chd)-1);
  cur=gtk_list_child_position(GTK_LIST(l),chd);
  if(cur<=0  || cur==last)
    {
      //repeating the same number is prevented.
      //      g_printf("Repeated...Exiting cur=%d last=%d\n",cur,last);
      return FALSE;
    }
  last=cur;
  str=clist[gtk_list_child_position(GTK_LIST(l),chd)-1];
  if(str!=NULL)
    {      
      //      g_printf("Entering block\n");
      sscanf(str,"%d %d %d",&r,&g,&b);
      x=(r<<16)+(g<<8)+b;
      //      g_printf("%s r=%d g=%d b=%d x=%X\n",str,r,g,b,x);

      im=(GtkImage *)data;
      pb=gtk_image_get_pixbuf(im);
      
      gdk_pixbuf_fill(pb,
		      x<<8);
      gtk_widget_queue_draw(GTK_WIDGET(im));
      sb=(GtkStatusbar *)g_object_get_data(G_OBJECT(data),
				       "status-bar");
      if(sb!= NULL)
	{
	  static char str[10];
	  sprintf(str,"#%06X",(long int)x);
	  gtk_statusbar_pop(GTK_STATUSBAR(sb),0);
	  gtk_statusbar_push(GTK_STATUSBAR(sb),
			     0,
			     str);
	}
      //      g_printf("Leaving block\n");	
    }

  return FALSE;
}

int main(int argc,char *argv[])
{  
  GtkWidget *win;
  GtkWidget *box;
  GtkWidget *vbox;
  GtkWidget *status;
  /*  GtkWidget *label;		  */
  GtkWidget *combo;
  GdkPixbuf *pb;
  GtkWidget *image;
  

  gtk_init(&argc,&argv);

  win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(win),
		       "Pick a Color:");

  box=gtk_hbox_new(FALSE,FALSE);
  vbox=gtk_vbox_new(FALSE,FALSE);

  status=gtk_statusbar_new();
  gtk_statusbar_push(GTK_STATUSBAR(status),
		     0,
		     "Pick A Color");
  
  gtk_box_pack_start_defaults(GTK_BOX(vbox),
			      box);
  gtk_box_pack_start_defaults(GTK_BOX(vbox),
			      status);

  /*
    label=gtk_label_new("<White>");
    gtk_label_set_justify(GTK_LABEL(label),
    GTK_JUSTIFY_LEFT);
    gtk_box_pack_start_defaults(GTK_BOX(box),
			      label);
  */


  combo=gtk_combo_new();
  gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry),
			 FALSE);
  list=g_list_alloc();


  {
    FILE *fp;
    char buf[256];
    char *ptr;
    int count=0;
    fp=fopen("rgb.txt","r");
    g_assert(fp!=NULL);

    while(!feof(fp))
      {
	fgets(buf,256,fp);
	ptr=buf+13;
	//g_printf("%s\n",ptr);
	g_list_append(list,g_strdup(ptr));
	ptr=buf+11;
	*ptr='\0';
	//g_printf("%s\n",buf);
	clist[count++]=g_strdup(buf);
      }
    fclose(fp);
  }
		
  
  gtk_combo_set_popdown_strings(GTK_COMBO(combo),
			     list);
  gtk_box_pack_start_defaults(GTK_BOX(box),
			      combo);
  
  pb=gdk_pixbuf_new(GDK_COLORSPACE_RGB,
		    FALSE,
		    8,
		    50,
		    50);
  gdk_pixbuf_fill(pb,
		  0xffffff<<8);//white
  image=gtk_image_new();
  gtk_image_set_from_pixbuf(GTK_IMAGE(image),
			    pb);
  gtk_box_pack_start_defaults(GTK_BOX(box),
			      image);


  gtk_container_add(GTK_CONTAINER(win),
		    vbox);
  g_object_set_data(G_OBJECT(image),"status-bar",status);

  g_signal_connect(G_OBJECT(GTK_COMBO(combo)->list),
		   "select_child",
		   G_CALLBACK(print_string),image);


  g_signal_connect(G_OBJECT(win),
		   "destroy",
		   G_CALLBACK(cleanup),NULL);



  gtk_widget_show_all(win);
  gtk_main();  
  return 0;
}		   
//gcc -Wall tempo.c `pkg-config gtk+-2.0 --cflags --libs` 

/**
void comb_at0(GtkList *l,gpointer data)
{  
  GtkImage *im;
  GdkPixbuf *pb;
  GtkLabel *la;
  unsigned int x;

  im=(GtkImage *)data;
  pb=gtk_image_get_pixbuf(im);
  x=random();
  x=((x<0)?-x:+x);
  x=x%0xffffff;

  gdk_pixbuf_fill(pb,
		  x<<8);
  gtk_widget_queue_draw(GTK_WIDGET(im));
  la=(GtkLabel *)g_object_get_data(G_OBJECT(data),
		       "color-label");
  if(la!= NULL)
  {
    static char str[10];
    sprintf(str,"#%06X",x);
    gtk_label_set_text(GTK_LABEL(la),
		       str);
  }
	

  //  printf(" %x Hello, Im activated..\n",x);
  
}

void comb_at1(GtkTreeSelection *ts,gpointer data)
{  
  GtkImage *im;
  GdkPixbuf *pb;
  GtkLabel *la;
  long int x;

  GtkTreeModel  *m;
  GtkTreeIter   ti;

  im=(GtkImage *)data;
  pb=gtk_image_get_pixbuf(im);
  x=random();
  x=((x<0)?-x:+x);
  x=x%0xffffff;

  gdk_pixbuf_fill(pb,
		  x<<8);
  gtk_widget_queue_draw(GTK_WIDGET(im));
  la=(GtkLabel *)g_object_get_data(G_OBJECT(data),
		       "color-label");
  if(la!= NULL)
  {
    static char str[10];
    sprintf(str,"#%06X",(long int)x);
    gtk_label_set_text(GTK_LABEL(la),
		       str);
  }
	
  gtk_tree_selection_get_selected(ts,
				  &m,
				  &ti);

  printf("%s\n",gtk_tree_model_get_string_from_iter(m,
						    &ti));
}
**/
