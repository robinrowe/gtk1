#include<glib.h>
#include<glib-object.h>

#define TEST_TYPE  (test_get_type())
#define TEST(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),TEST_TYPE,test))
#define TEST_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass),TEST_TYPE,testClass))
#define TEST_IS(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),TEST_TYPE))
#define TEST_IS_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),TEST_TYPE))
#define TEST_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj),TEST_TYPE,testClass))

typedef 	 struct _test  test;
typedef 	 struct _testClass testClass;

struct _test {
	GObject  _GObject;
	 /* Fill Your Member Data  Here*/
};
struct _testClass{
	GObjectClass klass;
	 /* your custom class functions  */
};


static void test_class_finalize(testClass* klass,gpointer data)
{
/* delete class specific data which you may have dynamically allocated*/
  g_message("Test class Finalize");
}

static void test_class_init(testClass* klass,gpointer data)
{
/* Do Class Init Stuff Here: set member fcns etal*/ 
  ((GObjectClass *)klass)->finalize=test_class_finalize;
  g_message("Test class init");
}

static void test_init(test* obj,testClass *klass)
{
	 /*constructor for your class */
  g_message("Test Object Initializer");
}

int test_get_type(){
static int type_id=0;
if (!type_id){
	static GTypeInfo test_type= {
	/*Class Size */ 
	 sizeof(testClass),
	/* base init */ 
	 NULL,
	/* base finalize */
	 NULL,
 
	 /* class init*/
	 (GClassInitFunc)test_class_init,
	 /* class finalize*/
	 (GClassFinalizeFunc) NULL,
	 /* not needed for statics ? test_class_finalize, */

	/* const static data */
	 NULL,
	/* instance types */
	/* instance size */
	 sizeof(test),
	/* n_preallocs   */
	 0,
	/* InstanceInit  */
	 (GInstanceInitFunc) test_init,
	/* value table! */ 
	 NULL
};
type_id=g_type_register_static(G_TYPE_OBJECT,
		"test",
		&test_type,
		  0);
/* Comment it if you need to. Good for beginners like me */
 g_message("registered type:test with id %d",type_id);
}

return type_id;
}

GObject* test_new(void)
{
  g_message("Test creating new object");
  return (GObject *) g_object_new(TEST_TYPE,/* speficy property,values */ NULL);
}


#define OBJ2_TYPE  (obj2_get_type())
#define OBJ2(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),OBJ2_TYPE,Obj2))
#define OBJ2_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass),OBJ2_TYPE,Obj2Class))
#define OBJ2_IS(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),OBJ2_TYPE))
#define OBJ2_IS_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),OBJ2_TYPE))
#define OBJ2_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj),OBJ2_TYPE,Obj2Class))
typedef 	 struct _Obj2  Obj2;
typedef 	 struct _Obj2Class Obj2Class;
struct _Obj2 {
	test  _test;
	 /* Fill Your Member Data  Here*/
};
struct _Obj2Class{
	testClass klass;
	 /* your custom class functions  */
};
static void obj2_class_init(Obj2Class* klass,gpointer data)
{
  /* Do Class Init Stuff Here: set member fcns etal*/ 
  g_message("obj2 class init");
}
static void obj2_class_finalize(Obj2Class* klass,gpointer data)
{
/* delete class specific data which you may have dynamically allocated*/
  g_message("obj2 class finalize");
}
static void obj2_init(Obj2* obj,Obj2Class *klass)
{
	 /*constructor for your class */
  g_message("Obj2 init");
}
int obj2_get_type(){
static int type_id=0;
if (!type_id){
	static GTypeInfo Obj2_type= {
	/*Class Size */ 
	 sizeof(Obj2Class),
	/* base init */ 
	 NULL,
	/* base finalize */
	 NULL,
 
	/* class init*/
	 (GClassInitFunc)obj2_class_init,
	/* class finalize*/
	 (GClassFinalizeFunc) NULL,
 /*    not needed for statics ? obj2_class_finalize, */

	/* const static data */
	 NULL,
	/* instance types */
	/* instance size */
	 sizeof(Obj2),
	/* n_preallocs   */
	 0,
	/* InstanceInit  */
	 (GInstanceInitFunc) obj2_init,
	/* value table! */ 
	 NULL
};
type_id=g_type_register_static(TEST_TYPE,
		"Obj2",
		&Obj2_type,
		  0);
/* Comment it if you need to. Good for beginners like me */
g_message("registered type:Obj2 with id %d",type_id);
}

return type_id;
}
GObject* obj2_new(void)
{
         g_message("Obj2 creating new");
	 return (GObject *) g_object_new(OBJ2_TYPE,/* speficy property,values */ NULL);
}


  /* 
     start creating a new object
  ** Message: Obj2 creating new

  ** Message: registered type:test with id 144467960 //parent  type registration
  ** Message: registered type:Obj2 with id 144468184 //derived  ''     ''

     Obj2 new() registers all the base types in a chain order.
     Remeber register_type() takes parent type number, so thats how it
     chains up.

 ** Message: Test class init //base class init  invoked only once
 ** Message: obj2 class init //derived class init ''     ''  ''

     After all the base types are registered() if not already done, the
     object class init is called. This also chains up, using the base_init()
     function and finally class_init() itself. N.B: our base_init() is someone 
     elses class_init().

     Now we have the class structure with all overridden functions ready.

  //constructor not implemented.

     Now we may invoke the constructor for our object, which could also be
     optionally overridden. This constructor will allocate space for our
     object, *after chaining up*, as otherwise its chaos/segfaults. 


  ** Message: Test Object Initializer //parent object initialization
  ** Message: Obj2 init               //derived ''         ''

     Now we may call the instance_init() also in a chaining  mode!

     Please return the object to the user.
  */

int main()
{
  GObject *obj;
  int i=0;
  g_type_init();

  for(i=1;i<4;i++)
    {
      g_message("Object %d",i);
      obj=obj2_new();
      g_object_unref(obj);
    }

  obj=obj2_new();
  g_message("[%s]'s parent is [%s]",g_type_name(G_OBJECT_TYPE(obj)),g_type_name(g_type_parent(G_OBJECT_TYPE(obj))));;

  {
    g_printf("\n\tInheritance Tree [derived -> parent]\n");
    int level=0;
    GType t=0;
    t=G_OBJECT_TYPE(obj);
    do
      {     
	int i=0;
	for(i=0;i<level;i++)
	  printf("  ");
	level++;
	g_printf("%s\n",g_type_name(t));
      }while(t=g_type_parent(t));	/* Drops to 0 at the end [G_TYPE_INVALID] */
  }
  g_object_unref(obj);
  
  return 0;
}
/* gcc -o gobj GObjectInheritance.c `pkg-config --cflags --libs gtk+-2.0` */

