#include<gtk/gtk.h>

int 
main(){
  GSList *sl,*ta;
  int i=0;
  sl=g_slist_alloc();  
  sl->data="GList";
  g_slist_append(sl,"Hello");
  g_slist_append(sl,"World");

  for(ta=sl;ta!=NULL;ta=ta->next)
    {
      i++;
      g_message("[%d] %s",i,(gchar *)ta->data);
    }
  g_slist_free(sl);
  return 0;
}
  
/*  gcc -Wall list.c  `pkg-config gtk+-2.0 --cflags --libs` */
