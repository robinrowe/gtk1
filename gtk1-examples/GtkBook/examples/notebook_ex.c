#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

gboolean page_change(GtkNotebook *n,GtkNotebookPage *p,guint pageno)
{
  g_printf("Page NO: %d\n",pageno);
  return FALSE;
}

GtkWidget * make_notebook(void)
{
  GtkWidget *w,*notebook;
  GtkWidget *text,*curve;
  gfloat value[]={90,100,95, 97,99,93,77,66,60, 89};

  PangoFontDescription *font_desc;
  static gchar *font_str="Times New Roman Italic  20";


  font_desc=pango_font_description_from_string(font_str);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  notebook=gtk_notebook_new();
  
  gtk_container_add(GTK_CONTAINER(w),notebook);

  text=gtk_text_view_new();
  gtk_widget_modify_font(text,font_desc);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
			   text,
			   gtk_label_new_with_mnemonic("_Heaven"));

  text=gtk_text_view_new();
  gtk_widget_modify_font(text,font_desc);

  gtk_notebook_insert_page(GTK_NOTEBOOK(notebook),
			   text,
			   gtk_label_new_with_mnemonic("H_ell"),
			   0);
  
  curve=gtk_curve_new();
  gtk_curve_set_range(GTK_CURVE(curve),
		      0,1,
		      0,100);

  gtk_curve_set_vector(GTK_CURVE(curve),
		       10,value);

  gtk_notebook_insert_page(GTK_NOTEBOOK(notebook),
			   curve,
			   gtk_label_new_with_mnemonic("_Curve"),
			   0);

  gtk_notebook_insert_page(GTK_NOTEBOOK(notebook),
			   gtk_calendar_new(),
			   gtk_label_new_with_mnemonic("_Calendar"),
			   0);


  gtk_notebook_popup_enable(GTK_NOTEBOOK(notebook));
  gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook),
				1);
  gtk_widget_show(notebook);

  //  gtk_notebook_next_page(GTK_NOTEBOOK(notebook));

  gtk_window_set_title(GTK_WINDOW(w),"Notebook[s]");
  gtk_window_set_default_size(GTK_WINDOW(w),200,100);
  pango_font_description_free(font_desc);

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(notebook),"switch_page",GTK_SIGNAL_FUNC(page_change),NULL);

  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);
  w=make_notebook();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}




//gcc -Wall notebook_ex.c `pkg-config libgnomeui-2.0 --cflags --libs`
