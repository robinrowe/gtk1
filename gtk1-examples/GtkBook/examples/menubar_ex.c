#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>


GtkWidget* menu_make(void)
{

  GtkWidget *file,*open,*close,*xit,*bar;
  GtkWidget *filemenu;

  bar=gtk_menu_bar_new();
 
  
  /*
  Making of the File menu.
  */

  file=gtk_menu_new();
  filemenu=gtk_menu_item_new_with_mnemonic("_File");
  
  open=gtk_menu_item_new_with_mnemonic("_Open");
  close=gtk_menu_item_new_with_mnemonic("_Close");
  xit=gtk_menu_item_new_with_mnemonic("_Exit");

  g_signal_connect(G_OBJECT(xit),"activate",G_CALLBACK(gtk_main_quit),NULL);

  gtk_menu_shell_append(GTK_MENU_SHELL(file),open);
  gtk_menu_shell_append(GTK_MENU_SHELL(file),close);
  gtk_menu_shell_append(GTK_MENU_SHELL(file),xit);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(filemenu),file);

  /*	
    Attaching the file menu to the bar.
  */

  gtk_menu_shell_append(GTK_MENU_SHELL(bar),filemenu);
  return bar;
}

int main(int argc,char *argv[])
{
  GtkWidget *w,*bar,*box;
  GtkWidget *menu;


  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);
  bar=gtk_statusbar_new();


  menu=menu_make();


  gtk_container_add(GTK_CONTAINER(w),box);
  gtk_box_pack_start_defaults(GTK_BOX(box),menu);
  gtk_box_pack_start(GTK_BOX(box),gtk_hseparator_new(),!FALSE,!FALSE,0);
  gtk_box_pack_start(GTK_BOX(box),bar,FALSE,FALSE,0);
  

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Menu's & Items");
  gtk_statusbar_push(GTK_STATUSBAR(bar),0,"Making Menu's");  
  gtk_window_set_default_size(GTK_WINDOW(w),200,40);
  gtk_widget_show_all(w);
  
  gtk_main();
  return 0;
}




//gcc -Wall x.c `pkg-config libgnomeui-2.0 --cflags --libs`

/*
  z=gtk_dialog_new();
  gtk_window_set_modal(GTK_WINDOW(z),TRUE);
  gtk_window_set_destroy_with_parent(GTK_WINDOW(z),TRUE);

  b=gtk_button_new_from_stock(GTK_MUTHU_DUCK);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),b);
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_hseparator_new());
  gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(z)->vbox),gtk_label_new(s));
 
  gtk_dialog_set_response_sensitive(GTK_DIALOG(z),
  GTK_RESPONSE_DELETE_EVENT,
  TRUE);
  gtk_widget_show_all(z);
  gtk_dialog_run(GTK_DIALOG(z));
  gtk_widget_destroy(z);
  GtkWidget *z,*b;
*/
