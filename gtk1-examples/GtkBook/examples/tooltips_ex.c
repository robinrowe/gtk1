#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

GtkWidget * make_tooltips_preview(void)
{
  GtkWidget *w,*l;
  GtkTooltips *tt;
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  l=gtk_button_new_from_stock(GTK_STOCK_QUIT);
  

  gtk_container_add(GTK_CONTAINER(w),l);
  

  tt=gtk_tooltips_new();
  gtk_tooltips_set_tip(tt,l,"Click to Exit",NULL);

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(l),"clicked",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_window_set_title(GTK_WINDOW(w),"Great Tips!");
  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);
  w=make_tooltips_preview();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}
//gcc -Wall tooltips_ex.c `pkg-config libgnomeui-2.0 --cflags --libs`
