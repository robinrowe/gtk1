#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<string.h>

//default sizes.

struct Marquee
{
  GtkWidget *w;//eventbox;
  GdkPixmap *pix; //blit the pixmap to the window, on expose.
  PangoLayout *ly;
  gchar *text;
  int rate;
  int count;
  gboolean flag;
  GtkTextDirection type;
  GdkPixbuf *bg_pb;
  GdkPixmap *bg_map;
  GdkBitmap *bg_mask;
};


static gboolean
marquee_draw(struct Marquee *marq);

static gboolean
marquee_scroll(gpointer data);

static void
marquee_size_request (GtkWidget      *widget,
		      GtkRequisition *requisition);

static void 
marquee_size_allocate (GtkWidget     *widget,
		       GtkAllocation *allocation);

static gboolean
marquee_configure_event (GtkWidget      *widget,
			 GdkEventConfigure *event,gpointer data);

static void
marquee_set_text(struct Marquee *marq,const gchar *txt);

gboolean marquee_expose(GtkWidget *w,GdkEventExpose *event,gpointer data);

void
marquee_custom_destroy(GtkWidget *w,gpointer data);


GtkWidget* marquee_widget(const gchar *txt,GtkTextDirection type);
