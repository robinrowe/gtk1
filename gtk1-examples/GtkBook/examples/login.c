#include <gtk/gtk.h>
#include <string.h>

gboolean verify_login(GtkWidget *w, gpointer data)
{
  GtkButton* ok;
  GtkEntry *passwd;
  GtkEntry *uname;
  

  ok=GTK_BUTTON(w);  
  gtk_widget_set_sensitive(w,FALSE);//make it insensitive for a while

  uname=GTK_ENTRY(g_object_get_data(G_OBJECT(ok),"uname"));
  passwd=GTK_ENTRY(g_object_get_data(G_OBJECT(ok),"passwd"));
  
  while(gtk_events_pending())
    gtk_main_iteration();

  if(GTK_IS_ENTRY(uname) && GTK_IS_ENTRY(passwd))
    {
      GtkWidget *msg;
      const gchar *pass,*un;
      static gchar *str[]={"Successfully logged in","Login Failed"};
      int index=1;
      int flag=GTK_MESSAGE_ERROR;

      un=gtk_entry_get_text(uname);
      pass=gtk_entry_get_text(passwd);

      if(un!=NULL && pass!=NULL && 
	 un[0]!='\0' && pass[0]!='\0' 
	 && strcmp(un,pass)==0)
	{
	  index--;
	  flag=GTK_MESSAGE_INFO;
	}

      gtk_widget_grab_focus(GTK_WIDGET(ok));//default button pre-selected.

      msg=gtk_message_dialog_new(NULL,
				 GTK_DIALOG_MODAL,
				 flag,
				 GTK_BUTTONS_OK,
				 "%s",str[index]);
      gtk_window_set_position(GTK_WINDOW(msg),GTK_WIN_POS_CENTER);
      gtk_dialog_run(GTK_DIALOG(msg));
      gtk_widget_destroy(msg);
      gtk_entry_set_text(uname,"");
      gtk_entry_set_text(passwd,"");
    }				     				    
  gtk_widget_set_sensitive(w,TRUE);//make it insensitive for a while
  
  return TRUE;
}


int
main (int argc, char **argv)
{
  GtkWidget* win;
  GtkWidget* uname;
  GtkWidget* luname;
  GtkWidget* passwd;
  GtkWidget* lpasswd;
  GtkWidget* ok;
  GtkWidget* cancel;
  GtkWidget* vbox;
  GtkWidget* hbox;

  gtk_disable_setlocale();
  gtk_init( &argc, &argv );


  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  vbox= gtk_vbox_new(TRUE,TRUE);
  gtk_container_add(GTK_CONTAINER(win),vbox);


  //First Row

  hbox= gtk_hbox_new(TRUE,TRUE);
  luname=gtk_label_new( "User name :");
  uname=gtk_entry_new();

  gtk_entry_set_max_length(GTK_ENTRY(uname),20);


  gtk_box_pack_start_defaults(GTK_BOX(hbox),luname);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),uname);
  gtk_box_pack_start_defaults(GTK_BOX(vbox),hbox);


  //Second Row

  hbox= gtk_hbox_new(TRUE,TRUE);
  lpasswd=gtk_label_new("Password  :");
  passwd=gtk_entry_new();

  gtk_entry_set_max_length(GTK_ENTRY(passwd),20);  
  gtk_entry_set_visibility(GTK_ENTRY(passwd),FALSE);
  gtk_entry_set_invisible_char(GTK_ENTRY(passwd),'*');

  gtk_box_pack_start_defaults(GTK_BOX(hbox),lpasswd);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),passwd);
  gtk_box_pack_start_defaults(GTK_BOX(vbox),hbox);

  //Third Row
  
  hbox= gtk_hbox_new(TRUE,TRUE);
  ok=gtk_button_new_from_stock(GTK_STOCK_OK);
  cancel=gtk_button_new_from_stock(GTK_STOCK_CANCEL);

  gtk_box_pack_start_defaults(GTK_BOX(hbox),cancel);
  gtk_box_pack_start_defaults(GTK_BOX(hbox),ok);
  gtk_box_pack_start_defaults(GTK_BOX(vbox),hbox);

  g_object_set_data(G_OBJECT(ok),"uname",uname);
  g_object_set_data(G_OBJECT(ok),"passwd",passwd);

  g_signal_connect (GTK_OBJECT(ok), "clicked",G_CALLBACK (verify_login), NULL);
  g_signal_connect (GTK_OBJECT(cancel), "clicked",G_CALLBACK (gtk_main_quit), NULL);
  g_signal_connect (GTK_OBJECT(win), "destroy",G_CALLBACK (gtk_main_quit), NULL);
  
  /* GTK_WIDGET_UNSET_FLAGS(cancel,GTK_CAN_FOCUS); */ /* Quite Annoying*/
  GTK_WIDGET_SET_FLAGS(ok,GTK_CAN_FOCUS);
  gtk_widget_grab_focus(ok);//default button pre-selected.

  gtk_window_set_title(GTK_WINDOW(win),"  Login Window  ");
  gtk_window_set_position(GTK_WINDOW(win),GTK_WIN_POS_CENTER);

  gtk_container_set_border_width(GTK_CONTAINER(win),10);
  
  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}
//gcc -Wall login.c -o login `pkg-config gtk+-2.0 --cflags --libs`
