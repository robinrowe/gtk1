#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>
#include<time.h>
#include<stdlib.h>


void color_select(GtkWidget *btn,gpointer data)
{
  GtkWidget* cs;
  cs=gtk_color_selection_dialog_new("Color Selection Dialog.");
 
  if(gtk_dialog_run(GTK_DIALOG(cs)) == GTK_RESPONSE_OK)
    {
	GdkColor c;
	GtkColorSelection *csl;
	csl=GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(cs)->colorsel);
	gtk_color_selection_get_current_color (csl,&c);
	g_printf("r=%02X g=%02X b=%02X\n",(c.red&0xff00) >> 8,(c.green&0xff00) >> 8,(c.blue& 0xff00) >> 8);
    }
  gtk_widget_destroy(cs);
}


GtkWidget * color_widget(void)
{
  GtkWidget *w;
  GtkWidget *box,*button;

  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  box=gtk_hbox_new(FALSE,FALSE);

  gtk_container_add(GTK_CONTAINER(w),box);

  button=gtk_button_new_with_label("Color Select");  
  gtk_box_pack_start_defaults(GTK_BOX(box),button);

  gtk_widget_show_all(box);
  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
  gtk_signal_connect(GTK_OBJECT(button),"clicked",GTK_SIGNAL_FUNC(color_select),NULL);
  gtk_window_set_title(GTK_WINDOW(w),"Color Select");
  gtk_window_resize(GTK_WINDOW(w),200,40);

  return w;
}



int main(int argc,char *argv[])
{
  GtkWidget *w;

  gtk_init(&argc,&argv);

  w=color_widget();
  gtk_widget_show_all(w);

  gtk_main();
  return 0;
}

//gcc -Wall colorsel_ex.c `pkg-config libgnomeui-2.0 --cflags --libs` -o colorsel_ex
