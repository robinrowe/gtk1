#include<gtk/gtk.h>
#include<gtk/gtkitemfactory.h>
#include<glib/gprintf.h>

static gchar *prefix="/usr/share/pixmaps/";
static gchar *fname[]={"gnome-emacs.png","disks.png","gnome-aorta.png","Bird.xpm","Ant.xpm","Antennae.xpm",NULL};
static gchar *sname[]={"E_MACS","_DISK","_AORTA","_BIRD","A_NT","AN_TENNAE",NULL};
static gchar cname []="mdabnt";

gboolean  kill_hide(GtkWidget *x,gpointer data)
{
  gtk_widget_destroy(GTK_WIDGET((GtkWidget *)data));
  return FALSE;
}

void show_me(GtkWidget *w, gpointer data)
{
  static GtkWidget *x=NULL;
  GtkWidget *j;
  g_printf("%s\n",(gchar *)data);

  //  if(x!=NULL)                  #destroy earlier windows?
  //    gtk_widget_destroy(x);

  x=gtk_window_new(GTK_WINDOW_TOPLEVEL);

  j=gtk_button_new_from_stock((const gchar *)data);
  gtk_container_add(GTK_CONTAINER(x),j);

  gtk_signal_connect(GTK_OBJECT(j),"clicked",GTK_SIGNAL_FUNC(kill_hide),x);
  gtk_widget_show_all(x);
}

void make_stock(void)
{
  int i=0;
  gchar *str;

  GtkIconFactory *ifac;
  GtkIconSet *iset;
  GdkPixbuf *pb;
  GtkStockItem *sitem;
  
  iset=gtk_icon_set_new();
  ifac=gtk_icon_factory_new();


  for(i=0;fname[i]!=NULL;i++)
    {
      str=g_strdup_printf("%s%s",prefix,fname[i]);     
      pb=gdk_pixbuf_new_from_file(str,NULL);
      iset=gtk_icon_set_new_from_pixbuf(pb);
      gtk_icon_factory_add(ifac,sname[i],iset);//add set to stock factory.
      gdk_pixbuf_unref(pb);
      g_free(str);
    }

  gtk_icon_factory_add_default(ifac);//add factory to default stock factory.    


  sitem=(GtkStockItem *)g_malloc(sizeof(GtkStockItem));
  //register the stocks.
  for(i=0;fname[i]!=NULL;i++)
    {
      sitem->stock_id=sname[i];
      sitem->label=sname[i];
      sitem->modifier=0;
      sitem->keyval=0;
      sitem->translation_domain=NULL;
      gtk_stock_add(sitem,1);
    }   
  g_free(sitem);
}



int main(int argc,char *argv[])
{
  GtkWidget *w,*box,*b;
  int i=0;

  gtk_init(&argc,&argv);
  
  w=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box=gtk_vbox_new(FALSE,FALSE);

  make_stock();

  gtk_container_add(GTK_CONTAINER(w),box);

  for(i=0;sname[i]!=NULL;i++)    
    {
      b=gtk_button_new_from_stock(sname[i]);
      gtk_box_pack_start_defaults(GTK_BOX(box),b);
      gtk_box_pack_start(GTK_BOX(box),gtk_hseparator_new(),!FALSE,!FALSE,0);
      gtk_signal_connect(GTK_OBJECT(b),"clicked",GTK_SIGNAL_FUNC(show_me),sname[i]);
    }

  

  gtk_signal_connect(GTK_OBJECT(w),"destroy",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);


  gtk_window_set_title(GTK_WINDOW(w),"Personal Stocks!");
  gtk_window_set_default_size(GTK_WINDOW(w),200,40);
  gtk_widget_show_all(w);
  
  gtk_main();
  return 0;
}

//gcc -Wall stock_ex.c `pkg-config libgnomeui-2.0 --cflags --libs`

