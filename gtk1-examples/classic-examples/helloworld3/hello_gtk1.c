// hello_gtk1.c

#include <gtk/gtk.h>

void click_event( GtkWidget *widget,gpointer data )
{    g_print ("Hello again - %s was pressed\n", (char *) data);
}

gint delete_event( GtkWidget *widget,GdkEvent  *event,gpointer data )
{   gtk_main_quit();
	return FALSE;
}

GtkWidget *window;
GtkWidget *button;
GtkWidget *box;

int main(int argc,char *argv[])
{   gtk_init (&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW (window), "Hello World!");
	gtk_signal_connect(GTK_OBJECT(window), "delete_event",GTK_SIGNAL_FUNC (delete_event), NULL);
	gtk_container_set_border_width(GTK_CONTAINER (window), 10);
	box = gtk_hbox_new(FALSE, 0);
	gtk_container_add (GTK_CONTAINER(window), box);
	button = gtk_button_new_with_label ("Button 1");
	gtk_signal_connect (GTK_OBJECT (button),"clicked",GTK_SIGNAL_FUNC(click_event),(gpointer)"button 1");
	gtk_box_pack_start(GTK_BOX(box),button,TRUE,TRUE,0);
	gtk_widget_show(button);
	button = gtk_button_new_with_label("Button 2");
	gtk_signal_connect (GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC(click_event), (gpointer) "button 2");
	gtk_box_pack_start(GTK_BOX(box),button,TRUE,TRUE,0);
	gtk_widget_show(button);
	gtk_widget_show(box);
	gtk_widget_show (window);
	gtk_main();
	return 0;
}