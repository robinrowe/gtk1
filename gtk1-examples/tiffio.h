// tiff.h: this is a fake-out of the real tiff library, for unit tests only
// Copyright 2021/03/17 Robin.Rowe@CinePaint.org
// License MIT open source

#ifndef tiff_h
#define tiff_h

#include <memory.h>
#include <stdio.h>

typedef unsigned int uint32;
typedef unsigned int uint16;
typedef int TIFF;
#define LSBFirst 0
#define TIFFTAG_IMAGEWIDTH 640
#define TIFFTAG_IMAGELENGTH 480
#define TIFFTAG_BITSPERSAMPLE 8
#define TIFFTAG_SAMPLESPERPIXEL 3

void TIFFGetField(TIFF* tif, unsigned size, int* out);
TIFF* TIFFOpen(const char* filename,const char* flags);
int TIFFReadRGBAImage(TIFF* tif, unsigned width, unsigned height, unsigned* raster, int flag);
unsigned PixelGet(unsigned RGB,unsigned offset);
unsigned TIFFGetR(unsigned RGB);
unsigned TIFFGetG(unsigned RGB);
unsigned TIFFGetB(unsigned RGB);
void PrintPixel(unsigned pixel);

#endif