// tiff.c: this is a fake-out of the real tiff library, for unit tests only
// Copyright 2021/03/17 Robin.Rowe@CinePaint.org
// License MIT open source

#include <stdlib.h>
#include "tiffio.h"

void TIFFGetField(TIFF* tif, unsigned size, int* out)
{	*out = size;
}

TIFF* TIFFOpen(const char* filename,const char* flags)
{	TIFF* tif = (TIFF*) malloc(sizeof(TIFF));
	return tif;
}

enum { r, g, b, a };

int TIFFReadRGBAImage(TIFF* tif, unsigned width, unsigned height, unsigned* raster, int flag)
{	unsigned pixel[3];
	unsigned char yellow[4];
	yellow[r] = 255;
	yellow[g] = 255;
	yellow[b] = 0;
	yellow[a] = 0;
	unsigned char blue[4];
	blue[r] = 0;
	blue[g] = 0;
	blue[b] = 255;
	blue[a] = 0;
	unsigned char red[4];
	red[r] = 255;
	red[g] = 0;
	red[b] = 0;
	red[a] = 0;
	pixel[0] = *(unsigned*) &yellow;
	pixel[1] = *(unsigned*) &blue;
	pixel[2] = *(unsigned*) &red;
	unsigned choice = 0;
	unsigned step = 1; 
	unsigned color = pixel[2];
	for (unsigned j = 0; j < width; j++)
	{	*raster = color;
		raster++;
	}
	for(unsigned i=1;i<height;i++)
	{	step--; 
		if(!step)
		{	step = 40;
			choice = !choice;
		}
//		printf("%i ", choice);
		unsigned color = pixel[choice];
		for(unsigned j=0;j<width;j++)
		{	*raster = color;
			raster++;
		}
	}
	return 1;
}

unsigned PixelGet(unsigned RGB,unsigned offset)
{	unsigned char pixel[4];
	unsigned* p = (unsigned*)&pixel;
	*p = RGB;
	return pixel[offset];
}

unsigned TIFFGetR(unsigned RGB)
{	return PixelGet(RGB,r);
}

unsigned TIFFGetG(unsigned RGB)
{	return PixelGet(RGB,g);
}

unsigned TIFFGetB(unsigned RGB)
{	return PixelGet(RGB,b);
}

void PrintPixel(unsigned pixel)
{	unsigned red = PixelGet(pixel, r);
	unsigned green = PixelGet(pixel, g);
	unsigned blue = PixelGet(pixel, b);
	printf("%u:%u:%u ",red,green,blue);
}
