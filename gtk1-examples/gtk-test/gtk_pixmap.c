// gtk_pixbuf.c
// Copyright 2021/03/17 Robin.Rowe@CinePaint.org
// License MIT open source

#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <gtk/gtkobject.h>
#include <gtk/gtkpixmap.h>
#include <tiffio.h>

gboolean on_delete(GtkWidget * widget, GdkEvent * event, gpointer data)
{	printf("delete event occured\n");
	return FALSE;
}

void on_destroy(GtkWidget *widget, GdkEvent *event, gpointer data)
{	printf("destroy event occured\n"); 
	gtk_main_quit();
}

void on_click(GtkWidget* widget,gpointer data)
{	g_print("Clicked %s\n", (char*)data);
}

int main(int argc, char *argv[])
{	gtk_init(&argc, &argv);
	GtkWidget* window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect(GTK_OBJECT(window), "delete_event", GTK_SIGNAL_FUNC(on_delete), NULL);
	gtk_signal_connect(GTK_OBJECT(window), "destroy", GTK_SIGNAL_FUNC(on_destroy), NULL);
	TIFF* tiff = 0;
	unsigned width = 640;
	unsigned height = 480;
	unsigned depth = 24;
	uint32* raster = (uint32*)malloc(width * height * sizeof(uint32));
	if (!raster) {
		perror("malloc");
		return 1;
	}
	if (!TIFFReadRGBAImage(tiff, width, height, raster, 0)) {
		printf("TIFFReadRGBAImage failed\n");
		return 2;
	}
	gtk_widget_set_usize(window, width, height);
	GtkWidget* vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	GdkColor fg;
	fg.red = 0xffff;
	fg.green = 0xffff;
	fg.blue = 0xffff;
	GdkColor bg;
	bg.red = 0;
	bg.green = 0;
	bg.blue = 0;
//	GdkPixmap* pixmap = gdk_pixmap_create_from_data(window->window,(const gchar*) raster,width, height,depth,&fg,&bg);
	GdkPixmap* pixmap = gdk_pixmap_new(0, width, height, depth);
	if (!pixmap)
	{	return 0;
	}
	pixmap->user_data = (gpointer)raster;
#if 0
	GdkDrawablePrivate* p = (GdkDrawablePrivate*)result;
	p->drawable.user_data = (gpointer)data;
	printf("GdkPixmap %ix%i data=%u xid=%u\n", p->width, p->height, (unsigned)p->drawable.user_data, (unsigned)GDK_DRAWABLE_XID(result));
#endif

#if 0
	gdk_draw_pixmap(window->window,
		window->style->black_gc,
		1,
		0, 0, width, height, -1, -1);
#endif
	GdkBitmap* mask = 0;
//	GtkImage* image = 0;
//	gtk_image_set_from_pixmap(image,pixmap,mask);
	GtkWidget* pixmap_widget = gtk_pixmap_new(pixmap, mask);
	gtk_pixmap_set_build_insensitive(GTK_PIXMAP(pixmap_widget), 1);
// BUG?	build_insensitive_pixmap(GTK_PIXMAP(pixmap_widget));
	gtk_box_pack_start(GTK_BOX(vbox), pixmap_widget, TRUE, TRUE, 0);
//	gtk_widget_show(image);
	gtk_widget_show(window);
	gtk_main();
	return 0;
}

