// gtk_main.c
// Copyright 2021/03/17 Robin.Rowe@CinePaint.org
// License MIT open source

#include <stdio.h>
#include <gtk/gtk.h>
#include <gtk/gtkobject.h>

gboolean on_delete(GtkWidget * widget, GdkEvent * event, gpointer data)
{	printf("delete event occured\n");
	return FALSE;
}

void on_destroy(GtkWidget *widget, GdkEvent *event, gpointer data)
{	printf("destroy event occured\n"); 
	gtk_main_quit();
}

void on_click(GtkWidget* widget,gpointer data)
{	g_print("Clicked %s\n", (char*)data);
}

int main(int argc, char *argv[])
{	GtkWidget *window;
	GtkWidget *button;
	/* initialize library and parse command line arguments */
	gtk_init(&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect(GTK_OBJECT(window), "delete_event", GTK_SIGNAL_FUNC(on_delete), NULL);
	gtk_signal_connect(GTK_OBJECT(window), "destroy", GTK_SIGNAL_FUNC(on_destroy), NULL);
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);
	button = gtk_button_new_with_label("Hello world");
	gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(on_click), (gpointer)"cool button");
	gtk_container_add(GTK_CONTAINER(window), button);
	gtk_widget_show(button);
	gtk_widget_show(window);
	gtk_main();
	return 0;
}

