# GtkTest/CMakeLists.txt
# Created by Robin Rowe 2021-03-16
# License MIT open source license

cmake_minimum_required(VERSION 3.8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
project(GtkTest)
message("Configuring ${PROJECT_NAME}...")

add_executable(test_gtk_main ./gtk_main.c)
add_executable(test_gtk_pixmap ./gtk_pixmap.c)