// hello_cgui.c
// Copyright 2023/3/10 Robin.Rowe@CinePaint.org
// License MIT Open Source

#include <stdio.h>
#include <cgui.h>

typedef enum boolean {false,true} boolean;
typedef int flag;
typedef struct cgui_display cgui_display;
typedef struct cgui_widget cgui_widget;
typedef struct cgui_event cgui_event;
typedef int cgui_callback(cgui_widget* widget,cgui_event* event,void* data);

cgui_display* cgui_open(int argc,const char* argv[]);
cgui_widget* cgui_create_window(int flag);
boolean cgui_set(int flag,cgui_widget* widget,void* data);
boolean cgui_connect(int flag,cgui_widget* widget,cgui_event* event,void* data0);
boolean cgui_show(cgui_widget* widget,boolean isVisible);
boolean cgui_get(int flag,cgui_widget* widget,void* data);
boolean cgui_destroy_window(cgui_widger* widget)
void cgui_close(const char* msg,int errorlevel);


boolean click_event(cgui_widget* widget,cgui_event* event,void* data)
{   printf("Hello again - %s was pressed\n", (const char* ) data);
	return true;
}

boolean delete_event(cgui_widget* widget,cgui_event* event,void* data)
{   cgui_quit("goodbye",0);
	return false;
}

cgui_display* display;
cgui_widget* frame;
cgui_widget* box;
cgui_widget* button;

void OpenGui()
{	display = cgui_open(CGUI_GTK1,argc,argv);
	char platform[20];
	cgui_get(CGUI_PLATFORM,display,&platform);
	printf("CGUI_PLATFORM = %s\n",platform);
}

void CreateWindow()
{	frame = cgui_create_window(CGUI_TOPLEVEL,0);
	cgui_set(CGUI_WINDOW_TITLE,frame,"Hello World!");
	cgui_connect(CGUI_EVENT_DELETE,winddow,delete_event,0);
	const int width = 10;
	cgui_set(CGUI_WINDOW_BORDER_WIDTH,frame,&width);
}

void CreateBox()
{	box = cgui_create_window(CGUI_HBOX,frame);
//gui_set(CGUI_CHILD,frame,box);
	cgui_show(box);
}

void CreateButtons()
{	button = cgui_window(CGUI_BUTTON);
	cgui_set(CGUI_LABEL,button,"Button 1");
	cgui_connect(CGUI_EVENT_CLICK,button,click_event,"button 1");
	cgui_set(CGUI_CHILD,box,button);
	cgui_show(button);
	button = cgui_window(CGUI_BUTTON);
	cgui_set(CGUI_LABEL,button,"Button 2");
	cgui_connect(CGUI_EVENT_CLICK,button,click_event,"button 2");
	cgui_set(CGUI_CHILD,box,button);
	cgui_show(button);
}

int main(int argc,char* argv[])
{	CreateContext();
	CreateWindow();
	CreateBox();
	CreateButtons();
	cgui_show(frame);
	return cgui_main();
}