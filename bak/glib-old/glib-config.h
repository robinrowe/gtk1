#ifndef glib_config_h
#define glib_config_h

#include <limits.h>
#include <float.h>
#include "gmacros.h"
#include <inttypes.h> 

#define GLIB_MAJOR_VERSION 2
#define GLIB_MICRO_VERSION 1
#define GLIB_MINOR_VERSION 2
#define GLIB_INTERFACE_AGE 1
#define GLIB_BINARY_AGE 201

//#define G_GUINT64_FORMAT "I64u"
#define G_GUINT64_FORMAT PRId64

#define G_BYTE_ORDER G_LITTLE_ENDIAN

#define HAVE_MALLOC_H 1
#define HAVE_MEMMOVE 1

typedef struct GMutex* GStaticMutex;

#define G_THREADS_ENABLED
#define G_THREADS_IMPL_WIN32
typedef struct GMutex* GStaticMutex;
#define G_STATIC_MUTEX_INIT NULL
#define g_static_mutex_get_mutex(mutex) (g_static_mutex_get_mutex_impl (mutex))
typedef union _GSystemThread GSystemThread;
union _GSystemThread
{
  char   data[4];
  double dummy_double;
  void  *dummy_pointer;
  long   dummy_long;
};

#define HAVE_FLOAT_H
#ifndef HAVE_GETCWD
#define HAVE_GETCWD
#endif
#ifndef HAVE_DIRENT_H
#define HAVE_DIRENT_H
#endif
#ifndef HAVE_UNISTD_H
#define HAVE_UNISTD_H
#endif
#define NO_SYS_SIGLIST 1
#define NO_SYS_SIGLIST_DECL
//#define ENABLE_NLS 1

#define GPOINTER_TO_INT(p)	((intptr_t)   (p))
#define GPOINTER_TO_UINT(p)	((uintptr_t)  (p))
#define GINT_TO_POINTER(i)	((gpointer)(intptr_t)  (i))
#define GUINT_TO_POINTER(u)	((gpointer)(uintptr_t)  (u))

#define GLIB_SIZEOF_VOID_P sizeof(void*)
#define GLIB_SIZEOF_LONG   sizeof(long)
#define GLIB_SIZEOF_SIZE_T sizeof(size_t)

#define GLIB_SYSDEF_POLLIN =1
#define GLIB_SYSDEF_POLLOUT =4
#define GLIB_SYSDEF_POLLPRI =2
#define GLIB_SYSDEF_POLLHUP =16
#define GLIB_SYSDEF_POLLERR =8
#define GLIB_SYSDEF_POLLNVAL =32

#define g_memmove(d,s,n) G_STMT_START { memmove ((d), (s), (n)); } G_STMT_END


#define G_MINFLOAT	FLT_MIN
#define G_MAXFLOAT	FLT_MAX
#define G_MINDOUBLE	DBL_MIN
#define G_MAXDOUBLE	DBL_MAX
#define G_MINSHORT	SHRT_MIN
#define G_MAXSHORT	SHRT_MAX
#define G_MAXUSHORT	USHRT_MAX
#define G_MININT	INT_MIN
#define G_MAXINT	INT_MAX
#define G_MAXUINT	UINT_MAX
#define G_MINLONG	LONG_MIN
#define G_MAXLONG	LONG_MAX
#define G_MAXULONG	ULONG_MAX

#define G_MININT64	((gint64)  0x8000000000000000)
#define G_MAXINT64	((gint64)  0x7fffffffffffffff)
#define G_MAXUINT64	((guint64) 0xffffffffffffffff)

#define HAVE_ATEXIT 1
#define STDC_HEADERS 1
//#define ENABLE_NLS 1
#ifdef ENABLE_NLS
#define GETTEXT_PACKAGE "glib20"
#define GLIB_LOCALE_DIR "NONE/lib/locale"
#define HAVE_GETTEXT 1
#define USE_LIBICONV_GNU Using GNU libiconv
//  https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.16.tar.gz
//  https://ftp.gnu.org/pub/gnu/gettext/gettext-0.21.tar.gz.
#endif

//#define GdkSetFlag(type,name,value) { int x = name; x |= value; name = (type) x; }

#define GDK_USE_UTF8_MBS
#define GDK_HAVE_WCHAR_H 1
#define GDK_HAVE_WCTYPE_H 1

#endif
