/* gmain.h - the GLib Main loop
 * Copyright (C) 1998-2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __G_MAIN_H__
#define __G_MAIN_H__
#include <unistd.h>
#include <glib/gslist.h>
#include <glib/gthread.h>
#include <glib/gmacros.h>
#include <glib/garray.h>

#if 0
typedef struct _GTimeoutSource GTimeoutSource;
typedef struct _GPollRec GPollRec;
typedef struct _GSourceCallback GSourceCallback;
#endif

typedef enum
{
  G_SOURCE_READY = 1 << G_HOOK_FLAG_USER_SHIFT,
  G_SOURCE_CAN_RECURSE = 1 << (G_HOOK_FLAG_USER_SHIFT + 1)
} GSourceFlags;

G_BEGIN_DECLS

#if 0
typedef struct _GMainContext	        GMainContext;	/* Opaque */
typedef struct _GMainLoop	        GMainLoop;	/* Opaque */
typedef struct _GSource	                GSource;
typedef struct _GSourceCallbackFuncs	GSourceCallbackFuncs;
typedef struct _GSourceFuncs	        GSourceFuncs;
#endif

typedef gboolean (*GSourceFunc)       (gpointer data);

#ifdef G_THREADS_ENABLED
//typedef struct _GMainWaiter GMainWaiter;

typedef struct GMainWaiter
{
	GCond* cond;
	GMutex* mutex;
} GMainWaiter;
#endif  

typedef struct GPollFD
{
	HANDLE		fd;
	gushort 	events;
	gushort 	revents;
} GPollFD;

typedef gint(*GPollFunc)	(GPollFD* ufds,
	guint	  nfsd,
	gint     timeout_);
#if 0
typedef struct GSourceFuncs GSourceFuncs;
typedef struct GMainContext GMainContext;
typedef struct GPollRec GPollRec;
#endif

typedef void (*GSourceDummyMarshal) (void);
typedef struct GSourceCallbackFuncs GSourceCallbackFuncs;
typedef struct GSource GSource;

typedef struct GSourceFuncs
{
#ifdef GTK_OLD_VERSION
  gboolean (*prepare)  (GSource    *source,
			gint       *timeout_);
  gboolean(*check)    (GSource* source);
  gboolean(*dispatch) (GSource* source,
	  GSourceFunc callback,
	  gpointer    user_data);
  GDestroyNotify destroy;
#else
	gboolean(*prepare) (gpointer  source_data,
		GTimeVal* current_time,
		gint* timeout,
		gpointer  user_data);
  gboolean(*check)(gpointer  source_data,
	  GTimeVal* current_time,
	  gpointer  user_data);
  gboolean(*dispatch) (gpointer source_data,
	  GTimeVal* dispatch_time,
	  gpointer user_data);
#endif
  void     (*finalize) (GSource* source); /* Can be NULL */
  /* For use by g_source_set_closure */
  GSourceFunc     closure_callback;	   
  GSourceDummyMarshal closure_marshal; /* Really is of type GClosureMarshal */
} GSourceFuncs;

typedef struct GMainContext GMainContext;

typedef struct GSource
{
	/*< private >*/
	gpointer callback_data;
	GSourceCallbackFuncs* callback_funcs;

	GSourceFuncs* source_funcs;
	guint ref_count;

	GMainContext* context;

	gint priority;
	guint flags;
	guint source_id;

	GSList* poll_fds;

	GSource* prev;
	GSource* next;

	gpointer source_data;//gpointer reserved1;
	GSourceFuncs hook;//gpointer reserved2;
} GSource;

typedef struct GPollRec GPollRec;

typedef struct GMainContext
{
#ifdef G_THREADS_ENABLED
	/* The following lock is used for both the list of sources
	 * and the list of poll records
	 */
	GStaticMutex mutex;
	GCond* cond;
	GThread* owner;
	guint owner_count;
	GSList* waiters;
#endif  

	guint ref_count;

	GPtrArray* pending_dispatches;
	gint timeout;			/* Timeout for current iteration */

	guint next_id;
	GSource* source_list;
	gint in_check_or_prepare;

	GPollRec* poll_records;
	GPollRec* poll_free_list;
	GMemChunk* poll_chunk;
	guint n_poll_records;
	GPollFD* cached_poll_array;
	guint cached_poll_array_size;

#ifdef G_THREADS_ENABLED  
#ifndef G_OS_WIN32
	/* this pipe is used to wake up the main loop when a source is added.
	 */
	gint wake_up_pipe[2];
#else /* G_OS_WIN32 */
	HANDLE wake_up_semaphore;
#endif /* G_OS_WIN32 */

	GPollFD wake_up_rec;
	gboolean poll_waiting;

	/* Flag indicating whether the set of fd's changed during a poll */
	gboolean poll_changed;
#endif /* G_THREADS_ENABLED */

	GPollFunc poll_func;

	GTimeVal current_time;
	gboolean time_is_current;
	GHookList hook_list;
} GMainContext;

typedef struct GSourceCallback
{
	guint ref_count;
	GSourceFunc func;
	gpointer    data;
	GDestroyNotify notify;
} GSourceCallback;

typedef struct GMainLoop
{
	GMainContext* context;
	gboolean is_running;
	guint ref_count;
} GMainLoop;

typedef struct GTimeoutSource
{
	GSource     source;
	GTimeVal    expiration;
	guint       interval;
} GTimeoutSource;

typedef struct GPollRec
{
	gint priority;
	GPollFD* fd;
	GPollRec* next;
} GPollRec;

typedef struct GSourceCallbackFuncs
{
  void (*ref)   (gpointer     cb_data);
  void (*unref) (gpointer     cb_data);
  void (*get)   (gpointer     cb_data,
		 GSource     *source, 
		 GSourceFunc *func,
		 gpointer    *data);
} GSourceCallbackFuncs;


/* Any definitions using GPollFD or GPollFunc are primarily
 * for Unix and not guaranteed to be the compatible on all
 * operating systems on which GLib runs. Right now, the
 * GLib does use these functions on Win32 as well, but interprets
 * them in a fairly different way than on Unix. If you use
 * these definitions, you are should be prepared to recode
 * for different operating systems.
 *
 *
 * On Win32, the fd in a GPollFD should be Win32 HANDLE (*not* a file
 * descriptor as provided by the C runtime) that can be used by
 * MsgWaitForMultipleObjects. This does *not* include file handles
 * from CreateFile, SOCKETs, nor pipe handles. (But you can use
 * WSAEventSelect to signal events when a SOCKET is readable).
 *
 * On Win32, fd can also be the special value G_WIN32_MSG_HANDLE to
 * indicate polling for messages. These message queue GPollFDs should
 * be added with the g_main_poll_win32_msg_add function.
 *
 * But note that G_WIN32_MSG_HANDLE GPollFDs should not be used by GDK
 * (GTK) programs, as GDK itself wants to read messages and convert them
 * to GDK events.
 *
 * So, unless you really know what you are doing, it's best not to try
 * to use the main loop polling stuff for your own needs on
 * Win32. It's really only written for CinePaint's needs so
 * far.
 */
//typedef struct _GPollFD GPollFD;

/* Standard priorities */

#define G_PRIORITY_HIGH            -100
#define G_PRIORITY_DEFAULT          0
#define G_PRIORITY_HIGH_IDLE        100
#define G_PRIORITY_DEFAULT_IDLE     200
#define G_PRIORITY_LOW	            300

/* GMainContext: */

GLIB_VAR GMainContext *g_main_context_new       (void);
GLIB_VAR void          g_main_context_ref       (GMainContext *context);
GLIB_VAR void          g_main_context_unref     (GMainContext *context);
GLIB_VAR GMainContext *g_main_context_default   (void);

GLIB_VAR gboolean      g_main_context_iteration (GMainContext *context,
					gboolean      may_block);
GLIB_VAR gboolean      g_main_context_pending   (GMainContext *context);

/* For implementation of legacy interfaces
 */
GLIB_VAR GSource      *g_main_context_find_source_by_id              (GMainContext *context,
							     guint         source_id);
GLIB_VAR GSource      *g_main_context_find_source_by_user_data       (GMainContext *context,
							     gpointer      user_data);
GLIB_VAR GSource      *g_main_context_find_source_by_funcs_user_data (GMainContext *context,
 							     GSourceFuncs *funcs,
							     gpointer      user_data);

/* Low level functions for implementing custom main loops.
 */
GLIB_VAR void     g_main_context_wakeup  (GMainContext *context);
GLIB_VAR gboolean g_main_context_acquire (GMainContext *context);
GLIB_VAR void     g_main_context_release (GMainContext *context);
GLIB_VAR gboolean g_main_context_wait    (GMainContext *context,
				 GCond        *cond,
				 GMutex       *mutex);

GLIB_VAR gboolean g_main_context_prepare  (GMainContext *context,
				  gint         *priority);
GLIB_VAR gint     g_main_context_query    (GMainContext *context,
				  gint          max_priority,
				  gint         *timeout_,
				  GPollFD      *fds,
				  gsize          n_fds);
GLIB_VAR gboolean     g_main_context_check    (GMainContext *context,
				  gint          max_priority,
				  GPollFD      *fds,
	gsize          n_fds);
GLIB_VAR void     g_main_context_dispatch (GMainContext *context);

GLIB_VAR void      g_main_context_set_poll_func (GMainContext *context,
					GPollFunc     func);
GLIB_VAR GPollFunc g_main_context_get_poll_func (GMainContext *context);

/* Low level functions for use by source implementations
 */
GLIB_VAR void g_main_context_add_poll      (GMainContext *context,
				   GPollFD      *fd,
				   gint          priority);
GLIB_VAR void g_main_context_remove_poll   (GMainContext *context,
				   GPollFD      *fd);

/* GMainLoop: */

GLIB_VAR GMainLoop *g_main_loop_new        (GMainContext *context,
			    	   gboolean      is_running);
GLIB_VAR void       g_main_loop_run        (GMainLoop    *loop);
GLIB_VAR void       g_main_loop_quit       (GMainLoop    *loop);
GLIB_VAR GMainLoop *g_main_loop_ref        (GMainLoop    *loop);
GLIB_VAR void       g_main_loop_unref      (GMainLoop    *loop);
GLIB_VAR gboolean   g_main_loop_is_running (GMainLoop    *loop);
GLIB_VAR GMainContext *g_main_loop_get_context (GMainLoop    *loop);

/* GSource: */

GLIB_VAR GSource *g_source_new             (GSourceFuncs   *source_funcs,
				   guint           struct_size);
GLIB_VAR GSource *g_source_ref             (GSource        *source);
GLIB_VAR void     g_source_unref           (GSource        *source);

GLIB_VAR guint    g_source_attach          (GSource        *source,
				   GMainContext   *context);
GLIB_VAR void     g_source_destroy         (GSource        *source);

GLIB_VAR void     g_source_set_priority    (GSource        *source,
				   gint            priority);
GLIB_VAR gint     g_source_get_priority    (GSource        *source);
GLIB_VAR void     g_source_set_can_recurse (GSource        *source,
				   gboolean        can_recurse);
GLIB_VAR gboolean g_source_get_can_recurse (GSource        *source);
GLIB_VAR guint    g_source_get_id          (GSource        *source);

GLIB_VAR GMainContext *g_source_get_context (GSource       *source);

GLIB_VAR void g_source_set_callback          (GSource              *source,
				     GSourceFunc           func,
				     gpointer              data,
				     GDestroyNotify        notify);


/* Used to implement g_source_connect_closure and internally*/
GLIB_VAR void g_source_set_callback_indirect (GSource              *source,
				     gpointer              callback_data,
				     GSourceCallbackFuncs *callback_funcs);

GLIB_VAR void     g_source_add_poll         (GSource        *source,
				    GPollFD        *fd);
GLIB_VAR void     g_source_remove_poll      (GSource        *source,
				    GPollFD        *fd);

GLIB_VAR void     g_source_get_current_time (GSource        *source,
				    GTimeVal       *timeval);

 /* void g_source_connect_closure (GSource        *source,
                                  GClosure       *closure);
 */

/* Specific source types
 */
GLIB_VAR GSource *g_idle_source_new    (void);
GLIB_VAR GSource *g_timeout_source_new (guint         interval);

/* Miscellaneous functions
 */
GLIB_VAR void g_get_current_time		        (GTimeVal	*result);

/* ============== Compat main loop stuff ================== */

#ifndef G_DISABLE_DEPRECATED

/* Legacy names for GMainLoop functions
 */
#define 	g_main_new(is_running)	g_main_loop_new (NULL, is_running);
#define         g_main_run(loop)        g_main_loop_run(loop)
#define         g_main_quit(loop)       g_main_loop_quit(loop)
#define         g_main_destroy(loop)    g_main_loop_unref(loop)
#define         g_main_is_running(loop) g_main_loop_is_running(loop)

/* Functions to manipulate the default main loop
 */

#define	g_main_iteration(may_block) g_main_context_iteration      (NULL, may_block)
#define g_main_pending()            g_main_context_pending        (NULL)

#define g_main_set_poll_func(func)   g_main_context_set_poll_func (NULL, func)

#endif /* G_DISABLE_DEPRECATED */

/* Source manipulation by ID */
GLIB_VAR gboolean g_source_remove                     (guint          tag);
GLIB_VAR gboolean g_source_remove_by_user_data        (gpointer       user_data);
GLIB_VAR gboolean g_source_remove_by_funcs_user_data  (GSourceFuncs  *funcs,
					      gpointer       user_data);

/* Idles and timeouts */
GLIB_VAR guint		g_timeout_add_full	(gint           priority,
					 guint          interval, 
					 GSourceFunc    function,
					 gpointer       data,
					 GDestroyNotify notify);
GLIB_VAR guint		g_timeout_add		(guint          interval,
					 GSourceFunc    function,
					 gpointer       data);
GLIB_VAR guint		g_idle_add	   	(GSourceFunc	function,
					 gpointer	data);
GLIB_VAR guint	   	g_idle_add_full		(gint   	priority,
					 GSourceFunc	function,
					 gpointer	data,
					 GDestroyNotify notify);
GLIB_VAR gboolean	g_idle_remove_by_data	(gpointer	data);

/* Hook for GClosure / GSource integration. Don't touch */
extern GSourceFuncs g_timeout_funcs;
extern GSourceFuncs g_idle_funcs;

#ifdef G_OS_WIN32

/* This is used to add polling for Windows messages. GDK (GTK+) programs
 * should *not* use this.
 */
GLIB_VAR void        g_main_poll_win32_msg_add (gint        priority,
				       GPollFD    *fd,
				       guint       hwnd);
#endif /* G_OS_WIN32 */

G_END_DECLS


/* Types */





#endif /* __G_MAIN_H__ */
