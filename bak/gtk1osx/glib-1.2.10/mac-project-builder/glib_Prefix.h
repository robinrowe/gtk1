/*
 * Prefix header for all source files in 'GLib' project.
 */

#define HAVE_CONFIG_H 1

/* use Mac specific Carbon event based application event loop and timers */
#define MAC_CARBON_EVENTS 1

/* #define G_MAIN_MAC_DEBUG 1 */
