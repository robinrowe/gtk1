
#include "MacGWind.h"

#include <Carbon/Carbon.h>

#include <gdkprivate.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "MacCarbonEvents.h"

#define DEBUG_UPDATE_EVENT 0

static MacGWindRef gEnteredWind = NULL;
static MacGWindRef gClickedWind = NULL;
static Point gLastWhere;

static void _MacGWind_InvalAll(MacGWindRef wRef, int clipChild);
static void _MacGWind_SendExposeOne(MacGWindRef in_wRef, RgnHandle in_update_rgn);

// gdk private
extern GdkEventFunc event_func;
extern gpointer event_data;

#define MENU_BAR_HEIGHT 42
// ....GetMBarHeight()

extern GWorldPtr gDummyGWorld;

static void AddRectToRgn(RgnHandle rgnH, Rect r) {

    RgnHandle tmpRgn = NULL;

    tmpRgn = NewRgn();
    RectRgn(tmpRgn, &r);
    UnionRgn(rgnH, tmpRgn, rgnH);
    DisposeRgn(tmpRgn);
}

static void SectWithPortClip(RgnHandle rgnH) {
    
    CGrafPtr port = (CGrafPtr)GetQDGlobalsThePort();  //justin
    RgnHandle portRgnH = NewRgn(); //justin

    //SectRgn(rgnH, qd.thePort->clipRgn, rgnH);
    GetPortClipRegion(port, portRgnH);
    SectRgn(rgnH, portRgnH, rgnH);
    SetPortClipRegion(port, rgnH);

}

/*static*/ void MyPostEvent(GdkEvent* event) {
    gdk_event_put(event);
}


static void TRACE_NUM_WINDS(int delta) {

#if 0
    static int gNumMacGW = 0;

    gNumMacGW += delta;

    printf("Num MacGWinds = %d\n", gNumMacGW);
#endif
}

static MacGWindRef _MacGWind_TempInvisible() {

    static MacGWindRef gBidonWind = NULL;

    if (gBidonWind == NULL)
        gBidonWind = MacGWind_NewTopLevel(0,0,100,50, GDK_WINDOW_TEMP, "");

    return gBidonWind;
}

static void _MacGWind_CheckValid(MacGWindRef wRef) {

    if (wRef == NULL || wRef->magic != wRef) {
        printf("MacGWind_CheckValid FAILED !!!\n");
        exit(1);
    }
}

MacGWindRef MacGWind_NewPixMap(int width, int height, int depth) {

    Rect gwRect;
    GWorldPtr gw = NULL;
    MacGWind* gwin = NULL;
    CGrafPtr savePort = NULL;
    GDHandle saveDev = NULL;

    GetGWorld(&savePort, &saveDev);

    gwRect.left = 0;
    gwRect.top = 0;
    gwRect.right = width;
    gwRect.bottom = height;

    NewGWorld(&gw, depth, &gwRect,  NULL, NULL, pixelsLocked);
    if (gw == NULL)
        return NULL;

    LockPixels(GetGWorldPixMap(gw)); // ??? (otherwise crashes even if pixelsLocked...)

    SetGWorld(gw, NULL);

    ForeColor(whiteColor); // ???

    //PaintRect(&gw->portRect);
    GetPortBounds(gw, &gwRect); 
    PaintRect(&gwRect);		 //justin

    gwin = (MacGWind*) malloc(sizeof(MacGWind));

    memset(gwin, 0, sizeof(MacGWind));

    gwin->magic = gwin;

    gwin->macport = gw;
    gwin->macgw = gw;
    gwin->parent = NULL;
    gwin->posrect = NULL;
    gwin->children = NULL;

    SetGWorld(savePort, saveDev);

    TRACE_NUM_WINDS(+1);

    return gwin;
}

int MacGWind_GetBytesPerRow(MacGWindRef img) {

    PixMapHandle pmH = NULL;

    if (img == NULL || img->macport == NULL)
        return 0;

    _MacGWind_CheckValid(img);

    pmH = GetGWorldPixMap(img->macport);

    return GetPixRowBytes(pmH);
}

void* MacGWind_GetBits(MacGWindRef img) {

    PixMapHandle pmH = NULL;

    if (img == NULL || img->macport == NULL)
        return 0;

    _MacGWind_CheckValid(img);
    //pmH = GetGWorldPixMap(img->macport);
    //LockPortBits(img->macport);
    pmH = (PixMapHandle)GetPortPixMap(img->macport);
    return GetPixBaseAddr(pmH);

    //justin - is this going to work with carbon if you don't lock the pixmap?
}


BitMap* MacGWind_GetMacBitmap(MacGWindRef wRef) {

    BitMap* result = NULL;
    
   // printf("\nMacGWind_GetMacBitmap wRef = %d, wRef->macport= %d\n",wRef, wRef->macport);
    while (wRef != NULL && wRef->macport == NULL) {
        wRef = wRef->parent;
    }
    if (wRef && wRef->macport) {
        //result = (BitMap*)(*wRef->macport->portPixMap);

        result = (BitMap*) (*(PixMapHandle)GetPortPixMap(wRef->macport));
        //justin - is something supposed to be locked here?
        
    }
    return result;
}

MacGWindRef MacGWind_NewTopLevel(
                                 int x, int y, int width, int height, int gdkType, char* title) {

    Rect wrect;
    MacGWind* gwin = NULL;
    int macType;
    WindowAttributes attrs;
    CFStringRef cfsTitle;
    WindowRef winRef;
    OSStatus err;
    
    gwin = (MacGWind*) malloc(sizeof(MacGWind));

    memset(gwin, 0, sizeof(MacGWind));

    gwin->magic = gwin;

    if (title == NULL)
        title = "";

    cfsTitle = CFStringCreateWithCString(kCFAllocatorDefault, title, CFStringGetSystemEncoding());
    
    wrect.left = x;
    if (y < MENU_BAR_HEIGHT)
        y = MENU_BAR_HEIGHT;
    wrect.top = y;
    wrect.right = wrect.left + width;
    wrect.bottom = wrect.top + height;

    macType = kDocumentWindowClass; //GDK_WINDOW_TOPLEVEL, GDK_WINDOW_DIALOG
    if (gdkType == GDK_WINDOW_TEMP)
        macType = kPlainWindowClass;
    else if (gdkType == GDK_WINDOW_DIALOG)
        macType = kMovableModalWindowClass;
        
    if (macType == kPlainWindowClass)
    {
      attrs = (kWindowNoAttributes | kWindowStandardHandlerAttribute);
    }
    else if (macType == kMovableModalWindowClass)
    {
      attrs = kWindowStandardHandlerAttribute;
    }
    else
    {
      attrs = (kWindowStandardDocumentAttributes | kWindowStandardHandlerAttribute);
    }
    
    err = CreateNewWindow (macType, attrs, &wrect, &winRef);
    
    if (err != noErr)
    {
      printf ("MacGWind_NewTopLevel - CreateNewWindow() failed, error: %d\n", err);
      return NULL;
    }
    
    gwin->macwind = winRef;
    SetWRefCon(winRef, (long)gwin);
    SetWindowTitleWithCFString(winRef, cfsTitle);
    CFRelease(cfsTitle);
    
    gwin->macport = GetWindowPort(gwin->macwind);	//justin
    gwin->parent = NULL;
    gwin->posrect = NULL;
    gwin->children = NULL;

    TRACE_NUM_WINDS(+1);
    
    setup_win_carbon_event_handlers(gwin->macwind);

    return gwin;
}

WindowRef MacGWind_GetMacWindow(MacGWindRef wRef) {
    while (wRef->parent) {
        wRef = wRef->parent;
    }
    return wRef->macwind;
}

void MacGWind_LockMacWindowBits(MacGWindRef wRef) {
    while (wRef->parent) {
        wRef = wRef->parent;
    }
    if (wRef->macwind)
        LockPortBits(wRef->macport);

}


void MacGWind_UnlockMacWindowBits(MacGWindRef wRef) {
    while (wRef->parent) {
        wRef = wRef->parent;
    }
    if (wRef->macwind)
        UnlockPortBits(wRef->macport);
    
}

void MacGWind_FocusPortWithClip(MacGWindRef in_wRef, int clipChild) {

    MacGWindRef wRef = NULL;
    Point origin;
    Rect visRect;
    Rect r, my_r;
    RgnHandle clipRgn = NULL;
    RgnHandle tmpRgn = NULL;
    GList* list = NULL;
    GrafPtr macPort = NULL;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

    if (in_wRef == NULL) {
        printf("\nMacGWind_Focus : ***unable to focus (NULL window) !***\n");
        MacGWind_FocusPortWithClip(_MacGWind_TempInvisible(), 0);

        return;
    }

    _MacGWind_CheckValid(in_wRef);
    wRef = in_wRef;
    visRect = MacGWind_GetInteriorRect(wRef);
    SetPt(&origin, 0, 0);

    while (wRef->parent) {

        r = MacGWind_GetPositionRect(wRef); // get rect in parent coord
        origin.h += r.left;
        origin.v += r.top;
        MacOffsetRect(&visRect, r.left, r.top);
        SectRect(&visRect, &r, &visRect);

        wRef = wRef->parent;
    }

    macPort = wRef->macport;

    if (macPort) {

        SetGWorld(macPort, NULL);
        PenNormal();
        ForeColor(blackColor);
        BackColor(whiteColor);

        SetOrigin(-origin.h, -origin.v);

        OffsetRect(&visRect, -origin.h, -origin.v);

        clipRgn = NewRgn();
        tmpRgn = NewRgn();
        RectRgn(clipRgn, &visRect);

        if (clipChild) {
            list = in_wRef->children;
            while (list) {
                wRef = (MacGWindRef) list->data;
                window = gdk_window_lookup(wRef);
                window_private = (GdkWindowPrivate*)(window);
                if(gdk_window_is_viewable(window) == TRUE) {
                    if (wRef->inputOnly == FALSE) { // must be a true "graphic" child
                        r = MacGWind_GetPositionRect(wRef);
                        RectRgn(tmpRgn, &r);
                        DiffRgn(clipRgn, tmpRgn, clipRgn);
                    }
                }
                list = list->next;
            }
        }

        //clip window z-order
        if (in_wRef->parent) {

            list = in_wRef->parent->children;
            my_r = MacGWind_GetPositionRect(in_wRef);

            while (list) {

                wRef = (MacGWindRef) list->data;
                if (wRef == in_wRef)
                    break;

                if (wRef->inputOnly == FALSE) {
                    window = gdk_window_lookup(wRef);
                    window_private = (GdkWindowPrivate*)(window);
                    if(gdk_window_is_viewable(window) /*->mapped*/ == TRUE) {
                        r = MacGWind_GetPositionRect(wRef); //parent coord
                        OffsetRect(&r, -my_r.left, -my_r.top); // parent -> focus child
                        RectRgn(tmpRgn, &r);
                        DiffRgn(clipRgn, tmpRgn, clipRgn);
                    }
                }

                list = list->next;
            }

        }

        SetClip(clipRgn);

        DisposeRgn(clipRgn);
        DisposeRgn(tmpRgn);
    }
    else {
        printf("\nMacGWind_Focus : ***unable to focus (no port) !***\n");
        MacGWind_FocusPort(_MacGWind_TempInvisible());

    }

}

void MacGWind_FocusPort(MacGWindRef in_wRef) {

    MacGWind_FocusPortWithClip(in_wRef, MAC_CLIP_CHILDREN);
}

MacGWindRef MacGWind_FromNative(WindowRef macWindowRef) {
    /*
     WindowPeek p = (WindowPeek)(macWindowPtr);
     return (MacGWindRef)(p->refCon);
     */
    return (MacGWindRef)GetWRefCon(macWindowRef);	//justin
}


MacGWindRef MacGWind_NewChild(
                              MacGWindRef parent, int x, int y, int width, int height) {

    Rect wrect;

    MacGWind* gwin = (MacGWind*) malloc(sizeof(MacGWind));

    memset(gwin, 0, sizeof(MacGWind));

    gwin->magic = gwin;

    wrect.left = x;
    wrect.top = y;
    wrect.right = x+width;
    wrect.bottom = y+height;

    gwin->macport = NULL;
    gwin->macwind = NULL; //justin
    gwin->parent = parent;
    gwin->posrect = malloc(sizeof(Rect));
    (*gwin->posrect) = wrect;
    gwin->children = NULL;

    if (parent) {
        parent->children = g_list_append(parent->children, gwin);
    }

    TRACE_NUM_WINDS(+1);

    return gwin;
}

void MacGWind_Release(MacGWindRef* refPtr) {


    MacGWind* p = NULL;
    MacGWind* child = NULL;

    if (refPtr == NULL || *refPtr == NULL)
        return;

    p = *refPtr;

    _MacGWind_CheckValid(p);

    while (p->children) {
        child = p->children->data;
        MacGWind_Release(&child);
        //MacGWind_SetParent(child, NULL);
    }

    if (gEnteredWind == p)
        gEnteredWind = NULL;
    if (gClickedWind == p)
        gClickedWind = NULL;

    p->macport = NULL;

    if (p->macwind) {
        DisposeWindow(p->macwind);
        p->macwind = NULL;
    }

    if (p->macgw) {
        DisposeGWorld(p->macgw);
        p->macgw = NULL;
    }

    if (p->posrect) {
        free(p->posrect);
        p->posrect = NULL;
    }

    if (p->parent) {
        p->parent->children = g_list_remove(p->parent->children, p);
    }

    if (p->bkgcol) {
        free(p->bkgcol);
        p->bkgcol = NULL;
    }

    memset(p, 0, sizeof(*p));
    free(p);
    *refPtr = NULL;

    TRACE_NUM_WINDS(-1);
}

MacGWindRef MacGWind_FindChildAt(MacGWindRef wRef, Point where, Point* local) {

    Rect my_rect;
    Rect child_pos;
    MacGWindRef child = NULL;
    GList* list = NULL;
    Point child_where;

    if (wRef == NULL) {
        return NULL;
    }

    my_rect = MacGWind_GetInteriorRect(wRef);

    if (! PtInRect(where, &my_rect)) {
        return NULL;
    }

    // backward iteration (inverse of display order)
    list = g_list_last(wRef->children);
    while (list) {
        child = (MacGWindRef)(list->data);
        child_pos = MacGWind_GetPositionRect(child);
        child_where.h = where.h - child_pos.left;// parent->child coords
            child_where.v = where.v - child_pos.top;
            child = MacGWind_FindChildAt(child, child_where, local);
            if (child && gdk_window_is_viewable(gdk_window_lookup (child)) == TRUE)
                return child;
            list = list->prev;
    }

    if (local) {
        *local = where;
    }

    return wRef;
}

void MacGWind_SendButtonPress(MacGWindRef wRef, Point where) {

    GdkEvent event;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

    //printf("MacGWind_SendButtonPress where = %d,%d\n",where.v, where.h);
    MacGWind_CheckMousePointer(wRef, where); // here if the mouse moves too fast

    wRef = MacGWind_FindChildAt(wRef, where, &where);

    window = gdk_window_lookup(wRef);
    window_private = (GdkWindowPrivate*)(window);

    if (window == NULL)
        return; // strange !!!!

    memset(&event, 0, sizeof(event));

    event.button.type = GDK_BUTTON_PRESS;
    event.button.window = window;
    event.button.time = gdk_time_get();
    event.button.x = where.h;
    event.button.y = where.v;
    //event.button.x_root = ???;
    //event.button.y_root = ???;
    event.expose.count = 0;

    event.button.state |= GDK_BUTTON1_MASK;		//TODO - real support for modifier keys?
    event.button.button = 1;

    event.button.source = GDK_SOURCE_MOUSE;
    event.button.deviceid = GDK_CORE_POINTER;

    gClickedWind = wRef;

    gdk_event_button_generate( &event );
}

void MacGWind_SendConfigure(MacGWindRef wRef) {

    GdkEvent event;
    Rect r;
    GdkWindow* window = NULL;

    r = MacGWind_GetPositionRect(wRef);

    window = gdk_window_lookup(wRef);
    if (window == NULL)
        return; //??

    memset(&event, 0, sizeof(event));

    event.configure.type = GDK_CONFIGURE;
    event.configure.window = window;
    event.configure.x = r.left;
    event.configure.y = r.top;
    event.configure.width = r.right - r.left;
    event.configure.height = r.bottom - r.top;

    MyPostEvent(&event);
    

}

void MacGWind_SendButtonRelease(MacGWindRef wRef, Point where) {

    GdkEvent event;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

    MacGWind_CheckMousePointer(wRef, where); // here if the mouse moves too fast
    
    wRef = MacGWind_FindChildAt(wRef, where, &where);

    window = gdk_window_lookup(wRef);
    window_private = (GdkWindowPrivate*)(window);

    if (window == NULL)
        return; // strange !!!!

    memset(&event, 0, sizeof(event));

    event.button.type = GDK_BUTTON_RELEASE;
    event.button.window = window;
    event.button.time = gdk_time_get();
    event.button.x = where.h;
    event.button.y = where.v;
    //event.button.x_root = ???;
    //event.button.y_root = ???;
    event.expose.count = 0;

    event.button.state |= GDK_BUTTON1_MASK;		//TODO - real support for modifier keys?
    event.button.button = 1;

    event.button.source = GDK_SOURCE_MOUSE;
    event.button.deviceid = GDK_CORE_POINTER;

    gClickedWind = wRef;

    MyPostEvent(&event);
}



void MacGWind_SendExpose(MacGWindRef in_wRef, RgnHandle in_update_rgn) {

    Rect child_bounds;
    GList* list = NULL;
    MacGWindRef wRef = NULL;
    RgnHandle tmpRgn = NULL;

    if (in_wRef == NULL)
        return;
   

    _MacGWind_SendExposeOne(in_wRef, in_update_rgn);

    tmpRgn = NewRgn();

    list = in_wRef->children;
    while (list)	{

        wRef = (MacGWindRef)(list->data);

        child_bounds = MacGWind_GetPositionRect(wRef);
        RectRgn(tmpRgn, &child_bounds);
        SectRgn(in_update_rgn, tmpRgn, tmpRgn);

        if (!EmptyRgn(tmpRgn)) {
            OffsetRgn(tmpRgn, -child_bounds.left, -child_bounds.top);
            MacGWind_SendExpose(wRef, tmpRgn);
        }

        list = list->next;
    }
    
    DisposeRgn(tmpRgn);
}


void MacGWind_Erase(MacGWindRef wRef, Rect r) {

    if (wRef == NULL)
        return;
   

    MacGWind_FocusPort(wRef);
    if (wRef->bkgcol)
        RGBForeColor(wRef->bkgcol);
    else
        ForeColor(whiteColor);
    PaintRect(&r);
    ForeColor(blackColor);
}

static void _MacGWind_SendExposeOne(MacGWindRef wRef, RgnHandle in_update_rgn) {

    // don't modify in_update_rgn !

    GdkEvent event;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;
    Rect rect;

   
    CGrafPtr port = (CGrafPtr)GetQDGlobalsThePort();  //justin
    RgnHandle rgn = NewRgn();

    if (wRef == NULL)
        return;

    if (wRef->inputOnly)
        return;

    window = gdk_window_lookup(wRef);
    window_private = (GdkWindowPrivate*)(window);
    
    if(gdk_window_is_viewable(window) /*->mapped*/ == FALSE) {
        return;
    }
    
    MacGWind_FocusPort(wRef);  
    
    //SectRgn(qd.thePort->clipRgn, in_update_rgn, qd.thePort->clipRgn); 
    //rect = (**qd.thePort->clipRgn).rgnBBox;
    
    //justin
    GetPortClipRegion(port, rgn);
    SectRgn(rgn, in_update_rgn, rgn);
    SetPortClipRegion(port, rgn);
    GetRegionBounds(rgn, &rect);

    if (EmptyRect(&rect))
        return;

//    window = gdk_window_lookup(wRef);
  //  window_private = (GdkWindowPrivate*)(window);

    if (window == NULL)
        return; // (?!)

    memset(&event, 0, sizeof(event));

    event.expose.type = GDK_EXPOSE;
    event.expose.window = window;

#if 0
    // debug: update the whole area
    rect = MacGWind_GetPositionRect(wRef);
    OffsetRect(&rect, -rect.left, -rect.top);
#endif

    event.expose.area.x = rect.left;
    event.expose.area.y = rect.top;
    event.expose.area.width = rect.right - rect.left;
    event.expose.area.height = rect.bottom - rect.top;

    event.expose.count = 0;


    //*** debug ***
#if DEBUG_UPDATE_EVENT
    MacGWind_FocusPort(wRef);
    ForeColor(greenColor);
    PaintRect(&rect);
    ForeColor(blackColor);
#endif
    //***

    if (wRef->bkgcol) {
        MacGWind_FocusPort(wRef);
        RGBForeColor(wRef->bkgcol);
        PaintRect(&rect);
        ForeColor(blackColor);
    }

    GDK_THREADS_ENTER ();

    if (event_func) {
        (*event_func) (&event, event_data);
    }

    GDK_THREADS_LEAVE ();

    gdk_event_put(&event); // doesn't work for update

    return;
}

Rect MacGWind_GetPositionRect(MacGWindRef wRef) {

    Rect r, pixBounds;
    PixMapHandle pmH;

    MacSetRect(&r,-1,-1,-1,-1);

    if (wRef->posrect) {
        r = *(wRef->posrect);
    }
    else if (wRef->macwind) {

        //justin

        /*
         pmH = ((CGrafPtr)(wRef->macwind))->portPixMap;
         r = wRef->macport->portRect;
         */

        GetPortBounds((CGrafPtr)(wRef->macport), &r);

        //LockPortBits(wRef->macport);

        pmH = (PixMapHandle)GetPortPixMap(wRef->macport);
        GetPixBounds(pmH, &pixBounds);
        //UnlockPortBits(wRef->macport);

        MacOffsetRect(&r,
                      -r.left-pixBounds.left,
                      -r.top-pixBounds.top/* - MENU_BAR_HEIGHT*/);


    }
    else if (wRef->macport) {
        //justin
        //r = wRef->macport->portRect;
        GetPortBounds((CGrafPtr)(wRef->macport), &r);

        MacOffsetRect(&r, -r.left, -r.top);
    }
    else {
        //oops ...
    }

    //printf("MacGWind_GetPositionRect wRef=%d wRef->macwind=%d rect={top=%d, left=%d, bottom=%d, right=%d}\n", wRef, wRef->macwind, r.top, r.left, r.bottom, r.right);
    return r;
}

Rect MacGWind_GetInteriorRect(MacGWindRef wRef) {

    Rect r;
    MacSetRect(&r,-1,-1,-1,-1);

    if (wRef) {
        r = MacGWind_GetPositionRect(wRef);
        OffsetRect(&r, -r.left, -r.top);
    }

    return r;
}

void MacGWind_Show(MacGWindRef wRef, int showBool) {

    Rect bounds;
    RgnHandle tmpRgn = NULL;

    if (wRef == NULL)
        return;

    _MacGWind_CheckValid(wRef);

    if (wRef->macwind) {
        if (showBool) {
            MacShowWindow(wRef->macwind);
        }
        else {
            HideWindow(wRef->macwind);
        }
    }

    bounds = MacGWind_GetPositionRect(wRef);

    if (wRef->parent) {
        MacGWind_FocusPortWithClip(wRef->parent, MAC_DONT_CLIP_CHILDREN);
        tmpRgn = NewRgn();
        RectRgn(tmpRgn, &bounds);
        SectWithPortClip(tmpRgn);
        //InvalRgn(tmpRgn);
        InvalWindowRgn((WindowRef)wRef->macwind, tmpRgn); //justin

        DisposeRgn(tmpRgn);
    }


    // "show" must send a configure event.
    MacGWind_SendConfigure(wRef);
    //justin - what about expose event?
    MacGWind_SendExpose(wRef, tmpRgn);
}

void MacGWind_Raise(MacGWindRef wRef) {

    Rect bounds;
    RgnHandle tmpRgn = NULL;

    _MacGWind_CheckValid(wRef);

    if (wRef->macwind) {
        SelectWindow(wRef->macwind);
    }
    else {
        if (wRef->parent) {
            _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
            wRef->parent->children = g_list_remove( wRef->parent->children, wRef);
            wRef->parent->children = g_list_prepend( wRef->parent->children, wRef);
            _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
        }
    }

    MacGWind_SendConfigure(wRef); // ???
    bounds = MacGWind_GetPositionRect(wRef);
    RectRgn(tmpRgn, &bounds);
    MacGWind_SendExpose(wRef, tmpRgn);
    
}

void MacGWind_Lower(MacGWindRef wRef) {

    _MacGWind_CheckValid(wRef);

    if (wRef->macwind) {
        SendBehind(wRef->macwind, NULL);
    }
    else {
        if (wRef->parent) {
            _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
            wRef->parent->children = g_list_remove( wRef->parent->children, wRef);
            wRef->parent->children = g_list_append( wRef->parent->children, wRef);
            _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
        }
    }

    MacGWind_SendConfigure(wRef); // ???
}


/*static*/
void _MacGWind_InvalAll(MacGWindRef wRef, int clipChild) {
/*


    MacGWind_FocusPortWithClip(wRef, clipChild);
    InvalRgn(qd.thePort->clipRgn);

*/

//justin - does this work?
    Rect gwRect;
    while ((wRef != NULL) && (wRef->macwind == NULL)) {
        wRef = wRef->parent;
    }
    if (wRef == NULL) {
        printf("ack!\n");
        exit(1);
        return;
    }
    GetPortBounds(wRef->macport, &gwRect);
    InvalWindowRect(wRef->macwind, &gwRect);
    //printf("_MacGWind_InvalAll gwRect {top=%d, left=%d, bottom=%d, right=%d}\n",gwRect.top, gwRect.left, gwRect.bottom, gwRect.right);

    
}


static void _MacGWind_GetValidRgnInParent(MacGWindRef wRef, RgnHandle rgnH) {

    CGrafPtr port = (CGrafPtr)GetQDGlobalsThePort(); //justin
    RgnHandle rgn = NewRgn();

    MacGWind_FocusPortWithClip(wRef, MAC_DONT_CLIP_CHILDREN);

    GetPortClipRegion(port, rgn);//justin

    //CopyRgn(qd.thePort->clipRgn, rgnH);
    CopyRgn(rgn, rgnH);

    if (wRef->posrect) { // child --> parent
        OffsetRgn(rgnH, wRef->posrect->left, wRef->posrect->top);
    }
}


void MacGWind_Move(MacGWindRef wRef, int x, int y) {

    int dx,dy;
    RgnHandle updateRgn = NULL;
    RgnHandle validRgn = NULL;
    Rect newRect;
    BitMap* bitmap;

    _MacGWind_CheckValid(wRef);

    if (wRef->macwind) {
        if (y < MENU_BAR_HEIGHT)
            y = MENU_BAR_HEIGHT;
        MacMoveWindow(wRef->macwind, x, y, true);
    }
    else if (wRef->posrect) {

        dx = x - wRef->posrect->left;
        dy = y - wRef->posrect->top;

#define MOVE_INVAL_ALL 1

#if MOVE_INVAL_ALL

        _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
        MacOffsetRect(wRef->posrect, dx, dy);
        _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);

#else

        newRect = *(wRef->posrect);
        MacOffsetRect(&newRect, dx, dy);
        
        if (wRef->parent) {
            
            updateRgn = NewRgn();
            validRgn = NewRgn();

            _MacGWind_GetValidRgnInParent(wRef, validRgn); // change focus

            MacGWind_FocusPortWithClip(wRef->parent, MAC_DONT_CLIP_CHILDREN);

#if 0
            // Debug
            //ForeColor(greenColor);
            //PaintRect(&qd.thePort->portRect);
            ForeColor(yellowColor);
            PaintRect(wRef->posrect);
            ForeColor(blueColor);
            PaintRect(&newRect);
            ForeColor(blackColor);
#endif
            bitmap = MacGWind_GetMacBitmap(wRef->parent);

            CopyBits(
                     bitmap,
                     bitmap,
                     wRef->posrect,
                     &newRect,
                     srcCopy,
                     NULL);


            AddRectToRgn(updateRgn, *wRef->posrect);
            AddRectToRgn(updateRgn, newRect);

            OffsetRgn(validRgn, dx, dy);
            DiffRgn(updateRgn, validRgn, updateRgn);

            SectWithPortClip(updateRgn);

#if DEBUG_UPDATE_EVENT
            ForeColor(magentaColor);
            PaintRgn(updateRgn);
            ForeColor(cyanColor);
            //InsetRgn(updateRgn, 2,2);
            //PaintRgn(updateRgn);
            ForeColor(blackColor);
#endif

            //InvalWindowRgn((WindowRef)wRef->macwind, updateRgn);
            //GetWindowBounds((WindowRef)wRef->macwind, kWindowStructureRgn, &newRect);
            //InvalWindowRect((WindowRef)wRef->macwind, &newRect);   //justin
            DisposeRgn(updateRgn);
            DisposeRgn(validRgn);
        }

        *(wRef->posrect) = newRect;
#endif
    }

    MacGWind_SendConfigure(wRef);
}

void MacGWind_Resize(MacGWindRef wRef, int width, int height) {

    WindowRef macWind = NULL;

    _MacGWind_CheckValid(wRef);

    if (wRef->macwind) {
        macWind = wRef->macwind;
        SizeWindow(macWind, width, height, true);
    }
    else if (wRef->posrect) {

        _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);

        wRef->posrect->right = wRef->posrect->left + width;
        wRef->posrect->bottom = wRef->posrect->top + height;

        _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
    }

    MacGWind_SendConfigure(wRef);
 
}


void MacGWind_CheckMousePointer(MacGWindRef wRef, Point where) {

    GdkEvent event;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

    wRef = MacGWind_FindChildAt(wRef, where, &where);

    if (gEnteredWind != wRef) {
        if (gEnteredWind != NULL) {
            //we have moved to a new window
            
            //post leave event for previous one
            
            window = gdk_window_lookup(gEnteredWind);
            window_private = (GdkWindowPrivate*)(window);


            memset(&event, 0, sizeof(event));

            event.crossing.type = GDK_LEAVE_NOTIFY;
            event.crossing.window = window; // <----- a faire
            event.crossing.time = gdk_time_get();
            event.crossing.subwindow = NULL;
            event.crossing.time = time(NULL);
            event.crossing.x = (gdouble) where.h;
            event.crossing.y = (gdouble) where.v;
            //event.crossing.x_root = (gdouble) root_x; // <----- a faire
            //event.crossing.y_root = (gdouble) root_y;
            event.crossing.mode = GDK_CROSSING_NORMAL;
            event.crossing.detail = GDK_NOTIFY_UNKNOWN; // <----- a faire
            event.crossing.focus = 0;
            event.crossing.state = 0;

            MyPostEvent(&event);

            //post enter event for new one
            if (wRef) {
                memset(&gLastWhere, 0, sizeof(gLastWhere));

                window = gdk_window_lookup(wRef);
                window_private = (GdkWindowPrivate*)(window);


                memset(&event, 0, sizeof(event));

                event.crossing.type = GDK_ENTER_NOTIFY;
                event.crossing.window = window;
                event.crossing.subwindow = NULL; // <----- a faire
                event.crossing.time = gdk_time_get();
                event.crossing.x = (gdouble) where.h;
                event.crossing.y = (gdouble) where.v;
                //event.crossing.x_root = (gdouble) root_x; // <----- a faire
                //event.crossing.y_root = (gdouble) root_y;
                event.crossing.mode = GDK_CROSSING_NORMAL;
                event.crossing.detail = GDK_NOTIFY_UNKNOWN; // <----- a faire
                event.crossing.focus = 0;
                event.crossing.state = 0;

                MyPostEvent(&event);
            }



        }
        else {
            if (!wRef) {
                return;
                //does this happen?
            }
                
            //we have entered a window, previously we were not in one
            memset(&gLastWhere, 0, sizeof(gLastWhere));

            window = gdk_window_lookup(wRef);
            window_private = (GdkWindowPrivate*)(window);


            memset(&event, 0, sizeof(event));

            event.crossing.type = GDK_ENTER_NOTIFY;
            event.crossing.window = window;
            event.crossing.subwindow = NULL; // <----- a faire
            event.crossing.time = gdk_time_get();
            event.crossing.x = (gdouble) where.h;
            event.crossing.y = (gdouble) where.v;
            //event.crossing.x_root = (gdouble) root_x; // <----- a faire
            //event.crossing.y_root = (gdouble) root_y;
            event.crossing.mode = GDK_CROSSING_NORMAL;
            event.crossing.detail = GDK_NOTIFY_UNKNOWN; // <----- a faire
            event.crossing.focus = 0;
            event.crossing.state = 0;

            MyPostEvent(&event);
        }

        gEnteredWind = wRef;

    }
    else {
        //gEnteredWind == wRef
        //we are already in the window the event occurred in
        if (wRef && ! EqualPt(gLastWhere, where)) {

            window = gdk_window_lookup(wRef);
            window_private = (GdkWindowPrivate*)(window);

            gLastWhere = where;

            //printf("post GDK_MOTION_NOTIFY\n"); //**********

            memset(&event, 0, sizeof(event));

            event.motion.type = GDK_MOTION_NOTIFY;
            event.motion.window = window;
            event.motion.time = time(NULL);
            event.motion.x = (gdouble) where.h;
            event.motion.y = (gdouble) where.v;
            //event.motion.x_root = (gdouble) root_x;
            //event.motion.y_root = (gdouble) root_y;
            event.motion.pressure = 0.5;
            event.motion.xtilt = 0;
            event.motion.ytilt = 0;
            event.motion.state = 0;
            event.motion.state |= MacGWind_GetMouseButtonMask();
            //if (mask & GDK_POINTER_MOTION_HINT_MASK)
            //  {
            //    event.motion.is_hint = 1;
            //    do_motion_hints = 0;
            //  }
            //else
            //  event.motion.is_hint = 0;
            event.motion.source = GDK_SOURCE_MOUSE;
            event.motion.deviceid = GDK_CORE_POINTER;

            MyPostEvent(&event);
        }
    }

}


int MacGWind_GetMouseButtonMask() {

    int mask = 0;

    if (Button()) {
        mask |= GDK_BUTTON1_MASK;
    }

    // A_FAIRE ...
    // |= GDK_BUTTON2_MASK
    // |= GDK_BUTTON3_MASK

    return mask;
}

void MacGWind_SetParent(MacGWindRef wRef, MacGWindRef newParent) {

    MacGWindRef oldParent = NULL;

    if (wRef == NULL)
        return;

    _MacGWind_CheckValid(wRef);

    oldParent = wRef->parent;

    _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);

    if (oldParent) {
        oldParent->children = g_list_remove(oldParent->children, wRef);
    }

    wRef->parent = newParent;

    if (newParent) {
        newParent->children = g_list_prepend(newParent->children, wRef);
    }

    _MacGWind_InvalAll(wRef, MAC_DONT_CLIP_CHILDREN);
}

void MacGWind_SetGeometryHints(MacGWindRef wRef, GdkGeometry * geometry, GdkWindowHints geom_mask) {
    
    if (wRef && wRef->macwind) {
       
        HISize    minLimits;
        HISize    maxLimits;
       
        minLimits.width    = ( float ) geometry->min_width;
        minLimits.height    = ( float ) geometry->min_height;
       
        maxLimits.width    = ( float ) geometry->max_width;
        maxLimits.height    = ( float ) geometry->max_height;
       
        SetWindowResizeLimits (
            wRef->macwind,
            (geom_mask & GDK_HINT_MIN_SIZE) ? &minLimits : NULL,
            (geom_mask & GDK_HINT_MAX_SIZE) ? &maxLimits : NULL );

    }
}

void MacGWind_SetTitle(MacGWindRef wRef, const char* title) {

    if (wRef && wRef->macwind) {
        //setwtitle(wRef->macwind, title);

		//justin
        Str255 title255;
        c2pstrcpy(title255, title);

        SetWTitle(wRef->macwind, title255);
    }
}

void MacGWind_SetBackground(MacGWindRef wRef, RGBColor bkgCol) {

    if (wRef == NULL)
        return;

    if (wRef->bkgcol && memcmp(wRef->bkgcol, &bkgCol, sizeof(bkgCol)) == 0) {
        return; // otherwise bug:  list redraw -> refresh -> list redraw -> etc.
    }

    if (wRef->bkgcol == NULL) {
        wRef->bkgcol = (RGBColor*) malloc(sizeof(bkgCol));
    }

    *wRef->bkgcol = bkgCol;

    if (wRef->macwind) {
        SetWindowContentColor(wRef->macwind, &bkgCol);
    }

    _MacGWind_InvalAll(wRef, MAC_CLIP_CHILDREN);
}

void MacGWind_SetInputOnly(MacGWindRef wRef) {

    if (wRef == NULL)
        return;

    wRef->inputOnly = TRUE;
}


void MacGWind_SendDelete(MacGWindRef wRef) {

    GdkEvent event;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

//    window = gdk_window_lookup(wRef);
  //  window_private = (GdkWindowPrivate*)(window);

    if (window == NULL)
        return;

    memset(&event, 0, sizeof(event));

    event.any.type = GDK_DELETE;
    event.any.window = window;

    MyPostEvent(&event);
}



void MacGWind_MapChildren(MacGWindRef in_wRef, guint mapped) {

    GList* list = NULL;
    MacGWindRef wRef = NULL;
    GdkWindow *window = NULL;
    GdkWindowPrivate *window_private = NULL;

    
    list = in_wRef->children;
    while (list)	{

        wRef = (MacGWindRef)(list->data);
        if(wRef == NULL) {
            list = list->next;
            continue;
        }
        window = gdk_window_lookup(wRef);
        window_private = (GdkWindowPrivate*)(window);
        if(window_private != NULL) {
            window_private->mapped = mapped;
        }

        list = list->next;
    }
}

