/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include <Region.h>
//#include <Window.h>
//#include <View.h>
//#include <Font.h>

#include <string.h>

#include "gdkconfig.h"
#include "gdk/gdk.h"
#include "gdkprivate.h"


void debug_print_gc_details(GdkGC* gc) {
    GdkGCPrivate *priv = (GdkGCPrivate*) gc;

    /*
     GdkGC gc;
     GdkGCValuesMask values_mask;
     GdkGCValues values;
     GdkRegion *clip_region;
     guint ref_count;
     */
    GdkGCValuesMask values_mask = priv->values_mask;
    GdkGCValues values = priv->values;

    printf("values_mask: ");
    if (values_mask & GDK_GC_FOREGROUND) {
        printf(" GDK_GC_FOREGROUND ");
    }
    if (values_mask & GDK_GC_BACKGROUND) {

        printf(" GDK_GC_BACKGROUND ");
    }
    if ((values_mask & GDK_GC_FONT)) {

        printf(" GDK_GC_FONT ");
    }
    if (values_mask & GDK_GC_FUNCTION) {

        printf(" GDK_GC_FUNCTION ");
    }
    if (values_mask & GDK_GC_TILE) {

        printf(" GDK_GC_TILE ");
    }
    printf("\n\n ");


    printf("values.foreground %d\n", values.foreground);
    printf("values.background %d\n", values.background);
    printf("values.font %d\n", values.font);
    printf("values.function %d\n", values.function);
    printf("values.font %d\n", values.font);
    printf("values.tile %d\n", values.tile);
    printf("values.stipple %d\n", values.stipple);
    printf("values.clip_mask %d\n", values.clip_mask);
    printf("values.ts_x_origin %d\n", values.ts_x_origin);
    printf("values.ts_y_origin %d\n", values.ts_y_origin);
    printf("values.clip_x_origin %d\n", values.clip_x_origin);
    printf("values.clip_y_origin %d\n", values.clip_y_origin);
    printf("values.line_width %d\n", values.line_width);


    
}


static RGBColor GToMacColor(GdkColor gColor) {

	RGBColor macColor;
	macColor.red = gColor.red;
	macColor.green = gColor.green;
	macColor.blue = gColor.blue;
	
	return macColor;
}

short GToMacFunc_Src(GdkFunction gf) {

  short macmode = 0;

  switch (gf)
   {
    case GDK_COPY:
        macmode = srcCopy; break;
    case GDK_INVERT:
        macmode = srcXor; break; // ?????
    case GDK_XOR:
        macmode = srcXor; break;
    case GDK_CLEAR:
        macmode = srcBic; break; // ?????
    
    case GDK_AND:
    case GDK_AND_REVERSE:
    case GDK_AND_INVERT:

    default:  /* since the rest of these would be guesses. see win32 */
     g_warning("drawingmode = %d, using copy instead.\n", (int)gf);
     macmode = srcCopy;
   }
   
    return macmode;
}

short GToMacFunc_Pat(GdkFunction gf) {

  short macmode = 0;

  switch (gf)
   {
    case GDK_COPY:
        macmode = patCopy; break;
    case GDK_INVERT:
        macmode = patXor; break; // ?????
    case GDK_XOR:
        macmode = patXor; break;
    case GDK_CLEAR:
        macmode = patBic; break; // ?????
    
    case GDK_AND:
    case GDK_AND_REVERSE:
    case GDK_AND_INVERT:

    default:  /* since the rest of these would be guesses. see win32 */
     g_warning("drawingmode = %d, using copy instead.\n", (int)gf);
     macmode = patCopy;
   }
   
    return macmode;
}

     
void
gdk_gc_predraw (MacGWindRef         gdkview,
		GdkGCPrivate     *gc_private)
{


    GdkGCPrivate *gc;
    GdkFontPrivate *font;
    GdkRegionPrivate *region;
    GdkGCValuesMask values_mask;
    RGBColor macColor;
    WindowRef macWindow;
    CGrafPtr macWindowPort;



    
    MacGWind_FocusPortWithClip(gdkview,
                               gc_private->values.subwindow_mode == GDK_CLIP_BY_CHILDREN
                               ? MAC_CLIP_CHILDREN : MAC_DONT_CLIP_CHILDREN);

    // ----- reset port -----
    PenNormal();
    ForeColor(blackColor);
    BackColor(whiteColor);
    //-----------------------

    gc = gc_private;
    font = (GdkFontPrivate*) gc->values.font;
    region = (GdkRegionPrivate*) gc->clip_region;
    values_mask = gc->values_mask;

    {
        if (values_mask & GDK_GC_FOREGROUND) {

            macColor = GToMacColor(gc->values.foreground);
            RGBForeColor(&macColor);
        }

        if (values_mask & GDK_GC_BACKGROUND) {

            macColor = GToMacColor(gc->values.foreground);
            RGBBackColor(&macColor);
        }

        if ((values_mask & GDK_GC_FONT) && (gc->values.font->type == GDK_FONT_FONT)) {

            // !!!MACOS_TO_DO!!!
        }

        if (values_mask & GDK_GC_FUNCTION) {

            PenMode(GToMacFunc_Pat(gc->values.function));
        }


        if (values_mask & GDK_GC_TILE) {

            // !!!MACOS_TO_DO!!!
        }

        if (region)
        {
            //SectRgn(qd.thePort->clipRgn, region->macrgn, qd.thePort->clipRgn); //justin

            CGrafPtr port = (CGrafPtr)GetQDGlobalsThePort();
            RgnHandle portRgnH = NewRgn();
            GetPortClipRegion(port, portRgnH);
            
            SectRgn(portRgnH, region->macrgn, portRgnH);
            SetPortClipRegion(port, portRgnH);
        }

        if (values_mask & GDK_GC_LINE_WIDTH) {
            PenSize(gc->values.line_width,gc->values.line_width);
        }

        if (values_mask & (GDK_GC_CAP_STYLE|GDK_GC_JOIN_STYLE))
        {
            // !!!MACOS_TO_DO!!!
        }

    }

    MacGWind_LockMacWindowBits(gdkview);

}

void
gdk_gc_postdraw (MacGWindRef         gdkview,
		 GdkGCPrivate     *gc_private)
{

    MacGWind_UnlockMacWindowBits(gdkview);
  // Mac: nothing to do ???
}

GdkGC*
gdk_gc_new (GdkWindow *window)
{
  return gdk_gc_new_with_values (window, NULL, (GdkGCValuesMask) 0);
}

GdkGC*
gdk_gc_new_with_values (GdkWindow	*window,
			GdkGCValues	*values,
			GdkGCValuesMask	 values_mask)
{
	
#if 1

  GdkWindowPrivate *window_private;
  GdkGC *gc;
  GdkGCPrivate *priv;
  GdkFontPrivate *font;

  g_return_val_if_fail (window != NULL, NULL);

  window_private = (GdkWindowPrivate*) window;
  if (window_private->destroyed)
    return NULL;

  priv = g_new (GdkGCPrivate, 1);
  
  memset(priv, 0, sizeof(GdkGCPrivate)); //AM
  
  gc = (GdkGC*) priv;

  priv->ref_count = 1;
  priv->values.fill = GDK_SOLID;
  priv->values_mask = values_mask;
  priv->values_mask = (GdkGCValuesMask) (priv->values_mask | GDK_GC_FUNCTION | GDK_GC_FILL);
  priv->clip_region = NULL;

  if (values_mask & GDK_GC_FOREGROUND)
    priv->values.foreground = values->foreground;

  if (values_mask & GDK_GC_BACKGROUND)
    priv->values.background = values->background;

  if ((values_mask & GDK_GC_FONT) && (values->font->type == GDK_FONT_FONT))
    {
      if (values->font)
        priv->values.font = gdk_font_ref(values->font);
    }

  if (values_mask & GDK_GC_FUNCTION)
    {
      priv->values.function = values->function;             
    }
  else
    priv->values.function = GDK_COPY;

  if (values_mask & GDK_GC_FILL)
    priv->values.fill = values->fill;
  else
    priv->values.fill = GDK_SOLID;
 
  if (values_mask & GDK_GC_TILE)
    {
      priv->values.tile = values->tile; 
      gdk_pixmap_ref (priv->values.tile);
    }
  else
    priv->values.tile = NULL;

  if (values_mask & GDK_GC_STIPPLE)
    {
      priv->values.stipple = values->stipple;
      gdk_pixmap_ref (priv->values.stipple);
    }
  else
    priv->values.stipple = NULL;

  if (values_mask & GDK_GC_CLIP_MASK)
    g_warning("gdk_gc_new_with_values: clip mask not implemented");
  priv->values.clip_mask = NULL;

  /* what should be the default here */
  if (values_mask & GDK_GC_SUBWINDOW)
    priv->values.subwindow_mode = values->subwindow_mode;
  else
    priv->values.subwindow_mode = GDK_CLIP_BY_CHILDREN;

  if (values_mask & GDK_GC_TS_X_ORIGIN)
    priv->values.ts_x_origin = values->ts_x_origin;
  
  if (values_mask & GDK_GC_TS_Y_ORIGIN)
    priv->values.ts_y_origin = values->ts_y_origin;

  if (values_mask & GDK_GC_CLIP_X_ORIGIN)
    priv->values.clip_x_origin = values->clip_x_origin;
  
  if (values_mask & GDK_GC_CLIP_Y_ORIGIN)
    priv->values.clip_y_origin = values->clip_y_origin;

  if (values_mask & GDK_GC_EXPOSURES)
    priv->values.graphics_exposures = values->graphics_exposures;

  if (values_mask & GDK_GC_LINE_WIDTH)
    priv->values.line_width = values->line_width;
  else
    priv->values.line_width = 1; 

  if (values_mask & GDK_GC_LINE_STYLE)
    priv->values.line_style = values->line_style;
  else
    priv->values.line_style = GDK_LINE_SOLID;

  if (values_mask & GDK_GC_CAP_STYLE)
    priv->values.cap_style = values->cap_style;
  else
    priv->values.cap_style = GDK_CAP_BUTT;

  if (values_mask & GDK_GC_JOIN_STYLE)
    priv->values.join_style = values->join_style;
  else
    priv->values.join_style = GDK_JOIN_MITER;

  return gc;
  #endif
}

void
gdk_gc_destroy (GdkGC *gc)
{
  gdk_gc_unref (gc);
}

GdkGC *
gdk_gc_ref (GdkGC *gc)
{
  GdkGCPrivate *priv = (GdkGCPrivate*) gc;

  g_return_val_if_fail (gc != NULL, NULL);
  priv->ref_count += 1;

  return gc;
}

void
gdk_gc_unref (GdkGC *gc)
{

  GdkGCPrivate *priv = (GdkGCPrivate*) gc;

  g_return_if_fail (gc != NULL);
  
  if (priv->ref_count > 1)
   priv->ref_count -= 1;
  else
    {
        
          if (priv->values.tile)
          {
            gdk_pixmap_unref (priv->values.tile);
          }

          if (priv->values.stipple)
          {
            gdk_pixmap_unref (priv->values.stipple);
          }
          
          if (priv->values.font) {
            gdk_font_unref(priv->values.font);
          }
          
          if (priv->values.clip_mask) {
            gdk_pixmap_unref(priv->values.clip_mask);
          }
          
          if (priv->clip_region) {
            gdk_region_destroy(priv->clip_region);
          }
          
        memset (gc, 0, sizeof (GdkGCPrivate));
        g_free (gc);
    }
}

void
gdk_gc_get_values (GdkGC       *gc,
		   GdkGCValues *values)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);
  g_return_if_fail (values != NULL);

  priv = (GdkGCPrivate*) gc;

  values->foreground = priv->values.foreground;
  values->background = priv->values.background;
 
  values->font = priv->values.font;
  values->function = priv->values.function;

  values->fill = priv->values.fill;

  values->tile = priv->values.tile;
  values->stipple = priv->values.stipple;
  /* region clip mask */
  values->subwindow_mode = priv->values.subwindow_mode;
  values->ts_x_origin = priv->values.ts_x_origin;
  values->ts_y_origin = priv->values.ts_y_origin;
  values->clip_x_origin = priv->values.clip_x_origin;
  values->clip_y_origin = priv->values.clip_y_origin;
  values->graphics_exposures = priv->values.graphics_exposures;
  values->line_width = priv->values.line_width;
 
  values->line_style = priv->values.line_style;
  values->cap_style = priv->values.cap_style;
  values->join_style = priv->values.join_style;
}

void
gdk_gc_set_foreground (GdkGC	*gc,
		       GdkColor *color)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);
  g_return_if_fail (color != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.foreground = *color;
  priv->values_mask |= GDK_GC_FOREGROUND;

  GDK_NOTE (MISC, g_print ("gdk_gc_set_foreground: gc %p, rgb %d %d %d\n",
                           gc,
                           color->red / 256,
                           color->green/ 256,
                           color->blue / 256));
}

void
gdk_gc_set_background (GdkGC	*gc,
		       GdkColor *color)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);
  g_return_if_fail (color != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.background = *color;
  priv->values_mask |= GDK_GC_BACKGROUND;

  GDK_NOTE (MISC, g_print ("gdk_gc_set_background: gc %p, rgb %d %d %d\n",
                           gc,
                           color->red / 256,
                           color->green/ 256,
                           color->blue / 256));
}

void
gdk_gc_set_font (GdkGC	 *gc,
		 GdkFont *font)
{
  GdkGCPrivate *priv;
  GdkFontPrivate *setfont;
  GdkFontPrivate *font_private;

  g_return_if_fail (gc != NULL);
  g_return_if_fail (font != NULL);

  if (font->type == GDK_FONT_FONT)
    {
      GDK_NOTE (MISC, g_print ("gdk_gc_set_font: gc %p  font %p\n",
                               gc, font));

      priv = (GdkGCPrivate*) gc;
      font_private = (GdkFontPrivate*) font;
      priv->values.font = (GdkFont*) g_new (GdkFontPrivate, 1);
      setfont = (GdkFontPrivate*) priv->values.font;

      priv->values.font->type = GDK_FONT_FONT;
      //setfont->xfont = font_private->xfont;
      setfont->font.ascent = font->ascent;
      setfont->font.descent = font->descent;
      priv->values_mask |= GDK_GC_FONT;

	  TextFont(font_private->fontnum);
	  TextSize(font_private->font.ascent);
	  TextFace(font_private->face);
    }      
}

void
gdk_gc_set_function (GdkGC	 *gc,
		     GdkFunction  function)
{

#if 1
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;
   
  priv->values.function = function;
  priv->values_mask |= GDK_GC_FUNCTION;
  #endif
}

void
gdk_gc_set_fill (GdkGC	 *gc,
		 GdkFill  fill)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.fill = fill;
  priv->values_mask |= GDK_GC_FILL;
}

void
gdk_gc_set_tile (GdkGC	   *gc,
		 GdkPixmap *tile)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  g_warning("gdk_gc_set_tile: not implemented!");
}

void
gdk_gc_set_stipple (GdkGC     *gc,
		    GdkPixmap *stipple)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  g_warning("gdk_gc_set_stipple: not implemented!");
}

void
gdk_gc_set_ts_origin (GdkGC *gc,
		      gint   x,
		      gint   y)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.ts_x_origin = x;
  priv->values.ts_y_origin = y;
  priv->values_mask |= GDK_GC_TS_X_ORIGIN | GDK_GC_TS_Y_ORIGIN;
}

void
gdk_gc_set_clip_origin (GdkGC *gc,
			gint   x,
			gint   y)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.clip_x_origin = x;
  priv->values.clip_y_origin = y;
  priv->values_mask |= GDK_GC_CLIP_X_ORIGIN | GDK_GC_CLIP_Y_ORIGIN;
}

void
gdk_gc_set_clip_mask (GdkGC	*gc,
		      GdkBitmap *mask)
{
  static int warned = 0;  

  g_return_if_fail (gc != NULL);

  if (!mask)
    return;
  
  if (!warned)
    {
      g_warning("gdk_gc_set_clip_mask: no yet implemented on MacOS");
      warned = 1;
    }
}


void
gdk_gc_set_clip_rectangle (GdkGC	*gc,
			   GdkRectangle *rectangle)
{
#if 1
  GdkGCPrivate *priv;
  GdkRegionPrivate *region_private;
  Rect r;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  if (rectangle)
    {

      if (!priv->clip_region)  
        priv->clip_region = gdk_region_new();

      region_private = (GdkRegionPrivate*) priv->clip_region;
      
      MacSetRect(&r,
             rectangle->x,
             rectangle->y, 
			 rectangle->x + rectangle->width,
			 rectangle->y + rectangle->height);
			 
      RectRgn(region_private->macrgn, &r);

      priv->values_mask |= GDK_GC_CLIP_MASK;
    }
  else
    {
      priv->values_mask &= ~GDK_GC_CLIP_MASK;

      if (priv->clip_region)
        {
          gdk_region_destroy(priv->clip_region);
          priv->clip_region = NULL;
        }
    }

  priv->values_mask &= ~(GDK_GC_CLIP_X_ORIGIN | GDK_GC_CLIP_Y_ORIGIN);
 #endif
} 

void
gdk_gc_set_clip_region (GdkGC		 *gc,
			GdkRegion	 *region)
{
#if 1
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;


  if (region)
    {

      GdkRegionPrivate *region_private;
      GdkRegionPrivate *gc_region_private;

      if (!priv->clip_region)  
        priv->clip_region = gdk_region_new();

      region_private = (GdkRegionPrivate*) region;
      gc_region_private = (GdkRegionPrivate*) priv->clip_region;
      
      MacCopyRgn(region_private->macrgn, gc_region_private->macrgn);
    }
  else
    {
      priv->values_mask &= ~GDK_GC_CLIP_MASK;

      if (priv->clip_region)
        {
          gdk_region_destroy(priv->clip_region);
          priv->clip_region = NULL;
        }
    }
  #endif
}

void
gdk_gc_set_subwindow (GdkGC	       *gc,
		      GdkSubwindowMode	mode)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  g_warning("gdk_gc_set_subwindow: not implemented!");
}

void
gdk_gc_set_exposures (GdkGC *gc,
		      gint   exposures)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.graphics_exposures = exposures;
  priv->values_mask |= GDK_GC_EXPOSURES;
}

void
gdk_gc_set_line_attributes (GdkGC	*gc,
			    gint	 line_width,
			    GdkLineStyle line_style,
			    GdkCapStyle	 cap_style,
			    GdkJoinStyle join_style)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);

  priv = (GdkGCPrivate*) gc;

  priv->values.line_width = line_width;
  priv->values.line_style = line_style;
  priv->values.cap_style = cap_style;
  priv->values.join_style = join_style;
}

void
gdk_gc_set_dashes (GdkGC *gc,
		   gint	  dash_offset,
		   gchar  dash_list[],
		   gint   n)
{
  GdkGCPrivate *priv;

  g_return_if_fail (gc != NULL);
  g_return_if_fail (dash_list != NULL);

  priv = (GdkGCPrivate*) gc;

  g_warning("gdk_gc_set_dashes: not implemented!");
}

void
gdk_gc_copy (GdkGC *dst_gc, GdkGC *src_gc)
{
  GdkGCPrivate *dst_private, *src_private;

  src_private = (GdkGCPrivate *) src_gc;
  dst_private = (GdkGCPrivate *) dst_gc;

  dst_private->values_mask = src_private->values_mask;
  dst_private->values.foreground = src_private->values.foreground;
  dst_private->values.background = src_private->values.background;

  dst_private->values.font  = src_private->values.font;

  dst_private->values.function = src_private->values.function;

  dst_private->values.fill = src_private->values.fill;
  dst_private->values.tile = src_private->values.tile;
  gdk_pixmap_ref (dst_private->values.tile);

  dst_private->values.stipple = src_private->values.stipple;
  gdk_pixmap_ref (dst_private->values.stipple);

  /* subwindow/coords */

  dst_private->clip_region = src_private->clip_region;

  dst_private->values.line_width = src_private->values.line_width;
  dst_private->values.line_style = src_private->values.line_style;
  dst_private->values.cap_style = src_private->values.cap_style;
  dst_private->values.join_style = src_private->values.join_style;
}
