
#ifndef __MAC_GWIND_H__
#define __MAC_GWIND_H__

#include <Carbon/Carbon.h>
#include <glib.h>
#include "gdk/gdk.h"

//#ifdef __cplusplus
//extern "C" {
//#endif


    //struct MacGWind_;

    typedef struct MacGWind_ MacGWind;
    typedef struct MacGWind_* MacGWindRef;


    struct MacGWind_ {

        void* magic;

        MacGWindRef parent;
        GList* children;

        GWorldPtr macport;
        WindowRef macwind;
        GWorldPtr macgw;

        Rect* posrect;
        RGBColor* bkgcol;

        int inputOnly;
    };




// creation
MacGWindRef MacGWind_NewPixMap(int width, int height, int depth);
MacGWindRef MacGWind_NewTopLevel(int x, int y, int width, int height, int gdkType, char* title);
MacGWindRef MacGWind_NewChild(MacGWindRef parent, int x, int y, int width, int height);
void MacGWind_Release(MacGWindRef* refPtr);

// drawing
#define MAC_CLIP_CHILDREN 1
#define MAC_DONT_CLIP_CHILDREN 0

CGrafPtr MacGWind_GetMacPort(MacGWindRef wRef);
int MacGWind_GetBytesPerRow(MacGWindRef img);
void* MacGWind_GetBits(MacGWindRef img);
BitMap* MacGWind_GetMacBitmap(MacGWindRef img);
void MacGWind_FocusPort(MacGWindRef wRef);
void MacGWind_FocusPortWithClip(MacGWindRef in_wRef, int clipChild);
void MacGWind_Erase(MacGWindRef in_wRef, Rect r);
void MacGWind_MapChildren(MacGWindRef in_wRef, guint mapped);

// attributs
Rect MacGWind_GetPositionRect(MacGWindRef wRef);
Rect MacGWind_GetInteriorRect(MacGWindRef wRef);
void MacGWind_Show(MacGWindRef wRef, int showBool);
void MacGWind_Move(MacGWindRef wRef, int x, int y);
void MacGWind_Resize(MacGWindRef wRef, int width, int height);
void MacGWind_Raise(MacGWindRef wRef);
void MacGWind_Lower(MacGWindRef wRef);
void MacGWind_SetParent(MacGWindRef wRef, MacGWindRef newParent);
void MacGWind_SetGeometryHints(MacGWindRef wRef, GdkGeometry * geometry, GdkWindowHints geom_mask);
void MacGWind_SetTitle(MacGWindRef wRef, const char* title);
void MacGWind_SetBackground(MacGWindRef wRef, RGBColor bkgCol);
void MacGWind_SetInputOnly(MacGWindRef wRef);
void MacGWind_LockMacWindowBits(MacGWindRef wRef);
void MacGWind_UnlockMacWindowBits(MacGWindRef wRef);

// events
MacGWindRef MacGWind_FromNative(WindowRef macWindowPtr);
void MacGWind_SendExpose(MacGWindRef wRef, RgnHandle in_update_rgn);
void MacGWind_SendButtonPress(MacGWindRef wRef, Point where);
void MacGWind_CheckMousePointer(MacGWindRef wRef, Point where);
int MacGWind_GetMouseButtonMask();
void MacGWind_SendDelete(MacGWindRef wRef);
void MacGWind_SendConfigure(MacGWindRef wRef);
void MyPostEvent(GdkEvent* event);
void MacGWind_SendButtonRelease(MacGWindRef wRef, Point where);
MacGWindRef MacGWind_FindChildAt(MacGWindRef wRef, Point where, Point* local);
//#ifdef __cplusplus
//}
//#endif

#endif

