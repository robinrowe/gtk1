/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#if HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <gdk/gdk.h>
#include <gdk/gdki18n.h>
#include "gdkx.h"

/* If this variable is FALSE, it indicates that we should
 * avoid trying to use multibyte conversion functions and
 * assume everything is 1-byte per character
 */
static gboolean gdk_use_mb;

/*
 *--------------------------------------------------------------
 * gdk_set_locale
 *
 * Arguments:
 *
 * Results:
 *
 * Side effects:
 *
 *--------------------------------------------------------------
 */

gchar*
gdk_set_locale (void)
{
  gchar *current_locale;

  gdk_use_mb = FALSE;

  if (!setlocale (LC_ALL,""))
    g_warning ("locale not supported by C library");
  
  current_locale = setlocale (LC_ALL, NULL);

  if (MB_CUR_MAX > 1)
    gdk_use_mb = TRUE;

  GDK_NOTE (XIM, g_message ("%s multi-byte string functions.", 
			    gdk_use_mb ? "Using" : "Not using"));
  
  return current_locale;
}

void 
gdk_im_begin (GdkIC *ic, GdkWindow* window)
{
}

void 
gdk_im_end (void)
{
}

GdkIMStyle
gdk_im_decide_style (GdkIMStyle supported_style)
{
  return (GdkIMStyle) (GDK_IM_PREEDIT_NONE | GDK_IM_STATUS_NONE);
}

GdkIMStyle
gdk_im_set_best_style (GdkIMStyle style)
{
  return (GdkIMStyle) (GDK_IM_PREEDIT_NONE | GDK_IM_STATUS_NONE);
}

gint 
gdk_im_ready (void)
{
  return FALSE;
}

GdkIC * 
gdk_ic_new (GdkICAttr *attr, GdkICAttributesType mask)
{
  return NULL;
}

void 
gdk_ic_destroy (GdkIC *ic)
{
}

GdkIMStyle
gdk_ic_get_style (GdkIC *ic)
{
  return (GdkIMStyle) (GDK_IM_PREEDIT_NONE | GDK_IM_STATUS_NONE);
}

void 
gdk_ic_set_values (GdkIC *ic, ...)
{
}

void 
gdk_ic_get_values (GdkIC *ic, ...)
{
}

GdkICAttributesType 
gdk_ic_set_attr (GdkIC *ic, GdkICAttr *attr, GdkICAttributesType mask)
{
  return (GdkICAttributesType) 0;
}

GdkICAttributesType 
gdk_ic_get_attr (GdkIC *ic, GdkICAttr *attr, GdkICAttributesType mask)
{
  return (GdkICAttributesType) 0;
}

GdkEventMask 
gdk_ic_get_events (GdkIC *ic)
{
  return (GdkEventMask) 0;
}

/*
 * gdk_wcstombs 
 *
 * Returns a multi-byte string converted from the specified array
 * of wide characters. The string is newly allocated. The array of
 * wide characters must be null-terminated. If the conversion is
 * failed, it returns NULL.
 */
gchar *
gdk_wcstombs (const GdkWChar *src)
{
  gchar buffer[4096];
  int i;

  i = 0;
  while (i < 4095 && src[i] != '\0')
    {
      buffer[i] = src[i];
      i++;
    }
  buffer[i] = src[i];
  buffer[4095] = '\0';

  return g_strdup(buffer);
}

  
/*
 * gdk_mbstowcs
 *
 * Converts the specified string into wide characters, and, returns the
 * number of wide characters written. The string 'src' must be
 * null-terminated. If the conversion is failed, it returns -1.
 */
gint
gdk_mbstowcs (GdkWChar *dest, const gchar *src, gint dest_max)
{
  int i;

  i = 0;
  while (i < dest_max)
    {
      dest[i] = src[i]; 

      if (src[i] == '\0')
        return i;

      i++;
    }

  return i;
}

