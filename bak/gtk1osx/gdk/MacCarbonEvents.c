/*
 *  GDK - The GIMP Drawing Kit
 *  MacCarbonEvents.c: Mac native Carbon event handlers and setup
 * 
 *  Copyright (C) 2003 Brian Griffith <bagriffith@alumni.ucsd.edu>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */
 
#include "MacCarbonEvents.h"

#include "MacToGTK.h"
#include "gdk.h"
#include "gdkprivate.h"

GdkWindow *propagate_event(GdkWindow *gdk_window, gint event);

CFRunLoopRef g_mac_main_run_loop = NULL;
CFRunLoopSourceRef g_mac_process_gdk_event_queue_source = NULL;
static GdkWindow* g_win_containing_mouse = NULL;
static EventHandlerUPP g_keyboard_handler_upp = NULL;
static EventHandlerUPP g_mouse_button_handler_upp = NULL;
static EventHandlerUPP g_mouse_scroll_handler_upp = NULL;
static EventHandlerUPP g_mouse_motion_handler_upp = NULL;
static EventHandlerUPP g_window_update_handler_upp = NULL;
static EventHandlerUPP g_window_handler_upp = NULL;

static WindowRef win_ref_grabbed = NULL;
static MacGWindRef mac_gwind_grabbed = NULL;


/* gdk private */
extern GdkEventFunc event_func;
extern gpointer event_data;

/*
 * using a RunLoopSource to process events appended to gdk event queue
 * source is signaled whenever an event is added to the gdk queue
 */
static void
process_gdk_event_queue (void *info)
{
  GdkEvent *event = NULL;
  
  GDK_THREADS_ENTER ();

  event = gdk_event_unqueue ();

  while (event)
  {
    if (event_func)
      (*event_func) (event, event_data);
    
    gdk_event_free (event);
    event = gdk_event_unqueue ();
  }
  
  GDK_THREADS_LEAVE ();
}

static OSStatus 
keyboard_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, void *in_user_data) 
{
  /* 
   * ideally this would handle text events, rather than raw keys
   * and possibly support unicode entry as well...
   */
  OSStatus result = noErr;
  char mac_key_code;
  UInt32 key_mods;
  
  WindowRef focusedWindow = FrontWindow ();
  
  GetEventParameter (in_event, kEventParamKeyMacCharCodes, typeChar,
                      NULL, sizeof(mac_key_code), NULL, &mac_key_code);
  
  GetEventParameter (in_event, kEventParamKeyModifiers, typeUInt32, 
                      NULL, sizeof(key_mods), NULL, &key_mods);
  
 switch (GetEventKind (in_event)) 
 {
    case kEventRawKeyDown: /* fall through */
    case kEventRawKeyRepeat:
      MacGTK_SendKey (focusedWindow, mac_key_code, key_mods, GDK_KEY_PRESS);
       printf("mac CARBON key pressed '%c' <%d>  front window=%d\n",mac_key_code,mac_key_code,focusedWindow);
      result = noErr;
      break;
    
    case kEventRawKeyUp:
      MacGTK_SendKey (focusedWindow, mac_key_code, key_mods, GDK_KEY_RELEASE);
       printf("mac CARBON key released '%c' <%d>  front window=%d\n",mac_key_code,mac_key_code,focusedWindow);
      result = noErr;
      break;
    
    default:
      break;
  }
    
  return result;
}


static OSStatus 
mouse_motion_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, 
    void *in_user_data) 
{
  OSStatus result = noErr;
  Point	global_point;
  Point local_point;
  WindowRef win_ref = NULL;
  UInt32 key_mods;
  UInt32 mac_event_kind;
  gint   gdk_event_kind;
  GdkEvent *gdk_crossing_event = NULL;
  GdkEvent *gdk_event = NULL;
  MacGWindRef mac_gwind = NULL;
  GdkWindow *gdk_window = NULL;
  guint32 time;
  guint state;
  Rect content_bounds;
  EventMouseButton button;


  mac_event_kind = GetEventKind(in_event);

  gdk_event_kind = GDK_POINTER_MOTION_MASK;

  /* event kind should be either

    kEventMouseMoved = 5,
    kEventMouseDragged = 6,
    */

  GetEventParameter (in_event, kEventParamMouseLocation, typeQDPoint, NULL,
                     sizeof(global_point), NULL, &global_point);

  GetEventParameter(in_event, kEventParamWindowRef, typeWindowRef, NULL,
                    sizeof(win_ref), NULL, &win_ref);


  GetEventParameter (in_event, kEventParamKeyModifiers, typeUInt32, NULL,
                     sizeof(key_mods), NULL, &key_mods);

  state = MacToGTKMods (key_mods);


  if(mac_event_kind = kEventMouseDragged) {

    gdk_event_kind |= GDK_BUTTON_MOTION_MASK;

    GetEventParameter (in_event, kEventParamMouseButton, typeMouseButton, NULL,
                       sizeof(button), NULL, &button);

    if (button == kEventMouseButtonPrimary)
    {
      state |= GDK_BUTTON1_MASK;
      gdk_event_kind |= GDK_BUTTON1_MOTION_MASK;
    }
    if (button == kEventMouseButtonSecondary)
    {
      state |= GDK_BUTTON3_MASK;
      gdk_event_kind |= GDK_BUTTON3_MOTION_MASK;
    }
    else if (button == kEventMouseButtonTertiary)
    {
      state |= GDK_BUTTON2_MASK;
      gdk_event_kind |= GDK_BUTTON2_MOTION_MASK;
    }

  }

  if (win_ref == NULL)
  {
    /* if the mouse button is up, win_ref is always NULL */
    /* find out if the mouse is actually moving over a window */
    MacFindWindow (global_point, &win_ref);
  }

  /* translate global point to windows content region */
  GetWindowBounds (win_ref, kWindowContentRgn, &content_bounds);
  SetPt (&local_point, global_point.h - content_bounds.left, global_point.v - content_bounds.top);

  if (win_ref != NULL)
  {
    mac_gwind = MacGWind_FindChildAt ((MacGWindRef)GetWRefCon (win_ref), local_point, &local_point);
  }

  if(win_ref_grabbed) {

    GetWindowBounds (win_ref_grabbed, kWindowContentRgn, &content_bounds);
    SetPt (&local_point, global_point.h - content_bounds.left, global_point.v - content_bounds.top);
    if(mac_gwind_grabbed->posrect) {
      SetPt (&local_point, local_point.h - mac_gwind_grabbed->posrect->left, local_point.v - mac_gwind_grabbed->posrect->top);
    }
    mac_gwind = mac_gwind_grabbed;

  }


  time = gdk_time_get ();
  

  
  if (mac_gwind != NULL)
  {
    // printf ("MOTION: mac_gwind VALID\n");
    gdk_window = gdk_window_lookup (mac_gwind);
  }
  else
  {
    // printf ("MOTION: mac_gwind NULL\n");
    gdk_window = (GdkWindow *)&gdk_root_parent;
    local_point.h = global_point.h;
    local_point.v = global_point.v;
  }

  if (gdk_window != g_win_containing_mouse)
  {

    if(GDK_LEAVE_NOTIFY_MASK & ((GdkWindowPrivate*) g_win_containing_mouse)->event_mask) {
      gdk_crossing_event = gdk_event_new ();

      gdk_crossing_event->crossing.send_event = FALSE;
      gdk_crossing_event->crossing.time = time;
      gdk_crossing_event->crossing.mode = GDK_CROSSING_NORMAL;

      /* not correctly set, but should be OK, need to determine child window ancestry here */
      gdk_crossing_event->crossing.detail = GDK_NOTIFY_NONLINEAR;
      gdk_crossing_event->crossing.x_root = global_point.h;
      gdk_crossing_event->crossing.y_root = global_point.v;
      gdk_crossing_event->crossing.x = local_point.h;
      gdk_crossing_event->crossing.y = local_point.v;
      gdk_crossing_event->crossing.state = state;

      /* first do leave */
      gdk_crossing_event->crossing.type = GDK_LEAVE_NOTIFY;
      gdk_crossing_event->crossing.window = gdk_window_ref (g_win_containing_mouse);
      gdk_crossing_event->crossing.subwindow = gdk_window_ref (gdk_window);
      gdk_crossing_event->crossing.focus = (win_ref == FrontWindow ());

      // printf("WINDOW LEAVE: %d\n", gdk_crossing_event->crossing.window);
      /* makes copy of event and appends to queue */
      gdk_event_put (gdk_crossing_event);
    }

    if(GDK_ENTER_NOTIFY_MASK  & ((GdkWindowPrivate*) gdk_window)->event_mask) {
      /* then do enter */
      if(NULL == gdk_crossing_event) {

        gdk_crossing_event = gdk_event_new ();

        gdk_crossing_event->crossing.send_event = FALSE;
        gdk_crossing_event->crossing.time = time;
        gdk_crossing_event->crossing.mode = GDK_CROSSING_NORMAL;

        /* not correctly set, but should be OK, need to determine child window ancestry here */
        gdk_crossing_event->crossing.detail = GDK_NOTIFY_NONLINEAR;
        gdk_crossing_event->crossing.x_root = global_point.h;
        gdk_crossing_event->crossing.y_root = global_point.v;
        gdk_crossing_event->crossing.x = local_point.h;
        gdk_crossing_event->crossing.y = local_point.v;
        gdk_crossing_event->crossing.state = state;
      }

      gdk_crossing_event->crossing.type = GDK_ENTER_NOTIFY;
      gdk_crossing_event->crossing.window = gdk_window_ref (gdk_window);
      gdk_crossing_event->crossing.subwindow = gdk_window_ref (g_win_containing_mouse);
      gdk_crossing_event->crossing.focus = (win_ref == FrontWindow ());

      // printf("WINDOW ENTER: %d\n", gdk_crossing_event->crossing.window);
      gdk_event_queue_append (gdk_crossing_event);
    }

    g_win_containing_mouse = gdk_window;
  }
  
  /*
   check event propagation, if a window does not want the event it passes
   it to its parent upto and including the root_window
  */

  gdk_window = propagate_event(gdk_window, gdk_event_kind);
  
  if(gdk_window) {
    gdk_event = gdk_event_new ();

    gdk_event->motion.type = GDK_MOTION_NOTIFY;
    gdk_event->motion.window = gdk_window_ref (gdk_window);
    gdk_event->motion.send_event = FALSE;
    gdk_event->motion.time = time;
    gdk_event->motion.pressure = 0.5;
    gdk_event->motion.xtilt = 0;
    gdk_event->motion.ytilt = 0;
    gdk_event->motion.source = GDK_SOURCE_MOUSE;
    gdk_event->motion.deviceid = GDK_CORE_POINTER;
    gdk_event->motion.x_root = global_point.h;
    gdk_event->motion.y_root = global_point.v;
    gdk_event->motion.x = local_point.h;
    gdk_event->motion.y = local_point.v;
    gdk_event->motion.state = state;

    // printf("MOTION global x=%f, y=%f\n", gdk_event->motion.x_root, gdk_event->motion.y_root);
    // printf("MOTION local x=%f, y=%f\n", gdk_event->motion.x, gdk_event->motion.y);

    gdk_event_queue_append (gdk_event);
  }

  return result;
}


static OSStatus 
mouse_button_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, 
    void *in_user_data) 
{
  OSStatus result = noErr;
  Point	global_point;
  Point local_point;
  EventMouseButton button;
  UInt32 click_count;
  WindowRef win_ref;
  UInt32 key_mods;
  MacGWindRef mac_gwind;
  GdkEvent *gdk_event;
  guint32 time;
  guint state;
  Rect content_bounds;
  GdkWindow *gdk_window;
  
  GetEventParameter (in_event, kEventParamMouseLocation, typeQDPoint, NULL,
                      sizeof(global_point), NULL, &global_point);
                      
  GetEventParameter(in_event, kEventParamWindowRef, typeWindowRef, NULL, 
                    sizeof(win_ref), NULL, &win_ref);
  
  /* translate global point to windows content region */
  GetWindowBounds (win_ref, kWindowContentRgn, &content_bounds);
  SetPt (&local_point, global_point.h - content_bounds.left, global_point.v - content_bounds.top);
  
  /* will return possible child window at point and convert the point to its local coords */
  mac_gwind = MacGWind_FindChildAt ((MacGWindRef)GetWRefCon (win_ref), local_point, &local_point);
  
  if (mac_gwind == NULL)
  {
   	// printf ("WARNING: mouse_button_handler window is NULL\n");
    /* ignoring all mouse downs that occur outside of top level windows */
    return eventNotHandledErr;
  }
  
  GetEventParameter (in_event, kEventParamKeyModifiers, typeUInt32, NULL, 
                      sizeof(key_mods), NULL, &key_mods);


  state = MacToGTKMods (key_mods);

  GetEventParameter (in_event, kEventParamMouseButton, typeMouseButton, NULL,
                     sizeof(button), NULL, &button);

  gdk_window = gdk_window_lookup (mac_gwind);

  switch (GetEventKind (in_event))
  {
    case kEventMouseDown:
      gdk_window = propagate_event(gdk_window, GDK_BUTTON_PRESS_MASK);      
      break;
    case kEventMouseUp:
      gdk_window = propagate_event(gdk_window, GDK_BUTTON_RELEASE_MASK);
      break;
    default:
      result = eventNotHandledErr;
      break;
  }

  if(gdk_window == NULL) {
    /* no windows wanted the event */
    return noErr;
  }

  if (button == kEventMouseButtonPrimary)
  {
    state |= GDK_BUTTON1_MASK;
  }
  if (button == kEventMouseButtonSecondary)
  {
    state |= GDK_BUTTON3_MASK;
  }
  else if (button == kEventMouseButtonTertiary)
  {
    state |= GDK_BUTTON2_MASK;
  }
  
  
  time = gdk_time_get ();
  
  gdk_event = gdk_event_new ();

  gdk_event->button.state = state; 
  gdk_event->button.window = gdk_window_ref (gdk_window);
  gdk_event->button.send_event = FALSE;
  gdk_event->button.time = time;
  gdk_event->button.pressure = 0.5;
  gdk_event->button.xtilt = 0;
  gdk_event->button.ytilt = 0;
  gdk_event->button.source = GDK_SOURCE_MOUSE;
  gdk_event->button.deviceid = GDK_CORE_POINTER;
  gdk_event->button.x_root = global_point.h;
  gdk_event->button.y_root = global_point.v;
  gdk_event->button.x = local_point.h;
  gdk_event->button.y = local_point.v;
  gdk_event->button.state = state;
  
  // printf("BUTTON global x=%f, y=%f\n", gdk_event->button.x_root, gdk_event->button.y_root);
  // printf("BUTTON local x=%f, y=%f\n", gdk_event->button.x, gdk_event->button.y);
  
  switch (GetEventKind (in_event)) 
  {
    case kEventMouseDown:
      // printf("CARBON mouse down\n");
      GetEventParameter (in_event, kEventParamClickCount, typeUInt32, NULL, 
                          sizeof(click_count), NULL, &click_count);
      
      GetEventParameter (in_event, kEventParamMouseButton, typeMouseButton, NULL,
                          sizeof(button), NULL, &button);
      
      gdk_event->button.type = GDK_BUTTON_PRESS;
      gdk_event->button.button = 1;

      if (button == kEventMouseButtonPrimary)
      {
        win_ref_grabbed = win_ref;
        mac_gwind_grabbed = mac_gwind;
      }
      if (button == kEventMouseButtonSecondary) 
      {
        gdk_event->button.button = 3;
      } 
      else if (button == kEventMouseButtonTertiary) 
      {
        gdk_event->button.button = 2;
      } 
      
      if (click_count == 2) 
      {
      	/* send plaing GDK_BUTTON_PRESS event first */
        gdk_event_put (gdk_event); /* makes copy of event and appends to queue */
        /* which is followed by second GDK_2BUTTON_PRESS event */
        gdk_event->button.type = GDK_2BUTTON_PRESS;
      } 
      else if (click_count == 3) 
      {
        /* send plaing GDK_BUTTON_PRESS event first */
        gdk_event_put (gdk_event); /* makes copy of event and appends to queue */
        /* which is followed by second GDK_3BUTTON_PRESS event */
        gdk_event->button.type = GDK_3BUTTON_PRESS;
      }
      break;
    
    case kEventMouseUp:
      // printf("CARBON mouse up\n");
      GetEventParameter (in_event, kEventParamMouseButton, typeMouseButton, NULL,
                    sizeof(button), NULL, &button);
      
      gdk_event->button.type = GDK_BUTTON_RELEASE;
      gdk_event->button.button = 1;

      if (button == kEventMouseButtonPrimary)
      {
        win_ref_grabbed = NULL;
        mac_gwind_grabbed = NULL;
      }
      if (button == kEventMouseButtonSecondary) 
      {
        gdk_event->button.button = 3;
      } 
      else if (button == kEventMouseButtonTertiary) 
      {
        gdk_event->button.button = 2;
      }
      break;
    
    default:
      result = eventNotHandledErr;
      break;
  }
  
  gdk_event_queue_append (gdk_event);
      
  return result;
}

static OSStatus 
mouse_scroll_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, 
    void *in_user_data) 
{
  OSStatus result = noErr;
  UInt32 key_mods;
  Point	global_point;
  Point local_point;
  EventMouseWheelAxis axis;
  SInt32 delta;
  WindowRef win_ref;
  MacGWindRef mac_gwind;
  GdkEvent *gdk_event;
  guint32 time;
  Rect content_bounds;
  GdkWindow *gdk_window;
  GdkScrollDirection scrollDirection = GDK_SCROLL_DOWN;

  GetEventParameter (in_event, kEventParamMouseLocation, typeQDPoint, NULL,
                      sizeof(global_point), NULL, &global_point);

  GetEventParameter(in_event, kEventParamWindowRef, typeWindowRef, NULL, 
                    sizeof(win_ref), NULL, &win_ref);

  /* translate global point to windows content region */
  GetWindowBounds (win_ref, kWindowContentRgn, &content_bounds);
  SetPt (&local_point, global_point.h - content_bounds.left, global_point.v - content_bounds.top);

  /* will return possible child window at point and convert the point to its local coords */
  mac_gwind = MacGWind_FindChildAt ((MacGWindRef)GetWRefCon (win_ref), local_point, &local_point);
  
  if (mac_gwind == NULL)
  {
    // printf ("WARNING: mouse_button_handler window is NULL\n");
    /* ignoring all mouse downs that occur outside of top level windows */
    return eventNotHandledErr;
  }
 
  time = gdk_time_get();   
  gdk_window = gdk_window_lookup (mac_gwind);
  
  gdk_event = gdk_event_new ();

  GetEventParameter( in_event, kEventParamKeyModifiers, typeUInt32, 
          NULL, sizeof(key_mods), NULL, &key_mods );

  GetEventParameter( in_event, kEventParamMouseWheelAxis, typeMouseWheelAxis, 
          NULL, sizeof(axis), NULL, &axis );

  GetEventParameter( in_event, kEventParamMouseWheelDelta, typeLongInteger, 
          NULL, sizeof(delta), NULL, &delta );

  if( kEventMouseWheelAxisX == axis )
  {
    if( delta > 0 )
    {
      scrollDirection = GDK_SCROLL_RIGHT;
    }
    else
    {
      scrollDirection = GDK_SCROLL_LEFT;
    }
  }
  else if( delta > 0 )
  {
    scrollDirection = GDK_SCROLL_UP;
  }

  gdk_event->scroll.type = GDK_SCROLL;
  gdk_event->scroll.deviceid = GDK_CORE_POINTER;
  gdk_event->scroll.send_event = FALSE;
  gdk_event->scroll.direction = scrollDirection;
  gdk_event->scroll.window = gdk_window_ref (gdk_window);
  gdk_event->scroll.time = time;
  gdk_event->scroll.x_root = global_point.h;
  gdk_event->scroll.y_root = global_point.v;
  gdk_event->scroll.x = local_point.h;
  gdk_event->scroll.y = local_point.v;
  gdk_event->scroll.state = 0;

  gdk_event_queue_append (gdk_event);

  return result;
}

static OSStatus
window_update_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, 
    void *in_user_data)
{
  OSStatus result = noErr;
  WindowRef win_ref;
  Boolean port_changed;
  CGrafPtr saved_port;
  RgnHandle update_rgn;
  
  GetEventParameter (in_event, kEventParamDirectObject, typeWindowRef, NULL, 
                      sizeof(win_ref), NULL, &win_ref);
  
  update_rgn = NewRgn ();
  
  GetWindowRegion (win_ref, kWindowUpdateRgn, update_rgn);
  
  if (EmptyRgn (update_rgn))
  {
    // printf ("UPDATE EMPTY, ignoring\n");
    DisposeRgn (update_rgn);
    return result;
  }
    
  port_changed = QDSwapPort (GetWindowPort (win_ref), &saved_port);
  SetOrigin (0, 0);
  
  BeginUpdate (win_ref);
  
  GetPortVisibleRegion (GetWindowPort (win_ref), update_rgn);

#if DEBUG_UPDATE_EVENT
  ClipRect (&win_ref->portRect);
  ForeColor (redColor);
  PaintRect (&win_ref->portRect);
#endif
  
  EndUpdate (win_ref);
  
  if (port_changed)
  {
    /* previous Mac update code did not change port back? */
    QDSwapPort (saved_port, NULL);
  }
  
  
  MacGTK_SendExposeEvent (win_ref, update_rgn);
  DisposeRgn (update_rgn);
  
  return result;
}

static OSStatus 
window_handler (EventHandlerCallRef in_handler_call_ref, EventRef in_event, void *in_user_data) 
{
  OSStatus result = noErr;
  GdkEvent *event;
  WindowRef win_ref;
  Rect win_rect;
  MacGWindRef mac_gwind;
  GdkWindow *gdk_window;
  Rect content_rect;
 
  GetEventParameter (in_event, kEventParamDirectObject, typeWindowRef, NULL, 
                      sizeof(win_ref), NULL, &win_ref);
  
  mac_gwind = (MacGWindRef)GetWRefCon (win_ref);
  gdk_window = gdk_window_lookup (mac_gwind);
  
  event = gdk_event_new ();
                                        
  switch (GetEventKind (in_event))
  {
    case kEventWindowBoundsChanged:
      GetEventParameter(in_event, kEventParamCurrentBounds, typeQDRectangle, NULL, 
                        sizeof(win_rect), NULL, &win_rect);
      
      event->configure.type = GDK_CONFIGURE;
      event->configure.window = gdk_window_ref (gdk_window); 
      event->configure.send_event = FALSE;
      event->configure.x = win_rect.left;
      event->configure.y = win_rect.top;
      event->configure.width = (win_rect.right - win_rect.left);
      event->configure.height = (win_rect.bottom - win_rect.top);
      // printf("Bounds changed: %d\n", gdk_window);
      // printf("x = %d, y = %d, w = %d, h = %d\n", event->configure.x, event->configure.y, event->configure.width, event->configure.height);
      break;
    
    case kEventWindowFocusAcquired:
      event->focus_change.type = GDK_FOCUS_CHANGE;
      event->focus_change.window = gdk_window_ref (gdk_window);
      event->focus_change.send_event = FALSE;
      event->focus_change.in = TRUE;
      // printf("Focus got: %d\n", gdk_window);
      break;
      
    case kEventWindowFocusRelinquish:
      event->focus_change.type = GDK_FOCUS_CHANGE;
      event->focus_change.window = gdk_window_ref (gdk_window);
      event->focus_change.send_event = FALSE;
      event->focus_change.in = FALSE;
      // printf("Focus lost: %d\n", gdk_window);
      break;
        
    case kEventWindowShown:
      GetWindowBounds (win_ref, kWindowContentRgn, &content_rect);
      content_rect.left = 0;
      content_rect.top = 0;
      InvalWindowRect (win_ref, &content_rect);
      /* fall through */
    case kEventWindowExpanded:
      event->visibility.type = GDK_VISIBILITY_NOTIFY;
      event->visibility.window = gdk_window_ref (gdk_window); 
      event->visibility.send_event = FALSE;
      event->visibility.state = GDK_VISIBILITY_UNOBSCURED;
      // printf("Window shown: %d\n", gdk_window);
      break;
      
    case kEventWindowHidden:
    case kEventWindowCollapsed:
      event->visibility.type = GDK_VISIBILITY_NOTIFY;
      event->visibility.window = gdk_window_ref (gdk_window);
      event->visibility.send_event = FALSE;
      event->visibility.state = GDK_VISIBILITY_FULLY_OBSCURED;
      // printf("window hidden: %d\n", gdk_window);
      break;
    
    case kEventWindowClose:
      event->any.type = GDK_DELETE;
      event->any.window = gdk_window_ref (gdk_window);
      // printf("window delete: %d\n", gdk_window);
      break;
      
    case kEventWindowClosed:
      event->any.type = GDK_DESTROY;
      event->any.window = gdk_window_ref (gdk_window);
      // printf("window destroy: %d\n", gdk_window);
      break;
      
    default:
      break;
  }
  
  gdk_event_queue_append (event);
  
  return result;
}

static OSStatus 
setup_keyboard_handler () 
{
  static const EventTypeSpec k_events[] =
    {
      { kEventClassKeyboard, kEventRawKeyDown },
      { kEventClassKeyboard, kEventRawKeyUp },
      { kEventClassKeyboard, kEventRawKeyRepeat }
    };
  
  if (g_keyboard_handler_upp == NULL)
  {
    g_keyboard_handler_upp = NewEventHandlerUPP (keyboard_handler);
  }
  
  return InstallApplicationEventHandler (g_keyboard_handler_upp, GetEventTypeCount (k_events), 
                                          k_events, NULL, NULL);
}

static OSStatus 
setup_mouse_button_handler (WindowRef in_win) 
{
  static const EventTypeSpec k_events[] =
    {
      { kEventClassMouse, kEventMouseDown },
      { kEventClassMouse, kEventMouseUp }
    };
  
  OSStatus result = noErr;
  
  if (g_mouse_button_handler_upp == NULL)
  {
    g_mouse_button_handler_upp = NewEventHandlerUPP (mouse_button_handler);
  }
  
  if (in_win != NULL) 
  {
    result = InstallWindowEventHandler (in_win, g_mouse_button_handler_upp, 
                  GetEventTypeCount(k_events), k_events, NULL, NULL);
  } 
  else 
  {
    result = InstallApplicationEventHandler (g_mouse_button_handler_upp, 
                  GetEventTypeCount(k_events), k_events, NULL, NULL);
  }
  
  return result;
}

static OSStatus 
setup_mouse_scroll_handler (WindowRef in_win) 
{
  static const EventTypeSpec k_events[] =
    {
      { kEventClassMouse, kEventMouseWheelMoved }
    };
  
  OSStatus result = noErr;
  
  if (g_mouse_scroll_handler_upp == NULL)
  {
    g_mouse_scroll_handler_upp = NewEventHandlerUPP (mouse_scroll_handler);
  }
  
  if (in_win != NULL) 
  {
    result = InstallWindowEventHandler (in_win, g_mouse_scroll_handler_upp, 
                  GetEventTypeCount(k_events), k_events, NULL, NULL);
  } 
  else 
  {
    result = InstallApplicationEventHandler (g_mouse_scroll_handler_upp, 
                  GetEventTypeCount(k_events), k_events, NULL, NULL);
  }
  
  return result;
}

static OSStatus 
setup_mouse_motion_handler (WindowRef in_win) 
{
  static const EventTypeSpec k_events[] =
    {
      { kEventClassMouse, kEventMouseMoved },
      { kEventClassMouse, kEventMouseDragged },
    };
  
  OSStatus result = noErr;
  
  if (g_mouse_motion_handler_upp == NULL)
  {
    g_mouse_motion_handler_upp = NewEventHandlerUPP (mouse_motion_handler);
  }
  
  g_win_containing_mouse = (GdkWindow*)&gdk_root_parent;
  
  if (in_win != NULL) 
  {
    result = InstallWindowEventHandler (in_win, g_mouse_motion_handler_upp, 						
                    GetEventTypeCount(k_events), k_events, NULL, NULL);
  } 
  else 
  {
    result = InstallApplicationEventHandler (g_mouse_motion_handler_upp, 
                    GetEventTypeCount(k_events), k_events, NULL, NULL);
  }
  
  return result;
}

static OSStatus 
setup_window_update_handler (WindowRef in_win) 
{
  static const EventTypeSpec k_update_events[] =
    {
      { kEventClassWindow, kEventWindowUpdate } /* GDK_EXPOSE */
    };
  
  if (g_window_update_handler_upp == NULL)
  {
    g_window_update_handler_upp = NewEventHandlerUPP (window_update_handler);
  }
  
  return InstallWindowEventHandler (in_win, g_window_update_handler_upp, sizeof(k_update_events), 
                                      k_update_events, NULL, NULL);
}

static OSStatus 
setup_window_handler (WindowRef in_win) 
{
  static const EventTypeSpec k_win_events[] =
    {
      { kEventClassWindow, kEventWindowShown }, /* GDK_VISIBILITY_NOTIFY */
      { kEventClassWindow, kEventWindowExpanded }, /* GDK_VISIBILITY_NOTIFY */
      { kEventClassWindow, kEventWindowHidden }, /* GDK_VISIBILITY_NOTIFY */
      { kEventClassWindow, kEventWindowCollapsed }, /* GDK_VISIBILITY_NOTIFY */
      
      { kEventClassWindow, kEventWindowFocusAcquired }, /* GDK_FOCUS_CHANGE */
      { kEventClassWindow, kEventWindowFocusRelinquish }, /* GDK_FOCUS_CHANGE */
      
      { kEventClassWindow, kEventWindowBoundsChanged }, /* GDK_CONFIGURE */

      { kEventClassWindow, kEventWindowClose }, /* GDK_DELETE */

      { kEventClassWindow, kEventWindowClosed } /* GDK_DESTROY */
    };
  
  if (g_window_handler_upp == NULL)
  {
    g_window_handler_upp = NewEventHandlerUPP (window_handler);
  }

  return InstallWindowEventHandler (in_win, g_window_handler_upp, sizeof(k_win_events), 
                                      k_win_events, NULL, NULL);
}

static OSStatus
setup_process_gdk_event_queue_source ()
{
  /* install a RunLoopSource to dispatch events posted to gdk queue */
  OSStatus err;
  CFRunLoopSourceContext context;
  
  context.version = 0;
  context.info = NULL;
  context.retain = NULL;
  context.release = NULL;
  context.copyDescription = NULL;
  context.equal = NULL;
  context.hash = NULL;
  context.schedule = NULL;
  context.cancel = NULL;
  context.perform = process_gdk_event_queue;
  
  g_mac_process_gdk_event_queue_source = CFRunLoopSourceCreate (kCFAllocatorDefault, 0, &context);
  
  g_mac_main_run_loop = (CFRunLoopRef) GetCFRunLoopFromEventLoop (GetMainEventLoop ());
    
  CFRunLoopAddSource (g_mac_main_run_loop, g_mac_process_gdk_event_queue_source, kCFRunLoopCommonModes);
                              
  return err;
}

OSStatus 
setup_app_carbon_event_handlers () 
{
  OSStatus result = noErr;
  
  if (result == noErr) 
  {
    result = setup_keyboard_handler ();
  }
  
  if (result == noErr) 
  {
    result = setup_mouse_button_handler (NULL);
  }
  
  if( result == noErr )
  {
    result = setup_mouse_scroll_handler (NULL);
  }
  
  if (result == noErr) 
  {
    result = setup_mouse_motion_handler (NULL);
  }
  
  if (result == noErr)
  {
    setup_process_gdk_event_queue_source ();
  }
  
  return result;
}

OSStatus 
setup_win_carbon_event_handlers (WindowRef win_ref) 
{
  OSStatus result = noErr;
  
  if (result == noErr) 
  { 
    result = setup_window_handler (win_ref);
  }
  
  if (result == noErr) 
  { 
    result = setup_window_update_handler (win_ref);
  }
  
  return result;
}


GdkWindow *propagate_event(GdkWindow *gdk_window, gint event) {

  while(gdk_window != NULL) {
    if( ((GdkWindowPrivate*) gdk_window)->event_mask & event) {
      break;
    }
    gdk_window = ((GdkWindowPrivate*) gdk_window)->parent;
  }

  return gdk_window;
}
