#include "MacToGTK.h"

#include "MacGWind.h"

#include "gdk/gdk.h"
#include <gdkprivate.h>
#include <gdkkeysyms.h>

#include <stdio.h>


static Boolean IsWindowActive(WindowRef w) {

    return IsWindowHilited(w);
}

void MacGTK_SendExposeEvent(WindowRef macWindPtr, RgnHandle rgnH) {

  MacGWindRef macGWin;
  
  macGWin = MacGWind_FromNative(macWindPtr); 

  if (macGWin)
  	MacGWind_SendExpose(macGWin, rgnH);

}


static void MacGTK_IdleMouse() {

    WindowRef	macWindPtr = NULL;
    SInt16		thePart;
    MacGWindRef macGWin = NULL;
    Point where;
    
    
    GetMouse(&where);
    LocalToGlobal(&where);

    thePart = MacFindWindow(where, &macWindPtr);

#if 0
    // Causes submenus to lock
    if (! IsWindowActive(macWindPtr)) {
        macWindPtr = NULL;
    }
#endif
    if (macWindPtr && thePart == inContent) {
        macGWin = MacGWind_FromNative(macWindPtr); 
    }
    
    if (macGWin) {
        MacSetPort(GetWindowPort(macWindPtr));
        SetOrigin(0,0);
        GlobalToLocal(&where);
    }
    
    // macGWin peut etre NULL 
    MacGWind_CheckMousePointer(macGWin, where);
}

void MacGTK_Loop() {

    //while (g_main_iteration(FALSE)) ; // ????
    //printf("idle loop\n");
    MacGTK_IdleMouse();

}

void MacGTK_SendButtonPressEvent(WindowRef macWindPtr, Point where) {

  MacGWindRef macGWin = NULL;
  
  if (! IsWindowActive(macWindPtr)) {
     SelectWindow(macWindPtr);
     return;
  }
  
  macGWin = MacGWind_FromNative(macWindPtr); 

  if (macGWin)
 	 MacGWind_SendButtonPress(macGWin, where);
}

static int _SpecialMacKeyToGTKKeySym(int theKey) {
    //icky special case code
    switch (theKey) {
        case kHomeCharCode:
            return GDK_Home;
        case kEnterCharCode:
            return GDK_Return;
        case kEndCharCode:
            return GDK_End;
        case kHelpCharCode:
            return GDK_Help;
        case kBackspaceCharCode:
            return GDK_BackSpace;
        case kTabCharCode:
            return GDK_Tab;
        case kLineFeedCharCode:
            return GDK_Linefeed;
        case kPageUpCharCode:
            return GDK_Page_Up;
        case kPageDownCharCode:
            return GDK_Page_Down;
        case kReturnCharCode:
            return GDK_Return;
        case kEscapeCharCode:
            return GDK_Escape;
        case kLeftArrowCharCode:
            return GDK_Left;
        case kRightArrowCharCode:
            return GDK_Right;
        case kUpArrowCharCode:
            return GDK_Up;
        case kDownArrowCharCode:
            return GDK_Down;
        case kDeleteCharCode:
            return GDK_Delete;
        default:
            return GDK_VoidSymbol;
    }
    
    return theKey;
}

guint MacToGTKMods( EventModifiers modifiers )
{
	guint state = 0;
	
	// CMD or CTL
    if( modifiers & cmdKey || modifiers & controlKey || modifiers & rightControlKey )
	   state |= GDK_CONTROL_MASK;
	   
	// The Option key seconds as the ALT key.
	if( modifiers & optionKey || modifiers & rightOptionKey )
		state |= GDK_MOD1_MASK;

	// SHIFT
	if( modifiers & shiftKey || modifiers & rightShiftKey )
		state |= GDK_SHIFT_MASK;
	
	// caps lock
	if( modifiers & alphaLock )
		state |= GDK_LOCK_MASK;
	
	// Mouse button pressed
	if( modifiers & btnState )
		state |= GDK_BUTTON1_MASK;
	
	return state;
}
#if 0
void MacGTK_SendKey(WindowRef focusedWindow, gchar theKey, EventModifiers modifiers, 
        GdkEventType eventType) 
{
    MacGWindRef wRef = NULL;
    wRef = MacGWind_FromNative(focusedWindow);

    if (wRef) {
        GdkEvent event;
        GdkWindow *window = NULL;
        char string[] = "x";
        gchar tempKey = GDK_VoidSymbol;

        tempKey = _SpecialMacKeyToGTKKeySym(theKey);
	
        window = gdk_window_lookup(wRef);

        memset(&event, 0, sizeof(event));

        event.key.type = eventType;
        event.key.window = window;
        event.key.send_event = NULL; //?????
        event.key.time = gdk_time_get();
        event.key.state = MacToGTKMods( modifiers );
        event.key.keyval = ( tempKey == GDK_VoidSymbol ? gdk_keyval_from_name(&theKey) : theKey );
        event.key.length = 1;
		
        if( event.key.state & GDK_SHIFT_MASK )
            string[0] = toupper(theKey);
        else
            string[0] = theKey;

        event.key.string = string;

        MyPostEvent(&event);
    }
}
#endif
void MacGTK_SendKey(WindowRef focusedWindow, gchar theKey,
EventModifiers modifiers,

        GdkEventType eventType)

{

    MacGWindRef wRef = NULL;

    wRef = MacGWind_FromNative(focusedWindow);


    if (wRef) {

        GdkEvent event;

        GdkWindow *window = NULL;

        char string[] = "x";

        guint keyVal = GDK_VoidSymbol;


        keyVal = _SpecialMacKeyToGTKKeySym(theKey);

        if (keyVal == GDK_VoidSymbol) {

            keyVal = theKey;

        }

 

        window = gdk_window_lookup(wRef);


        memset(&event, 0, sizeof(event));


        event.key.type = eventType;

        event.key.window = window;

        event.key.send_event = FALSE;

        event.key.time = gdk_time_get();

        event.key.state = MacToGTKMods( modifiers );

        event.key.keyval = keyVal;

        event.key.length = 1;

   

        if( event.key.state & GDK_SHIFT_MASK )

            string[0] = toupper(theKey);

        else

            string[0] = theKey;


        event.key.string = string;


        MyPostEvent(&event);

    }

}

void MacGTK_SendButtonReleaseEvent(WindowRef macWindPtr, Point where) {
    MacGWindRef macGWin;

    // mouseup events don't work if the button is released too fast ???
    // ==> cf MacGWind_CheckEnter

    macGWin = MacGWind_FromNative(macWindPtr);

    if (macGWin)
        MacGWind_SendButtonRelease( macGWin, where );
}

void MacGTK_WindowResized(WindowRef macWindPtr) {

  MacGWindRef macGWin = NULL;

  macGWin = MacGWind_FromNative(macWindPtr); 

  if (macGWin)
 	 MacGWind_SendConfigure(macGWin);
}

void MacGTK_HandleGoAway(WindowRef macWindPtr) {

  MacGWindRef macGWin = NULL;

  macGWin = MacGWind_FromNative(macWindPtr); 

  if (macGWin)
 	 MacGWind_SendDelete(macGWin);
}


