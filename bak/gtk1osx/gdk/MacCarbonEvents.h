/*
 *  GDK - The GIMP Drawing Kit
 *  MacCarbonEvents.h: Mac native Carbon event handlers and setup
 * 
 *  Copyright (C) 2003 Brian Griffith <bagriffith@alumni.ucsd.edu>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

#ifndef __MAC_CARBON_EVENTS_H__
#define __MAC_CARBON_EVENTS_H__

#include <Carbon/Carbon.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

extern CFRunLoopRef g_mac_main_run_loop;
extern CFRunLoopSourceRef g_mac_process_gdk_event_queue_source;

OSStatus setup_app_carbon_event_handlers ();
OSStatus setup_win_carbon_event_handlers (WindowRef winRef);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MAC_CARBON_EVENTS_H__ */

