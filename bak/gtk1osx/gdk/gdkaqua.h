
#ifndef __GDK_AQUA_H__
#define __GDK_AQUA_H__

#include <glib.h>
#include "gdk/gdk.h"
#include "MacGWind.h"


void gdk_aqua_draw_pushbutton( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus );
void gdk_aqua_draw_checkbox( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus, int selected );
void gdk_aqua_draw_bevelbutton( GdkWindow* window, GdkGC* gc, GdkRectangle *area, int pressed, int focus, int selected );


#endif