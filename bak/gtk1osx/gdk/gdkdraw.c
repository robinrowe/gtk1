/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "config.h"

#include <math.h>
#include <string.h>
#include <gdk/gdk.h>

#include "gdkconfig.h"
#include "gdkprivate.h"

#ifndef M_TWOPI
#define M_TWOPI         (2.0 * 3.14159265358979323846)
#endif


void
gdk_draw_point (GdkDrawable *drawable,
                GdkGC       *gc,
                gint         x,
                gint         y)
{
	//!!!MACOS_TO_DO!!!
}

void
gdk_draw_line (GdkDrawable *drawable,
	       GdkGC       *gc,
	       gint         x1,
	       gint         y1,
	       gint         x2,
	       gint         y2)
{

#if 1
  GdkWindowPrivate *drawable_private;
  GdkGCPrivate *gc_private;
  MacGWindRef macgwin;
  
  g_return_if_fail (drawable != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  macgwin = drawable_private->macgwin;
  gdk_gc_predraw (macgwin, gc_private);
    
  MoveTo(x1, y1);
  MacLineTo(x2, y2);
  
  gdk_gc_postdraw (macgwin, gc_private);
#endif
}


void
gdk_draw_rectangle (GdkDrawable *drawable,
		    GdkGC       *gc,
		    gint         filled,
		    gint         x,
		    gint         y,
		    gint         width,
		    gint         height)
{
#if 1
  GdkWindowPrivate *drawable_private;
  GdkGCPrivate *gc_private;
  //GWorldPtr macgwin = NULL;
  MacGWindRef macgwin;	//justin
  Rect rect;
  
  MacSetRect(&rect, x, y, x+width, y+height);
  
  g_return_if_fail (drawable != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  if (width == -1)
    width = drawable_private->width;
  if (height == -1)
    height = drawable_private->height;

  macgwin = drawable_private->macgwin;
  gdk_gc_predraw (macgwin, gc_private);

  {
      if (filled)
          PaintRect(&rect);
      else
          FrameRect(&rect);
  }
 
  gdk_gc_postdraw (macgwin, gc_private);
#endif
}

void
gdk_draw_arc (GdkDrawable *drawable,
	      GdkGC       *gc,
	      gint         filled,
	      gint         x,
	      gint         y,
	      gint         width,
	      gint         height,
	      gint         angle1,
	      gint         angle2)
{
  GdkWindowPrivate *drawable_private;
  GdkGCPrivate *gc_private;
  //GWorldPtr macgwin = NULL;
  MacGWindRef macgwin;	//justin
  Rect rect;
  short startAngle, arcAngle;
  
  MacSetRect(&rect, x, y, x+width, y+height);
  
  g_return_if_fail (drawable != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  if (width == -1)
    width = drawable_private->width;
  if (height == -1)
    height = drawable_private->height;

  macgwin = drawable_private->macgwin;
  gdk_gc_predraw (macgwin, gc_private);


  startAngle = angle1; // ????
  arcAngle = angle2 - angle1; // ????
  
  if (filled)
        PaintArc(&rect, startAngle, arcAngle);
  else
        FrameArc(&rect, startAngle, arcAngle);


  gdk_gc_postdraw (macgwin, gc_private);

}

void
gdk_draw_polygon (GdkDrawable *drawable,
		  GdkGC       *gc,
		  gint         filled,
		  GdkPoint    *points,
		  gint         npoints)
{
//!!!MACOS_TO_DO!!!
}


/* gdk_draw_string
 */
void
gdk_draw_string (GdkDrawable *drawable,
		 GdkFont     *font,
		 GdkGC       *gc,
		 gint         x,
		 gint         y,
		 const gchar *string)
{
  gdk_draw_text (drawable, font, gc, x, y, string, strlen (string));
}

/* gdk_draw_text
 *
 * Modified by Li-Da Lho to draw 16 bits and Multibyte strings
 *
 * Interface changed: add "GdkFont *font" to specify font or fontset explicitely
 */
void
gdk_draw_text (GdkDrawable *drawable,
	       GdkFont     *font,
	       GdkGC       *gc,
	       gint         x,
	       gint         y,
	       const gchar *text,
	       gint         text_length)
{

#if 1
  GdkWindowPrivate *drawable_private;
  GdkGCPrivate *gc_private;

  g_return_if_fail (drawable != NULL);
  g_return_if_fail (font != NULL);
  g_return_if_fail (gc != NULL);
  g_return_if_fail (text != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;

  gc_private = (GdkGCPrivate*) gc;

  if (font->type == GDK_FONT_FONT)
    {
      gdk_gc_set_font(gc, font);
      gdk_gc_predraw (drawable_private->macgwin, gc_private);

	    MoveTo(x,y);
	    //TextSize(12);
	    //TextFont(1);

	    MacDrawText(text, 0, text_length);
	  	
      gdk_gc_postdraw (drawable_private->macgwin, gc_private);
    }
  else
    g_error ("undefined font type");
#endif
}


void
gdk_draw_text_wc (GdkDrawable	 *drawable,
		  GdkFont	 *font,
		  GdkGC		 *gc,
		  gint		  x,
		  gint		  y,
		  const GdkWChar *text,
		  gint		  text_length)
{
   gchar *string;

   string = gdk_wcstombs(text);

   gdk_draw_text(drawable, font, gc, x, y, string, strlen(string)); 
}

void
gdk_draw_pixmap (GdkDrawable *drawable,
		 GdkGC       *gc,
		 GdkPixmap   *src,
		 gint         xsrc,
		 gint         ysrc,
		 gint         xdest,
		 gint         ydest,
		 gint         width,
		 gint         height)
{
  // cf gdk_window_copy_area

  GdkWindowPrivate *drawable_private;
  GdkPixmapPrivate *src_private;
  GdkGCPrivate *gc_private;
  Rect src_rect, dst_rect;
  BitMap* src_bm = NULL;
  BitMap* dst_bm = NULL;
  
  g_return_if_fail (drawable != NULL);
  g_return_if_fail (src != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  src_private = (GdkPixmapPrivate*) src;
  if (drawable_private->destroyed || src_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  if (width == -1)
    width = src_private->width;
  if (height == -1)
    height = src_private->height;

  gdk_gc_predraw (drawable_private->macgwin, gc_private);
  
  MacSetRect(&src_rect, xsrc, ysrc, xsrc+width, ysrc+height);
  MacSetRect(&dst_rect, xdest, ydest, xdest+width, ydest+height);

  src_bm = MacGWind_GetMacBitmap(src_private->macgwin);
  dst_bm = MacGWind_GetMacBitmap(drawable_private->macgwin);
 
  //LockPixels(src_bm); //???? - justin
  //LockPixels(dst_bm);  

  ForeColor(blackColor);
  BackColor(whiteColor);
  CopyBits(src_bm, dst_bm, &src_rect, &dst_rect, srcCopy, NULL);
  
  //UnlockPixels(src_bm);
  //UnlockPixels(dst_bm);


  gdk_gc_postdraw (drawable_private->macgwin, gc_private);

}


void
gdk_draw_image (GdkDrawable *drawable,
		GdkGC       *gc,
		GdkImage    *image,
		gint         xsrc,
		gint         ysrc,
		gint         xdest,
		gint         ydest,
		gint         width,
		gint         height)
{
//!!!MACOS_TO_DO!!!
#if 1
  GdkImagePrivate *image_private;

  g_return_if_fail (drawable != NULL);
  g_return_if_fail (image != NULL);
  g_return_if_fail (gc != NULL);

  image_private = (GdkImagePrivate*) image;

  g_return_if_fail (image_private->image_put != NULL);

  if (width == -1)
    width = image->width;
  if (height == -1)
    height = image->height;

  (* image_private->image_put) (drawable, gc, image, xsrc, ysrc,
				xdest, ydest, width, height);
#endif
}

void
gdk_draw_points (GdkDrawable *drawable,
		 GdkGC       *gc,
		 GdkPoint    *points,
		 gint         npoints)
{
#if 1
  GdkWindowPrivate *drawable_private;
  GdkGCPrivate *gc_private;
  MacGWindRef macgwin;
  GdkPoint* p = NULL;
  int i;
  
  g_return_if_fail (drawable != NULL);
  g_return_if_fail (gc != NULL);

  drawable_private = (GdkWindowPrivate*) drawable;
  if (drawable_private->destroyed)
    return;
  gc_private = (GdkGCPrivate*) gc;

  macgwin = drawable_private->macgwin;
  gdk_gc_predraw (macgwin, gc_private);
    
  for (i=0; i<npoints; i++) {
    p = &points[i];
    MoveTo(p->x, p->y);
    MacLineTo(p->x, p->y);
  }
    
  gdk_gc_postdraw (macgwin, gc_private);

#endif
}

void
gdk_draw_segments (GdkDrawable *drawable,
		   GdkGC       *gc,
		   GdkSegment  *segs,
		   gint         nsegs)
{
//!!!MACOS_TO_DO!!!
}

void
gdk_draw_lines (GdkDrawable *drawable,
		GdkGC       *gc,
		GdkPoint    *points,
		gint         npoints)
{
 //!!!MACOS_TO_DO!!!
}
