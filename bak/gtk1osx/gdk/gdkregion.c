/* GDK - The GIMP Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * MacOS Port Copyright (c) 2000 Arnaud Masson
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

/* #include <Region.h> */
/* #include <Rect.h> */

#include "gdk/gdk.h"
#include "gdkprivate.h"

GdkRegion*
gdk_region_new (void)
{
#if 1
  GdkRegionPrivate *priv;
  GdkRegion *region;

  priv = g_new (GdkRegionPrivate, 1);
  region = (GdkRegion*) priv;
  region->user_data = NULL;

  priv->macrgn = NewRgn();

  return region;
#endif
}

void
gdk_region_destroy (GdkRegion *region)
{
#if 1
  GdkRegionPrivate *priv;

  g_return_if_fail (region != NULL);

  priv = (GdkRegionPrivate *) region;

  DisposeRgn(priv->macrgn);

  g_free (priv);
#endif
}

gboolean
gdk_region_empty (GdkRegion      *region)
{ 
#if 1
  GdkRegionPrivate *priv;

  g_return_val_if_fail (region != NULL, 0);

  priv = (GdkRegionPrivate *) region;
  
  return EmptyRgn(priv->macrgn);
#endif
}

gboolean
gdk_region_equal (GdkRegion      *region1,
                  GdkRegion      *region2)
{
#if 1
  GdkRegionPrivate *private1;
  GdkRegionPrivate *private2;

  g_return_val_if_fail (region1 != NULL, 0);
  g_return_val_if_fail (region2 != NULL, 0);

  private1 = (GdkRegionPrivate *) region1;
  private2 = (GdkRegionPrivate *) region2;

  return EqualRgn(private1->macrgn, private2->macrgn);
#endif
}

void
gdk_region_get_clipbox(GdkRegion    *region,
		       GdkRectangle *rect)
{
  /*sebi 2003-01-03*/
  
  GdkRegionPrivate *private_rgn;
  Rect macrect;

  g_return_if_fail (region != NULL);
  g_return_if_fail (rect != NULL);

  private_rgn = (GdkRegionPrivate *) region;
  
  GetRegionBounds(private_rgn->macrgn, &macrect);
 
  /*stuff new values into the gdk rect*/
  rect->x = macrect.left;
  rect->y = macrect.top;
  rect->width = macrect.right - macrect.left;
  rect->height = macrect.bottom - macrect.top;
}

gboolean
gdk_region_point_in (GdkRegion      *region,
                     gint           x,
		     gint           y)
{
#if 1
  GdkRegionPrivate *priv;
  Point pt;

  g_return_val_if_fail (region != NULL, 0);

  priv = (GdkRegionPrivate *) region;
  
  pt.h = x;
  pt.v = y;
  
  return PtInRgn(pt, priv->macrgn);
 #endif
}

GdkOverlapType
gdk_region_rect_in (GdkRegion      *region,
                    GdkRectangle   *rect)
{
  /*sebi 2003-01-03*/
  
  /*!!!if the region is completely inside the rectangle it will return GDK_OVERLAP_RECTANGLE_PART. correct?!!!*/
	
  GdkRegionPrivate *private_rgn;
  GdkRegion *tmp_rect_rgn;
  GdkRegion *tmp_intersect_rgn;
  GdkOverlapType res;

  g_return_val_if_fail (region != NULL, 0);
  g_return_val_if_fail (rect != NULL, 0);

  private_rgn = (GdkRegionPrivate *) region;
  
  /*set up a rectangular region*/
  tmp_rect_rgn = gdk_region_rect(rect);

  /*get the intersection*/
  tmp_intersect_rgn = gdk_regions_intersect(tmp_rect_rgn, region);
  
  if(gdk_region_empty(tmp_intersect_rgn))
    res = GDK_OVERLAP_RECTANGLE_OUT;
  else if (gdk_region_equal(tmp_intersect_rgn, tmp_rect_rgn))
    res = GDK_OVERLAP_RECTANGLE_IN;
  else
    res = GDK_OVERLAP_RECTANGLE_PART;
  
  /*clean up*/
  gdk_region_destroy(tmp_rect_rgn);
  gdk_region_destroy(tmp_intersect_rgn);
  
  return res;
}

/* sebi 2003-01-03 */
/* !!! this is not in the original source, but in the to do list for gtk-osx-0.2a.
i assume it is supposed to create a new region from the given rect. !!! */
GdkRegion*
gdk_region_rect   (GdkRectangle  *rect) {

  GdkRegionPrivate *priv;
  GdkRegion *region;

  priv = g_new (GdkRegionPrivate, 1);
  region = (GdkRegion*) priv;
  region->user_data = NULL;

  priv->macrgn = NewRgn();
  
  SetRectRgn (priv->macrgn, 
    rect->x, rect->y, 
    rect->x + rect->width, 
    rect->y + rect->height
  );

  return region;
}
				    
GdkRegion *
gdk_region_polygon (GdkPoint    *points,
		    gint         npoints,
		    GdkFillRule  fill_rule)
{
  /* sebi 2003-01-03 */
  
  GdkRegionPrivate *priv;
  GdkRegion *region;
  PolyHandle macpoly;
  int i; /*lopcounter;*/
  
  g_return_val_if_fail (points != NULL,(GdkRegion *) NULL);
  g_return_val_if_fail (npoints >= 3, (GdkRegion *)NULL); 
  

  priv = g_new (GdkRegionPrivate, 1);
  region = (GdkRegion*) priv;
  region->user_data = NULL;
  
  /*set up a mac polygon structure to be filled in the next step*/
  macpoly = OpenPoly();
  MoveTo(points[0].x, points[0].y);
  for(i = 1; i < npoints; ++i) {
    LineTo(points[i].x, points[i].y);
  }
  LineTo(points[0].x, points[0].y);
  ClosePoly();
  
  /*create the new region*/
  priv->macrgn = NewRgn();
  
  /*prepare for drawing*/
  OpenRgn();
  
  switch (fill_rule) {
    case GDK_EVEN_ODD_RULE:
      FramePoly(macpoly);
      break;

    case GDK_WINDING_RULE:
		g_warning("gdk_region_polygon: not correctly implemented for GDK_WINDING_RULE on MacOS!");
		FramePoly(macpoly);
      break;
  }
  CloseRgn(priv->macrgn); 
  
  /*dispose of the polygon*/
  KillPoly(macpoly);

  return region;
}

void          
gdk_region_offset (GdkRegion      *region,
                   gint           dx,
		   gint           dy)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private_rgn;
  g_return_if_fail (region != NULL);
  private_rgn = (GdkRegionPrivate *) region;
  OffsetRgn(private_rgn->macrgn, dx, dy);
}

void
gdk_region_shrink (GdkRegion      *region,
                   gint           dx,
		   gint           dy)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private_rgn;
  g_return_if_fail (region != NULL);
  private_rgn = (GdkRegionPrivate *) region;
  InsetRgn(private_rgn->macrgn, dx, dy);
}

GdkRegion*    
gdk_region_union_with_rect (GdkRegion      *region,
                            GdkRectangle   *rect)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private_rgn;
  GdkRegion* res; 
  GdkRegion* tmp_rect_region; 
  
  g_return_val_if_fail (region != NULL, 0);
  g_return_val_if_fail (rect != NULL, 0);

  private_rgn = (GdkRegionPrivate *) region;
  
  tmp_rect_region = gdk_region_rect(rect);
  res = gdk_regions_union(region, tmp_rect_region);
  
  gdk_region_destroy(tmp_rect_region);
  
  return res;
}


GdkRegion*    
gdk_regions_intersect (GdkRegion      *source1,
                       GdkRegion      *source2)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private1;
  GdkRegionPrivate *private2;
  GdkRegion *res;
  GdkRegionPrivate *res_private;

  g_return_val_if_fail (source1 != NULL, (GdkRegion *)NULL);
  g_return_val_if_fail (source2 != NULL, (GdkRegion *)NULL);

  private1 = (GdkRegionPrivate *) source1;
  private2 = (GdkRegionPrivate *) source2;

  res = gdk_region_new ();
  res_private = (GdkRegionPrivate *) res;
  
  SectRgn (
    private1->macrgn, 
    private2->macrgn, 
    res_private->macrgn
  );
  
  return res;
}

GdkRegion* 
gdk_regions_union (GdkRegion      *source1,
                   GdkRegion      *source2)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private1;
  GdkRegionPrivate *private2;
  GdkRegion *res;
  GdkRegionPrivate *res_private;

  g_return_val_if_fail (source1 != NULL, (GdkRegion *)NULL);
  g_return_val_if_fail (source2 != NULL, (GdkRegion *)NULL);

  private1 = (GdkRegionPrivate *) source1;
  private2 = (GdkRegionPrivate *) source2;

  res = gdk_region_new ();
  res_private = (GdkRegionPrivate *) res;
  
  UnionRgn (
    private1->macrgn, 
    private2->macrgn, 
    res_private->macrgn
  );
  
  return res;
}

GdkRegion*    
gdk_regions_subtract (GdkRegion      *source1,
                      GdkRegion      *source2)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private1;
  GdkRegionPrivate *private2;
  GdkRegion *res;
  GdkRegionPrivate *res_private;

  g_return_val_if_fail (source1 != NULL, (GdkRegion *)NULL);
  g_return_val_if_fail (source2 != NULL, (GdkRegion *)NULL);

  private1 = (GdkRegionPrivate *) source1;
  private2 = (GdkRegionPrivate *) source2;

  res = gdk_region_new ();
  res_private = (GdkRegionPrivate *) res;
  
  DiffRgn (
    private1->macrgn, 
    private2->macrgn, 
    res_private->macrgn
  );
  
  return res;
}

GdkRegion*    
gdk_regions_xor(GdkRegion      *source1,
                 GdkRegion      *source2)
{
  /* sebi 2003-01-03 */
  GdkRegionPrivate *private1;
  GdkRegionPrivate *private2;
  GdkRegion *res;
  GdkRegionPrivate *res_private;

  g_return_val_if_fail (source1 != NULL, (GdkRegion *)NULL);
  g_return_val_if_fail (source2 != NULL, (GdkRegion *)NULL);

  private1 = (GdkRegionPrivate *) source1;
  private2 = (GdkRegionPrivate *) source2;

  res = gdk_region_new ();
  res_private = (GdkRegionPrivate *) res;
  
  XorRgn (
    private1->macrgn, 
    private2->macrgn, 
    res_private->macrgn
  );
  
  return res;
}


