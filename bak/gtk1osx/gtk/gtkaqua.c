/*
 *  gtkaqua.c:	Aqua appearance style class functions
 * 
 *  Written by:		Andreas Varga
 *		
 *  Copyright (C) 2003 Andreas Varga <sid@galway.c64.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *  Boston, MA 02111-1307, USA.
 */

/*
 *  $Date: 2003/12/27 22:33:36 $
 *  $Author: awreynolds $
 *  $Revision: 1.2 $
 *  $State: Exp $
 */
#include <math.h>
#include "gtkgc.h"
#include "gtkrc.h"
#include "gtkstyle.h"
#include "gtkthemes.h"
#include "gtkwidget.h"
#include "gtkthemes.h"
#include "gdkprivate.h"
#include "gdkaqua.h"


static void    gtk_aqua_draw_hline(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       gint x1,
			       gint x2,
			       gint y);
static void    gtk_aqua_draw_vline(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       gint y1,
			       gint y2,
			       gint x);
static void    gtk_aqua_draw_shadow(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GtkShadowType shadow_type,
				GdkRectangle * area,
				GtkWidget * widget,
				gchar * detail,
				gint x,
				gint y,
				gint width,
				gint height);

static void    gtk_aqua_draw_polygon(GtkStyle * style,
				 GdkWindow * window,
				 GtkStateType state_type,
				 GtkShadowType shadow_type,
				 GdkRectangle * area,
				 GtkWidget * widget,
				 gchar * detail,
				 GdkPoint * point,
				 gint npoints,
				 gint fill);
static void    gtk_aqua_draw_arrow(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GtkShadowType shadow_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       GtkArrowType arrow_type,
			       gint fill,
			       gint x,
			       gint y,
			       gint width,
			       gint height);
static void    gtk_aqua_draw_diamond(GtkStyle * style,
				 GdkWindow * window,
				 GtkStateType state_type,
				 GtkShadowType shadow_type,
				 GdkRectangle * area,
				 GtkWidget * widget,
				 gchar * detail,
				 gint x,
				 gint y,
				 gint width,
				 gint height);
static void    gtk_aqua_draw_oval(GtkStyle * style,
			      GdkWindow * window,
			      GtkStateType state_type,
			      GtkShadowType shadow_type,
			      GdkRectangle * area,
			      GtkWidget * widget,
			      gchar * detail,
			      gint x,
			      gint y,
			      gint width,
			      gint height);
static void    gtk_aqua_draw_string(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GdkRectangle * area,
				GtkWidget * widget,
				gchar * detail,
				gint x,
				gint y,
				const gchar * string);
static void    gtk_aqua_draw_box(GtkStyle * style,
			     GdkWindow * window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle * area,
			     GtkWidget * widget,
			     gchar * detail,
			     gint x,
			     gint y,
			     gint width,
			     gint height);
static void    gtk_aqua_draw_flat_box(GtkStyle * style,
				  GdkWindow * window,
				  GtkStateType state_type,
				  GtkShadowType shadow_type,
				  GdkRectangle * area,
				  GtkWidget * widget,
				  gchar * detail,
				  gint x,
				  gint y,
				  gint width,
				  gint height);
static void    gtk_aqua_draw_check(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GtkShadowType shadow_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       gint x,
			       gint y,
			       gint width,
			       gint height);
static void    gtk_aqua_draw_option(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GtkShadowType shadow_type,
				GdkRectangle * area,
				GtkWidget * widget,
				gchar * detail,
				gint x,
				gint y,
				gint width,
				gint height);
static void    gtk_aqua_draw_cross(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GtkShadowType shadow_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       gint x,
			       gint y,
			       gint width,
			       gint height);
static void    gtk_aqua_draw_ramp(GtkStyle * style,
			      GdkWindow * window,
			      GtkStateType state_type,
			      GtkShadowType shadow_type,
			      GdkRectangle * area,
			      GtkWidget * widget,
			      gchar * detail,
			      GtkArrowType arrow_type,
			      gint x,
			      gint y,
			      gint width,
			      gint height);
static void    gtk_aqua_draw_tab(GtkStyle * style,
			     GdkWindow * window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle * area,
			     GtkWidget * widget,
			     gchar * detail,
			     gint x,
			     gint y,
			     gint width,
			     gint height);
static void    gtk_aqua_draw_shadow_gap(GtkStyle * style,
				    GdkWindow * window,
				    GtkStateType state_type,
				    GtkShadowType shadow_type,
				    GdkRectangle * area,
				    GtkWidget * widget,
				    gchar * detail,
				    gint x,
				    gint y,
				    gint width,
				    gint height,
				    GtkPositionType gap_side,
				    gint gap_x,
				    gint gap_width);
static void    gtk_aqua_draw_box_gap(GtkStyle * style,
				 GdkWindow * window,
				 GtkStateType state_type,
				 GtkShadowType shadow_type,
				 GdkRectangle * area,
				 GtkWidget * widget,
				 gchar * detail,
				 gint x,
				 gint y,
				 gint width,
				 gint height,
				 GtkPositionType gap_side,
				 gint gap_x,
				 gint gap_width);
static void    gtk_aqua_draw_extension(GtkStyle * style,
				   GdkWindow * window,
				   GtkStateType state_type,
				   GtkShadowType shadow_type,
				   GdkRectangle * area,
				   GtkWidget * widget,
				   gchar * detail,
				   gint x,
				   gint y,
				   gint width,
				   gint height,
				   GtkPositionType gap_side);
static void    gtk_aqua_draw_focus(GtkStyle * style,
			       GdkWindow * window,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       gchar * detail,
			       gint x,
			       gint y,
			       gint width,
			       gint height);
static void    gtk_aqua_draw_slider(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GtkShadowType shadow_type,
				GdkRectangle * area,
				GtkWidget * widget,
				gchar * detail,
				gint x,
				gint y,
				gint width,
				gint height,
				GtkOrientation orientation);
static void    gtk_aqua_draw_handle(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GtkShadowType shadow_type,
				GdkRectangle * area,
				GtkWidget * widget,
				gchar * detail,
				gint x,
				gint y,
				gint width,
				gint height,
				GtkOrientation orientation);


GtkStyleClass gtk_aqua_class =
{
  2,
  2,
  gtk_aqua_draw_hline,
  gtk_aqua_draw_vline,
  gtk_aqua_draw_shadow,
  gtk_aqua_draw_polygon,
  gtk_aqua_draw_arrow,
  gtk_aqua_draw_diamond,
  gtk_aqua_draw_oval,
  gtk_aqua_draw_string,
  gtk_aqua_draw_box,
  gtk_aqua_draw_flat_box,
  gtk_aqua_draw_check,
  gtk_aqua_draw_option,
  gtk_aqua_draw_cross,
  gtk_aqua_draw_ramp,
  gtk_aqua_draw_tab,
  gtk_aqua_draw_shadow_gap,
  gtk_aqua_draw_box_gap,
  gtk_aqua_draw_extension,
  gtk_aqua_draw_focus,
  gtk_aqua_draw_slider,
  gtk_aqua_draw_handle
};

// reference default class for interim drawing
extern GtkStyleClass default_class;


static void
gtk_aqua_draw_hline (GtkStyle     *style,
                        GdkWindow    *window,
                        GtkStateType  state_type,
                        GdkRectangle  *area,
                        GtkWidget     *widget,
                        gchar         *detail,
                        gint          x1,
                        gint          x2,
                        gint          y)
{
  gint thickness_light;
  gint thickness_dark;
  gint i;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  thickness_light = style->klass->ythickness / 2;
  thickness_dark = style->klass->ythickness - thickness_light;
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], area);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], area);
    }
  
  if (detail && !strcmp (detail, "label"))
    {
      if (state_type == GTK_STATE_INSENSITIVE)
        gdk_draw_line (window, style->white_gc, x1 + 1, y + 1, x2 + 1, y + 1);   
      gdk_draw_line (window, style->fg_gc[state_type], x1, y, x2, y);     
    }
  else
    {
      for (i = 0; i < thickness_dark; i++)
        {
          gdk_draw_line (window, style->light_gc[state_type], x2 - i - 1, y + i, x2, y + i);
          gdk_draw_line (window, style->dark_gc[state_type], x1, y + i, x2 - i - 1, y + i);
        }
      
      y += thickness_dark;
      for (i = 0; i < thickness_light; i++)
        {
          gdk_draw_line (window, style->dark_gc[state_type], x1, y + i, x1 + thickness_light - i - 1, y + i);
          gdk_draw_line (window, style->light_gc[state_type], x1 + thickness_light - i - 1, y + i, x2, y + i);
        }
    }
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], NULL);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], NULL);
    }
}


static void
gtk_aqua_draw_vline (GtkStyle     *style,
                        GdkWindow    *window,
                        GtkStateType  state_type,
                        GdkRectangle  *area,
                        GtkWidget     *widget,
                        gchar         *detail,
                        gint          y1,
                        gint          y2,
                        gint          x)
{
  gint thickness_light;
  gint thickness_dark;
  gint i;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  thickness_light = style->klass->xthickness / 2;
  thickness_dark = style->klass->xthickness - thickness_light;
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], area);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], area);
    }
  for (i = 0; i < thickness_dark; i++)
    {
      gdk_draw_line (window, style->light_gc[state_type], x + i, y2 - i - 1, x + i, y2);
      gdk_draw_line (window, style->dark_gc[state_type], x + i, y1, x + i, y2 - i - 1);
    }
  
  x += thickness_dark;
  for (i = 0; i < thickness_light; i++)
    {
      gdk_draw_line (window, style->dark_gc[state_type], x + i, y1, x + i, y1 + thickness_light - i);
      gdk_draw_line (window, style->light_gc[state_type], x + i, y1 + thickness_light - i, x + i, y2);
    }
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], NULL);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], NULL);
    }
}


static void
gtk_aqua_draw_shadow (GtkStyle      *style,
                         GdkWindow     *window,
                         GtkStateType   state_type,
                         GtkShadowType  shadow_type,
                         GdkRectangle  *area,
                         GtkWidget     *widget,
                         gchar         *detail,
                         gint           x,
                         gint           y,
                         gint           width,
                         gint           height)
{
  GdkGC *gc1 = NULL;
  GdkGC *gc2 = NULL;
  gint thickness_light;
  gint thickness_dark;
  gint i;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if ((width == -1) && (height == -1))
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
      return;
    case GTK_SHADOW_IN:
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      break;
    case GTK_SHADOW_OUT:
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      break;
    }
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      if (shadow_type == GTK_SHADOW_IN || 
          shadow_type == GTK_SHADOW_OUT)
        {
          gdk_gc_set_clip_rectangle (style->black_gc, area);
          gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);
        }
    }
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
      break;
      
    case GTK_SHADOW_IN:
      gdk_draw_line (window, gc1,
                     x, y + height - 1, x + width - 1, y + height - 1);
      gdk_draw_line (window, gc1,
                     x + width - 1, y, x + width - 1, y + height - 1);
      
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + 1, y + height - 2, x + width - 2, y + height - 2);
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + width - 2, y + 1, x + width - 2, y + height - 2);
      
      gdk_draw_line (window, style->black_gc,
                     x + 1, y + 1, x + width - 2, y + 1);
      gdk_draw_line (window, style->black_gc,
                     x + 1, y + 1, x + 1, y + height - 2);
      
      gdk_draw_line (window, gc2,
                     x, y, x + width - 1, y);
      gdk_draw_line (window, gc2,
                     x, y, x, y + height - 1);
      break;
      
    case GTK_SHADOW_OUT:
      gdk_draw_line (window, gc1,
                     x + 1, y + height - 2, x + width - 2, y + height - 2);
      gdk_draw_line (window, gc1,
                     x + width - 2, y + 1, x + width - 2, y + height - 2);
      
      gdk_draw_line (window, gc2,
                     x, y, x + width - 1, y);
      gdk_draw_line (window, gc2,
                     x, y, x, y + height - 1);
      
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + 1, y + 1, x + width - 2, y + 1);
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + 1, y + 1, x + 1, y + height - 2);
      
      gdk_draw_line (window, style->black_gc,
                     x, y + height - 1, x + width - 1, y + height - 1);
      gdk_draw_line (window, style->black_gc,
                     x + width - 1, y, x + width - 1, y + height - 1);
      break;
      
    case GTK_SHADOW_ETCHED_IN:
    case GTK_SHADOW_ETCHED_OUT:
      thickness_light = 1;
      thickness_dark = 1;
      
      for (i = 0; i < thickness_dark; i++)
        {
          gdk_draw_line (window, gc1,
                         x + i,
                         y + height - i - 1,
                         x + width - i - 1,
                         y + height - i - 1);
          gdk_draw_line (window, gc1,
                         x + width - i - 1,
                         y + i,
                         x + width - i - 1,
                         y + height - i - 1);
          
          gdk_draw_line (window, gc2,
                         x + i,
                         y + i,
                         x + width - i - 2,
                         y + i);
          gdk_draw_line (window, gc2,
                         x + i,
                         y + i,
                         x + i,
                         y + height - i - 2);
        }
      
      for (i = 0; i < thickness_light; i++)
        {
          gdk_draw_line (window, gc1,
                         x + thickness_dark + i,
                         y + thickness_dark + i,
                         x + width - thickness_dark - i - 1,
                         y + thickness_dark + i);
          gdk_draw_line (window, gc1,
                         x + thickness_dark + i,
                         y + thickness_dark + i,
                         x + thickness_dark + i,
                         y + height - thickness_dark - i - 1);
          
          gdk_draw_line (window, gc2,
                         x + thickness_dark + i,
                         y + height - thickness_light - i - 1,
                         x + width - thickness_light - 1,
                         y + height - thickness_light - i - 1);
          gdk_draw_line (window, gc2,
                         x + width - thickness_light - i - 1,
                         y + thickness_dark + i,
                         x + width - thickness_light - i - 1,
                         y + height - thickness_light - 1);
        }
      break;
    }
  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      if (shadow_type == GTK_SHADOW_IN || 
          shadow_type == GTK_SHADOW_OUT)
        {
          gdk_gc_set_clip_rectangle (style->black_gc, NULL);
          gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
        }
    }
}

static void
gtk_aqua_draw_polygon (GtkStyle      *style,
                          GdkWindow     *window,
                          GtkStateType   state_type,
                          GtkShadowType  shadow_type,
                          GdkRectangle  *area,
                          GtkWidget     *widget,
                          gchar         *detail,
                          GdkPoint      *points,
                          gint           npoints,
                          gboolean       fill)
{
  static const gdouble pi_over_4 = M_PI_4;
  static const gdouble pi_3_over_4 = M_PI_4 * 3;
  GdkGC *gc1;
  GdkGC *gc2;
  GdkGC *gc3;
  GdkGC *gc4;
  gdouble angle;
  gint xadjust;
  gint yadjust;
  gint i;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  g_return_if_fail (points != NULL);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_IN:
      gc1 = style->bg_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->black_gc;
      break;
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_OUT:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->black_gc;
      gc4 = style->bg_gc[state_type];
      break;
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->dark_gc[state_type];
      break;
    default:
      return;
    }
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      gdk_gc_set_clip_rectangle (gc3, area);
      gdk_gc_set_clip_rectangle (gc4, area);
    }
  
  if (fill)
    gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, npoints);
  
  npoints--;
  
  for (i = 0; i < npoints; i++)
    {
      if ((points[i].x == points[i+1].x) &&
          (points[i].y == points[i+1].y))
        {
          angle = 0;
        }
      else
        {
          angle = atan2 (points[i+1].y - points[i].y,
                         points[i+1].x - points[i].x);
        }
      
      if ((angle > -pi_3_over_4) && (angle < pi_over_4))
        {
          if (angle > -pi_over_4)
            {
              xadjust = 0;
              yadjust = 1;
            }
          else
            {
              xadjust = 1;
              yadjust = 0;
            }
          
          gdk_draw_line (window, gc1,
                         points[i].x-xadjust, points[i].y-yadjust,
                         points[i+1].x-xadjust, points[i+1].y-yadjust);
          gdk_draw_line (window, gc3,
                         points[i].x, points[i].y,
                         points[i+1].x, points[i+1].y);
        }
      else
        {
          if ((angle < -pi_3_over_4) || (angle > pi_3_over_4))
            {
              xadjust = 0;
              yadjust = 1;
            }
          else
            {
              xadjust = 1;
              yadjust = 0;
            }
          
          gdk_draw_line (window, gc4,
                         points[i].x+xadjust, points[i].y+yadjust,
                         points[i+1].x+xadjust, points[i+1].y+yadjust);
          gdk_draw_line (window, gc2,
                         points[i].x, points[i].y,
                         points[i+1].x, points[i+1].y);
        }
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      gdk_gc_set_clip_rectangle (gc3, NULL);
      gdk_gc_set_clip_rectangle (gc4, NULL);
    }
}

static void
gtk_aqua_draw_arrow (GtkStyle      *style,
                        GdkWindow     *window,
                        GtkStateType   state_type,
                        GtkShadowType  shadow_type,
                        GdkRectangle  *area,
                        GtkWidget     *widget,
                        gchar         *detail,
                        GtkArrowType   arrow_type,
                        gboolean       fill,
                        gint           x,
                        gint           y,
                        gint           width,
                        gint           height)
{
  GdkGC *gc1;
  GdkGC *gc2;
  GdkGC *gc3;
  GdkGC *gc4;
  gint half_width;
  gint half_height;
  GdkPoint points[3];
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_IN:
      gc1 = style->bg_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->black_gc;
      break;
    case GTK_SHADOW_OUT:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->black_gc;
      gc4 = style->bg_gc[state_type];
      break;
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = NULL;
      gc4 = NULL;
      break;
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = NULL;
      gc4 = NULL;
      break;
    default:
      return;
    }
  
  if ((width == -1) && (height == -1))
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  half_width = width / 2;
  half_height = height / 2;
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      if ((gc3) && (gc4))
        {
          gdk_gc_set_clip_rectangle (gc3, area);
          gdk_gc_set_clip_rectangle (gc4, area);
        }
    }
  
  switch (arrow_type)
    {
    case GTK_ARROW_UP:
      if (fill)
        {
          points[0].x = x + half_width;
          points[0].y = y;
          points[1].x = x;
          points[1].y = y + height - 1;
          points[2].x = x + width - 1;
          points[2].y = y + height - 1;
          
          gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, 3);
        }
      
      switch (shadow_type)
        {
        case GTK_SHADOW_IN:
        case GTK_SHADOW_OUT:
          
          gdk_draw_line (window, gc1,
                         x + 1, y + height - 2,
                         x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + 0, y + height - 1,
                         x + width - 1, y + height - 1);
          
          gdk_draw_line (window, gc1,
                         x + width - 2, y + height - 1,
                         x + half_width, y + 1);
          gdk_draw_line (window, gc3,
                         x + width - 1, y + height - 1,
                         x + half_width, y);
          
          gdk_draw_line (window, gc4,
                         x + half_width, y + 1,
                         x + 1, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + half_width, y,
                         x, y + height - 1);
          break;
          
        case GTK_SHADOW_ETCHED_IN:
        case GTK_SHADOW_ETCHED_OUT:
          gdk_draw_line (window, gc1,
                         x + half_width, y + 1,
                         x + 1, y + height - 1);
          gdk_draw_line (window, gc1,
                         x + 1, y + height - 1,
                         x + width - 1, y + height - 1);
          gdk_draw_line (window, gc1,
                         x + width - 1, y + height - 1,
                         x + half_width + 1, y + 1);
          
          points[0].x = x + half_width;
          points[0].y = y;
          points[1].x = x;
          points[1].y = y + height - 2;
          points[2].x = x + width - 2;
          points[2].y = y + height - 2;
          
          gdk_draw_polygon (window, gc2, FALSE, points, 3);
          break;
          
        default:
          break;
        }
      break;
      
    case GTK_ARROW_DOWN:
      if (fill)
        {
          points[0].x = x + width - 1;
          points[0].y = y;
          points[1].x = x;
          points[1].y = y;
          points[2].x = x + half_width;
          points[2].y = y + height - 1;
          
          gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, 3);
        }
      switch (shadow_type)
        {
        case GTK_SHADOW_IN:
        case GTK_SHADOW_OUT:
          gdk_draw_line (window, gc4,
                         x + width - 2,
                         y + 1, x + 1, y + 1);
          gdk_draw_line (window, gc2,
                         x + width - 1, y,
                         x, y);
          
          gdk_draw_line (window, gc4,
                         x + 1, y,
                         x + half_width, y + height - 2);
          gdk_draw_line (window, gc2,
                         x, y,
                         x + half_width, y + height - 1);
          
          gdk_draw_line (window, gc1,
                         x + half_width, y + height - 2,
                         x + width - 2, y);
          gdk_draw_line (window, gc3,
                         x + half_width, y + height - 1,
                         x + width - 1, y);
          break;
          
        case GTK_SHADOW_ETCHED_IN:
        case GTK_SHADOW_ETCHED_OUT:
          gdk_draw_line (window, gc1,
                         x + width - 1, y + 1,
                         x + 1, y + 1);
          gdk_draw_line (window, gc1,
                         x + 1, y + 1,
                         x + half_width + 1, y + height - 1);
          gdk_draw_line (window, gc1,
                         x + half_width + 1, y + height - 2,
                         x + width - 1, y);
          
          points[0].x = x + width - 2;
          points[0].y = y;
          points[1].x = x;
          points[1].y = y;
          points[2].x = x + half_width;
          points[2].y = y + height - 2;
          
          gdk_draw_polygon (window, gc2, FALSE, points, 3);
          break;
          
        default:
          break;
        }
      break;
    case GTK_ARROW_LEFT:
      if (fill)
        {
          points[0].x = x;
          points[0].y = y + half_height;
          points[1].x = x + width - 1;
          points[1].y = y + height - 1;
          points[2].x = x + width - 1;
          points[2].y = y;
          
          gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, 3);
        }
      
      switch (shadow_type)
        {
        case GTK_SHADOW_IN:
        case GTK_SHADOW_OUT:
          gdk_draw_line (window, gc1,
                         x + 1, y + half_height,
                         x + width - 1, y + height - 1);
          gdk_draw_line (window, gc3,
                         x, y + half_height,
                         x + width - 1, y + height - 1);
          
          gdk_draw_line (window, gc1,
                         x + width - 2, y + height - 1,
                         x + width - 2, y + 1);
          gdk_draw_line (window, gc3,
                         x + width - 1, y + height - 1,
                         x + width - 1, y);
          
          gdk_draw_line (window, gc4,
                         x + width - 1, y + 1,
                         x + 1, y + half_height);
          gdk_draw_line (window, gc2,
                         x + width - 1, y,
                         x, y + half_height);
          break;
          
        case GTK_SHADOW_ETCHED_IN:
        case GTK_SHADOW_ETCHED_OUT:
          gdk_draw_line (window, gc1,
                         x + width - 1, y + 1,
                         x + 1, y + half_height);
          gdk_draw_line (window, gc1,
                         x + 1, y + half_height + 1,
                         x + width - 1, y + height - 1);
          gdk_draw_line (window, gc1,
                         x + width - 1, y + height - 1,
                         x + width - 1, y + 1);
          
          points[0].x = x + width - 2;
          points[0].y = y;
          points[1].x = x;
          points[1].y = y + half_height;
          points[2].x = x + width - 2;
          points[2].y = y + height - 2;
          
          gdk_draw_polygon (window, gc2, FALSE, points, 3);
          break;
          
        default:
          break;
        }
      break;
    case GTK_ARROW_RIGHT:
      if (fill)
        {
          points[0].x = x + width - 1;
          points[0].y = y + half_height;
          points[1].x = x;
          points[1].y = y;
          points[2].x = x;
          points[2].y = y + height - 1;
          
          gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, 3);
        }
      
      switch (shadow_type)
        {
        case GTK_SHADOW_IN:
        case GTK_SHADOW_OUT:
          gdk_draw_line (window, gc4,
                         x + width - 1, y + half_height,
                         x + 1, y + 1);
          gdk_draw_line (window, gc2,
                         x + width - 1, y + half_height,
                         x, y);
          gdk_draw_line (window, gc4,
                         x + 1, y + 1,
                         x + 1, y + height - 2);
          gdk_draw_line (window, gc2,
                         x, y,
                         x, y + height - 1);
          
          gdk_draw_line (window, gc1,
                         x + 1, y + height - 2,
                         x + width - 1, y + half_height);
          gdk_draw_line (window, gc3,
                         x, y + height - 1,
                         x + width - 1, y + half_height);
          break;
          
        case GTK_SHADOW_ETCHED_IN:
        case GTK_SHADOW_ETCHED_OUT:
          gdk_draw_line (window, gc1,
                         x + width - 1, y + half_height + 1,
                         x + 1, y + 1);
          gdk_draw_line (window, gc1,
                         x + 1, y + 1,
                         x + 1, y + height - 1);
          gdk_draw_line (window, gc1,
                         x + 1, y + height - 1,
                         x + width - 1, y + half_height + 1);
          
          points[0].x = x + width - 2;
          points[0].y = y + half_height;
          points[1].x = x;
          points[1].y = y;
          points[2].x = x;
          points[2].y = y + height - 1;
          
          gdk_draw_polygon (window, gc2, FALSE, points, 3);
          break;
          
        default:
          break;
        }
      break;
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      if (gc3)
        {
          gdk_gc_set_clip_rectangle (gc3, NULL);
          gdk_gc_set_clip_rectangle (gc4, NULL);
        }
    }
}

static void
gtk_aqua_draw_diamond (GtkStyle      *style,
                          GdkWindow     *window,
                          GtkStateType   state_type,
                          GtkShadowType  shadow_type,
                          GdkRectangle  *area,
                          GtkWidget     *widget,
                          gchar         *detail,
                          gint           x,
                          gint           y,
                          gint           width,
                          gint           height)
{
  gint half_width;
  gint half_height;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if ((width == -1) && (height == -1))
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  half_width = width / 2;
  half_height = height / 2;
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], area);
      gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], area);
      gdk_gc_set_clip_rectangle (style->black_gc, area);
    }
  switch (shadow_type)
    {
    case GTK_SHADOW_IN:
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + 2, y + half_height,
                     x + half_width, y + height - 2);
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + half_width, y + height - 2,
                     x + width - 2, y + half_height);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + 1, y + half_height,
                     x + half_width, y + height - 1);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + half_width, y + height - 1,
                     x + width - 1, y + half_height);
      gdk_draw_line (window, style->light_gc[state_type],
                     x, y + half_height,
                     x + half_width, y + height);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + half_width, y + height,
                     x + width, y + half_height);
      
      gdk_draw_line (window, style->black_gc,
                     x + 2, y + half_height,
                     x + half_width, y + 2);
      gdk_draw_line (window, style->black_gc,
                     x + half_width, y + 2,
                     x + width - 2, y + half_height);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + 1, y + half_height,
                     x + half_width, y + 1);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + half_width, y + 1,
                     x + width - 1, y + half_height);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x, y + half_height,
                     x + half_width, y);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + half_width, y,
                     x + width, y + half_height);
      break;
    case GTK_SHADOW_OUT:
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + 2, y + half_height,
                     x + half_width, y + height - 2);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + half_width, y + height - 2,
                     x + width - 2, y + half_height);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + 1, y + half_height,
                     x + half_width, y + height - 1);
      gdk_draw_line (window, style->dark_gc[state_type],
                     x + half_width, y + height - 1,
                     x + width - 1, y + half_height);
      gdk_draw_line (window, style->black_gc,
                     x, y + half_height,
                     x + half_width, y + height);
      gdk_draw_line (window, style->black_gc,
                     x + half_width, y + height,
                     x + width, y + half_height);
      
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + 2, y + half_height,
                     x + half_width, y + 2);
      gdk_draw_line (window, style->bg_gc[state_type],
                     x + half_width, y + 2,
                     x + width - 2, y + half_height);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + 1, y + half_height,
                     x + half_width, y + 1);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + half_width, y + 1,
                     x + width - 1, y + half_height);
      gdk_draw_line (window, style->light_gc[state_type],
                     x, y + half_height,
                     x + half_width, y);
      gdk_draw_line (window, style->light_gc[state_type],
                     x + half_width, y,
                     x + width, y + half_height);
      break;
    default:
      break;
    }
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->light_gc[state_type], NULL);
      gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
      gdk_gc_set_clip_rectangle (style->dark_gc[state_type], NULL);
      gdk_gc_set_clip_rectangle (style->black_gc, NULL);
    }
}

static void
gtk_aqua_draw_oval (GtkStyle      *style,
                       GdkWindow     *window,
                       GtkStateType   state_type,
                       GtkShadowType  shadow_type,
                       GdkRectangle  *area,
                       GtkWidget     *widget,
                       gchar         *detail,
                       gint           x,
                       gint           y,
                       gint           width,
                       gint           height)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);

  g_warning ("gtk_aqua_draw_oval(): FIXME, this function is currently unimplemented");
}

static void
gtk_aqua_draw_string (GtkStyle      *style,
                         GdkWindow     *window,
                         GtkStateType   state_type,
                         GdkRectangle  *area,
                         GtkWidget     *widget,
                         gchar         *detail,
                         gint           x,
                         gint           y,
                         const gchar   *string)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (area)
    {
      gdk_gc_set_clip_rectangle (style->white_gc, area);
      gdk_gc_set_clip_rectangle (style->fg_gc[state_type], area);
    }

  if (state_type == GTK_STATE_INSENSITIVE)
    gdk_draw_string (window, style->font, style->white_gc, x + 1, y + 1, string);

  gdk_draw_string (window, style->font, style->fg_gc[state_type], x, y, string);

  if (area)
    {
      gdk_gc_set_clip_rectangle (style->white_gc, NULL);
      gdk_gc_set_clip_rectangle (style->fg_gc[state_type], NULL);
    }
}

static void 
gtk_aqua_draw_box (GtkStyle      *style,
		      GdkWindow     *window,
		      GtkStateType   state_type,
		      GtkShadowType  shadow_type,
		      GdkRectangle  *area,
		      GtkWidget     *widget,
		      gchar         *detail,
		      gint           x,
		      gint           y,
		      gint           width,
		      gint           height)
{
  GdkRectangle rect;
  int pressed = false;
  int focus = false;
  int selected = false;
	
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);

  printf( "detail: %s\n", detail );
  
  if ( strcmp( detail, "button" ) == 0 ) {

	if ( GTK_WIDGET_HAS_FOCUS (widget) ) {

		// undo the button resizing hat is supposed to identify the focus
		x -= 1;
		y -= 1;
		width += 2;
		height += 2;
		focus = true;
	}

	rect.x = x;
	rect.y = y;
	rect.width = width;
	rect.height = height;

	if (GTK_WIDGET_STATE (widget) == GTK_STATE_ACTIVE) {
		pressed = true;
	}
		
	gdk_aqua_draw_pushbutton( window, style->bg_gc[state_type], &rect, pressed, focus );
	  
  } else if ( strcmp( detail, "buttondefault" ) == 0 ) {

	// draw default button (note: rectangle is larger than normal)

  } else if ( strcmp( detail, "checkbutton" ) == 0 ) {

	if ( GTK_WIDGET_HAS_FOCUS (widget) ) {
		// undo the button resizing hat is supposed to identify the focus
		x -= 1;
		y -= 1;
		width += 2;
		height += 2;
		focus = true;
	}

	rect.x = x;
	rect.y = y;
	rect.width = width;
	rect.height = height;

	if (GTK_WIDGET_STATE (widget) == GTK_STATE_ACTIVE) {
		pressed = true;
	}

	if (state_type == GTK_STATE_ACTIVE) {
		selected = true;
	}

	gdk_aqua_draw_checkbox( window, style->bg_gc[state_type], &rect, pressed, focus, selected );

  } else if ( strcmp( detail, "togglebutton" ) == 0 ) {

	  if ( GTK_WIDGET_HAS_FOCUS (widget) ) {
		  focus = true;
	  }

	  rect.x = x;
	  rect.y = y;
	  rect.width = width;
	  rect.height = height;

	  if (GTK_WIDGET_STATE (widget) == GTK_STATE_ACTIVE) {
		  pressed = true;
	  }

	  if (state_type == GTK_STATE_ACTIVE) {
		  selected = true;
	  }

	  gdk_aqua_draw_bevelbutton( window, style->bg_gc[state_type], &rect, pressed, focus, selected );
	  
  } else {

    default_class.draw_box( style, window, state_type, shadow_type, area, widget, detail, x, y, width, height );
  }
  
  /*
  if (!style->bg_pixmap[state_type] || 
      gdk_window_get_type (window) == GDK_WINDOW_PIXMAP)
    {
      if (area)
	gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);

      gdk_draw_rectangle (window, style->bg_gc[state_type], TRUE,
                          x, y, width, height);
      if (area)
	gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
    }
  else
    gtk_style_apply_default_background (style, window,
                                        widget && !GTK_WIDGET_NO_WINDOW (widget),
                                        state_type, area, x, y, width, height);
  */

  /*
  gtk_paint_shadow (style, window, state_type, shadow_type, area, widget, detail,
                    x, y, width, height);
   */
}

static void 
gtk_aqua_draw_flat_box (GtkStyle      *style,
                           GdkWindow     *window,
                           GtkStateType   state_type,
                           GtkShadowType  shadow_type,
                           GdkRectangle  *area,
                           GtkWidget     *widget,
                           gchar         *detail,
                           gint           x,
                           gint           y,
                           gint           width,
                           gint           height)
{
  GdkGC *gc1;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);

  if (detail)
    {
	  if ( strcmp( detail, "checkbutton" ) == 0 ) {
		return;
	  }

      if (!strcmp ("text", detail) && state_type == GTK_STATE_SELECTED)
	gc1 = style->bg_gc[GTK_STATE_SELECTED];
      else if (!strcmp ("viewportbin", detail))
	gc1 = style->bg_gc[GTK_STATE_NORMAL];
      else if (!strcmp ("entry_bg", detail))
	gc1 = style->base_gc[state_type];
      else
	gc1 = style->bg_gc[state_type];
    }
  else
    gc1 = style->bg_gc[state_type];
  
  if (!style->bg_pixmap[state_type] || gc1 != style->bg_gc[state_type] ||
      gdk_window_get_type (window) == GDK_WINDOW_PIXMAP)
    {
      if (area)
	gdk_gc_set_clip_rectangle (gc1, area);

      gdk_draw_rectangle (window, gc1, TRUE,
                          x, y, width, height);

      if (detail && !strcmp ("tooltip", detail))
        gdk_draw_rectangle (window, style->black_gc, FALSE,
                            x, y, width - 1, height - 1);

      if (area)
	gdk_gc_set_clip_rectangle (gc1, NULL);
    }
  else
    gtk_style_apply_default_background (style, window,
                                        widget && !GTK_WIDGET_NO_WINDOW (widget),
                                        state_type, area, x, y, width, height);
}

static void 
gtk_aqua_draw_check (GtkStyle      *style,
			GdkWindow     *window,
			GtkStateType   state_type,
			GtkShadowType  shadow_type,
			GdkRectangle  *area,
			GtkWidget     *widget,
			gchar         *detail,
			gint           x,
			gint           y,
			gint           width,
			gint           height)
{
  gtk_paint_box (style, window, state_type, shadow_type, area, widget, detail,
                 x, y, width, height);
}

static void 
gtk_aqua_draw_option (GtkStyle      *style,
			 GdkWindow     *window,
			 GtkStateType   state_type,
			 GtkShadowType  shadow_type,
			 GdkRectangle  *area,
			 GtkWidget     *widget,
			 gchar         *detail,
			 gint           x,
			 gint           y,
			 gint           width,
			 gint           height)
{
  gtk_paint_diamond (style, window, state_type, shadow_type, area, widget, 
                     detail, x, y, width, height);
}

static void 
gtk_aqua_draw_cross (GtkStyle      *style,
			GdkWindow     *window,
			GtkStateType   state_type,
			GtkShadowType  shadow_type,
			GdkRectangle  *area,
			GtkWidget     *widget,
			gchar         *detail,
			gint           x,
			gint           y,
			gint           width,
			gint           height)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);

  g_warning ("gtk_aqua_draw_cross(): FIXME, this function is currently unimplemented");
}

static void 
gtk_aqua_draw_ramp    (GtkStyle      *style,
                          GdkWindow     *window,
                          GtkStateType   state_type,
                          GtkShadowType  shadow_type,
                          GdkRectangle  *area,
                          GtkWidget     *widget,
                          gchar         *detail,
                          GtkArrowType   arrow_type,
                          gint           x,
                          gint           y,
                          gint           width,
                          gint           height)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);

  g_warning ("gtk_aqua_draw_ramp(): FIXME, this function is currently unimplemented");
}

static void
gtk_aqua_draw_tab (GtkStyle      *style,
		      GdkWindow     *window,
		      GtkStateType   state_type,
		      GtkShadowType  shadow_type,
		      GdkRectangle  *area,
		      GtkWidget     *widget,
		      gchar         *detail,
		      gint           x,
		      gint           y,
		      gint           width,
		      gint           height)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  gtk_paint_box (style, window, state_type, shadow_type, area, widget, detail,
                 x, y, width, height);
}

static void 
gtk_aqua_draw_shadow_gap (GtkStyle       *style,
                             GdkWindow      *window,
                             GtkStateType    state_type,
                             GtkShadowType   shadow_type,
                             GdkRectangle   *area,
                             GtkWidget      *widget,
                             gchar          *detail,
                             gint            x,
                             gint            y,
                             gint            width,
                             gint            height,
                             GtkPositionType gap_side,
                             gint            gap_x,
                             gint            gap_width)
{
  GdkGC *gc1 = NULL;
  GdkGC *gc2 = NULL;
  GdkGC *gc3 = NULL;
  GdkGC *gc4 = NULL;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
      return;
    case GTK_SHADOW_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->black_gc;
      gc3 = style->bg_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->bg_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->black_gc;
      break;
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->dark_gc[state_type];
      break;
    }
  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      gdk_gc_set_clip_rectangle (gc3, area);
      gdk_gc_set_clip_rectangle (gc4, area);
    }
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
    case GTK_SHADOW_IN:
    case GTK_SHADOW_OUT:
    case GTK_SHADOW_ETCHED_IN:
    case GTK_SHADOW_ETCHED_OUT:
      switch (gap_side)
        {
        case GTK_POS_TOP:
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 1, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y, x + gap_x - 1, y);
              gdk_draw_line (window, gc2,
                             x + 1, y + 1, x + gap_x - 1, y + 1);
              gdk_draw_line (window, gc2,
                             x + gap_x, y, x + gap_x, y);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc1,
                             x + gap_x + gap_width, y, x + width - 2, y);
              gdk_draw_line (window, gc2,
                             x + gap_x + gap_width, y + 1, x + width - 2, y + 1);
              gdk_draw_line (window, gc2,
                             x + gap_x + gap_width - 1, y, x + gap_x + gap_width - 1, y);
            }
          break;
        case GTK_POS_BOTTOM:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 2, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 1);
          
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 1, x + width - 2, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc4,
                             x, y + height - 1, x + gap_x - 1, y + height - 1);
              gdk_draw_line (window, gc3,
                             x + 1, y + height - 2, x + gap_x - 1, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + gap_x, y + height - 1, x + gap_x, y + height - 1);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc4,
                             x + gap_x + gap_width, y + height - 1, x + width - 2, y + height - 1);
              gdk_draw_line (window, gc3,
                             x + gap_x + gap_width, y + height - 2, x + width - 2, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + gap_x + gap_width - 1, y + height - 1, x + gap_x + gap_width - 1, y + height - 1);
            }
          break;
        case GTK_POS_LEFT:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc2,
                         x, y + 1, x + width - 2, y + 1);
          
          gdk_draw_line (window, gc3,
                         x, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 1, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y, x, y + gap_x - 1);
              gdk_draw_line (window, gc2,
                             x + 1, y + 1, x + 1, y + gap_x - 1);
              gdk_draw_line (window, gc2,
                             x, y + gap_x, x, y + gap_x);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y + gap_x + gap_width, x, y + height - 2);
              gdk_draw_line (window, gc2,
                             x + 1, y + gap_x + gap_width, x + 1, y + height - 2);
              gdk_draw_line (window, gc2,
                             x, y + gap_x + gap_width - 1, x, y + gap_x + gap_width - 1);
            }
          break;
        case GTK_POS_RIGHT:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 1, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 1, y + height - 2, x + width - 1, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc4,
                             x + width - 1, y, x + width - 1, y + gap_x - 1);
              gdk_draw_line (window, gc3,
                             x + width - 2, y + 1, x + width - 2, y + gap_x - 1);
              gdk_draw_line (window, gc3,
                             x + width - 1, y + gap_x, x + width - 1, y + gap_x);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc4,
                             x + width - 1, y + gap_x + gap_width, x + width - 1, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + width - 2, y + gap_x + gap_width, x + width - 2, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + width - 1, y + gap_x + gap_width - 1, x + width - 1, y + gap_x + gap_width - 1);
            }
          break;
        }
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      gdk_gc_set_clip_rectangle (gc3, NULL);
      gdk_gc_set_clip_rectangle (gc4, NULL);
    }
}

static void 
gtk_aqua_draw_box_gap (GtkStyle       *style,
                          GdkWindow      *window,
                          GtkStateType    state_type,
                          GtkShadowType   shadow_type,
                          GdkRectangle   *area,
                          GtkWidget      *widget,
                          gchar          *detail,
                          gint            x,
                          gint            y,
                          gint            width,
                          gint            height,
                          GtkPositionType gap_side,
                          gint            gap_x,
                          gint            gap_width)
{
  GdkGC *gc1 = NULL;
  GdkGC *gc2 = NULL;
  GdkGC *gc3 = NULL;
  GdkGC *gc4 = NULL;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  gtk_style_apply_default_background (style, window,
                                      widget && !GTK_WIDGET_NO_WINDOW (widget),
                                      state_type, area, x, y, width, height);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
      return;
    case GTK_SHADOW_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->black_gc;
      gc3 = style->bg_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->bg_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->black_gc;
      break;
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->dark_gc[state_type];
      break;
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      gdk_gc_set_clip_rectangle (gc3, area);
      gdk_gc_set_clip_rectangle (gc4, area);
    }
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
    case GTK_SHADOW_IN:
    case GTK_SHADOW_OUT:
    case GTK_SHADOW_ETCHED_IN:
    case GTK_SHADOW_ETCHED_OUT:
      switch (gap_side)
        {
        case GTK_POS_TOP:
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 1, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y, x + gap_x - 1, y);
              gdk_draw_line (window, gc2,
                             x + 1, y + 1, x + gap_x - 1, y + 1);
              gdk_draw_line (window, gc2,
                             x + gap_x, y, x + gap_x, y);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc1,
                             x + gap_x + gap_width, y, x + width - 2, y);
              gdk_draw_line (window, gc2,
                             x + gap_x + gap_width, y + 1, x + width - 2, y + 1);
              gdk_draw_line (window, gc2,
                             x + gap_x + gap_width - 1, y, x + gap_x + gap_width - 1, y);
            }
          break;
        case  GTK_POS_BOTTOM:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 2, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 1);
          
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 1, x + width - 2, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc4,
                             x, y + height - 1, x + gap_x - 1, y + height - 1);
              gdk_draw_line (window, gc3,
                             x + 1, y + height - 2, x + gap_x - 1, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + gap_x, y + height - 1, x + gap_x, y + height - 1);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc4,
                             x + gap_x + gap_width, y + height - 1, x + width - 2, y + height - 1);
              gdk_draw_line (window, gc3,
                             x + gap_x + gap_width, y + height - 2, x + width - 2, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + gap_x + gap_width - 1, y + height - 1, x + gap_x + gap_width - 1, y + height - 1);
            }
          break;
        case GTK_POS_LEFT:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc2,
                         x, y + 1, x + width - 2, y + 1);
          
          gdk_draw_line (window, gc3,
                         x, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 1, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y, x, y + gap_x - 1);
              gdk_draw_line (window, gc2,
                             x + 1, y + 1, x + 1, y + gap_x - 1);
              gdk_draw_line (window, gc2,
                             x, y + gap_x, x, y + gap_x);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc1,
                             x, y + gap_x + gap_width, x, y + height - 2);
              gdk_draw_line (window, gc2,
                             x + 1, y + gap_x + gap_width, x + 1, y + height - 2);
              gdk_draw_line (window, gc2,
                             x, y + gap_x + gap_width - 1, x, y + gap_x + gap_width - 1);
            }
          break;
        case GTK_POS_RIGHT:
          gdk_draw_line (window, gc1,
                         x, y, x + width - 1, y);
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 1, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 1, y + height - 2, x + width - 1, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 1, y + height - 1);
          if (gap_x > 0)
            {
              gdk_draw_line (window, gc4,
                             x + width - 1, y, x + width - 1, y + gap_x - 1);
              gdk_draw_line (window, gc3,
                             x + width - 2, y + 1, x + width - 2, y + gap_x - 1);
              gdk_draw_line (window, gc3,
                             x + width - 1, y + gap_x, x + width - 1, y + gap_x);
            }
          if ((width - (gap_x + gap_width)) > 0)
            {
              gdk_draw_line (window, gc4,
                             x + width - 1, y + gap_x + gap_width, x + width - 1, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + width - 2, y + gap_x + gap_width, x + width - 2, y + height - 2);
              gdk_draw_line (window, gc3,
                             x + width - 1, y + gap_x + gap_width - 1, x + width - 1, y + gap_x + gap_width - 1);
            }
          break;
        }
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      gdk_gc_set_clip_rectangle (gc3, NULL);
      gdk_gc_set_clip_rectangle (gc4, NULL);
    }
}

static void 
gtk_aqua_draw_extension (GtkStyle       *style,
                            GdkWindow      *window,
                            GtkStateType    state_type,
                            GtkShadowType   shadow_type,
                            GdkRectangle   *area,
                            GtkWidget      *widget,
                            gchar          *detail,
                            gint            x,
                            gint            y,
                            gint            width,
                            gint            height,
                            GtkPositionType gap_side)
{
  GdkGC *gc1 = NULL;
  GdkGC *gc2 = NULL;
  GdkGC *gc3 = NULL;
  GdkGC *gc4 = NULL;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  gtk_style_apply_default_background (style, window,
                                      widget && !GTK_WIDGET_NO_WINDOW (widget),
                                      GTK_STATE_NORMAL, area, x, y, width, height);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
      return;
    case GTK_SHADOW_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->black_gc;
      gc3 = style->bg_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_ETCHED_IN:
      gc1 = style->dark_gc[state_type];
      gc2 = style->light_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->light_gc[state_type];
      break;
    case GTK_SHADOW_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->bg_gc[state_type];
      gc3 = style->dark_gc[state_type];
      gc4 = style->black_gc;
      break;
    case GTK_SHADOW_ETCHED_OUT:
      gc1 = style->light_gc[state_type];
      gc2 = style->dark_gc[state_type];
      gc3 = style->light_gc[state_type];
      gc4 = style->dark_gc[state_type];
      break;
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, area);
      gdk_gc_set_clip_rectangle (gc2, area);
      gdk_gc_set_clip_rectangle (gc3, area);
      gdk_gc_set_clip_rectangle (gc4, area);
    }

  switch (shadow_type)
    {
    case GTK_SHADOW_NONE:
    case GTK_SHADOW_IN:
    case GTK_SHADOW_OUT:
    case GTK_SHADOW_ETCHED_IN:
    case GTK_SHADOW_ETCHED_OUT:
      switch (gap_side)
        {
        case GTK_POS_TOP:
          gtk_style_apply_default_background (style, window,
                                              widget && !GTK_WIDGET_NO_WINDOW (widget),
                                              state_type, area,
                                              x + style->klass->xthickness, 
                                              y, 
                                              width - (2 * style->klass->xthickness), 
                                              height - (style->klass->ythickness));
          gdk_draw_line (window, gc1,
                         x, y, x, y + height - 2);
          gdk_draw_line (window, gc2,
                         x + 1, y, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 2, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x + 1, y + height - 1, x + width - 2, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y, x + width - 1, y + height - 2);
          break;
        case GTK_POS_BOTTOM:
          gtk_style_apply_default_background (style, window,
                                              widget && !GTK_WIDGET_NO_WINDOW (widget),
                                              state_type, area,
                                              x + style->klass->xthickness, 
                                              y + style->klass->ythickness, 
                                              width - (2 * style->klass->xthickness), 
                                              height - (style->klass->ythickness));
          gdk_draw_line (window, gc1,
                         x + 1, y, x + width - 2, y);
          gdk_draw_line (window, gc1,
                         x, y + 1, x, y + height - 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 2, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 1);
          
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 2, x + width - 2, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y + 1, x + width - 1, y + height - 1);
          break;
        case GTK_POS_LEFT:
          gtk_style_apply_default_background (style, window,
                                              widget && !GTK_WIDGET_NO_WINDOW (widget),
                                              state_type, area,
                                              x, 
                                              y + style->klass->ythickness, 
                                              width - (style->klass->xthickness), 
                                              height - (2 * style->klass->ythickness));
          gdk_draw_line (window, gc1,
                         x, y, x + width - 2, y);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 2, y + 1);
          
          gdk_draw_line (window, gc3,
                         x, y + height - 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc3,
                         x + width - 2, y + 2, x + width - 2, y + height - 2);
          gdk_draw_line (window, gc4,
                         x, y + height - 1, x + width - 2, y + height - 1);
          gdk_draw_line (window, gc4,
                         x + width - 1, y + 1, x + width - 1, y + height - 2);
          break;
        case GTK_POS_RIGHT:
          gtk_style_apply_default_background (style, window,
                                              widget && !GTK_WIDGET_NO_WINDOW (widget),
                                              state_type, area,
                                              x + style->klass->xthickness, 
                                              y + style->klass->ythickness, 
                                              width - (style->klass->xthickness), 
                                              height - (2 * style->klass->ythickness));
          gdk_draw_line (window, gc1,
                         x + 1, y, x + width - 1, y);
          gdk_draw_line (window, gc1,
                         x, y + 1, x, y + height - 2);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + width - 1, y + 1);
          gdk_draw_line (window, gc2,
                         x + 1, y + 1, x + 1, y + height - 2);
          
          gdk_draw_line (window, gc3,
                         x + 2, y + height - 2, x + width - 1, y + height - 2);
          gdk_draw_line (window, gc4,
                         x + 1, y + height - 1, x + width - 1, y + height - 1);
          break;
        }
    }

  if (area)
    {
      gdk_gc_set_clip_rectangle (gc1, NULL);
      gdk_gc_set_clip_rectangle (gc2, NULL);
      gdk_gc_set_clip_rectangle (gc3, NULL);
      gdk_gc_set_clip_rectangle (gc4, NULL);
    }
}

static void 
gtk_aqua_draw_focus (GtkStyle      *style,
                        GdkWindow     *window,
                        GdkRectangle  *area,
                        GtkWidget     *widget,
                        gchar         *detail,
                        gint           x,
                        gint           y,
                        gint           width,
                        gint           height)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);

  // aqua button focus is not just a simple rectangle, so we don't draw anything here
  if ( strcmp( detail, "button" ) == 0 ) {
	  return;
  } else if ( strcmp( detail, "checkbutton" ) == 0 ) {
	  return;
  } else if ( strcmp( detail, "togglebutton" ) == 0 ) {
	  return;
  }
  
  if (width == -1 && height == -1)
    {
      gdk_window_get_size (window, &width, &height);
      width -= 1;
      height -= 1;
    }
  else if (width == -1)
    {
      gdk_window_get_size (window, &width, NULL);
      width -= 1;
    }
  else if (height == -1)
    {
      gdk_window_get_size (window, NULL, &height);
      height -= 1;
    }

  if (area)
    gdk_gc_set_clip_rectangle (style->black_gc, area);

  if (detail && !strcmp (detail, "add-mode"))
    {
      gdk_gc_set_line_attributes (style->black_gc, 1, GDK_LINE_ON_OFF_DASH, 0, 0);
      gdk_gc_set_dashes (style->black_gc, 0, "\4\4", 2);
      
      gdk_draw_rectangle (window,
                          style->black_gc, FALSE,
                          x, y, width, height);
      
      gdk_gc_set_line_attributes (style->black_gc, 1, GDK_LINE_SOLID, 0, 0);
    }
  else
    {
      gdk_draw_rectangle (window,
                          style->black_gc, FALSE,
                          x, y, width, height);
    }

  if (area)
    gdk_gc_set_clip_rectangle (style->black_gc, NULL);
}

static void 
gtk_aqua_draw_slider (GtkStyle      *style,
                         GdkWindow     *window,
                         GtkStateType   state_type,
                         GtkShadowType  shadow_type,
                         GdkRectangle  *area,
                         GtkWidget     *widget,
                         gchar         *detail,
                         gint           x,
                         gint           y,
                         gint           width,
                         gint           height,
                         GtkOrientation orientation)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  gtk_paint_box (style, window, state_type, shadow_type,
                 area, widget, detail, x, y, width, height);

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    gtk_paint_vline (style, window, state_type, area, widget, detail, 
                     style->klass->ythickness, 
                     height - style->klass->ythickness - 1, width / 2);
  else
    gtk_paint_hline (style, window, state_type, area, widget, detail, 
                     style->klass->xthickness, 
                     width - style->klass->xthickness - 1, height / 2);
}

static void 
gtk_aqua_draw_handle (GtkStyle      *style,
			 GdkWindow     *window,
			 GtkStateType   state_type,
			 GtkShadowType  shadow_type,
			 GdkRectangle  *area,
			 GtkWidget     *widget,
			 gchar         *detail,
			 gint           x,
			 gint           y,
			 gint           width,
			 gint           height,
			 GtkOrientation orientation)
{
  gint xx, yy;
  gint xthick, ythick;
  GdkGC *light_gc, *dark_gc;
  GdkRectangle rect;
  GdkRectangle dest;
  gint intersect;
  
  g_return_if_fail (style != NULL);
  g_return_if_fail (window != NULL);
  
  if (width == -1 && height == -1)
    gdk_window_get_size (window, &width, &height);
  else if (width == -1)
    gdk_window_get_size (window, &width, NULL);
  else if (height == -1)
    gdk_window_get_size (window, NULL, &height);
  
  gtk_paint_box (style, window, state_type, shadow_type, area, widget, 
                 detail, x, y, width, height);
  
  light_gc = style->light_gc[state_type];
  dark_gc = style->dark_gc[state_type];
  
  xthick = style->klass->xthickness;
  ythick = style->klass->ythickness;
  
  rect.x = x + xthick;
  rect.y = y + ythick;
  rect.width = width - (xthick * 2);
  rect.height = height - (ythick * 2);

  if (area)
    intersect = gdk_rectangle_intersect (area, &rect, &dest);
  else
    {
      intersect = TRUE;
      dest = rect;
    }

  if (!intersect)
    return;

#define DRAW_POINT(w, gc, clip, xx, yy)		\
  {						\
    if ((xx) >= (clip).x			\
	&& (yy) >= (clip).y			\
	&& (xx) < (clip).x + (clip).width	\
	&& (yy) < (clip).y + (clip).height)	\
      gdk_draw_point ((w), (gc), (xx), (yy));	\
  }

  for (yy = y + ythick; yy < (y + height - ythick); yy += 3)
    for (xx = x + xthick; xx < (x + width - xthick); xx += 6)
      {
	DRAW_POINT (window, light_gc, dest, xx, yy);
	DRAW_POINT (window, dark_gc, dest, xx + 1, yy + 1);

	DRAW_POINT (window, light_gc, dest, xx + 3, yy + 1);
	DRAW_POINT (window, dark_gc, dest, xx + 4, yy + 2);
      }
}
