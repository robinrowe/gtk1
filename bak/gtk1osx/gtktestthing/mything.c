
#include <stdio.h>
//#include <stdlib.h>
//#include <signal.h>
#include <gdk/gdk.h>

//#include "mything.h"


static const gchar * xpm_data[] = {
    "48 48 64 1",
    "       c None",
    ".      c #DF7DCF3CC71B",
    "X      c #965875D669A6",
    "o      c #71C671C671C6",
    "O      c #A699A289A699",
    "+      c #965892489658",
    "@      c #8E38410330C2",
    "#      c #D75C7DF769A6",
    "$      c #F7DECF3CC71B",
    "%      c #96588A288E38",
    "&      c #A69992489E79",
    "*      c #8E3886178E38",
    "=      c #104008200820",
    "-      c #596510401040",
    ";      c #C71B30C230C2",
    ":      c #C71B9A699658",
    ">      c #618561856185",
    ",      c #20811C712081",
    "<      c #104000000000",
    "1      c #861720812081",
    "2      c #DF7D4D344103",
    "3      c #79E769A671C6",
    "4      c #861782078617",
    "5      c #41033CF34103",
    "6      c #000000000000",
    "7      c #49241C711040",
    "8      c #492445144924",
    "9      c #082008200820",
    "0      c #69A618611861",
    "q      c #B6DA71C65144",
    "w      c #410330C238E3",
    "e      c #CF3CBAEAB6DA",
    "r      c #71C6451430C2",
    "t      c #EFBEDB6CD75C",
    "y      c #28A208200820",
    "u      c #186110401040",
    "i      c #596528A21861",
    "p      c #71C661855965",
    "a      c #A69996589658",
    "s      c #30C228A230C2",
    "d      c #BEFBA289AEBA",
    "f      c #596545145144",
    "g      c #30C230C230C2",
    "h      c #8E3882078617",
    "j      c #208118612081",
    "k      c #38E30C300820",
    "l      c #30C2208128A2",
    "z      c #38E328A238E3",
    "x      c #514438E34924",
    "c      c #618555555965",
    "v      c #30C2208130C2",
    "b      c #38E328A230C2",
    "n      c #28A228A228A2",
    "m      c #41032CB228A2",
    "M      c #104010401040",
    "N      c #492438E34103",
    "B      c #28A2208128A2",
    "V      c #A699596538E3",
    "C      c #30C21C711040",
    "Z      c #30C218611040",
    "A      c #965865955965",
    "S      c #618534D32081",
    "D      c #38E31C711040",
    "F      c #082000000820",
    "                                                ",
    "          .XoO                                  ",
    "         +@#$%o&                                ",
    "         *=-;#::o+                              ",
    "           >,<12#:34                            ",
    "             45671#:X3                          ",
    "               +89<02qwo                        ",
    "e*                >,67;ro                       ",
    "ty>                 459@>+&&                    ",
    "$2u+                  ><ipas8*                  ",
    "%$;=*                *3:.Xa.dfg>                ",
    "Oh$;ya             *3d.a8j,Xe.d3g8+             ",
    " Oh$;ka          *3d$a8lz,,xxc:.e3g54           ",
    "  Oh$;kO       *pd$%svbzz,sxxxxfX..&wn>         ",
    "   Oh$@mO    *3dthwlsslszjzxxxxxxx3:td8M4       ",
    "    Oh$@g& *3d$XNlvvvlllm,mNwxxxxxxxfa.:,B*     ",
    "     Oh$@,Od.czlllllzlmmqV@V#V@fxxxxxxxf:%j5&   ",
    "      Oh$1hd5lllslllCCZrV#r#:#2AxxxxxxxxxcdwM*  ",
    "       OXq6c.%8vvvllZZiqqApA:mq:Xxcpcxxxxxfdc9* ",
    "        2r<6gde3bllZZrVi7S@SV77A::qApxxxxxxfdcM ",
    "        :,q-6MN.dfmZZrrSS:#riirDSAX@Af5xxxxxfevo",
    "         +A26jguXtAZZZC7iDiCCrVVii7Cmmmxxxxxx%3g",
    "          *#16jszN..3DZZZZrCVSA2rZrV7Dmmwxxxx&en",
    "           p2yFvzssXe:fCZZCiiD7iiZDiDSSZwwxx8e*>",
    "           OA1<jzxwwc:$d%NDZZZZCCCZCCZZCmxxfd.B ",
    "            3206Bwxxszx%et.eaAp77m77mmmf3&eeeg* ",
    "             @26MvzxNzvlbwfpdettttttttttt.c,n&  ",
    "             *;16=lsNwwNwgsvslbwwvccc3pcfu<o    ",
    "              p;<69BvwwsszslllbBlllllllu<5+     ",
    "              OS0y6FBlvvvzvzss,u=Blllj=54       ",
    "               c1-699Blvlllllu7k96MMMg4         ",
    "               *10y8n6FjvllllB<166668           ",
    "                S-kg+>666<M<996-y6n<8*          ",
    "                p71=4 m69996kD8Z-66698&&        ",
    "                &i0ycm6n4 ogk17,0<6666g         ",
    "                 N-k-<>     >=01-kuu666>        ",
    "                 ,6ky&      &46-10ul,66,        ",
    "                 Ou0<>       o66y<ulw<66&       ",
    "                  *kk5       >66By7=xu664       ",
    "                   <<M4      466lj<Mxu66o       ",
    "                   *>>       +66uv,zN666*       ",
    "                              566,xxj669        ",
    "                              4666FF666>        ",
    "                               >966666M         ",
    "                                oM6668+         ",
    "                                  *4            ",
    "                                                ",
    "                                                "};

void doSomethingIncredible() {

    GdkWindow* topLevel;
    GdkWindow* inner;

    GdkWindowAttr tla;

    GdkEvent* event = NULL;
    GdkGC* gcontext;
    GdkGCValues gcvalues;
    GdkColor white;

    GdkBitmap* mask;
    GdkPixmap* gdkPixmap;
    int x1,x2,y1,y2;
    int mouseDown = FALSE;

    tla.title = "hello";
    //tla.event_mask = GDK_ALL_EVENTS_MASK ^ GDK_POINTER_MOTION_HINT_MASK;
    tla.event_mask =
        GDK_BUTTON_MOTION_MASK |
        GDK_BUTTON_PRESS_MASK |
        GDK_BUTTON_RELEASE_MASK |
        GDK_KEY_PRESS_MASK |
        GDK_KEY_RELEASE_MASK |
        GDK_EXPOSURE_MASK;

    tla.x = 200;
    tla.y = 400;
    tla.width = 300;
    tla.height = 400;
    tla.wclass = GDK_INPUT_OUTPUT;
    tla.window_type = GDK_WINDOW_TOPLEVEL;

    gdk_set_show_events(TRUE);

    topLevel = gdk_window_new(NULL, &tla,
                              GDK_WA_TITLE |
                              GDK_WA_X |
                              GDK_WA_Y);

/*
    tla.x = 100;
    tla.y = 200;
    tla.width = 100;
    tla.height = 100;
    tla.window_type = GDK_WINDOW_CHILD;

    inner = gdk_window_new(topLevel, &tla,
                           GDK_WA_X | GDK_WA_Y);
*/    
    gdk_window_show(topLevel);

    gdk_flush();

    event = gdk_event_get();

    gcontext = gdk_gc_new(topLevel);

    gdkPixmap = gdk_pixmap_create_from_xpm_d( topLevel, &mask,
                                              NULL,
                                              xpm_data );

    gdk_gc_set_foreground(gcontext,
                          &(gcvalues.background));


    event = gdk_event_get();
    printWindowInfo(topLevel);

    debug_print_gc_details(gcontext);
    //uncomment this to see crash! - why does gdk_draw_pixmap not crash when called later, after motion event???
    gdk_draw_pixmap(topLevel,
                     gcontext,
                     gdkPixmap,
                     0,
                     0,
                     100,
                     200,
                     100,
                     100);

    
    /*gdk_draw_rectangle((GdkDrawable*)inner,
                        gcontext,
                        1,
                        90,
                        190,
                        120,
                       120);*/
    
    while ((!event) || (event->type != GDK_DELETE)) {

        if (event) {

            if (event->type == GDK_BUTTON_PRESS) {
                GdkEventButton* buttonEvent = (GdkEventButton*)event;
                x1 = buttonEvent->x;
                y1 = buttonEvent->y;
                printf("click at %i,%i!\n", x1, y1);
                mouseDown = TRUE;

            }
            else if (event->type == GDK_BUTTON_RELEASE) {
                GdkEventButton* buttonEvent = (GdkEventButton*)event;
                x2 = buttonEvent->x;
                y2 = buttonEvent->y;
                printf("release at %i,%i!\n",(int) buttonEvent->x, (int)buttonEvent->y);
                mouseDown = FALSE;
            }
            else if (event->type == GDK_KEY_PRESS) {
                GdkEventKey* keyEvent = (GdkEventKey*)event;
                if (keyEvent->length > 0)
                    printf("key pressed = %s\n", keyEvent->string);
                printf("keysym name = %s\n", gdk_keyval_name(keyEvent->keyval));

            }
            else if (event->type == GDK_KEY_RELEASE) {
                GdkEventKey* keyEvent = (GdkEventKey*)event;
                if (keyEvent->length > 0)
                    printf("key released = %s\n", keyEvent->string);
                printf("keysym name = %s\n", gdk_keyval_name(keyEvent->keyval));

            }
            else if (event->type == GDK_MOTION_NOTIFY) {
                GdkEventMotion* motionEvent = (GdkEventMotion*)event;
                //printf("mouse moved at %i,%i!\n", (int)motionEvent->x, (int)motionEvent->y);
                //if (mouseDown)
                printWindowInfo(topLevel);
                debug_print_gc_details(gcontext);

                gdk_draw_pixmap(topLevel,
                                gcontext,
                                gdkPixmap,
                                0,
                                0,
                                (gint)motionEvent->x,
                                (gint)motionEvent->y,
                                100,
                                100);

            }
            else if (event->type == GDK_ENTER_NOTIFY) {
                //printf("gdk_enter_notify\n");
                gdk_draw_pixmap(topLevel,
                                     gcontext,
                                     gdkPixmap,
                                     0,
                                     0,
                                     100,
                                     200,
                                     100,
                                     100);                

            }
            else if (event->type == GDK_LEAVE_NOTIFY) {
                //printf("gdk_leave_notify\n");

            }
            else if (event->type == GDK_DRAG_ENTER) {
                //printf("gdk_drag_enter\n");

            }
            else if (event->type == GDK_DRAG_LEAVE) {
                printf("gdk_drag_leave\n");

            }
            else if (event->type == GDK_DRAG_MOTION) {
                printf("gdk_drag_motion\n");

            }
            else if (event->type == GDK_DROP_START) {
                printf("gdk_drop_start\n");

            }
            else if (event->type == GDK_DROP_FINISHED) {
                printf("gdk_drop_finished\n");

            }
            else if (event->type == GDK_EXPOSE) {
                printf("gdk_expose\n");
                
            }
            else if (event->type == GDK_CONFIGURE) {
                printf("gdk_configure\n");

            }
            else
                printf("\nevent in %d is of type %d\n",((GdkEventAny*)event)->window, ((GdkEventAny*)event)->type);

            gdk_event_free(event);

        }
        event = gdk_event_get();

    }

}

void printVisualInfo() {
    GdkVisual* visual;

    printf("\n\n");
    visual = gdk_visual_get_system();

    printf("depth %i\n",visual->depth);
    printf("colormap_size %i\n",visual->colormap_size);
    printf("bits_per_rgb %i\n",visual->bits_per_rgb);

    printf("gdk_screen_width() %d\n", gdk_screen_width());
    printf("gdk_screen_height() %d\n", gdk_screen_height());

   
    
    if (visual->type == GDK_VISUAL_TRUE_COLOR) {
        printf("GDK_VISUAL_TRUE_COLOR\n");
    }
    else if (visual->type == GDK_VISUAL_DIRECT_COLOR) {
        printf("GDK_VISUAL_DIRECT_COLOR\n");
    }
    else if (visual->type == GDK_VISUAL_PSEUDO_COLOR) {
        printf("GDK_VISUAL_PSEUDO_COLOR\n");
    }
    else if (visual->type == GDK_VISUAL_STATIC_COLOR) {
        printf("GDK_VISUAL_STATIC_COLOR\n");
    }

}

void printWindowInfo(GdkWindow* window) {
    int x,y;
    GdkWindowType windowType;

    printf("\n\n");

    gdk_window_get_origin(window, &x, &y);
    printf("gdk_window_get_origin x=%d, y=%d\n", x, y);

    gdk_window_get_position(window, &x, &y);
    printf("gdk_window_get_position x=%d, y=%d\n", x, y);

    windowType = gdk_window_get_type(window);
    if (windowType == GDK_WINDOW_ROOT) {
        printf("GDK_WINDOW_ROOT\n");
    }
    else if (windowType == GDK_WINDOW_TOPLEVEL) {
        printf("GDK_WINDOW_TOPLEVEL\n");

    }
    else if (windowType == GDK_WINDOW_CHILD) {
        printf("GDK_WINDOW_CHILD\n");

    }
    else if (windowType == GDK_WINDOW_DIALOG) {
        printf("GDK_WINDOW_DIALOG\n");

    }
    else if (windowType == GDK_WINDOW_TEMP) {
        printf("GDK_WINDOW_TEMP\n");

    }
    else if (windowType == GDK_WINDOW_PIXMAP) {
        printf("GDK_WINDOW_PIXMAP\n");

    }
    else if (windowType == GDK_WINDOW_FOREIGN) {
        printf("GDK_WINDOW_FOREIGN\n");

    }

    g_assert(gdk_window_get_visual(window) == gdk_visual_get_system());

    {
        int width, height;
        gdk_window_get_size(window, &width, &height);
        printf("gdk_window_get_size: width = %d, height = %d \n", width, height);
    
    }

    {
        int depth, width, height;
        gdk_window_get_geometry(window, &x, &y, &width, &height, &depth);
        printf("gdk_window_get_geometry: x=%d, y= %d, width = %d, height = %d, depth = %d\n", x, y, width, height, depth);

    }
}

int main (int argc, char *argv[])
{
    gdk_init(&argc, &argv);

    printVisualInfo();
    doSomethingIncredible();

    gdk_exit(0);

    return 0;
}




