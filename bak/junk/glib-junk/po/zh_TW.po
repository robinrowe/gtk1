# Traditional chinese translation for glib 2.x
# Copyright (C) 2001-2003 Free Software Foundation, Inc.
# Abel Cheung <maddog@linux.org.hk>, 2001-2003.
#
msgid ""
msgstr ""
"Project-Id-Version: glib 2.2.0\n"
"POT-Creation-Date: 2003-01-28 17:15-0500\n"
"PO-Revision-Date: 2003-01-22 21:12+0800\n"
"Last-Translator: Abel Cheung <maddog@linux.org.hk>\n"
"Language-Team: Chinese (traditional) <zh-l10n@linux.org.tw>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: glib/gconvert.c:401
#, c-format
msgid "Conversion from character set '%s' to '%s' is not supported"
msgstr "不支援將字元集‘%s’轉換成‘%s’"

#: glib/gconvert.c:405
#, c-format
msgid "Could not open converter from '%s' to '%s': %s"
msgstr "無法開啟將‘%s’轉換至‘%s’的程序：%s"

#: glib/gconvert.c:603 glib/gconvert.c:893 glib/giochannel.c:1295
#: glib/giochannel.c:1337 glib/giochannel.c:2179 glib/gutf8.c:907
#: glib/gutf8.c:1352
msgid "Invalid byte sequence in conversion input"
msgstr "轉換輸入資料時遇到不正確的位元組次序"

#: glib/gconvert.c:608 glib/gconvert.c:824 glib/giochannel.c:1302
#: glib/giochannel.c:2191
#, c-format
msgid "Error during conversion: %s"
msgstr "轉換時發生錯誤：%s"

#: glib/gconvert.c:626 glib/gutf8.c:903 glib/gutf8.c:1103 glib/gutf8.c:1244
#: glib/gutf8.c:1348
msgid "Partial character sequence at end of input"
msgstr "輸入資料結束時出現未完成的字元次序"

#: glib/gconvert.c:799
#, c-format
msgid "Cannot convert fallback '%s' to codeset '%s'"
msgstr "無法將後備字串‘%s’的字元集轉換成‘%s’"

#: glib/gconvert.c:1633
#, c-format
msgid "The URI '%s' is not an absolute URI using the file scheme"
msgstr "URI‘%s’不是使用 file 格式的絕對 URI"

#: glib/gconvert.c:1643
#, c-format
msgid "The local file URI '%s' may not include a '#'"
msgstr "本機檔案 URI‘%s’不應含有‘#’"

#: glib/gconvert.c:1660
#, c-format
msgid "The URI '%s' is invalid"
msgstr "URI‘%s’無效"

#: glib/gconvert.c:1672
#, c-format
msgid "The hostname of the URI '%s' is invalid"
msgstr "URI‘%s’中的主機名稱無效"

#: glib/gconvert.c:1688
#, c-format
msgid "The URI '%s' contains invalidly escaped characters"
msgstr "URI‘%s’含有不正確的「跳出字元」(escaped characters)"

#: glib/gconvert.c:1759
#, c-format
msgid "The pathname '%s' is not an absolute path"
msgstr "路徑名稱‘%s’不是絕對路徑"

#: glib/gconvert.c:1769
msgid "Invalid hostname"
msgstr "無效的主機名稱"

#: glib/gdir.c:80
#, c-format
msgid "Error opening directory '%s': %s"
msgstr "開啟目錄‘%s’時發生錯誤：%s"

#: glib/gfileutils.c:384 glib/gfileutils.c:449
#, c-format
msgid "Could not allocate %lu bytes to read file \"%s\""
msgstr "無法分配 %lu 位元來讀取檔案“%s”"

#: glib/gfileutils.c:395
#, c-format
msgid "Error reading file '%s': %s"
msgstr "讀取檔案‘%s’時發生錯誤：%s"

#: glib/gfileutils.c:471
#, c-format
msgid "Failed to read from file '%s': %s"
msgstr "讀取檔案‘%s’失敗：%s"

#: glib/gfileutils.c:518 glib/gfileutils.c:586
#, c-format
msgid "Failed to open file '%s': %s"
msgstr "開啟檔案‘%s’失敗：%s"

#: glib/gfileutils.c:532
#, c-format
msgid "Failed to get attributes of file '%s': fstat() failed: %s"
msgstr "無法獲取檔案‘%s’的屬性：fstat() 失敗：%s"

#: glib/gfileutils.c:558
#, c-format
msgid "Failed to open file '%s': fdopen() failed: %s"
msgstr "開啟檔案‘%s’失敗：fdopen() 失敗：%s"

#: glib/gfileutils.c:765
#, c-format
msgid "Template '%s' invalid, should not contain a '%s'"
msgstr "樣式‘%s’無效，不應含有‘%s’"

#: glib/gfileutils.c:777
#, c-format
msgid "Template '%s' doesn't end with XXXXXX"
msgstr "樣式‘%s’末端不是 XXXXXX"

#: glib/gfileutils.c:798
#, c-format
msgid "Failed to create file '%s': %s"
msgstr "無法建立檔案‘%s’：%s"

#: glib/giochannel.c:1123
#, c-format
msgid "Conversion from character set `%s' to `%s' is not supported"
msgstr "不支援將字元集‘%s’轉換成‘%s’"

#: glib/giochannel.c:1127
#, c-format
msgid "Could not open converter from `%s' to `%s': %s"
msgstr "無法開啟將‘%s’轉換至‘%s’的程序：%s"

#: glib/giochannel.c:1472
msgid "Can't do a raw read in g_io_channel_read_line_string"
msgstr "g_io_channel_read_line_string 中無法讀取原始資料"

#: glib/giochannel.c:1519 glib/giochannel.c:1776 glib/giochannel.c:1862
msgid "Leftover unconverted data in read buffer"
msgstr "用來讀取資料的緩衝區中仍有未轉換的資料"

#: glib/giochannel.c:1599 glib/giochannel.c:1676
msgid "Channel terminates in a partial character"
msgstr "輸入管道在字元未完成前已經結束"

#: glib/giochannel.c:1662
msgid "Can't do a raw read in g_io_channel_read_to_end"
msgstr "g_io_channel_read_to_end 中無法讀取原始資料"

#: glib/giowin32.c:712 glib/giowin32.c:741
msgid "Incorrect message size"
msgstr "訊息長度不正確"

#: glib/giowin32.c:1034 glib/giowin32.c:1087
msgid "Socket error"
msgstr "Socket 發生錯誤"

#: glib/gmarkup.c:222
#, c-format
msgid "Error on line %d char %d: %s"
msgstr "在第 %d 行第 %d 個字元發生錯誤：%s"

#: glib/gmarkup.c:306
#, c-format
msgid "Error on line %d: %s"
msgstr "在第 %d 行發生錯誤：%s"

#: glib/gmarkup.c:385
msgid ""
"Empty entity '&;' seen; valid entities are: &amp; &quot; &lt; &gt; &apos;"
msgstr "出現空白的實體‘&;’；可用的實體為：&amp; &quot; &lt; &gt; &apos;"

#: glib/gmarkup.c:395
#, c-format
msgid ""
"Character '%s' is not valid at the start of an entity name; the & character "
"begins an entity; if this ampersand isn't supposed to be an entity, escape "
"it as &amp;"
msgstr ""
"實體名稱不應以‘%s’開始，應該使用 & 字元；如果這個 & 字元不是作為實體使用，請"
"將 & 轉換為 &amp;"

#: glib/gmarkup.c:431
#, c-format
msgid "Character '%s' is not valid inside an entity name"
msgstr "實體名稱中不應含有字元‘%s’"

#: glib/gmarkup.c:475
#, c-format
msgid "Entity name '%s' is not known"
msgstr "不詳的實體名稱‘%s’"

#: glib/gmarkup.c:485
msgid ""
"Entity did not end with a semicolon; most likely you used an ampersand "
"character without intending to start an entity - escape ampersand as &amp;"
msgstr ""
"實體的末端不是分號；很可能您想使用 & 字元但未將它變為實體 ─ 請將 & 轉換為 "
"&amp;"

#: glib/gmarkup.c:531
#, c-format
msgid ""
"Failed to parse '%s', which should have been a digit inside a character "
"reference (&#234; for example) - perhaps the digit is too large"
msgstr "分析‘%s’失敗，字元參引內應該含有數字（例如 &#234;）─ 可能是數字太大"

#: glib/gmarkup.c:556
#, c-format
msgid "Character reference '%s' does not encode a permitted character"
msgstr "字元參引‘%s’含有不正當的字元"

#: glib/gmarkup.c:573
msgid "Empty character reference; should include a digit such as &#454;"
msgstr "空白的字元參引；應該包括數字，像 &#454;"

#: glib/gmarkup.c:583
msgid ""
"Character reference did not end with a semicolon; most likely you used an "
"ampersand character without intending to start an entity - escape ampersand "
"as &amp;"
msgstr ""
"字元參引的末端不是分號；很可能您想使用 & 字元但未將它變為實體 ─ 請將 & 轉換"
"為 &amp;"

#: glib/gmarkup.c:609
msgid "Unfinished entity reference"
msgstr "未完成的實體參引"

#: glib/gmarkup.c:615
msgid "Unfinished character reference"
msgstr "未完成的字元參引"

#: glib/gmarkup.c:860 glib/gmarkup.c:888 glib/gmarkup.c:919
msgid "Invalid UTF-8 encoded text"
msgstr "無效的 UTF-8 編碼文字"

#: glib/gmarkup.c:955
msgid "Document must begin with an element (e.g. <book>)"
msgstr "文件開始必須為一元素 (例如 <book>)"

#: glib/gmarkup.c:994
#, c-format
msgid ""
"'%s' is not a valid character following a '<' character; it may not begin an "
"element name"
msgstr "‘<’字元後的‘%s’不是有效的字元；這樣不可能是元素名稱的開始部份"

#: glib/gmarkup.c:1057
#, c-format
msgid ""
"Odd character '%s', expected a '>' character to end the start tag of element "
"'%s'"
msgstr "不尋常的字元‘%s’，元素‘%s’的開始標籤應該以‘>’字元結束"

#: glib/gmarkup.c:1144
#, c-format
msgid ""
"Odd character '%s', expected a '=' after attribute name '%s' of element '%s'"
msgstr "不尋常的字元‘%s’，屬性名稱‘%s’(屬於元素‘%s’) 後應該是‘=’字元"

#: glib/gmarkup.c:1185
#, c-format
msgid ""
"Odd character '%s', expected a '>' or '/' character to end the start tag of "
"element '%s', or optionally an attribute; perhaps you used an invalid "
"character in an attribute name"
msgstr ""
"不尋常的字元‘%s’，元素‘%s’的開始標籤應該以‘>’或‘/’字元終結，也可以是屬性；或"
"許您在屬性名稱中使用了無效的字元"

#: glib/gmarkup.c:1268
#, c-format
msgid ""
"Odd character '%s', expected an open quote mark after the equals sign when "
"giving value for attribute '%s' of element '%s'"
msgstr ""
"不尋常的字元‘%s’，當指定屬性‘%s’的值 (屬於元素‘%s’) 時，等號後應該出現開引號"

#: glib/gmarkup.c:1408
#, c-format
msgid ""
"'%s' is not a valid character following the characters '</'; '%s' may not "
"begin an element name"
msgstr "‘</’字元後的‘%s’不是有效的字元；‘%s’不可能是元素名稱的開始部份"

#: glib/gmarkup.c:1446
#, c-format
msgid ""
"'%s' is not a valid character following the close element name '%s'; the "
"allowed character is '>'"
msgstr "字元‘%s’是無效的 (位置在關閉元素‘%s’末端)；允許的字元為‘>’"

#: glib/gmarkup.c:1457
#, c-format
msgid "Element '%s' was closed, no element is currently open"
msgstr "元素‘%s’已關閉，沒有開啟中的元素"

#: glib/gmarkup.c:1466
#, c-format
msgid "Element '%s' was closed, but the currently open element is '%s'"
msgstr "元素‘%s’已關閉，但開啟中的元素是‘%s’"

#: glib/gmarkup.c:1613
msgid "Document was empty or contained only whitespace"
msgstr "文件完全空白或只含有空白字元"

#: glib/gmarkup.c:1627
msgid "Document ended unexpectedly just after an open angle bracket '<'"
msgstr "文件在尖角括號‘<’後突然終止"

#: glib/gmarkup.c:1635 glib/gmarkup.c:1679
#, c-format
msgid ""
"Document ended unexpectedly with elements still open - '%s' was the last "
"element opened"
msgstr "文件在仍然有開啟中的元素時突然終止 ─‘%s’是最後一個開啟的元素"

#: glib/gmarkup.c:1643
#, c-format
msgid ""
"Document ended unexpectedly, expected to see a close angle bracket ending "
"the tag <%s/>"
msgstr "文件突然終止，預期應該出現用來關閉標籤 <%s/> 的尖角括號"

#: glib/gmarkup.c:1649
msgid "Document ended unexpectedly inside an element name"
msgstr "文件在元素的名稱內突然終止"

#: glib/gmarkup.c:1654
msgid "Document ended unexpectedly inside an attribute name"
msgstr "文件在屬性名稱內突然終止"

#: glib/gmarkup.c:1659
msgid "Document ended unexpectedly inside an element-opening tag."
msgstr "文件在元素的開啟標籤內突然終止"

#: glib/gmarkup.c:1665
msgid ""
"Document ended unexpectedly after the equals sign following an attribute "
"name; no attribute value"
msgstr "文件在屬性名稱的等號後突然終止；沒有屬性值"

#: glib/gmarkup.c:1672
msgid "Document ended unexpectedly while inside an attribute value"
msgstr "文件在屬性值內突然終止"

#: glib/gmarkup.c:1687
#, c-format
msgid "Document ended unexpectedly inside the close tag for element '%s'"
msgstr "文件在元素‘%s’的關閉標籤內突然終止"

#: glib/gmarkup.c:1693
msgid "Document ended unexpectedly inside a comment or processing instruction"
msgstr "文件在註解或處理指示內突然終止"

#: glib/gshell.c:72
msgid "Quoted text doesn't begin with a quotation mark"
msgstr "應該用引號括起來的文字不是以括號為開始"

#: glib/gshell.c:162
msgid "Unmatched quotation mark in command line or other shell-quoted text"
msgstr "指令列或其它標為指令的字串內有不對稱的引號"

#: glib/gshell.c:530
#, c-format
msgid "Text ended just after a '\\' character. (The text was '%s')"
msgstr "文字在‘\\’字元後就終止了。(文字為‘%s’)"

#: glib/gshell.c:537
#, c-format
msgid "Text ended before matching quote was found for %c. (The text was '%s')"
msgstr "字串完結前仍沒有對應於 %c 的引號 (字串為‘%s’)"

#: glib/gshell.c:549
msgid "Text was empty (or contained only whitespace)"
msgstr "文字是空白的 (或只含有空白字元)"

#: glib/gspawn-win32.c:286
msgid "Failed to read data from child process"
msgstr "無法由副程序讀取資料"

#: glib/gspawn-win32.c:414
msgid ""
"Unexpected error in g_io_channel_win32_poll() reading data from a child "
"process"
msgstr "當 g_io_channel_win32_poll() 由副程序讀取資料時發生不可預計的錯誤"

#: glib/gspawn-win32.c:784 glib/gspawn.c:979
#, c-format
msgid "Failed to read from child pipe (%s)"
msgstr "無法由副管道讀取資料 (%s)"

#: glib/gspawn-win32.c:871
msgid "Failed to execute helper program"
msgstr "無法執行輔助程式"

#: glib/gspawn-win32.c:904 glib/gspawn.c:1184
#, c-format
msgid "Failed to change to directory '%s' (%s)"
msgstr "無法進入目錄‘%s’(%s)"

#: glib/gspawn-win32.c:913
#, c-format
msgid "Failed to execute child process (%s)"
msgstr "無法執行副程序 (%s)"

#: glib/gspawn-win32.c:956 glib/gspawn.c:1315
#, c-format
msgid "Failed to create pipe for communicating with child process (%s)"
msgstr "無法建立管道來和副程序溝通 (%s)"

#: glib/gspawn.c:167
#, c-format
msgid "Failed to read data from child process (%s)"
msgstr "無法由副程序讀取資料 (%s)"

#: glib/gspawn.c:299
#, c-format
msgid "Unexpected error in select() reading data from a child process (%s)"
msgstr "當 select() 由副程序讀取資料時發生不可預計的錯誤 (%s)"

#: glib/gspawn.c:382
#, c-format
msgid "Unexpected error in waitpid() (%s)"
msgstr "waitpid() 時發生不可預計的錯誤 (%s)"

#: glib/gspawn.c:1044
#, c-format
msgid "Failed to fork (%s)"
msgstr "無法衍生程序 (%s)"

#: glib/gspawn.c:1194
#, c-format
msgid "Failed to execute child process \"%s\" (%s)"
msgstr "無法執行副程序“%s”(%s)"

#: glib/gspawn.c:1204
#, c-format
msgid "Failed to redirect output or input of child process (%s)"
msgstr "無法重新導向副程序的輸出或輸入 (%s)"

#: glib/gspawn.c:1213
#, c-format
msgid "Failed to fork child process (%s)"
msgstr "無法衍生副程序 (%s)"

#: glib/gspawn.c:1221
#, c-format
msgid "Unknown error executing child process \"%s\""
msgstr "執行副程序“%s”時發生不明的錯誤"

#: glib/gspawn.c:1243
#, c-format
msgid "Failed to read enough data from child pid pipe (%s)"
msgstr "無法由副程序 pid 管道讀取足夠的資料 (%s)"

#: glib/gutf8.c:982
msgid "Character out of range for UTF-8"
msgstr "字元不在 UTF-8 範圍之內"

#: glib/gutf8.c:1071 glib/gutf8.c:1080 glib/gutf8.c:1212 glib/gutf8.c:1221
#: glib/gutf8.c:1362 glib/gutf8.c:1458
msgid "Invalid sequence in conversion input"
msgstr "轉換輸入時遇到不正確的次序"

#: glib/gutf8.c:1373 glib/gutf8.c:1469
msgid "Character out of range for UTF-16"
msgstr "字元不在 UTF-16 範圍之內"

#~ msgid "Channel set flags unsupported"
#~ msgstr "不支援設定管道旗標的操作"
