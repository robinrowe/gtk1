# Slovenian translation file for glib.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Andraz Tori <andraz.tori1@guest.arnes.si> 2000
#
msgid ""
msgstr ""
"Project-Id-Version: glib\n"
"POT-Creation-Date: 2003-01-28 17:15-0500\n"
"PO-Revision-Date: 2001-02-16 12:30+0200\n"
"Last-Translator: Andraz Tori <andraz.tori1@guest.arnes.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: glib/gconvert.c:401
#, c-format
msgid "Conversion from character set '%s' to '%s' is not supported"
msgstr "Pretvorba iz nabora znakov '%s' v '%s' ni podprta"

#: glib/gconvert.c:405
#, c-format
msgid "Could not open converter from '%s' to '%s': %s"
msgstr "Nisem mogel odpreti pretvornika iz '%s' v '%s': %s"

#: glib/gconvert.c:603 glib/gconvert.c:893 glib/giochannel.c:1295
#: glib/giochannel.c:1337 glib/giochannel.c:2179 glib/gutf8.c:907
#: glib/gutf8.c:1352
msgid "Invalid byte sequence in conversion input"
msgstr "Neveljavna sekvenca bajtov na vhodu pretvorbe"

#: glib/gconvert.c:608 glib/gconvert.c:824 glib/giochannel.c:1302
#: glib/giochannel.c:2191
#, c-format
msgid "Error during conversion: %s"
msgstr "Napaka med pretvorbo: %s"

#: glib/gconvert.c:626 glib/gutf8.c:903 glib/gutf8.c:1103 glib/gutf8.c:1244
#: glib/gutf8.c:1348
msgid "Partial character sequence at end of input"
msgstr "Delna (nedokončana) sekvenca znakov na koncu vhoda"

#: glib/gconvert.c:799
#, c-format
msgid "Cannot convert fallback '%s' to codeset '%s'"
msgstr "Ne morem pretvoriti '%s' v nabor znakov '%s'"

#: glib/gconvert.c:1633
#, c-format
msgid "The URI '%s' is not an absolute URI using the file scheme"
msgstr "URI '%s' pri uporabi sheme datotek ni absoluten"

#: glib/gconvert.c:1643
#, c-format
msgid "The local file URI '%s' may not include a '#'"
msgstr "Krajevna datoteka URI '%s' ne sme vsebovati '#'"

#: glib/gconvert.c:1660
#, c-format
msgid "The URI '%s' is invalid"
msgstr "URI '%s' je neveljaven"

#: glib/gconvert.c:1672
#, c-format
msgid "The hostname of the URI '%s' is invalid"
msgstr "Ime gostitelja URIja '%s' ni veljavno"

#: glib/gconvert.c:1688
#, c-format
msgid "The URI '%s' contains invalidly escaped characters"
msgstr "URI '%s' vsebuje neveljavne ubežne znake"

#: glib/gconvert.c:1759
#, c-format
msgid "The pathname '%s' is not an absolute path"
msgstr "Pot '%s' ni absolutna pot"

#: glib/gconvert.c:1769
msgid "Invalid hostname"
msgstr "Neveljavno ime gostitelja"

#: glib/gdir.c:80
#, c-format
msgid "Error opening directory '%s': %s"
msgstr "Napaka ob odpiranju imenika '%s': %s"

#: glib/gfileutils.c:384 glib/gfileutils.c:449
#, c-format
msgid "Could not allocate %lu bytes to read file \"%s\""
msgstr "Nisem mogel rezervirati %lu bajtov za branje datoteke \"%s\""

#: glib/gfileutils.c:395
#, c-format
msgid "Error reading file '%s': %s"
msgstr "Napaka ob branju datoteke '%s': %s"

#: glib/gfileutils.c:471
#, c-format
msgid "Failed to read from file '%s': %s"
msgstr "Nisem uspel brati iz datoteke '%s': %s"

#: glib/gfileutils.c:518 glib/gfileutils.c:586
#, c-format
msgid "Failed to open file '%s': %s"
msgstr "Nisem uspel odpreti datoteke '%s': %s"

#: glib/gfileutils.c:532
#, c-format
msgid "Failed to get attributes of file '%s': fstat() failed: %s"
msgstr "Nisem uspel dobiti atributov datoteke '%s': fstat() ni uspel: %s"

#: glib/gfileutils.c:558
#, c-format
msgid "Failed to open file '%s': fdopen() failed: %s"
msgstr "Nisem uspel odpreti datoteke '%s': fdopen() ni uspel: %s"

#: glib/gfileutils.c:765
#, c-format
msgid "Template '%s' invalid, should not contain a '%s'"
msgstr "Šablona '%s' je neveljavna, ne bi smela vsebovati '%s'"

#: glib/gfileutils.c:777
#, c-format
msgid "Template '%s' doesn't end with XXXXXX"
msgstr "Šablona '%s' se ne konča z XXXXXX"

#: glib/gfileutils.c:798
#, c-format
msgid "Failed to create file '%s': %s"
msgstr "Nisem uspel ustvariti datoteke '%s' %s"

#: glib/giochannel.c:1123
#, c-format
msgid "Conversion from character set `%s' to `%s' is not supported"
msgstr "Pretvorba iz nabora znakov `%s' v `%s' ni podprta"

#: glib/giochannel.c:1127
#, c-format
msgid "Could not open converter from `%s' to `%s': %s"
msgstr "Nisem mogel odpreti pretvornika iz `%s' v `%s': %s"

#: glib/giochannel.c:1472
msgid "Can't do a raw read in g_io_channel_read_line_string"
msgstr "Ne morem narediti surovega branja v g_io_channel_read_line_string"

#: glib/giochannel.c:1519 glib/giochannel.c:1776 glib/giochannel.c:1862
msgid "Leftover unconverted data in read buffer"
msgstr "Preostanek nepretvorjenih podatkov v bralnem medpomnilniku"

#: glib/giochannel.c:1599 glib/giochannel.c:1676
msgid "Channel terminates in a partial character"
msgstr "Kanal se je ustavil v delnem znaku"

#: glib/giochannel.c:1662
msgid "Can't do a raw read in g_io_channel_read_to_end"
msgstr "Ne morem narediti surovega branja v g_io_channel_read_to_end"

#: glib/giowin32.c:712 glib/giowin32.c:741
msgid "Incorrect message size"
msgstr "Neveljavna velikost sporočila"

#: glib/giowin32.c:1034 glib/giowin32.c:1087
msgid "Socket error"
msgstr "Napaka vtiča"

#: glib/gmarkup.c:222
#, c-format
msgid "Error on line %d char %d: %s"
msgstr "Napaka v vrstici %d, znak %d: %s"

#: glib/gmarkup.c:306
#, c-format
msgid "Error on line %d: %s"
msgstr "Napaka v vrstici %d: %s"

#: glib/gmarkup.c:385
msgid ""
"Empty entity '&;' seen; valid entities are: &amp; &quot; &lt; &gt; &apos;"
msgstr ""
"Viden prazna entiteta '&;'; veljavne entitete so: &amp; &quot; &lt; &gt; "
"&apos;"

#: glib/gmarkup.c:395
#, c-format
msgid ""
"Character '%s' is not valid at the start of an entity name; the & character "
"begins an entity; if this ampersand isn't supposed to be an entity, escape "
"it as &amp;"
msgstr ""
"Znak '%s' ni veljaven na začetku imena entitete; znak & začne entiteto; če "
"ta znak ni mišljen kot entiteta ga napišite kot  &amp;"

#: glib/gmarkup.c:431
#, c-format
msgid "Character '%s' is not valid inside an entity name"
msgstr "Znak '%s' ni veljaven znotraj imena entitete"

#: glib/gmarkup.c:475
#, c-format
msgid "Entity name '%s' is not known"
msgstr "Ime entitete '%s' ni poznano"

#: glib/gmarkup.c:485
msgid ""
"Entity did not end with a semicolon; most likely you used an ampersand "
"character without intending to start an entity - escape ampersand as &amp;"
msgstr ""
"Entiteta se ni končala s podpičjem; verjetno ste uporabili znak '&' "
"breznamena, da bi začeli entiteto - znak '&' napišite kot '&amp;'"

#: glib/gmarkup.c:531
#, c-format
msgid ""
"Failed to parse '%s', which should have been a digit inside a character "
"reference (&#234; for example) - perhaps the digit is too large"
msgstr ""
"Nisem uspel razčleniti '%s', ki bi morala biti številka znotraj reference "
"znaka (na primer &#234;) - mogoče je številka prevelika"

#: glib/gmarkup.c:556
#, c-format
msgid "Character reference '%s' does not encode a permitted character"
msgstr "Referenca znaka '%s' ne predstavlja dovoljenega znaka"

#: glib/gmarkup.c:573
msgid "Empty character reference; should include a digit such as &#454;"
msgstr "Prazna referenca znaka; vsebovati bi morala številko kot &#454;"

#: glib/gmarkup.c:583
msgid ""
"Character reference did not end with a semicolon; most likely you used an "
"ampersand character without intending to start an entity - escape ampersand "
"as &amp;"
msgstr ""
"Referenca znaka se ni končala s podpičjem; verjetno ste uporabili znak '&' "
"brez namen, da bi začeli entiteto - znak '&' napišite kot '&amp;'"

#: glib/gmarkup.c:609
msgid "Unfinished entity reference"
msgstr "Nedokončano nanašanje na entiteto"

#: glib/gmarkup.c:615
msgid "Unfinished character reference"
msgstr "Nedokončano nanašanje na znak"

#: glib/gmarkup.c:860 glib/gmarkup.c:888 glib/gmarkup.c:919
msgid "Invalid UTF-8 encoded text"
msgstr "Napačno kodirano besedilo UTF-8"

#: glib/gmarkup.c:955
msgid "Document must begin with an element (e.g. <book>)"
msgstr "Dokument se mora začeti z elementom (na primer <book>)"

#: glib/gmarkup.c:994
#, c-format
msgid ""
"'%s' is not a valid character following a '<' character; it may not begin an "
"element name"
msgstr ""
"'%s' ni veljaven znak, kadar sledi znaku '<'; morda se ne začne z imenom "
"elementa"

#: glib/gmarkup.c:1057
#, c-format
msgid ""
"Odd character '%s', expected a '>' character to end the start tag of element "
"'%s'"
msgstr "Čuden znak '%s', pričakovan znak '>', da zaključi oznako elementa '%s'"

#: glib/gmarkup.c:1144
#, c-format
msgid ""
"Odd character '%s', expected a '=' after attribute name '%s' of element '%s'"
msgstr ""
"Čuden znak '%s'. Po imenu atributa '%s' (elementa '%s') je pričakovan znak "
"'='"

#: glib/gmarkup.c:1185
#, c-format
msgid ""
"Odd character '%s', expected a '>' or '/' character to end the start tag of "
"element '%s', or optionally an attribute; perhaps you used an invalid "
"character in an attribute name"
msgstr ""
"Čuden znak '%s'. Pričakovan znak '>' ali '/', ki bi zaključil oznako "
"elementa '%s' ali atribut; morda ste uporabili neveljaven znak v imenu "
"atributa'"

#: glib/gmarkup.c:1268
#, c-format
msgid ""
"Odd character '%s', expected an open quote mark after the equals sign when "
"giving value for attribute '%s' of element '%s'"
msgstr ""
"Čuden znak '%s'. Za enačajem je pričakovan narekovaj znotraj katerega je "
"podana vrednost za atribut '%s' elementa '%s'"

#: glib/gmarkup.c:1408
#, c-format
msgid ""
"'%s' is not a valid character following the characters '</'; '%s' may not "
"begin an element name"
msgstr ""
"'%s' ni veljaven znak za znakoma '</'; ime elementa se ne sme začeti z '%s'"

#: glib/gmarkup.c:1446
#, c-format
msgid ""
"'%s' is not a valid character following the close element name '%s'; the "
"allowed character is '>'"
msgstr ""
"Znak '%s' ni veljaven kadar sledi zaprtju imena elementa '%s'; dovoljen znak "
"je '>'"

#: glib/gmarkup.c:1457
#, c-format
msgid "Element '%s' was closed, no element is currently open"
msgstr "Element '%s' je bil zaprt, trenutno ni odprtega elementa"

#: glib/gmarkup.c:1466
#, c-format
msgid "Element '%s' was closed, but the currently open element is '%s'"
msgstr "Element '%s' je bil zaprt, a trenutno odprt element je '%s'"

#: glib/gmarkup.c:1613
msgid "Document was empty or contained only whitespace"
msgstr "Dokument je bil prazen ali pa je vseboval le presledke"

#: glib/gmarkup.c:1627
msgid "Document ended unexpectedly just after an open angle bracket '<'"
msgstr "Dokument nepričakovano končan takoj za odprtjem z '<'"

#: glib/gmarkup.c:1635 glib/gmarkup.c:1679
#, c-format
msgid ""
"Document ended unexpectedly with elements still open - '%s' was the last "
"element opened"
msgstr ""
"Dokument nepričakovano končan s še odprtimi elementi - '%s' je bil zadnji "
"odprt element"

#: glib/gmarkup.c:1643
#, c-format
msgid ""
"Document ended unexpectedly, expected to see a close angle bracket ending "
"the tag <%s/>"
msgstr ""
"Dokument nepričakovano končan, pričakovan je bil zaključni zaklepaj oznake <%"
"s/>"

#: glib/gmarkup.c:1649
msgid "Document ended unexpectedly inside an element name"
msgstr "Dokument nepričakovano končan sredi imena elementa"

#: glib/gmarkup.c:1654
msgid "Document ended unexpectedly inside an attribute name"
msgstr "Dokument nepričakovano končan sredi imena atributa"

#: glib/gmarkup.c:1659
msgid "Document ended unexpectedly inside an element-opening tag."
msgstr "Dokument nepričakovano končan sredi oznake za odprtje elementa."

#: glib/gmarkup.c:1665
msgid ""
"Document ended unexpectedly after the equals sign following an attribute "
"name; no attribute value"
msgstr ""
"Dokumen nepričakovano končan po enečaju, ki je sledil imenu atributa; ni "
"vrednosti atributa"

#: glib/gmarkup.c:1672
msgid "Document ended unexpectedly while inside an attribute value"
msgstr "Dokument nepričakovano končan sredi vrednosti atributa"

#: glib/gmarkup.c:1687
#, c-format
msgid "Document ended unexpectedly inside the close tag for element '%s'"
msgstr "Dokument nepričakovano končan sredi oznake zaprtja elementa '%s'"

#: glib/gmarkup.c:1693
msgid "Document ended unexpectedly inside a comment or processing instruction"
msgstr "Dokument nepričakovano končan sredi komentarja ali ukaza"

#: glib/gshell.c:72
msgid "Quoted text doesn't begin with a quotation mark"
msgstr "Citirano besedilo se ne začne z narekovajem"

#: glib/gshell.c:162
msgid "Unmatched quotation mark in command line or other shell-quoted text"
msgstr "V ukazni vrstici ali v citiranem besedilu manjka končni narekovaj"

#: glib/gshell.c:530
#, c-format
msgid "Text ended just after a '\\' character. (The text was '%s')"
msgstr "Besedilo končano takoj po znaku '\\'. (Besedilo je bilo '%s')"

#: glib/gshell.c:537
#, c-format
msgid "Text ended before matching quote was found for %c. (The text was '%s')"
msgstr ""
"Besedilo končano preden je bil najden zaključni narekovaj za %c. (besedilo "
"je bilo '%s')"

#: glib/gshell.c:549
msgid "Text was empty (or contained only whitespace)"
msgstr "Besedilo je bilo prazno (ali vsebovalo le presledke)"

#: glib/gspawn-win32.c:286
msgid "Failed to read data from child process"
msgstr "Nisem uspel prebrati podatkov iz procesa otroka"

#: glib/gspawn-win32.c:414
msgid ""
"Unexpected error in g_io_channel_win32_poll() reading data from a child "
"process"
msgstr ""
"Nepričakovana napaka v g_io_channel_win32_poll() med branjem podatkov "
"procesa otroka"

#: glib/gspawn-win32.c:784 glib/gspawn.c:979
#, c-format
msgid "Failed to read from child pipe (%s)"
msgstr "Nisem uspel brati iz cevi otroka (%s)"

#: glib/gspawn-win32.c:871
msgid "Failed to execute helper program"
msgstr "Nisem uspel izvesti pomožnega programa "

#: glib/gspawn-win32.c:904 glib/gspawn.c:1184
#, c-format
msgid "Failed to change to directory '%s' (%s)"
msgstr "Nisem uspel spremeniti imenika v '%s' (%s)"

#: glib/gspawn-win32.c:913
#, c-format
msgid "Failed to execute child process (%s)"
msgstr "Nisem uspel izvesti procesa otroka (%s)"

#: glib/gspawn-win32.c:956 glib/gspawn.c:1315
#, c-format
msgid "Failed to create pipe for communicating with child process (%s)"
msgstr "Nisem uspel ustvariti cevi za komunikacijo s procesom otroka (%s)"

#: glib/gspawn.c:167
#, c-format
msgid "Failed to read data from child process (%s)"
msgstr "Nisem uspel prebrati podatkov iz procesa otroka (%s)"

#: glib/gspawn.c:299
#, c-format
msgid "Unexpected error in select() reading data from a child process (%s)"
msgstr ""
"Nepričakovana napaka v select() med branjem podatkov procesa otroka (%s)"

#: glib/gspawn.c:382
#, c-format
msgid "Unexpected error in waitpid() (%s)"
msgstr "Nepričakovana napaka v waitpid() (%s)"

#: glib/gspawn.c:1044
#, c-format
msgid "Failed to fork (%s)"
msgstr "Nisem se uspel razvejiti (%s)"

#: glib/gspawn.c:1194
#, c-format
msgid "Failed to execute child process \"%s\" (%s)"
msgstr "Nisem uspel izvesti procesa otroka \"%s\" (%s)"

#: glib/gspawn.c:1204
#, c-format
msgid "Failed to redirect output or input of child process (%s)"
msgstr "Nisem uspel preusmeriti vhoda ali izhoda procesa otroka (%s)"

#: glib/gspawn.c:1213
#, c-format
msgid "Failed to fork child process (%s)"
msgstr "Nisem uspel razvejiti procesa otroka (%s)"

#: glib/gspawn.c:1221
#, c-format
msgid "Unknown error executing child process \"%s\""
msgstr "Neznana napaka med izvajanjem procesa otroka \"%s\""

#: glib/gspawn.c:1243
#, c-format
msgid "Failed to read enough data from child pid pipe (%s)"
msgstr "Nisem uspel prebrati dovolj podatkov iz cevi otroka (%s)"

#: glib/gutf8.c:982
msgid "Character out of range for UTF-8"
msgstr "Znak izven intervala za UTF-8"

#: glib/gutf8.c:1071 glib/gutf8.c:1080 glib/gutf8.c:1212 glib/gutf8.c:1221
#: glib/gutf8.c:1362 glib/gutf8.c:1458
msgid "Invalid sequence in conversion input"
msgstr "Neveljavna sekvenca na vhodu pretvorbe"

#: glib/gutf8.c:1373 glib/gutf8.c:1469
msgid "Character out of range for UTF-16"
msgstr "Znak izven intervala za UTF-16"

#~ msgid "Channel set flags unsupported"
#~ msgstr "Nastavitev zastavic kanala ni podprta"
