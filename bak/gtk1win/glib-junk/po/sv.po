# Swedish messages for glib.
# Copyright (C) 2001, 2002 Free Software Foundation, Inc.
# Christian Rose <menthos@menthos.com>, 2001, 2002.
#
# $Id: sv.po,v 1.35 2002/12/20 15:23:11 owen Exp $
#
msgid ""
msgstr ""
"Project-Id-Version: glib\n"
"POT-Creation-Date: 2003-01-28 17:15-0500\n"
"PO-Revision-Date: 2002-11-26 20:54+0100\n"
"Last-Translator: Christian Rose <menthos@menthos.com>\n"
"Language-Team: Swedish <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: glib/gconvert.c:401
#, c-format
msgid "Conversion from character set '%s' to '%s' is not supported"
msgstr "Konvertering från teckentabellen \"%s\" till \"%s\" stöds inte"

#: glib/gconvert.c:405
#, c-format
msgid "Could not open converter from '%s' to '%s': %s"
msgstr "Kunde inte öppna konverteraren från \"%s\" till \"%s\": %s"

#: glib/gconvert.c:603 glib/gconvert.c:893 glib/giochannel.c:1295
#: glib/giochannel.c:1337 glib/giochannel.c:2179 glib/gutf8.c:907
#: glib/gutf8.c:1352
msgid "Invalid byte sequence in conversion input"
msgstr "Ogiltig bytesekvens i konverteringsindata"

#: glib/gconvert.c:608 glib/gconvert.c:824 glib/giochannel.c:1302
#: glib/giochannel.c:2191
#, c-format
msgid "Error during conversion: %s"
msgstr "Fel vid konvertering: %s"

#: glib/gconvert.c:626 glib/gutf8.c:903 glib/gutf8.c:1103 glib/gutf8.c:1244
#: glib/gutf8.c:1348
msgid "Partial character sequence at end of input"
msgstr "Ofullständig teckensekvens vid slutet av indata"

# fallback syftar på en sträng
#: glib/gconvert.c:799
#, c-format
msgid "Cannot convert fallback '%s' to codeset '%s'"
msgstr "Kan inte konvertera reservsträngen \"%s\" till kodningen \"%s\""

#: glib/gconvert.c:1633
#, c-format
msgid "The URI '%s' is not an absolute URI using the file scheme"
msgstr "URI:n \"%s\" är ingen absolut URI som använder filschemat"

#: glib/gconvert.c:1643
#, c-format
msgid "The local file URI '%s' may not include a '#'"
msgstr "Lokala fil-URI:n \"%s\" får inte innehålla en \"#\""

#: glib/gconvert.c:1660
#, c-format
msgid "The URI '%s' is invalid"
msgstr "URI:n \"%s\" är ogiltig"

#: glib/gconvert.c:1672
#, c-format
msgid "The hostname of the URI '%s' is invalid"
msgstr "Värdnamnet i URI:n \"%s\" är ogiltigt"

#: glib/gconvert.c:1688
#, c-format
msgid "The URI '%s' contains invalidly escaped characters"
msgstr "URI:n \"%s\" innehåller ogiltigt kodade tecken"

#: glib/gconvert.c:1759
#, c-format
msgid "The pathname '%s' is not an absolute path"
msgstr "Sökvägen \"%s\" är ingen absolut sökväg"

#: glib/gconvert.c:1769
msgid "Invalid hostname"
msgstr "Ogiltigt värdnamn"

#: glib/gdir.c:80
#, c-format
msgid "Error opening directory '%s': %s"
msgstr "Fel vid öppning av katalogen \"%s\": %s"

#: glib/gfileutils.c:384 glib/gfileutils.c:449
#, c-format
msgid "Could not allocate %lu bytes to read file \"%s\""
msgstr "Kunde inte allokera %lu byte för att läsa filen \"%s\""

#: glib/gfileutils.c:395
#, c-format
msgid "Error reading file '%s': %s"
msgstr "Fel vid läsning av filen \"%s\": %s"

#: glib/gfileutils.c:471
#, c-format
msgid "Failed to read from file '%s': %s"
msgstr "Misslyckades med att läsa från filen \"%s\": %s"

#: glib/gfileutils.c:518 glib/gfileutils.c:586
#, c-format
msgid "Failed to open file '%s': %s"
msgstr "Misslyckades med att öppna filen \"%s\": %s"

#: glib/gfileutils.c:532
#, c-format
msgid "Failed to get attributes of file '%s': fstat() failed: %s"
msgstr ""
"Misslyckades med att få tag på attributen på filen \"%s\": fstat() "
"misslyckades: %s"

#: glib/gfileutils.c:558
#, c-format
msgid "Failed to open file '%s': fdopen() failed: %s"
msgstr "Misslyckades med att öppna filen \"%s\": fdopen() misslyckades: %s"

#: glib/gfileutils.c:765
#, c-format
msgid "Template '%s' invalid, should not contain a '%s'"
msgstr "Mallen \"%s\" är ogiltig, den får inte innehålla ett \"%s\""

#: glib/gfileutils.c:777
#, c-format
msgid "Template '%s' doesn't end with XXXXXX"
msgstr "Mallen \"%s\" slutar inte med XXXXXX"

#: glib/gfileutils.c:798
#, c-format
msgid "Failed to create file '%s': %s"
msgstr "Misslyckades med att skapa filen \"%s\": %s"

#: glib/giochannel.c:1123
#, c-format
msgid "Conversion from character set `%s' to `%s' is not supported"
msgstr "Konvertering från teckentabellen \"%s\" till \"%s\" stöds inte"

#: glib/giochannel.c:1127
#, c-format
msgid "Could not open converter from `%s' to `%s': %s"
msgstr "Kunde inte öppna konverteraren från \"%s\" till \"%s\": %s"

#: glib/giochannel.c:1472
msgid "Can't do a raw read in g_io_channel_read_line_string"
msgstr "Kan inte göra en rå läsning i g_io_channel_read_line_string"

#: glib/giochannel.c:1519 glib/giochannel.c:1776 glib/giochannel.c:1862
msgid "Leftover unconverted data in read buffer"
msgstr "Överbliven okonverterad data i läsbufferten"

#: glib/giochannel.c:1599 glib/giochannel.c:1676
msgid "Channel terminates in a partial character"
msgstr "Kanalen slutar med ett ofullständigt tecken"

#: glib/giochannel.c:1662
msgid "Can't do a raw read in g_io_channel_read_to_end"
msgstr "Kan inte göra en rå läsning i g_io_channel_read_to_end"

#: glib/giowin32.c:712 glib/giowin32.c:741
msgid "Incorrect message size"
msgstr "Felaktig meddelandestorlek"

#: glib/giowin32.c:1034 glib/giowin32.c:1087
msgid "Socket error"
msgstr "Uttagsfel"

#: glib/gmarkup.c:222
#, c-format
msgid "Error on line %d char %d: %s"
msgstr "Fel på rad %d kolumn %d: %s"

#: glib/gmarkup.c:306
#, c-format
msgid "Error on line %d: %s"
msgstr "Fel på rad %d: %s"

#: glib/gmarkup.c:385
msgid ""
"Empty entity '&;' seen; valid entities are: &amp; &quot; &lt; &gt; &apos;"
msgstr ""
"Tom entitet \"&;\" hittades, giltiga entiteter är: &amp; &quot; &lt; &gt; "
"&apos;"

#: glib/gmarkup.c:395
#, c-format
msgid ""
"Character '%s' is not valid at the start of an entity name; the & character "
"begins an entity; if this ampersand isn't supposed to be an entity, escape "
"it as &amp;"
msgstr ""
"Tecknet \"%s\" är inte giltigt i början på ett entitetsnamn; tecknet & "
"inleder en entitet. Om detta &-tecken inte ska vara en entitet måste du "
"skriva om det som &amp;."

#: glib/gmarkup.c:431
#, c-format
msgid "Character '%s' is not valid inside an entity name"
msgstr "Tecknet \"%s\" är inte giltigt inuti ett entitetsnamn"

#: glib/gmarkup.c:475
#, c-format
msgid "Entity name '%s' is not known"
msgstr "Entitetsnamnet \"%s\" är okänt"

#: glib/gmarkup.c:485
msgid ""
"Entity did not end with a semicolon; most likely you used an ampersand "
"character without intending to start an entity - escape ampersand as &amp;"
msgstr ""
"Entiteten slutade inte med ett semikolon. Troligtvis använde du ett &-tecken "
"utan att avse att starta en entitet. Skriv om &-tecknet som &amp;"

#: glib/gmarkup.c:531
#, c-format
msgid ""
"Failed to parse '%s', which should have been a digit inside a character "
"reference (&#234; for example) - perhaps the digit is too large"
msgstr ""
"Misslyckades med att tolka \"%s\", som skulle ha varit ett tal inuti en "
"teckenreferens (&#234; till exempel). Talet är kanske för stort"

#: glib/gmarkup.c:556
#, c-format
msgid "Character reference '%s' does not encode a permitted character"
msgstr "Teckenreferensen \"%s\" innehåller inte koden för ett tillåtet tecken"

#: glib/gmarkup.c:573
msgid "Empty character reference; should include a digit such as &#454;"
msgstr "Tom teckenreferens, måste innehålla ett tal som exempelvis &#454;"

#: glib/gmarkup.c:583
msgid ""
"Character reference did not end with a semicolon; most likely you used an "
"ampersand character without intending to start an entity - escape ampersand "
"as &amp;"
msgstr ""
"Teckenreferensen slutade inte med ett semikolon. Troligtvis använde du ett &-"
"tecken utan att avse att starta en entitet. Skriv om &-tecknet som &amp;"

#: glib/gmarkup.c:609
msgid "Unfinished entity reference"
msgstr "Oavslutad entitetsreferens"

#: glib/gmarkup.c:615
msgid "Unfinished character reference"
msgstr "Oavslutad teckenreferens"

#: glib/gmarkup.c:860 glib/gmarkup.c:888 glib/gmarkup.c:919
msgid "Invalid UTF-8 encoded text"
msgstr "Ogiltigt UTF-8-kodad text"

#: glib/gmarkup.c:955
msgid "Document must begin with an element (e.g. <book>)"
msgstr "Dokumentet måste börja med ett element (exempelvis <book>)"

#: glib/gmarkup.c:994
#, c-format
msgid ""
"'%s' is not a valid character following a '<' character; it may not begin an "
"element name"
msgstr ""
"\"%s\" är inte ett giltigt tecken efter ett \"<\"-tecken. Det får inte "
"inleda ett elementnamn"

#: glib/gmarkup.c:1057
#, c-format
msgid ""
"Odd character '%s', expected a '>' character to end the start tag of element "
"'%s'"
msgstr ""
"Konstigt tecken \"%s\", ett \">\"-tecken förväntades för att avsluta "
"starttaggen för elementet \"%s\""

#: glib/gmarkup.c:1144
#, c-format
msgid ""
"Odd character '%s', expected a '=' after attribute name '%s' of element '%s'"
msgstr ""
"Konstigt tecken \"%s\", ett \"=\" förväntades efter attributnamnet \"%s\" "
"till elementet \"%s\""

#: glib/gmarkup.c:1185
#, c-format
msgid ""
"Odd character '%s', expected a '>' or '/' character to end the start tag of "
"element '%s', or optionally an attribute; perhaps you used an invalid "
"character in an attribute name"
msgstr ""
"Konstigt tecken \"%s\", ett \">\"- eller \"/\"-tecken förväntades för att "
"avsluta starttaggen för elementet \"%s\", eller möjligtvis ett attribut. Du "
"kanske använde ett ogiltigt tecken i ett attributnamn"

#: glib/gmarkup.c:1268
#, c-format
msgid ""
"Odd character '%s', expected an open quote mark after the equals sign when "
"giving value for attribute '%s' of element '%s'"
msgstr ""
"Konstigt tecken \"%s\", ett startcitationstecken förväntades efter "
"likhetstecknet när värdet av attributet \"%s\" till elementet \"%s\" "
"tilldelades"

#: glib/gmarkup.c:1408
#, c-format
msgid ""
"'%s' is not a valid character following the characters '</'; '%s' may not "
"begin an element name"
msgstr ""
"\"%s\" är inte ett giltigt tecken efter tecknen \"</\". \"%s\" får inte "
"inleda ett elementnamn"

#: glib/gmarkup.c:1446
#, c-format
msgid ""
"'%s' is not a valid character following the close element name '%s'; the "
"allowed character is '>'"
msgstr ""
"\"%s\" är inte ett giltigt tecken efter stängelementnamnet \"%s\". Det "
"tillåtna tecknet är \">\""

#: glib/gmarkup.c:1457
#, c-format
msgid "Element '%s' was closed, no element is currently open"
msgstr "Elementet \"%s\" stängdes, inget element är öppet för tillfället"

#: glib/gmarkup.c:1466
#, c-format
msgid "Element '%s' was closed, but the currently open element is '%s'"
msgstr ""
"Elementet \"%s\" stängdes, men det element som är öppet för tillfället är \"%"
"s\""

#: glib/gmarkup.c:1613
msgid "Document was empty or contained only whitespace"
msgstr "Dokumentet var tomt eller innehöll endast tomrum"

#: glib/gmarkup.c:1627
msgid "Document ended unexpectedly just after an open angle bracket '<'"
msgstr "Dokumentet tog oväntat slut efter ett öppningsklammer \"<\""

#: glib/gmarkup.c:1635 glib/gmarkup.c:1679
#, c-format
msgid ""
"Document ended unexpectedly with elements still open - '%s' was the last "
"element opened"
msgstr ""
"Dokumentet tog oväntat slut då element fortfarande var öppna. \"%s\" var det "
"senast öppnade elementet"

#: glib/gmarkup.c:1643
#, c-format
msgid ""
"Document ended unexpectedly, expected to see a close angle bracket ending "
"the tag <%s/>"
msgstr ""
"Dokumentet tog oväntat slut, en stängningsklammer föräntades för att avsluta "
"taggen <%s/>"

#: glib/gmarkup.c:1649
msgid "Document ended unexpectedly inside an element name"
msgstr "Dokumentet tog oväntat slut inuti ett elementnamn"

#: glib/gmarkup.c:1654
msgid "Document ended unexpectedly inside an attribute name"
msgstr "Dokumentet tog oväntat slut inuti ett attributnamn"

#: glib/gmarkup.c:1659
msgid "Document ended unexpectedly inside an element-opening tag."
msgstr "Dokumentet tog oväntat slut inuti en elementöppnande tagg."

#: glib/gmarkup.c:1665
msgid ""
"Document ended unexpectedly after the equals sign following an attribute "
"name; no attribute value"
msgstr ""
"Dokumentet tog oväntat slut efter likhetstecknet som följde ett "
"attributnamn. Inget attributvärde"

#: glib/gmarkup.c:1672
msgid "Document ended unexpectedly while inside an attribute value"
msgstr "Dokumentet tog oväntat slut inuti ett attributvärde"

#: glib/gmarkup.c:1687
#, c-format
msgid "Document ended unexpectedly inside the close tag for element '%s'"
msgstr ""
"Dokumentet tog oväntat slut inuti stängningstaggen för elementet \"%s\""

#: glib/gmarkup.c:1693
msgid "Document ended unexpectedly inside a comment or processing instruction"
msgstr ""
"Dokumentet tog oväntat slut inuti en kommentar eller behandlingsinstruktion"

#: glib/gshell.c:72
msgid "Quoted text doesn't begin with a quotation mark"
msgstr "Citerad text börjar inte med citationstecken"

#: glib/gshell.c:162
msgid "Unmatched quotation mark in command line or other shell-quoted text"
msgstr "Ensamt citationstecken på kommandoraden eller annan skalciterad text"

#: glib/gshell.c:530
#, c-format
msgid "Text ended just after a '\\' character. (The text was '%s')"
msgstr "Texten slutade efter ett \"\\\"-tecken (texten var \"%s\")."

#: glib/gshell.c:537
#, c-format
msgid "Text ended before matching quote was found for %c. (The text was '%s')"
msgstr ""
"Texten slutade innan matchande citationstecken hittades för %c (texten var "
"\"%s\")."

#: glib/gshell.c:549
msgid "Text was empty (or contained only whitespace)"
msgstr "Texten var tom (eller innehöll bara tomrum)"

#: glib/gspawn-win32.c:286
msgid "Failed to read data from child process"
msgstr "Misslyckades med att läsa data från barnprocessen"

#: glib/gspawn-win32.c:414
msgid ""
"Unexpected error in g_io_channel_win32_poll() reading data from a child "
"process"
msgstr ""
"Oväntat fel i g_io_channel_win32_poll() vid inläsning av data från en "
"barnprocess"

#: glib/gspawn-win32.c:784 glib/gspawn.c:979
#, c-format
msgid "Failed to read from child pipe (%s)"
msgstr "Misslyckades med att läsa från rör till barn (%s)"

#: glib/gspawn-win32.c:871
msgid "Failed to execute helper program"
msgstr "Misslyckades med att köra hjälparprogram"

#: glib/gspawn-win32.c:904 glib/gspawn.c:1184
#, c-format
msgid "Failed to change to directory '%s' (%s)"
msgstr "Misslyckades med att byta till katalogen \"%s\" (%s)"

#: glib/gspawn-win32.c:913
#, c-format
msgid "Failed to execute child process (%s)"
msgstr "Misslyckades med att köra barnprocess (%s)"

#: glib/gspawn-win32.c:956 glib/gspawn.c:1315
#, c-format
msgid "Failed to create pipe for communicating with child process (%s)"
msgstr "Misslyckades med att skapa rör för kommunikation med barnprocess (%s)"

#: glib/gspawn.c:167
#, c-format
msgid "Failed to read data from child process (%s)"
msgstr "Misslyckades med att läsa data från barnprocess (%s)"

#: glib/gspawn.c:299
#, c-format
msgid "Unexpected error in select() reading data from a child process (%s)"
msgstr "Oväntat fel i select() vid läsning av data från en barnprocess (%s)"

#: glib/gspawn.c:382
#, c-format
msgid "Unexpected error in waitpid() (%s)"
msgstr "Oväntat fel i waitpid() (%s)"

#: glib/gspawn.c:1044
#, c-format
msgid "Failed to fork (%s)"
msgstr "Misslyckades med att grena (%s)"

#: glib/gspawn.c:1194
#, c-format
msgid "Failed to execute child process \"%s\" (%s)"
msgstr "Misslyckades med att köra barnprocessen \"%s\" (%s)"

#: glib/gspawn.c:1204
#, c-format
msgid "Failed to redirect output or input of child process (%s)"
msgstr ""
"Misslyckades med att dirigera om utdata eller indata från barnprocess (%s)"

#: glib/gspawn.c:1213
#, c-format
msgid "Failed to fork child process (%s)"
msgstr "Misslyckades med att skapa barnprocess (%s)"

#: glib/gspawn.c:1221
#, c-format
msgid "Unknown error executing child process \"%s\""
msgstr "Okänt fel vid körning av barnprocessen \"%s\""

#: glib/gspawn.c:1243
#, c-format
msgid "Failed to read enough data from child pid pipe (%s)"
msgstr ""
"Misslyckades med att läsa tillräckligt med data från röret till barnets pid "
"(%s)"

#: glib/gutf8.c:982
msgid "Character out of range for UTF-8"
msgstr "Tecknet är utanför intervallet för UTF-8"

#: glib/gutf8.c:1071 glib/gutf8.c:1080 glib/gutf8.c:1212 glib/gutf8.c:1221
#: glib/gutf8.c:1362 glib/gutf8.c:1458
msgid "Invalid sequence in conversion input"
msgstr "Ogiltig sekvens i konverteringsindata"

#: glib/gutf8.c:1373 glib/gutf8.c:1469
msgid "Character out of range for UTF-16"
msgstr "Tecknet är utanför intervallet för UTF-16"

#~ msgid "Channel set flags unsupported"
#~ msgstr "Kanalinställningsflaggor stöds inte"

#~ msgid ""
#~ "The hostname of the URI `%s' is contains invalidly escaped characters"
#~ msgstr "Värdnamnet för URI:n \"%s\" innehåller felaktigt inbäddade tecken"

#~ msgid "The URI `%s' is contains invalidly escaped characters"
#~ msgstr "URI:n \"%s\" innehåller ogiltigt inbäddade tecken"
