# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# 辛立仁 <hsin@med.cgu.edu.tw>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: gtk+ 1.1.7 \n"
"POT-Creation-Date: 2003-01-15 06:53+0000\n"
"PO-Revision-Date: 1999-01-31 19:33+0800\n"
"Last-Translator: 辛立仁 <hsin@med.cgu.edu.tw>\n"
"Language-Team: Chinese <zh@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=big5\n"
"Content-Transfer-Encoding: 8bit\n"

#: gtk/gtkcolorsel.c:221
msgid "Hue:"
msgstr "色相:"

#: gtk/gtkcolorsel.c:222
msgid "Saturation:"
msgstr "彩度:"

#: gtk/gtkcolorsel.c:223
msgid "Value:"
msgstr "明度:"

#: gtk/gtkcolorsel.c:224
msgid "Red:"
msgstr "紅:"

#: gtk/gtkcolorsel.c:225
msgid "Green:"
msgstr "綠:"

#: gtk/gtkcolorsel.c:226
msgid "Blue:"
msgstr "藍:"

#: gtk/gtkcolorsel.c:227
msgid "Opacity:"
msgstr "不透明度:"

#. The OK button
#: gtk/gtkcolorsel.c:1727 gtk/gtkfilesel.c:588 gtk/gtkfontsel.c:3500
#: gtk/gtkgamma.c:415
msgid "OK"
msgstr "確定"

#. The Cancel button
#: gtk/gtkcolorsel.c:1733 gtk/gtkfilesel.c:595 gtk/gtkfilesel.c:987
#: gtk/gtkfilesel.c:1094 gtk/gtkfilesel.c:1216 gtk/gtkfontsel.c:3513
#: gtk/gtkgamma.c:423
msgid "Cancel"
msgstr "取消"

#: gtk/gtkcolorsel.c:1738
msgid "Help"
msgstr "求助"

#. The directories clist
#: gtk/gtkfilesel.c:537
msgid "Directories"
msgstr "目錄"

#. The files clist
#: gtk/gtkfilesel.c:556
msgid "Files"
msgstr "檔案"

#: gtk/gtkfilesel.c:626 gtk/gtkfilesel.c:1682
#, c-format
msgid "Directory unreadable: %s"
msgstr "無法讀取目錄: %s"

#: gtk/gtkfilesel.c:658
msgid "Create Dir"
msgstr "新增目錄"

#: gtk/gtkfilesel.c:669 gtk/gtkfilesel.c:1063
msgid "Delete File"
msgstr "刪除檔案"

#: gtk/gtkfilesel.c:680 gtk/gtkfilesel.c:1174
msgid "Rename File"
msgstr "重新命名"

#.
#. gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
#. (GtkSignalFunc) gtk_file_selection_fileop_destroy,
#. (gpointer) fs);
#.
#: gtk/gtkfilesel.c:850
msgid "Error"
msgstr "錯誤"

#. close button
#: gtk/gtkfilesel.c:873 gtk/gtkinputdialog.c:354
msgid "Close"
msgstr "關閉"

#: gtk/gtkfilesel.c:951
msgid "Create Directory"
msgstr "新增目錄"

#: gtk/gtkfilesel.c:965
msgid "Directory name:"
msgstr "目錄名稱:"

#. buttons
#: gtk/gtkfilesel.c:978
msgid "Create"
msgstr "新增"

#. buttons
#: gtk/gtkfilesel.c:1085
msgid "Delete"
msgstr "刪除"

#. buttons
#: gtk/gtkfilesel.c:1207
msgid "Rename"
msgstr "更新"

#: gtk/gtkfilesel.c:1661
msgid "Selection: "
msgstr "字型選項: "

#: gtk/gtkfontsel.c:199
msgid "Foundry:"
msgstr "提供:"

#: gtk/gtkfontsel.c:200
msgid "Family:"
msgstr "字體:"

#: gtk/gtkfontsel.c:201
msgid "Weight:"
msgstr "大小:"

#: gtk/gtkfontsel.c:202
msgid "Slant:"
msgstr "斜體:"

#: gtk/gtkfontsel.c:203
msgid "Set Width:"
msgstr "寬度:"

#: gtk/gtkfontsel.c:204
msgid "Add Style:"
msgstr "增加形式:"

#: gtk/gtkfontsel.c:205
msgid "Pixel Size:"
msgstr "像素大小:"

#: gtk/gtkfontsel.c:206
msgid "Point Size:"
msgstr "點陣大小:"

#: gtk/gtkfontsel.c:207
msgid "Resolution X:"
msgstr "X 解析度:"

#: gtk/gtkfontsel.c:208
msgid "Resolution Y:"
msgstr "Y 解析度:"

#: gtk/gtkfontsel.c:209
msgid "Spacing:"
msgstr "間距:"

#: gtk/gtkfontsel.c:210
msgid "Average Width:"
msgstr "平均寬度:"

#: gtk/gtkfontsel.c:211
msgid "Charset:"
msgstr "字集:"

#. Number of internationalized titles here must match number
#. of NULL initializers above
#: gtk/gtkfontsel.c:448
msgid "Font Property"
msgstr "字型屬性"

#: gtk/gtkfontsel.c:449
msgid "Requested Value"
msgstr "需求值"

#: gtk/gtkfontsel.c:450
msgid "Actual Value"
msgstr "實際值"

#: gtk/gtkfontsel.c:483
msgid "Font"
msgstr "字型"

#: gtk/gtkfontsel.c:493 gtk/gtkfontsel.c:2161 gtk/gtkfontsel.c:2391
msgid "Font:"
msgstr "字型:"

#: gtk/gtkfontsel.c:498
msgid "Font Style:"
msgstr "字型樣式:"

#: gtk/gtkfontsel.c:503
msgid "Size:"
msgstr "大小:"

#: gtk/gtkfontsel.c:636 gtk/gtkfontsel.c:858
msgid "Reset Filter"
msgstr "重設過濾器"

#: gtk/gtkfontsel.c:650
msgid "Metric:"
msgstr "度量:"

#: gtk/gtkfontsel.c:654
msgid "Points"
msgstr "點陣"

#: gtk/gtkfontsel.c:661
msgid "Pixels"
msgstr "像素"

#. create the text entry widget
#: gtk/gtkfontsel.c:677
msgid "Preview:"
msgstr "預覽:"

#: gtk/gtkfontsel.c:706
msgid "Font Information"
msgstr "字型資訊"

#: gtk/gtkfontsel.c:739
msgid "Requested Font Name:"
msgstr "要求的字型名稱:"

#: gtk/gtkfontsel.c:750
msgid "Actual Font Name:"
msgstr "實際字型名稱:"

#: gtk/gtkfontsel.c:761
#, c-format
msgid "%i fonts available with a total of %i styles."
msgstr "有 %i 種字型供選用(共 %i 種樣式)"

#: gtk/gtkfontsel.c:776
msgid "Filter"
msgstr "過濾器"

#: gtk/gtkfontsel.c:789
msgid "Font Types:"
msgstr "字型種類:"

#: gtk/gtkfontsel.c:797
msgid "Bitmap"
msgstr "點陣"

#: gtk/gtkfontsel.c:803
msgid "Scalable"
msgstr "可縮放"

#: gtk/gtkfontsel.c:809
msgid "Scaled Bitmap"
msgstr "可縮放點陣"

#: gtk/gtkfontsel.c:880
msgid "*"
msgstr "*"

#. Convert '(nil)' weights to 'regular', since it looks nicer.
#: gtk/gtkfontsel.c:1205
msgid "(nil)"
msgstr ""

#: gtk/gtkfontsel.c:1205
msgid "regular"
msgstr "正常"

#: gtk/gtkfontsel.c:1210 gtk/gtkfontsel.c:1953
msgid "italic"
msgstr "斜體"

#: gtk/gtkfontsel.c:1211 gtk/gtkfontsel.c:1954
msgid "oblique"
msgstr "傾斜"

#: gtk/gtkfontsel.c:1212 gtk/gtkfontsel.c:1955
msgid "reverse italic"
msgstr "反斜體"

#: gtk/gtkfontsel.c:1213 gtk/gtkfontsel.c:1956
msgid "reverse oblique"
msgstr "反傾斜"

#: gtk/gtkfontsel.c:1214 gtk/gtkfontsel.c:1957
msgid "other"
msgstr "其它"

#: gtk/gtkfontsel.c:1221
msgid "[M]"
msgstr ""

#: gtk/gtkfontsel.c:1222
msgid "[C]"
msgstr ""

#: gtk/gtkfontsel.c:1790
msgid "The selected font is not available."
msgstr "選定的字型不存在。"

#: gtk/gtkfontsel.c:1796
msgid "The selected font is not a valid font."
msgstr "選定的是無效的字型。"

#: gtk/gtkfontsel.c:1857
msgid "This is a 2-byte font and may not be displayed correctly."
msgstr "字型為雙位元字體，可能無法正常顯示。"

#: gtk/gtkfontsel.c:1941 gtk/gtkinputdialog.c:607
msgid "(unknown)"
msgstr "(未知)"

#: gtk/gtkfontsel.c:1952
msgid "roman"
msgstr "羅馬體"

#: gtk/gtkfontsel.c:1964
msgid "proportional"
msgstr "成比例的"

#: gtk/gtkfontsel.c:1965
msgid "monospaced"
msgstr "定寬度的"

#: gtk/gtkfontsel.c:1966
msgid "char cell"
msgstr "字體格子"

#: gtk/gtkfontsel.c:2166
msgid "Font: (Filter Applied)"
msgstr "字型: (採用過濾器)"

#: gtk/gtkfontsel.c:3507
msgid "Apply"
msgstr "套用"

#: gtk/gtkfontsel.c:3529
msgid "Font Selection"
msgstr "字型選擇"

#: gtk/gtkgamma.c:395
msgid "Gamma"
msgstr "Gamma"

#: gtk/gtkgamma.c:402
msgid "Gamma value"
msgstr "Gamma 值"

#. shell and main vbox
#: gtk/gtkinputdialog.c:200
msgid "Input"
msgstr "輸入"

#: gtk/gtkinputdialog.c:208
msgid "No input devices"
msgstr "缺少輸入裝置"

#: gtk/gtkinputdialog.c:237
msgid "Device:"
msgstr "裝置:"

#: gtk/gtkinputdialog.c:253
msgid "Disabled"
msgstr "已失效"

#: gtk/gtkinputdialog.c:261
msgid "Screen"
msgstr "螢幕"

#: gtk/gtkinputdialog.c:269
msgid "Window"
msgstr "視窗"

#: gtk/gtkinputdialog.c:277
msgid "Mode: "
msgstr "模式:"

#. The axis listbox
#: gtk/gtkinputdialog.c:307
msgid "Axes"
msgstr "軸"

#. Keys listbox
#: gtk/gtkinputdialog.c:323
msgid "Keys"
msgstr "鍵鈕"

#. We create the save button in any case, so that clients can
#. connect to it, without paying attention to whether it exits
#: gtk/gtkinputdialog.c:345
msgid "Save"
msgstr "儲存"

#: gtk/gtkinputdialog.c:500
msgid "X"
msgstr ""

#: gtk/gtkinputdialog.c:501
msgid "Y"
msgstr ""

#: gtk/gtkinputdialog.c:502
msgid "Pressure"
msgstr "壓力"

#: gtk/gtkinputdialog.c:503
msgid "X Tilt"
msgstr "X 傾斜"

#: gtk/gtkinputdialog.c:504
msgid "Y Tilt"
msgstr "Y 傾斜"

#: gtk/gtkinputdialog.c:544
msgid "none"
msgstr "無"

#: gtk/gtkinputdialog.c:578 gtk/gtkinputdialog.c:614
msgid "(disabled)"
msgstr "(已失效)"

#. and clear button
#: gtk/gtkinputdialog.c:692
msgid "clear"
msgstr "清除"

#: gtk/gtknotebook.c:2059 gtk/gtknotebook.c:4131
#, c-format
msgid "Page %u"
msgstr "第 %u 頁"

#: gtk/gtkrc.c:1732
#, c-format
msgid "Unable to locate image file in pixmap_path: \"%s\" line %d"
msgstr "無法由 pixmap_path 找到圖檔: \"%s\" 第 %d 行"

#: gtk/gtkrc.c:1735
#, c-format
msgid "Unable to locate image file in pixmap_path: \"%s\""
msgstr "無法由 pixmap_path 找到圖檔: \"%s\""

#: gtk/gtkthemes.c:103
#, c-format
msgid "Unable to locate loadable module in module_path: \"%s\","
msgstr "無法由 module_path 找到可載入模組: \"%s\","

#: gtk/gtktipsquery.c:180
msgid "--- No Tip ---"
msgstr "--- 已無提示 ---"

#, fuzzy
#~ msgid "light"
#~ msgstr "大小:"

#~ msgid "MAX_FONTS exceeded. Some fonts may be missing."
#~ msgstr "已超出 MAX_FONTS ,可能遺失某些字型。"
