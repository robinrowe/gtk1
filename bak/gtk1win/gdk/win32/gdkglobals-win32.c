/* GDK - CinePaint Drawing Kit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "gdktypes.h"
#include "gdkprivate-win32.h"

HWND              gdk_root_window = NULL;
gboolean              gdk_event_func_from_window_proc = FALSE;
gint		  gdk_max_colors = 0;
HDC		  gdk_DC;
HINSTANCE	  gdk_DLLInstance;
HINSTANCE	  gdk_ProgInstance;
GdkAtom		  gdk_clipboard_atom;
GdkAtom		  gdk_win32_dropfiles_atom;
GdkAtom		  gdk_ole2_dnd_atom;
GdkAtom           gdk_selection_property;
GdkAtom		  text_uri_list_atom;
GdkAtom		  compound_text_atom;
gint              gdk_null_window_warnings = TRUE;
WORD		  cf_rtf;
WORD		  cf_utf8_string;

DWORD		  windows_version = 0;

#ifdef STRICT_ISWINDOW
GDKVAR void KillInvalidHwnd(void* win);

void KillInvalidHwnd(void* win)
{	GdkDrawableWin32Data * data=GDK_DRAWABLE_WIN32DATA(win);
	HWND hwnd=data->xid;
	if(!IsWindow(hwnd))
	{	data->xid=INVALID_HANDLE_VALUE;
	}
}
#else

GDKVAR HWND GetSafeHwnd(HWND hwnd)
{	if(IsWindow(hwnd))
	{	return hwnd;
	}
	return INVALID_HANDLE_VALUE;
}

#endif

GdkDrawableClass  _gdk_win32_drawable_class;
HWND		 gdk_root_window;
gchar		*gdk_progclass;
gboolean		 gdk_event_func_from_window_proc;
gint		 gdk_max_colors;

HDC		 gdk_DC;
HINSTANCE	 gdk_DLLInstance;
HINSTANCE	 gdk_ProgInstance;

GdkAtom		 gdk_selection_property;
GdkAtom		 text_uri_list_atom;
GdkAtom		 compound_text_atom;
GdkAtom		 gdk_clipboard_atom;
GdkAtom		 gdk_win32_dropfiles_atom;
GdkAtom		 gdk_ole2_dnd_atom;
WORD		 cf_rtf;
WORD		 cf_utf8_string;