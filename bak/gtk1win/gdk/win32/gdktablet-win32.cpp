// SDL + windows pressure sensitivity demo
// Written in July 2013
//
// This file is public domain. No copyright is claimed and you may use it for any purpose you like.
//
// No warranty for any purpose is expressed or implied by the author. You use this file at your own risk.
//
// At the time of this writing, you can get the SimpleDirect Media library at http://www.libsdl.org
// The stylus code in this file was researched in the RealTimeStylus demo from the windows SDK.
// You can pick up the Windows SDK from http://www.microsoft.com if you need more windows touch-interface demos. 

#include <unistd.h>
#include <stdio.h>
#include <ole2.h>
#include <rtscom.h>
#include <rtscom_i.c>
#include <gdkwindow.h>
#include "gdkwin32.h"
#include "../gdktablet.h"

//#define DEBUG_TABLET

typedef unsigned long	uint32;

struct TabletContext
{	TABLET_CONTEXT_ID	iTabletContext;
	uint32				iPressure;
	float				PressureRcp;
	long				xTilt;
	long				yTilt;
	const static uint32		max_contexts = 4;
	static uint32		num_contexts;
};

uint32 TabletContext::num_contexts;

struct StylusHandler
{	bool g_bTriedToCreateRTSHandler = false;
	class CRTSEventHandler* g_pRTSHandler = NULL;
	IRealTimeStylus* g_pRTSStylus = NULL;
};


//-------------------------------------------------------------------------------------------------
// RealTimeStylus syncevent plugin
class CRTSEventHandler : public IStylusSyncPlugin
{protected:
	TabletContext contexts[TabletContext::max_contexts];
	GdkPen* pen;
public:
	CRTSEventHandler() 
	: m_nRef(1)
	, m_pPunkFTMarshaller(0)
	{	pen = GdkGetPen(); 
		memset(pen,0,sizeof(*pen));
		pen->pressure = 0.5f;
	}
	virtual ~CRTSEventHandler() 
	{
		if (m_pPunkFTMarshaller != NULL)
			m_pPunkFTMarshaller->Release(); 
	}

    // IStylusSyncPlugin inherited methods

    // Methods whose data we use
    STDMETHOD(Packets)(IRealTimeStylus* pStylus, const StylusInfo* pStylusInfo, ULONG nPackets, ULONG nPacketBuf, LONG* pPackets, ULONG* nOutPackets, LONG** ppOutPackets)
	{
		uint32 iCtx = TabletContext::max_contexts;
		for( uint32 i = 0; i < TabletContext::num_contexts;i++ )
		{
			if( contexts[i].iTabletContext == pStylusInfo->tcid )
			{
				iCtx = i;
				break;
			}
		}

		// If we are not getting pressure values, just ignore it
		if( iCtx >= TabletContext::max_contexts )
			return S_OK;

		// Get properties
		ULONG nProps = nPacketBuf / nPackets;

		// Pointer to the last packet in the batch - we don't really care about intermediates for this example
		LONG *pLastPacket = pPackets + (nPacketBuf - nProps);
		
		pen->pressure = pLastPacket[contexts[iCtx].iPressure] * contexts[iCtx].PressureRcp;
		pen->is_inverted = (pStylusInfo->bIsInvertedCursor != 0);
		pen->tilt_x = pLastPacket[contexts[iCtx].xTilt];
		pen->tilt_y = pLastPacket[contexts[iCtx].yTilt];

		return S_OK;
	}
    STDMETHOD(DataInterest)(RealTimeStylusDataInterest* pEventInterest)
	{
		*pEventInterest = (RealTimeStylusDataInterest)(RTSDI_DefaultEvents); //RTSDI_Packets);
		return S_OK;
	}

    // Methods you can add if you need the alerts - don't forget to change DataInterest!
	STDMETHOD(StylusDown)(IRealTimeStylus*, const StylusInfo*, ULONG, LONG* _pPackets, LONG**) { return S_OK; }
	STDMETHOD(StylusUp)(IRealTimeStylus*, const StylusInfo*, ULONG, LONG* _pPackets, LONG**) { return S_OK; }
    STDMETHOD(RealTimeStylusEnabled)(IRealTimeStylus*, ULONG, const TABLET_CONTEXT_ID*) { return S_OK; }
    STDMETHOD(RealTimeStylusDisabled)(IRealTimeStylus*, ULONG, const TABLET_CONTEXT_ID*) { return S_OK; }
    STDMETHOD(StylusInRange)(IRealTimeStylus*, TABLET_CONTEXT_ID, STYLUS_ID) { return S_OK; }
    STDMETHOD(StylusOutOfRange)(IRealTimeStylus*, TABLET_CONTEXT_ID, STYLUS_ID) { return S_OK; }
    STDMETHOD(InAirPackets)(IRealTimeStylus*, const StylusInfo*, ULONG, ULONG, LONG*, ULONG*, LONG**) { return S_OK; }
    STDMETHOD(StylusButtonUp)(IRealTimeStylus*, STYLUS_ID, const GUID*, POINT*) { return S_OK; }
    STDMETHOD(StylusButtonDown)(IRealTimeStylus*, STYLUS_ID, const GUID*, POINT*) { return S_OK; }
    STDMETHOD(SystemEvent)(IRealTimeStylus*, TABLET_CONTEXT_ID, STYLUS_ID, SYSTEM_EVENT, SYSTEM_EVENT_DATA) { return S_OK; }
    STDMETHOD(TabletAdded)(IRealTimeStylus*, IInkTablet*) { return S_OK; }
    STDMETHOD(TabletRemoved)(IRealTimeStylus*, LONG) { return S_OK; }
    STDMETHOD(CustomStylusDataAdded)(IRealTimeStylus*, const GUID*, ULONG, const BYTE*) { return S_OK; }
    STDMETHOD(Error)(IRealTimeStylus*, IStylusPlugin*, RealTimeStylusDataInterest, HRESULT, LONG_PTR*) { return S_OK; }
    STDMETHOD(UpdateMapping)(IRealTimeStylus*) { return S_OK; }

    // IUnknown methods
    STDMETHOD_(ULONG,AddRef)()
	{
		return InterlockedIncrement(&m_nRef);
	}
    STDMETHOD_(ULONG,Release)()
	{
		ULONG nNewRef = InterlockedDecrement(&m_nRef);
		if (nNewRef == 0)
			delete this;

		return nNewRef;
	}
    STDMETHOD(QueryInterface)(REFIID riid, LPVOID *ppvObj)
	{
		*ppvObj = NULL;
		if ((riid == IID_IStylusSyncPlugin) || (riid == IID_IUnknown))
		{
			*ppvObj = this;
			AddRef();
			return S_OK;
		}
		if (riid != IID_IMarshal)
		{
			return E_NOINTERFACE;
		}
		if (!m_pPunkFTMarshaller)
		{
			return E_NOINTERFACE;
		}
		return m_pPunkFTMarshaller->QueryInterface(riid, ppvObj);
	}

    LONG m_nRef;					// COM object reference count
    IUnknown* m_pPunkFTMarshaller;  // free-threaded marshaller
};

struct GdkTablet
:	public CRTSEventHandler
{
private:
	StylusHandler stylus;
	void ReleaseRTS()
	{
		if (stylus.g_pRTSStylus)
		{
			stylus.g_pRTSStylus->Release();
			stylus.g_pRTSStylus = NULL;
		}
		if (stylus.g_pRTSHandler)
		{
			stylus.g_pRTSHandler->Release();
			stylus.g_pRTSHandler = NULL;
		}
	}
	bool CreateRTS(HWND hWnd);
public:
	GdkTablet()
	{}
	bool Start(GdkWindow* gw)
	{	HWND hwnd = GetSafeHwnd((HWND) GDK_DRAWABLE_XID(gw));
		if(INVALID_HANDLE_VALUE == hwnd)
		{	return false;
		}
		return CreateRTS(hwnd);
	}
	void Close()
	{	ReleaseRTS();
	}
};

#define PG(guid,property) if (!memcmp((void*) guid, &property,sizeof(GUID))) return puts("GUID: " #property)

int PrintGUID(const GUID* guid)
{	PG(guid,GUID_PACKETPROPERTY_GUID_X);
	PG(guid, GUID_PACKETPROPERTY_GUID_Y);
	PG(guid, GUID_PACKETPROPERTY_GUID_Z);
	PG(guid, GUID_PACKETPROPERTY_GUID_PACKET_STATUS);
	PG(guid, GUID_PACKETPROPERTY_GUID_TIMER_TICK);
	PG(guid, GUID_PACKETPROPERTY_GUID_SERIAL_NUMBER);
	PG(guid, GUID_PACKETPROPERTY_GUID_NORMAL_PRESSURE);
	PG(guid, GUID_PACKETPROPERTY_GUID_TANGENT_PRESSURE);
	PG(guid, GUID_PACKETPROPERTY_GUID_BUTTON_PRESSURE);
	PG(guid, GUID_PACKETPROPERTY_GUID_X_TILT_ORIENTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_Y_TILT_ORIENTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_AZIMUTH_ORIENTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_ALTITUDE_ORIENTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_TWIST_ORIENTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_PITCH_ROTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_ROLL_ROTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_YAW_ROTATION);
	PG(guid, GUID_PACKETPROPERTY_GUID_WIDTH);
	PG(guid, GUID_PACKETPROPERTY_GUID_HEIGHT);
	PG(guid,GUID_PACKETPROPERTY_GUID_FINGERCONTACTCONFIDENCE);
	PG(guid, GUID_PACKETPROPERTY_GUID_DEVICE_CONTACT_ID);
	return puts("GUID: unknown");
}

//-------------------------------------------------------------------------------------------------
// Create stylus
bool GdkTablet::CreateRTS(HWND hWnd)
{
	// Release, just in case
	ReleaseRTS();

	// Create stylus
    HRESULT hr = CoCreateInstance(CLSID_RealTimeStylus, NULL, CLSCTX_ALL, IID_PPV_ARGS(&stylus.g_pRTSStylus));
    if (FAILED(hr))
		return false;

    // Attach RTS object to a window
    hr = stylus.g_pRTSStylus->put_HWND((HANDLE_PTR)hWnd);
    if (FAILED(hr))
    {
		ReleaseRTS();
        return false;
    }

	// Create eventhandler
	stylus.g_pRTSHandler = new CRTSEventHandler();

    // Create free-threaded marshaller for this object and aggregate it.
    hr = CoCreateFreeThreadedMarshaler(stylus.g_pRTSHandler, 
		&stylus.g_pRTSHandler->m_pPunkFTMarshaller);
    if (FAILED(hr))
    {	ReleaseRTS();
        return false;
    }
    // Add handler object to the list of synchronous plugins in the RTS object.
    hr = stylus.g_pRTSStylus->AddStylusSyncPlugin(0, stylus.g_pRTSHandler);
    if (FAILED(hr))
    {	ReleaseRTS();
        return false;
    }
	// Set data we want - we're not actually using all of this, but we're gonna get X and Y anyway so might as well set it
	GUID lWantedProps[] = 
	{	GUID_PACKETPROPERTY_GUID_X, 
		GUID_PACKETPROPERTY_GUID_Y, 
		GUID_PACKETPROPERTY_GUID_NORMAL_PRESSURE,
		GUID_PACKETPROPERTY_GUID_X_TILT_ORIENTATION,
		GUID_PACKETPROPERTY_GUID_Y_TILT_ORIENTATION
	};
	const int prop_count = sizeof(lWantedProps) / sizeof(GUID);
	stylus.g_pRTSStylus->SetDesiredPacketDescription(prop_count, lWantedProps);
	stylus.g_pRTSStylus->put_Enabled(true);
	// Detect what tablet context IDs will give us pressure data
	{	TabletContext::num_contexts = 0;
		ULONG nTabletContexts = 0;
		TABLET_CONTEXT_ID *piTabletContexts;
		HRESULT res = stylus.g_pRTSStylus->GetAllTabletContextIds(&nTabletContexts, &piTabletContexts);
		for( ULONG i = 0; i < nTabletContexts; i++ )
		{
			IInkTablet * pInkTablet;
			if( SUCCEEDED(stylus.g_pRTSStylus->GetTabletFromTabletContextId(piTabletContexts[i],&pInkTablet)) )
			{
				float ScaleX = 0.;
				float ScaleY = 0.;
				ULONG nPacketProps = 0;
				PACKET_PROPERTY *pPacketProps = 0;
				stylus.g_pRTSStylus->GetPacketDescriptionData(piTabletContexts[i], &ScaleX, &ScaleY, &nPacketProps, &pPacketProps);

				for( long j = 0; j < nPacketProps; j++ )
				{
#ifdef DEBUG_TABLET
					PrintGUID(&pPacketProps[j].guid);
#endif
					if(GUID_PACKETPROPERTY_GUID_NORMAL_PRESSURE == pPacketProps[j].guid)
					{	// Tablet context to pressure sensitivity
						contexts[TabletContext::num_contexts].iTabletContext = piTabletContexts[i];
						contexts[TabletContext::num_contexts].iPressure = j;
						contexts[TabletContext::num_contexts].PressureRcp = 1.0f / pPacketProps[j].PropertyMetrics.nLogicalMax;
						TabletContext::num_contexts++;
					}
					else if (GUID_PACKETPROPERTY_GUID_X_TILT_ORIENTATION == pPacketProps[j].guid)
					{	contexts[TabletContext::num_contexts].xTilt = j;
					}
					else if (GUID_PACKETPROPERTY_GUID_Y_TILT_ORIENTATION == pPacketProps[j].guid)
					{	contexts[TabletContext::num_contexts].yTilt = j;
					}
#if 0
					 GUID GUID_PACKETPROPERTY_GUID_TANGENT_PRESSURE 
					 GUID GUID_PACKETPROPERTY_GUID_BUTTON_PRESSURE 

					 GUID GUID_PACKETPROPERTY_GUID_AZIMUTH_ORIENTATION 
					 GUID GUID_PACKETPROPERTY_GUID_ALTITUDE_ORIENTATION 
					 GUID GUID_PACKETPROPERTY_GUID_TWIST_ORIENTATION 
					 GUID GUID_PACKETPROPERTY_GUID_PITCH_ROTATION 
					 GUID GUID_PACKETPROPERTY_GUID_ROLL_ROTATION 
					 GUID GUID_PACKETPROPERTY_GUID_YAW_ROTATION 
#endif
				}
				CoTaskMemFree(pPacketProps);
			}
		}

		// If we can't get pressure information, no use in having the tablet context
		if(TabletContext::num_contexts == 0 )
		{
			ReleaseRTS();
			return false;
		}
	}

    return true;
}

extern "C" {

GdkTablet* GdkTabletOpen()
{	GdkTablet* gt = new GdkTablet;
	return gt;
}

int GdkTabletStart(GdkTablet* gt, GdkWindow* gw)
{	if (!gt)
	{	return 0;
	}
	return true == gt->Start(gw);
}

void GdkTabletClose(GdkTablet* gt)
{	if(!gt)
	{	return;
	}
	gt->Close();
	delete gt;
}

GdkPen gdkpen;

GdkPen* GdkGetPen()
{	return &gdkpen;
}

void GdkPenPrint()
{	printf("PEN: mouse(%li,%li) pressure(%f) tilt(%li,%li) %s",
		gdkpen.mouse_x,
		gdkpen.mouse_y,
		gdkpen.pressure,
		gdkpen.tilt_x,
		gdkpen.tilt_y,
		gdkpen.is_inverted ? "inverted":"");
}

}

#if 0
#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_syswm.h"

const uint32 c_WinWidth = 1024;
const uint32 c_WinHeight = 768;
const char* c_WinCaption = "Pressure test SDL window";
bool g_bValid = true;

// SDL main surface
SDL_Surface* g_pScreen = NULL;

// GL resources
GLuint g_iTexture;

void StartRTS(HWND hwnd)
{
	if (!stylus.g_bTriedToCreateRTSHandler)
	{
		LPCREATESTRUCT pStruct = (LPCREATESTRUCT)pMsg->lParam;
		CreateRTS(pMsg->hwnd);
	}
	stylus.g_bTriedToCreateRTSHandler = true;
}

//-------------------------------------------------------------------------------------------------
// Initialize SDL openGL window
bool CreateSDLWindow()
{
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	
	g_pScreen = SDL_SetVideoMode(c_WinWidth,c_WinHeight,32,SDL_OPENGL);
	if( !g_pScreen )
		return false;

	SDL_WM_SetCaption(c_WinCaption,0);
	return true;
}


//-------------------------------------------------------------------------------------------------
// Setup GL state
void InitializeGL()
{
	glViewport(0,0,c_WinWidth,c_WinHeight);
	glMatrixMode(GL_PROJECTION);
	glOrtho(0,c_WinWidth,0,c_WinHeight,-1.0f,1.0f);
	glMatrixMode(GL_MODELVIEW);

	//Generate texture data
	GLubyte * pTexData = new GLubyte[64*64];
	GLubyte * pDst = pTexData;
	for(int i = 0; i < 64; i++)
	{
		int y = i-31;
		for(int j = 0; j < 64; j++)
		{
			int x = j-31;
			int intensity = (1024 - (x*x + y*y));
			if( intensity < 0 ) 
				intensity = 0;
			else if( intensity > 255 )
				intensity = 255;

			*pDst = intensity;
			pDst++;
		}
	}

	//Create texture
	glGenTextures(1, &g_iTexture);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, g_iTexture);
	glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE,64,64,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,pTexData);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);

	//Release data
	delete [] pTexData;
}


//-------------------------------------------------------------------------------------------------
// Release GL resources
void ReleaseGL()
{
	glDeleteTextures(1, &g_iTexture);
}


//-------------------------------------------------------------------------------------------------
// Render each frame
void Render()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();
	glTranslatef((GLfloat)mouse_x,(GLfloat)mouse_y,0);

	float scale = 1.0f + pressure * 9.0f;
	glScalef(scale,scale,scale);

	if( is_inverted )
		glColor3ub(255,0,0);
	else
		glColor3ub(0,255,0);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f,0.0f);
	glVertex2f(-32.0f,-32.0f);
	glTexCoord2f(1.0f,0.0f);
	glVertex2f(32.0f,-32.0f);
	glTexCoord2f(1.0f,1.0f);
	glVertex2f(32.0f,32.0f);
	glTexCoord2f(0.0f,1.0f);
	glVertex2f(-32.0f,32.0f);
	glEnd();

	SDL_GL_SwapBuffers();
}

//-------------------------------------------------------------------------------------------------
// Process events
void PumpSDLEvents()
{
	SDL_PumpEvents();
	
	SDL_Event Ev;
	while( SDL_PollEvent(&Ev) )
	{
		switch( Ev.type )
		{
		case SDL_QUIT:
			g_bValid = false;
			break;

		case SDL_SYSWMEVENT:

			// We would normally do this in WM_CREATE but SDL intercepts that message so use WM_SETFOCUS instead
			SDL_SysWMmsg *pMsg = Ev.syswm.msg;
			if( pMsg && pMsg->msg == WM_SETFOCUS )
			{
				if( !g_bTriedToCreateRTSHandler )
				{
					LPCREATESTRUCT pStruct = (LPCREATESTRUCT)pMsg->lParam;
					CreateRTS(pMsg->hwnd);
				}
				g_bTriedToCreateRTSHandler = true;
			}
			break;
		}
	}
	
	SDL_GetMouseState(&mouse_x,&mouse_y);

	//Invert Y coordinate
	mouse_y = c_WinHeight - mouse_y;
}



//-------------------------------------------------------------------------------------------------
// Application entry point
int main(int argc, char** argv)
{
	if( SDL_Init(SDL_INIT_VIDEO) )
		return 1;

	// Need to enable SysWM messages so we can get the window handle
	SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

	if( !CreateSDLWindow() )
	{
		SDL_Quit();
		return 1;
	}
	
	InitializeGL();

	// Global event loop
	while( g_bValid )
	{
		PumpSDLEvents();
		Render();
	}

	ReleaseRTS();
	ReleaseGL();

	SDL_FreeSurface(g_pScreen);
	SDL_Quit();
	return 0;
}

#endif