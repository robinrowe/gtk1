// gdkspeech-win32.cpp
// Copyright Robin.Rowe@CinePaint.org 2021/05/14
// License open source MIT

#include <sapi.h>
#include <iostream>
#include <stdio.h>
#include "../gdkspeech.h"

class StringConvert
{   std::wstring ws;
public:
    StringConvert(const char* s)
    {   if(!s)
        {   return;
        }
        const size_t len = strlen(s);
        ws.resize(len);
        wchar_t* p = (wchar_t*) ws.c_str();
        swprintf_s(p, len, L"%S", s);
    }
    operator const wchar_t*() const
    {   return ws.c_str();
    }
};

struct GdkSpeech
{private:
    ISpVoice* pVoice;
public:
    GdkSpeech()
    :   pVoice(0)
    {}
    bool Open()
    {   if(pVoice)
        {   return true;
        }
        if(FAILED(::CoInitialize(NULL)))
        {   return false;
        }
        HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
        if(!SUCCEEDED(hr))
        {   return false;
        }
        return true;
    }
    bool Say(const char* text,unsigned speaker_id = 0)
    {   StringConvert ws(text);
        HRESULT hr = pVoice->Speak(ws, 0, NULL);
// Change pitch
//        hr = pVoice->Speak(L"This sounds normal <pitch middle = '-10'/> but the pitch drops half way through", SPF_IS_XML, NULL );
        return true;
    }
    void Close()
    {   if(!pVoice)
        {   return;
        }
        pVoice->Release();
        pVoice = NULL;
        ::CoUninitialize();
    }
};

extern "C" {

GdkSpeech* GdkOpenSpeech()
{   GdkSpeech* gs = new GdkSpeech;
    if(!gs)
    {   return 0;
    }
    if(!gs->Open())
    {   return 0;
    }
    return gs;
}

int GdkSay(GdkSpeech* gs,const char* text)
{   if (!gs)
    {   return 0;
    }
    if(!gs->Say(text))
    {   return 0;
    }
    return 1;
}

void GdkCloseSpeech(GdkSpeech* gs)
{   if(!gs)
    {   return;
    }
    delete gs;
}

}


#if 0    
int main(int argc, char* argv[])
{
    ISpVoice* pVoice = NULL;

    if (FAILED(::CoInitialize(NULL)))
        return FALSE;

    HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void**)&pVoice;);
    if (SUCCEEDED(hr))
    {
        hr = pVoice->Speak(L"Hello world", 0, NULL);

        // Change pitch
        hr = pVoice->Speak(L"This sounds normal <pitch middle = '-10'/> but the pitch drops half way through", SPF_IS_XML, NULL);
        pVoice->Release();
        pVoice = NULL;
    }
    ::CoUninitialize();
    return TRUE;
}
#endif