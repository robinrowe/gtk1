#ifndef __GDK_FONT_H__
#define __GDK_FONT_H__

#include <gdk/gdktypes.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Types of font.
 *   GDK_FONT_FONT: the font is an XFontStruct.
 *   GDK_FONT_FONTSET: the font is an XFontSet used for I18N.
 */
typedef enum
{
  GDK_FONT_FONT,
  GDK_FONT_FONTSET
} GdkFontType;

struct _GdkFont
{
  GdkFontType type;
  gint ascent;
  gint descent;
};

GDKVAR gchar**  gdk_font_list_new  (const gchar    *font_pattern, gint *n_returned);
GDKVAR void     gdk_font_list_free (gchar **font_list);
GDKVAR GdkFont* gdk_font_load	    (const gchar    *font_name);
GDKVAR GdkFont* gdk_fontset_load   (const gchar    *fontset_name);
GDKVAR GdkFont* gdk_font_ref	    (GdkFont        *font);
GDKVAR void	 gdk_font_unref	    (GdkFont        *font);
GDKVAR uintptr_t	 gdk_font_id	    (const GdkFont  *font);
GDKVAR gboolean gdk_font_equal	    (const GdkFont  *fonta,
			     const GdkFont  *fontb);

GDKVAR gint	 gdk_string_width   (GdkFont        *font,
			     const gchar    *string);
GDKVAR gint	 gdk_text_width	    (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDKVAR gint	 gdk_text_width_wc  (GdkFont        *font,
			     const GdkWChar *text,
			     gint            text_length);
GDKVAR gint	 gdk_char_width	    (GdkFont        *font,
			     gchar           character);
GDKVAR gint	 gdk_char_width_wc  (GdkFont        *font,
			     GdkWChar        character);
GDKVAR gint	 gdk_string_measure (GdkFont        *font,
			     const gchar    *string);
GDKVAR gint	 gdk_text_measure   (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDKVAR gint	 gdk_char_measure   (GdkFont        *font,
			     gchar           character);
GDKVAR gint	 gdk_string_height  (GdkFont        *font,
			     const gchar    *string);
GDKVAR gint	 gdk_text_height    (GdkFont        *font,
			     const gchar    *text,
			     gint            text_length);
GDKVAR gint	 gdk_char_height    (GdkFont        *font,
			     gchar           character);

GDKVAR void     gdk_text_extents   (GdkFont     *font,
			     const gchar *text,
			     gint         text_length,
			     gint        *lbearing,
			     gint        *rbearing,
			     gint        *width,
			     gint        *ascent,
			     gint        *descent);
GDKVAR void    gdk_text_extents_wc (GdkFont        *font,
			     const GdkWChar *text,
			     gint            text_length,
			     gint           *lbearing,
			     gint           *rbearing,
			     gint           *width,
			     gint           *ascent,
			     gint           *descent);
GDKVAR void     gdk_string_extents (GdkFont     *font,
			     const gchar *string,
			     gint        *lbearing,
			     gint        *rbearing,
			     gint        *width,
			     gint        *ascent,
			     gint        *descent);

GDKVAR gchar*   gdk_font_full_name_get (GdkFont *font);
GDKVAR void	 gdk_font_full_name_free (gchar *name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GDK_FONT_H__ */
