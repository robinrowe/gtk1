// gdkspeech.h
// Copyright Robin.Rowe@CinePaint.org 2021/05/14
// License open source MIT

#ifndef gdkspeech_h
#define gdkspeech_h

#include <gdktypes.h>

typedef struct GdkPen
{	int is_inverted;
	float pressure;
	long tilt_x;
	long tilt_y;
	long mouse_x;
	long mouse_y;
} GdkPen;

extern GdkPen gdkpen;

#ifdef __cplusplus
extern "C" {
#endif

GDKVAR struct GdkTablet* GdkTabletOpen();
GDKVAR int GdkTabletStart(struct GdkTablet* gt,GdkWindow* gw);
GDKVAR void GdkTabletClose(struct GdkTablet* gt);
GDKVAR GdkPen* GdkGetPen();
GDKVAR void GdkPenPrint();

#ifdef __cplusplus
}
#endif

#endif