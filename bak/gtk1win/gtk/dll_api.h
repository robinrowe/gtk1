/* gtk/dll_api.h
// The usual Windows DLL export/import trick
// Copyright Mar 8, 2003, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifndef GTK_DLL_API_H
#define GTK_DLL_API_H

#ifdef _WIN32
	#ifdef DLL_EXPORTS
		#define DLL_GTK __declspec(dllexport)
	#else
		#define DLL_GTK __declspec(dllimport)
	#endif
#else
	#define DLL_GTK 
#endif

#endif