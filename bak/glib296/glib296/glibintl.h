#ifndef __GLIBINTL_H__
#define __GLIBINTL_H__

#include "config.h"
#include <direct.h>
#ifdef _WIN32
#include <io.h>

#define mkdir mkdir2
inline
int mkdir2(const char* pathname,...)
{   return _mkdir(pathname);
}

#define S_ISDIR(mode) ((mode)&_S_IFDIR)
#endif

#ifndef SIZEOF_CHAR
#error "config.h must be included prior to glibintl.h"
#endif

#ifdef ENABLE_NLS

gchar *_glib_gettext (const gchar *str) G_GNUC_FORMAT (1);

#include <libintl.h>
#define _(String) _glib_gettext(String)

#ifdef gettext_noop
#define N_(String) gettext_noop(String)
#else
#define N_(String) (String)
#endif
#else /* NLS is disabled */
#define _(String) (String)
#define N_(String) (String)
#define textdomain(String) (String)
#define gettext(String) (String)
#define dgettext(Domain,String) (String)
#define dcgettext(Domain,String,Type) (String)
#define bindtextdomain(Domain,Directory) (Domain) 
typedef int iconv_t;
inline
iconv_t iconv_open(const char* tocode, const char* fromcode)
{	return -1;
}
inline
int iconv_close(iconv_t cd)
{   return -1;
}
inline
size_t iconv(iconv_t cd,
    char** restrict inbuf, size_t* restrict inbytesleft,
    char** restrict outbuf, size_t* restrict outbytesleft)
{   return 0;
}
#endif

#endif /* __GLIBINTL_H__ */
