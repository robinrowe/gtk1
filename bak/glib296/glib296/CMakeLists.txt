# glib/glib/CMakeLists.txt
# Created by Robin Rowe 2024-06-28
# License Open Source LGPL

set (MODULE_NAME glib)
message("Configuring ${MODULE_NAME} 123 source file(s)")
#if(NLS)
#	set (NLS_SOURCES gconvert.h gconvert.c) 
#endif(NLS)
file(STRINGS sources.cmake SOURCES)
file(STRINGS ${OS}/sources.cmake OS_SOURCES)
file(STRINGS gnulib/sources.cmake GNU_SOURCES)
file(STRINGS libcharset/sources.cmake CHAR_SOURCES)
add_library(${MODULE_NAME} ${SOURCES} ${OS_SOURCES} ${GNU_SOURCES} ${CHAR_SOURCES})
