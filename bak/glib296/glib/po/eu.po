# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1999 Free Software Foundation, Inc.
# Joseba Bidaurrazaga van Dierdonck <gcpbivaj@lg.ehu.es>, 1999-2000
#
msgid ""
msgstr ""
"Project-Id-Version: gtk+ VERSION\n"
"POT-Creation-Date: 2003-01-15 06:53+0000\n"
"PO-Revision-Date: 1999-07-05 19:17+0200\n"
"Last-Translator: Joseba Bidaurrazaga van Dierdonck <gcpbivaj@lg.ehu.es>\n"
"Language-Team: euskare <linux-eu@chanae.alphanet.ch>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bits\n"

#: gtk/gtkcolorsel.c:221
msgid "Hue:"
msgstr "Kolorea:"

#: gtk/gtkcolorsel.c:222
msgid "Saturation:"
msgstr "Saturazioa:"

#: gtk/gtkcolorsel.c:223
msgid "Value:"
msgstr "Balioa:"

#: gtk/gtkcolorsel.c:224
msgid "Red:"
msgstr "Gorria:"

#: gtk/gtkcolorsel.c:225
msgid "Green:"
msgstr "Berdea:"

#: gtk/gtkcolorsel.c:226
msgid "Blue:"
msgstr "Urdina"

#: gtk/gtkcolorsel.c:227
msgid "Opacity:"
msgstr "Opazitatea"

#. The OK button
#: gtk/gtkcolorsel.c:1727 gtk/gtkfilesel.c:588 gtk/gtkfontsel.c:3500
#: gtk/gtkgamma.c:415
msgid "OK"
msgstr "Ados"

#. The Cancel button
#: gtk/gtkcolorsel.c:1733 gtk/gtkfilesel.c:595 gtk/gtkfilesel.c:987
#: gtk/gtkfilesel.c:1094 gtk/gtkfilesel.c:1216 gtk/gtkfontsel.c:3513
#: gtk/gtkgamma.c:423
msgid "Cancel"
msgstr "Ezeztatu"

#: gtk/gtkcolorsel.c:1738
msgid "Help"
msgstr "Laguntza"

#. The directories clist
#: gtk/gtkfilesel.c:537
msgid "Directories"
msgstr "Direktorioak"

#. The files clist
#: gtk/gtkfilesel.c:556
msgid "Files"
msgstr "Fitxategiak"

#: gtk/gtkfilesel.c:626 gtk/gtkfilesel.c:1682
#, c-format
msgid "Directory unreadable: %s"
msgstr "Direktorio irakurtezina: %s"

#: gtk/gtkfilesel.c:658
msgid "Create Dir"
msgstr "Sortu Dir"

#: gtk/gtkfilesel.c:669 gtk/gtkfilesel.c:1063
msgid "Delete File"
msgstr "Ezabatu fitxategia"

#: gtk/gtkfilesel.c:680 gtk/gtkfilesel.c:1174
msgid "Rename File"
msgstr "Berizendatu fitxategia"

#.
#. gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
#. (GtkSignalFunc) gtk_file_selection_fileop_destroy,
#. (gpointer) fs);
#.
#: gtk/gtkfilesel.c:850
msgid "Error"
msgstr "Errorea"

#. close button
#: gtk/gtkfilesel.c:873 gtk/gtkinputdialog.c:354
msgid "Close"
msgstr "Itxi"

#: gtk/gtkfilesel.c:951
msgid "Create Directory"
msgstr "Sortu Direktorioa"

#: gtk/gtkfilesel.c:965
msgid "Directory name:"
msgstr "Direktorioaren izena"

#. buttons
#: gtk/gtkfilesel.c:978
msgid "Create"
msgstr "Sortu"

#. buttons
#: gtk/gtkfilesel.c:1085
msgid "Delete"
msgstr "Ezabatu"

#. buttons
#: gtk/gtkfilesel.c:1207
msgid "Rename"
msgstr "Berizendatu"

#: gtk/gtkfilesel.c:1661
msgid "Selection: "
msgstr "Aukera: "

#: gtk/gtkfontsel.c:199
msgid "Foundry:"
msgstr "Ola:"

#: gtk/gtkfontsel.c:200
msgid "Family:"
msgstr "Familia:"

#: gtk/gtkfontsel.c:201
msgid "Weight:"
msgstr "Pisua:"

#: gtk/gtkfontsel.c:202
msgid "Slant:"
msgstr "Alborapena:"

#: gtk/gtkfontsel.c:203
msgid "Set Width:"
msgstr "Zabalera: "

#: gtk/gtkfontsel.c:204
msgid "Add Style:"
msgstr "Gehitu estiloa"

#: gtk/gtkfontsel.c:205
msgid "Pixel Size:"
msgstr "Pixel tamaina:"

#: gtk/gtkfontsel.c:206
msgid "Point Size:"
msgstr "Puntu tamaina:"

#: gtk/gtkfontsel.c:207
msgid "Resolution X:"
msgstr "X erresoluzioa:"

#: gtk/gtkfontsel.c:208
msgid "Resolution Y:"
msgstr "Y erresoluzioa:"

#: gtk/gtkfontsel.c:209
msgid "Spacing:"
msgstr "Tartea:"

#: gtk/gtkfontsel.c:210
msgid "Average Width:"
msgstr "Batazbesteko zabalera:"

#: gtk/gtkfontsel.c:211
msgid "Charset:"
msgstr "Charset:"

#. Number of internationalized titles here must match number
#. of NULL initializers above
#: gtk/gtkfontsel.c:448
msgid "Font Property"
msgstr "Font-aren ezaugarriak"

#: gtk/gtkfontsel.c:449
msgid "Requested Value"
msgstr "Eskatutako balioa"

#: gtk/gtkfontsel.c:450
msgid "Actual Value"
msgstr "Oraingo balioa"

#: gtk/gtkfontsel.c:483
msgid "Font"
msgstr "Font"

#: gtk/gtkfontsel.c:493 gtk/gtkfontsel.c:2161 gtk/gtkfontsel.c:2391
msgid "Font:"
msgstr "Font"

#: gtk/gtkfontsel.c:498
msgid "Font Style:"
msgstr "Font-aren estiloa"

#: gtk/gtkfontsel.c:503
msgid "Size:"
msgstr "Tamaina"

#: gtk/gtkfontsel.c:636 gtk/gtkfontsel.c:858
msgid "Reset Filter"
msgstr "Berrosatu iragazkia"

#: gtk/gtkfontsel.c:650
msgid "Metric:"
msgstr "Metrotan"

#: gtk/gtkfontsel.c:654
msgid "Points"
msgstr "Puntutan"

#: gtk/gtkfontsel.c:661
msgid "Pixels"
msgstr "Pixeletan"

#. create the text entry widget
#: gtk/gtkfontsel.c:677
msgid "Preview:"
msgstr "Aurrikusi"

#: gtk/gtkfontsel.c:706
msgid "Font Information"
msgstr "Font-aren informazioa"

#: gtk/gtkfontsel.c:739
msgid "Requested Font Name:"
msgstr "Eskatutako Font-aren izena"

#: gtk/gtkfontsel.c:750
msgid "Actual Font Name:"
msgstr "Oraingo Font-aren izena"

#: gtk/gtkfontsel.c:761
#, c-format
msgid "%i fonts available with a total of %i styles."
msgstr "%i font aukeran guztira %i estiloez"

#: gtk/gtkfontsel.c:776
msgid "Filter"
msgstr "Iragazkia"

#: gtk/gtkfontsel.c:789
msgid "Font Types:"
msgstr "Font motak"

#: gtk/gtkfontsel.c:797
msgid "Bitmap"
msgstr "Bitmap"

#: gtk/gtkfontsel.c:803
msgid "Scalable"
msgstr "Eskalatzeko gai"

#: gtk/gtkfontsel.c:809
msgid "Scaled Bitmap"
msgstr "Eskalaratutako bitmap"

#: gtk/gtkfontsel.c:880
msgid "*"
msgstr "*"

#. Convert '(nil)' weights to 'regular', since it looks nicer.
#: gtk/gtkfontsel.c:1205
msgid "(nil)"
msgstr "(nil)"

#: gtk/gtkfontsel.c:1205
msgid "regular"
msgstr "arrunta"

#: gtk/gtkfontsel.c:1210 gtk/gtkfontsel.c:1953
msgid "italic"
msgstr "italikoa"

#: gtk/gtkfontsel.c:1211 gtk/gtkfontsel.c:1954
msgid "oblique"
msgstr "laprana"

#: gtk/gtkfontsel.c:1212 gtk/gtkfontsel.c:1955
msgid "reverse italic"
msgstr "atzerazko italikoa"

#: gtk/gtkfontsel.c:1213 gtk/gtkfontsel.c:1956
msgid "reverse oblique"
msgstr "atzerazko laprana"

#: gtk/gtkfontsel.c:1214 gtk/gtkfontsel.c:1957
msgid "other"
msgstr "bestelakoa"

#: gtk/gtkfontsel.c:1221
msgid "[M]"
msgstr "[M]"

#: gtk/gtkfontsel.c:1222
msgid "[C]"
msgstr "[C]"

#: gtk/gtkfontsel.c:1790
msgid "The selected font is not available."
msgstr "Aukeratutako Font-a ez dago"

#: gtk/gtkfontsel.c:1796
msgid "The selected font is not a valid font."
msgstr "Aukeratutako Font-a ez da Font baliagarria."

#: gtk/gtkfontsel.c:1857
msgid "This is a 2-byte font and may not be displayed correctly."
msgstr "Hau 2 byteko Font-a da eta agian ezin da modu egokian agertu."

#: gtk/gtkfontsel.c:1941 gtk/gtkinputdialog.c:607
msgid "(unknown)"
msgstr "(ezezaguna)"

#: gtk/gtkfontsel.c:1952
msgid "roman"
msgstr "erromatarra"

#: gtk/gtkfontsel.c:1964
msgid "proportional"
msgstr "proportziozkoa"

#: gtk/gtkfontsel.c:1965
msgid "monospaced"
msgstr "tartebakarrekoa"

#: gtk/gtkfontsel.c:1966
msgid "char cell"
msgstr "zelula ikuzia"

#: gtk/gtkfontsel.c:2166
msgid "Font: (Filter Applied)"
msgstr "Font-a: (Iragazkiaz)"

#: gtk/gtkfontsel.c:3507
msgid "Apply"
msgstr "Ezarri"

#: gtk/gtkfontsel.c:3529
msgid "Font Selection"
msgstr "Font aukera"

#: gtk/gtkgamma.c:395
msgid "Gamma"
msgstr "Gamma"

#: gtk/gtkgamma.c:402
msgid "Gamma value"
msgstr "Gamma balioa"

#. shell and main vbox
#: gtk/gtkinputdialog.c:200
msgid "Input"
msgstr "Sarrera"

#: gtk/gtkinputdialog.c:208
msgid "No input devices"
msgstr "Ez dago sarrerarako tresnarik"

#: gtk/gtkinputdialog.c:237
msgid "Device:"
msgstr "Tresna:"

#: gtk/gtkinputdialog.c:253
msgid "Disabled"
msgstr "Ezindua"

#: gtk/gtkinputdialog.c:261
msgid "Screen"
msgstr "Pantaila"

#: gtk/gtkinputdialog.c:269
msgid "Window"
msgstr "Lehioa"

#: gtk/gtkinputdialog.c:277
msgid "Mode: "
msgstr "Modua: "

#. The axis listbox
#: gtk/gtkinputdialog.c:307
msgid "Axes"
msgstr "Ardatzak"

#. Keys listbox
#: gtk/gtkinputdialog.c:323
msgid "Keys"
msgstr "Giltzak"

#. We create the save button in any case, so that clients can
#. connect to it, without paying attention to whether it exits
#: gtk/gtkinputdialog.c:345
msgid "Save"
msgstr "Gorde"

#: gtk/gtkinputdialog.c:500
msgid "X"
msgstr "X"

#: gtk/gtkinputdialog.c:501
msgid "Y"
msgstr "Y"

#: gtk/gtkinputdialog.c:502
msgid "Pressure"
msgstr "Presioa"

#: gtk/gtkinputdialog.c:503
msgid "X Tilt"
msgstr "X makur"

#: gtk/gtkinputdialog.c:504
msgid "Y Tilt"
msgstr "Y makur"

#: gtk/gtkinputdialog.c:544
msgid "none"
msgstr "batez"

#: gtk/gtkinputdialog.c:578 gtk/gtkinputdialog.c:614
msgid "(disabled)"
msgstr "(ezindua)"

#. and clear button
#: gtk/gtkinputdialog.c:692
msgid "clear"
msgstr "garbitu"

#: gtk/gtknotebook.c:2059 gtk/gtknotebook.c:4131
#, c-format
msgid "Page %u"
msgstr "Orria %u"

#: gtk/gtkrc.c:1732
#, c-format
msgid "Unable to locate image file in pixmap_path: \"%s\" line %d"
msgstr "Imaginaren fitxategia ezin aurkitu pixmap_path-en: \"%s\",%d lerroa"

#: gtk/gtkrc.c:1735
#, c-format
msgid "Unable to locate image file in pixmap_path: \"%s\""
msgstr "Imaginaren fitxategia ezin aurkitu pixmap_path-en: \"%s\""

#: gtk/gtkthemes.c:103
#, c-format
msgid "Unable to locate loadable module in module_path: \"%s\","
msgstr "Modulua ezin aurkitu modulu_path-en: \"%s\","

#: gtk/gtktipsquery.c:180
msgid "--- No Tip ---"
msgstr "--- Erpinik ez ---"

#~ msgid "heavy"
#~ msgstr "astuna"

#~ msgid "extrabold"
#~ msgstr "extrailuna"

#~ msgid "bold"
#~ msgstr "iluna"

#~ msgid "demibold"
#~ msgstr "erdi iluna"

#~ msgid "medium"
#~ msgstr "ertaina"

#~ msgid "normal"
#~ msgstr "arrunta"

#~ msgid "light"
#~ msgstr "argia"

#~ msgid "extralight"
#~ msgstr "extra argia"

#~ msgid "thin"
#~ msgstr "mehea"

#~ msgid "MAX_FONTS exceeded. Some fonts may be missing."
#~ msgstr "MAX_FONTS gehiegizkoa. Zenbait Font galduta daitezke."
