# trio/CMakeLists.txt
# Created by Robin Rowe 2020-03-09
# License: License GNU Lesser General Public License (GNU LGPL)

project(trio)
file(STRINGS sources.cmake SOURCES)
add_library(trio ${SOURCES})
