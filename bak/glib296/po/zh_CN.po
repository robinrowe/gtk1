# glib simplified chinese translation
# Copyright (C) YERA Free Software Foundation, Inc.
# He Qiangqiang <carton@263.net>, 2001.
# Funda Wang <fundawang@linux.net.cn>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: glib\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-02-10 15:36-0500\n"
"PO-Revision-Date: 2006-01-21 22:25+0800\n"
"Last-Translator: He Qiangqiang <carton@263.net>\n"
"Language-Team: zh_CN <i18n-translation@lists.linux.net.cn>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: glib/gconvert.c:408 glib/gconvert.c:486 glib/giochannel.c:1150
#, c-format
msgid "Conversion from character set '%s' to '%s' is not supported"
msgstr "不支持从字符集“%s”到“%s”的转换"

#: glib/gconvert.c:412 glib/gconvert.c:490
#, c-format
msgid "Could not open converter from '%s' to '%s'"
msgstr "无法打开从“%s”到“%s”的转换器"

#: glib/gconvert.c:606 glib/gconvert.c:995 glib/giochannel.c:1322
#: glib/giochannel.c:1364 glib/giochannel.c:2206 glib/gutf8.c:943
#: glib/gutf8.c:1392
#, c-format
msgid "Invalid byte sequence in conversion input"
msgstr "转换输入中出现无效字符序列"

#: glib/gconvert.c:612 glib/gconvert.c:922 glib/giochannel.c:1329
#: glib/giochannel.c:2218
#, c-format
msgid "Error during conversion: %s"
msgstr "转换过程中出错：%s"

#: glib/gconvert.c:647 glib/gutf8.c:939 glib/gutf8.c:1143 glib/gutf8.c:1284
#: glib/gutf8.c:1388
#, c-format
msgid "Partial character sequence at end of input"
msgstr "输入末尾出现未尽字符序列"

#: glib/gconvert.c:897
#, c-format
msgid "Cannot convert fallback '%s' to codeset '%s'"
msgstr "无法转换后备字符集“%s”到字符集“%s”"

#: glib/gconvert.c:1706
#, c-format
msgid "The URI '%s' is not an absolute URI using the \"file\" scheme"
msgstr "URI“%s”不是“file”格式的绝对 URI"

#: glib/gconvert.c:1716
#, c-format
msgid "The local file URI '%s' may not include a '#'"
msgstr "本地文件 URI“%s”不能包含“#”"

#: glib/gconvert.c:1733
#, c-format
msgid "The URI '%s' is invalid"
msgstr "URI“%s”无效"

#: glib/gconvert.c:1745
#, c-format
msgid "The hostname of the URI '%s' is invalid"
msgstr "URI中的主机名“%s”无效"

#: glib/gconvert.c:1761
#, c-format
msgid "The URI '%s' contains invalidly escaped characters"
msgstr "URI“%s”中包含无效的转义字符"

#: glib/gconvert.c:1855
#, c-format
msgid "The pathname '%s' is not an absolute path"
msgstr "路径名“%s”不是绝对路径"

#: glib/gconvert.c:1865
#, c-format
msgid "Invalid hostname"
msgstr "无效的主机名"

#: glib/gdir.c:121 glib/gdir.c:141
#, c-format
msgid "Error opening directory '%s': %s"
msgstr "打开目录“%s”时发生错误：%s"

#: glib/gfileutils.c:576 glib/gfileutils.c:649
#, c-format
msgid "Could not allocate %lu bytes to read file \"%s\""
msgstr "无法分配 %lu 字节以读取文件“%s”"

#: glib/gfileutils.c:591
#, c-format
msgid "Error reading file '%s': %s"
msgstr "读取文件“%s”出错：%s"

#: glib/gfileutils.c:673
#, c-format
msgid "Failed to read from file '%s': %s"
msgstr "读取文件“%s”失败：%s"

#: glib/gfileutils.c:724 glib/gfileutils.c:811
#, c-format
msgid "Failed to open file '%s': %s"
msgstr "打开文件“%s”失败：%s"

#: glib/gfileutils.c:741 glib/gmappedfile.c:133
#, c-format
msgid "Failed to get attributes of file '%s': fstat() failed: %s"
msgstr "获得文件“%s”的属性失败：fstat() 失败：%s"

#: glib/gfileutils.c:775
#, c-format
msgid "Failed to open file '%s': fdopen() failed: %s"
msgstr "打开文件“%s”失败：fdopen() 失败：%s"

#: glib/gfileutils.c:909
#, c-format
msgid "Failed to rename file '%s' to '%s': g_rename() failed: %s"
msgstr "将文件“%s”重命名为“%s”失败：g_rename() 失败：%s"

#: glib/gfileutils.c:950 glib/gfileutils.c:1415
#, c-format
msgid "Failed to create file '%s': %s"
msgstr "创建文件“%s”失败：%s"

#: glib/gfileutils.c:964
#, c-format
msgid "Failed to open file '%s' for writing: fdopen() failed: %s"
msgstr "打开文件“%s”写入失败：fdopen() 失败：%s"

#: glib/gfileutils.c:989
#, c-format
msgid "Failed to write file '%s': fwrite() failed: %s"
msgstr "写入文件“%s”失败：fwrite() 失败：%s"

#: glib/gfileutils.c:1008
#, c-format
msgid "Failed to close file '%s': fclose() failed: %s"
msgstr "关闭文件“%s”失败：fclose() 失败：%s"

#: glib/gfileutils.c:1126
#, c-format
msgid "Existing file '%s' could not be removed: g_unlink() failed: %s"
msgstr "无法删除已有文件“%s”：g_unlink() 失败：%s"

#: glib/gfileutils.c:1376
#, c-format
msgid "Template '%s' invalid, should not contain a '%s'"
msgstr "模板“%s”无效，不应该包含“%s”"

#: glib/gfileutils.c:1390
#, c-format
msgid "Template '%s' doesn't end with XXXXXX"
msgstr "模板“%s”的结尾不是 XXXXXX"

#: glib/gfileutils.c:1865
#, c-format
msgid "Failed to read the symbolic link '%s': %s"
msgstr "读取符号连接“%s”失败：%s"

#: glib/gfileutils.c:1886
#, c-format
msgid "Symbolic links not supported"
msgstr "不支持符号连接"

#: glib/giochannel.c:1154
#, c-format
msgid "Could not open converter from '%s' to '%s': %s"
msgstr "无法打开从“%s”到“%s”的转换器：%s"

#: glib/giochannel.c:1499
#, c-format
msgid "Can't do a raw read in g_io_channel_read_line_string"
msgstr "g_io_channel_read_line_string 函数无法进行原始读取"

#: glib/giochannel.c:1546 glib/giochannel.c:1803 glib/giochannel.c:1889
#, c-format
msgid "Leftover unconverted data in read buffer"
msgstr "在读缓冲里留有未转换数据"

#: glib/giochannel.c:1626 glib/giochannel.c:1703
#, c-format
msgid "Channel terminates in a partial character"
msgstr "通道终止于未尽字符"

#: glib/giochannel.c:1689
#, c-format
msgid "Can't do a raw read in g_io_channel_read_to_end"
msgstr "g_io_channel_read_to_end 函数无法进行原始读取"

#: glib/gmappedfile.c:116
#, c-format
msgid "Failed to open file '%s': open() failed: %s"
msgstr "打开文件“%s”失败：open() 失败：%s"

#: glib/gmappedfile.c:193
#, c-format
msgid "Failed to map file '%s': mmap() failed: %s"
msgstr "映射文件“%s”失败：mmap() 失败：%s"

#: glib/gmarkup.c:232
#, c-format
msgid "Error on line %d char %d: %s"
msgstr "第%d行第%d个字符出错：%s"

#: glib/gmarkup.c:330
#, c-format
msgid "Error on line %d: %s"
msgstr "第%d行出错：%s"

#: glib/gmarkup.c:434
msgid ""
"Empty entity '&;' seen; valid entities are: &amp; &quot; &lt; &gt; &apos;"
msgstr "发现空的实体“&;”。有效的实体为：&amp; &quot; &lt; &gt; &apos;"

#: glib/gmarkup.c:444
#, c-format
msgid ""
"Character '%s' is not valid at the start of an entity name; the & character "
"begins an entity; if this ampersand isn't supposed to be an entity, escape "
"it as &amp;"
msgstr ""
"字符“%s”出现在实体名的开头无效。实体都以 & 字符 开头，如果这个 & 不是一个实体"
"的开头，把它变为 &amp;"

#: glib/gmarkup.c:478
#, c-format
msgid "Character '%s' is not valid inside an entity name"
msgstr "字符“%s”在实体名中无效"

#: glib/gmarkup.c:515
#, c-format
msgid "Entity name '%s' is not known"
msgstr "未知的实体名“%s”"

#: glib/gmarkup.c:526
msgid ""
"Entity did not end with a semicolon; most likely you used an ampersand "
"character without intending to start an entity - escape ampersand as &amp;"
msgstr ""
"实体没有以分号结束。很可能您使用了 & 字符而又不是一个实体 - 将这个 & 变为 "
"&amp;"

#: glib/gmarkup.c:579
#, c-format
msgid ""
"Failed to parse '%-.*s', which should have been a digit inside a character "
"reference (&#234; for example) - perhaps the digit is too large"
msgstr "分析“%-.*s”失败。它应该是字符引用中的数字(如&#234;) - 可能该数字太大了"

#: glib/gmarkup.c:604
#, c-format
msgid "Character reference '%-.*s' does not encode a permitted character"
msgstr "字符引用“%-.*s”不是编码一个被允许的字符"

#: glib/gmarkup.c:619
msgid "Empty character reference; should include a digit such as &#454;"
msgstr "空的字符引用；应该包括数字，如 &#454;"

#: glib/gmarkup.c:629
msgid ""
"Character reference did not end with a semicolon; most likely you used an "
"ampersand character without intending to start an entity - escape ampersand "
"as &amp;"
msgstr ""
"字符引用没有以分号结束。很可能您使用了 & 字符而又不是一个实体 - 将这个 & 变"
"为 &amp;"

#: glib/gmarkup.c:715
msgid "Unfinished entity reference"
msgstr "未完成的实体引用"

#: glib/gmarkup.c:721
msgid "Unfinished character reference"
msgstr "未完成的字符引用"

#: glib/gmarkup.c:964 glib/gmarkup.c:992 glib/gmarkup.c:1023
msgid "Invalid UTF-8 encoded text"
msgstr "无效的 UTF-8 编码文本"

#: glib/gmarkup.c:1059
msgid "Document must begin with an element (e.g. <book>)"
msgstr "文档必须以一个元素开始(例如 <book>)"

#: glib/gmarkup.c:1099
#, c-format
msgid ""
"'%s' is not a valid character following a '<' character; it may not begin an "
"element name"
msgstr "“%s”出现在字符“<”后是无效字符；它不能作为元素名的开头"

#: glib/gmarkup.c:1163
#, c-format
msgid ""
"Odd character '%s', expected a '>' character to end the start tag of element "
"'%s'"
msgstr "字符“%s”无效，应该以字符“>”来结束元素“%s”的起始标记"

#: glib/gmarkup.c:1252
#, c-format
msgid ""
"Odd character '%s', expected a '=' after attribute name '%s' of element '%s'"
msgstr "字符“%s”无效，在属性名“%s”(元素“%s”)的后应该是字符“=”"

#: glib/gmarkup.c:1294
#, c-format
msgid ""
"Odd character '%s', expected a '>' or '/' character to end the start tag of "
"element '%s', or optionally an attribute; perhaps you used an invalid "
"character in an attribute name"
msgstr ""
"字符“%s”无效，应该以“>”或“/”结束元素“%s”的起始标记，或紧跟该元素的属性；可能"
"您在属性名中使用了无效字符"

#: glib/gmarkup.c:1383
#, c-format
msgid ""
"Odd character '%s', expected an open quote mark after the equals sign when "
"giving value for attribute '%s' of element '%s'"
msgstr "字符“%s”无效，在给属性“%s”(元素“%s”)赋值时，在等号后应该是引号"

#: glib/gmarkup.c:1528
#, c-format
msgid ""
"'%s' is not a valid character following the characters '</'; '%s' may not "
"begin an element name"
msgstr "“%s”出现在字符“</”后无效；“%s”不能作为元素名的开头"

#: glib/gmarkup.c:1568
#, c-format
msgid ""
"'%s' is not a valid character following the close element name '%s'; the "
"allowed character is '>'"
msgstr "“%s”出现在结束的元素名“%s”后无效；允许的字符是“>”"

#: glib/gmarkup.c:1579
#, c-format
msgid "Element '%s' was closed, no element is currently open"
msgstr "元素“%s”已经结束，没有未结束的元素"

#: glib/gmarkup.c:1588
#, c-format
msgid "Element '%s' was closed, but the currently open element is '%s'"
msgstr "元素“%s”已经结束，当前未结束的元素是“%s”"

#: glib/gmarkup.c:1735
msgid "Document was empty or contained only whitespace"
msgstr "文档为空或仅含空白字符"

#: glib/gmarkup.c:1749
msgid "Document ended unexpectedly just after an open angle bracket '<'"
msgstr "文档在一个打开的尖括号“<”后意外结束"

#: glib/gmarkup.c:1757 glib/gmarkup.c:1801
#, c-format
msgid ""
"Document ended unexpectedly with elements still open - '%s' was the last "
"element opened"
msgstr "文档在还存在未结束元素时意外结束 - 最后的未结束元素是“%s”"

#: glib/gmarkup.c:1765
#, c-format
msgid ""
"Document ended unexpectedly, expected to see a close angle bracket ending "
"the tag <%s/>"
msgstr "文档意外结束，应该以右尖括号“>”来结束标记 <%s/>"

#: glib/gmarkup.c:1771
msgid "Document ended unexpectedly inside an element name"
msgstr "文档在元素名中意外结束"

#: glib/gmarkup.c:1776
msgid "Document ended unexpectedly inside an attribute name"
msgstr "文档在属性名中意外结束"

#: glib/gmarkup.c:1781
msgid "Document ended unexpectedly inside an element-opening tag."
msgstr "文档在元素起始标记中意外结束"

#: glib/gmarkup.c:1787
msgid ""
"Document ended unexpectedly after the equals sign following an attribute "
"name; no attribute value"
msgstr "文档在跟在属性名后的等号后意外结束；没有属性值"

#: glib/gmarkup.c:1794
msgid "Document ended unexpectedly while inside an attribute value"
msgstr "文档在属性值中意外结束"

#: glib/gmarkup.c:1809
#, c-format
msgid "Document ended unexpectedly inside the close tag for element '%s'"
msgstr "文档在元素“%s”结束标记中意外结束"

#: glib/gmarkup.c:1815
msgid "Document ended unexpectedly inside a comment or processing instruction"
msgstr "文档在注释或处理指令中意外结束"

#: glib/gshell.c:73
#, c-format
msgid "Quoted text doesn't begin with a quotation mark"
msgstr "引用的文本没有以引号开头"

#: glib/gshell.c:163
#, c-format
msgid "Unmatched quotation mark in command line or other shell-quoted text"
msgstr "命令行或其他shell引用文本中出现不匹配的引号"

#: glib/gshell.c:541
#, c-format
msgid "Text ended just after a '\\' character. (The text was '%s')"
msgstr "文本在一个“\\”字符后结束。(文本为“%s”)"

#: glib/gshell.c:548
#, c-format
msgid "Text ended before matching quote was found for %c. (The text was '%s')"
msgstr "文本在找到与 %c 匹配的引号之前结束。(文本为“%s”)"

#: glib/gshell.c:560
#, c-format
msgid "Text was empty (or contained only whitespace)"
msgstr "空文本(或仅含空白字符)"

#: glib/gspawn-win32.c:276
#, c-format
msgid "Failed to read data from child process"
msgstr "从子进程中读取数据失败"

#: glib/gspawn-win32.c:291 glib/gspawn.c:1364
#, c-format
msgid "Failed to create pipe for communicating with child process (%s)"
msgstr "创建与子进程通讯的管道失败(%s)"

#: glib/gspawn-win32.c:329 glib/gspawn.c:1028
#, c-format
msgid "Failed to read from child pipe (%s)"
msgstr "从子管道中读取失败(%s)"

#: glib/gspawn-win32.c:355 glib/gspawn.c:1233
#, c-format
msgid "Failed to change to directory '%s' (%s)"
msgstr "更改到目录“%s”失败(%s)"

#: glib/gspawn-win32.c:361 glib/gspawn-win32.c:581
#, c-format
msgid "Failed to execute child process (%s)"
msgstr "执行子进程失败(%s)"

#: glib/gspawn-win32.c:471 glib/gspawn-win32.c:527
#, c-format
msgid "Invalid program name: %s"
msgstr "无效的程序名：%s"

#: glib/gspawn-win32.c:481 glib/gspawn-win32.c:537 glib/gspawn-win32.c:780
#: glib/gspawn-win32.c:835 glib/gspawn-win32.c:1370
#, c-format
msgid "Invalid string in argument vector at %d: %s"
msgstr "%d 处的参数中有无效的字符串：%s"

#: glib/gspawn-win32.c:492 glib/gspawn-win32.c:548 glib/gspawn-win32.c:794
#: glib/gspawn-win32.c:848 glib/gspawn-win32.c:1403
#, c-format
msgid "Invalid string in environment: %s"
msgstr "环境中有无效的字符串：%s"

#: glib/gspawn-win32.c:776 glib/gspawn-win32.c:831 glib/gspawn-win32.c:1351
#, c-format
msgid "Invalid working directory: %s"
msgstr "无效的工作目录：%s"

#: glib/gspawn-win32.c:890
#, c-format
msgid "Failed to execute helper program (%s)"
msgstr "执行助手程序(%s)失败"

#: glib/gspawn-win32.c:1090
#, c-format
msgid ""
"Unexpected error in g_io_channel_win32_poll() reading data from a child "
"process"
msgstr "g_io_channel_win32_poll() 从子进程中读取数据时出现异常错误"

#: glib/gspawn.c:168
#, c-format
msgid "Failed to read data from child process (%s)"
msgstr "从子进程中读取数据失败(%s)"

#: glib/gspawn.c:300
#, c-format
msgid "Unexpected error in select() reading data from a child process (%s)"
msgstr "select() 在从子进程中读取数据时出现异常错误 (%s)"

#: glib/gspawn.c:383
#, c-format
msgid "Unexpected error in waitpid() (%s)"
msgstr "waitpid() 出现异常错误 (%s)"

#: glib/gspawn.c:1093
#, c-format
msgid "Failed to fork (%s)"
msgstr "fork 失败(%s)"

#: glib/gspawn.c:1243
#, c-format
msgid "Failed to execute child process \"%s\" (%s)"
msgstr "执行子进程“%s”失败(%s)"

#: glib/gspawn.c:1253
#, c-format
msgid "Failed to redirect output or input of child process (%s)"
msgstr "重定向子进程(%s)的输入或输出失败"

#: glib/gspawn.c:1262
#, c-format
msgid "Failed to fork child process (%s)"
msgstr "fork 子进程失败 (%s)"

#: glib/gspawn.c:1270
#, c-format
msgid "Unknown error executing child process \"%s\""
msgstr "执行子进程“%s”时出现未知错误"

#: glib/gspawn.c:1292
#, c-format
msgid "Failed to read enough data from child pid pipe (%s)"
msgstr "从子进程管道中读取足够的数据失败(%s)"

#: glib/gutf8.c:1017
#, c-format
msgid "Character out of range for UTF-8"
msgstr "字符超出 UTF-8 范围"

#: glib/gutf8.c:1111 glib/gutf8.c:1120 glib/gutf8.c:1252 glib/gutf8.c:1261
#: glib/gutf8.c:1402 glib/gutf8.c:1498
#, c-format
msgid "Invalid sequence in conversion input"
msgstr "转换输入中出现无效序列"

#: glib/gutf8.c:1413 glib/gutf8.c:1509
#, c-format
msgid "Character out of range for UTF-16"
msgstr "字符超出 UTF-16 范围"

#: glib/goption.c:468
msgid "Usage:"
msgstr "用法："

#: glib/goption.c:468
msgid "[OPTION...]"
msgstr "[选项...]"

#: glib/goption.c:556
msgid "Help Options:"
msgstr "帮助选项："

#: glib/goption.c:557
msgid "Show help options"
msgstr "显示帮助选项"

#: glib/goption.c:562
msgid "Show all help options"
msgstr "显示全部帮助选项"

#: glib/goption.c:612
msgid "Application Options:"
msgstr "应用程序选项："

#: glib/goption.c:653
#, c-format
msgid "Cannot parse integer value '%s' for %s"
msgstr "无法处理 %2$s 所用的整数值“%1$s”"

#: glib/goption.c:663
#, c-format
msgid "Integer value '%s' for %s out of range"
msgstr "%2$s 所用的整数值“%1$s”超出范围"

#: glib/goption.c:926
#, c-format
msgid "Error parsing option %s"
msgstr "分析选项出错：%s"

#: glib/goption.c:959 glib/goption.c:1070
#, c-format
msgid "Missing argument for %s"
msgstr "缺少 %s 的参数"

#: glib/goption.c:1474
#, c-format
msgid "Unknown option %s"
msgstr "未知选项 %s"

#: glib/gkeyfile.c:339
#, c-format
msgid "Valid key file could not be found in data dirs"
msgstr "无法在数据目录中找到有效的键文件"

#: glib/gkeyfile.c:374
#, c-format
msgid "Not a regular file"
msgstr "不是普通文件"

#: glib/gkeyfile.c:382
#, c-format
msgid "File is empty"
msgstr "文件为空"

#: glib/gkeyfile.c:697
#, c-format
msgid ""
"Key file contains line '%s' which is not a key-value pair, group, or comment"
msgstr "键文件中的行“%s”不是键-值对、组或注释"

#: glib/gkeyfile.c:765
#, c-format
msgid "Key file does not start with a group"
msgstr "键文件不以组开始"

#: glib/gkeyfile.c:808
#, c-format
msgid "Key file contains unsupported encoding '%s'"
msgstr "键文件包含不支持的编码“%s”"

#: glib/gkeyfile.c:1017 glib/gkeyfile.c:1176 glib/gkeyfile.c:2177
#: glib/gkeyfile.c:2242 glib/gkeyfile.c:2361 glib/gkeyfile.c:2497
#: glib/gkeyfile.c:2649 glib/gkeyfile.c:2823 glib/gkeyfile.c:2880
#, c-format
msgid "Key file does not have group '%s'"
msgstr "键文件没有组“%s”"

#: glib/gkeyfile.c:1188
#, c-format
msgid "Key file does not have key '%s'"
msgstr "键文件没有键“%s”"

#: glib/gkeyfile.c:1289 glib/gkeyfile.c:1398
#, c-format
msgid "Key file contains key '%s' with value '%s' which is not UTF-8"
msgstr "键文件包含“%s”，其值“%s”不是 UTF-8"

#: glib/gkeyfile.c:1307 glib/gkeyfile.c:1416 glib/gkeyfile.c:1788
#, c-format
msgid "Key file contains key '%s' which has value that cannot be interpreted."
msgstr "键文件包含键“%s”，其值无法解释。"

#: glib/gkeyfile.c:2004
#, c-format
msgid ""
"Key file contains key '%s' in group '%s' which has value that cannot be "
"interpreted."
msgstr "键文件在“%2$s”中包含“%1$s”，其值无法解释。"

#: glib/gkeyfile.c:2192 glib/gkeyfile.c:2376 glib/gkeyfile.c:2891
#, c-format
msgid "Key file does not have key '%s' in group '%s'"
msgstr "键文件的组“%2$s”中不包含键“%1$s”"

#: glib/gkeyfile.c:3067
#, c-format
msgid "Key file contains escape character at end of line"
msgstr "键文件在行尾含有转义字符"

#: glib/gkeyfile.c:3089
#, c-format
msgid "Key file contains invalid escape sequence '%s'"
msgstr "键文件中包含无效的转义序列“%s”"

#: glib/gkeyfile.c:3230
#, c-format
msgid "Value '%s' cannot be interpreted as a number."
msgstr "无法将值“%s”解释为数值。"

#: glib/gkeyfile.c:3240
#, c-format
msgid "Integer value '%s' out of range"
msgstr "整数值“%s”超出范围"

#: glib/gkeyfile.c:3270
#, c-format
msgid "Value '%s' cannot be interpreted as a boolean."
msgstr "无法将值“%s”解释为布尔值。"

#~ msgid "Could not change file mode: fork() failed: %s"
#~ msgstr "无法更改文件模式：fork() 失败：%s"

#~ msgid "Could not change file mode: waitpid() failed: %s"
#~ msgstr "无法更改文件模式：waitpid() 失败：%s"

#~ msgid "Could not change file mode: chmod() failed: %s"
#~ msgstr "无法更改文件模式：chmod() 失败：%s"

#~ msgid "Could not change file mode: Child terminated by signal: %s"
#~ msgstr "无法更改文件模式：子进程被信号中止：%s"

#~ msgid "Could not change file mode: Child terminated abnormally"
#~ msgstr "无法更改文件模式：子进程异常中止"
