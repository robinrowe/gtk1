# Icelandic translation of glib
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the glib package.
# Richard Allen <ra@ra.is>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: glib 2.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-02-10 15:36-0500\n"
"PO-Revision-Date: 2003-08-18 18:05+0000\n"
"Last-Translator: Richard Allen <ra@ra.is>\n"
"Language-Team: is <is@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: glib/gconvert.c:408 glib/gconvert.c:486 glib/giochannel.c:1150
#, c-format
msgid "Conversion from character set '%s' to '%s' is not supported"
msgstr "Umbreyting úr stafasettinu '%s' í '%s' er ekki stutt"

#: glib/gconvert.c:412 glib/gconvert.c:490
#, fuzzy, c-format
msgid "Could not open converter from '%s' to '%s'"
msgstr "Gat ekki opnað umbreyti úr '%s' í '%s': %s"

#: glib/gconvert.c:606 glib/gconvert.c:995 glib/giochannel.c:1322
#: glib/giochannel.c:1364 glib/giochannel.c:2206 glib/gutf8.c:943
#: glib/gutf8.c:1392
#, c-format
msgid "Invalid byte sequence in conversion input"
msgstr "Ógild bætaruna í ílagi umbreytingar"

#: glib/gconvert.c:612 glib/gconvert.c:922 glib/giochannel.c:1329
#: glib/giochannel.c:2218
#, c-format
msgid "Error during conversion: %s"
msgstr "Villa við umbreytingu: %s"

#: glib/gconvert.c:647 glib/gutf8.c:939 glib/gutf8.c:1143 glib/gutf8.c:1284
#: glib/gutf8.c:1388
#, c-format
msgid "Partial character sequence at end of input"
msgstr "Ókláruð stafaruna í enda ílags"

#: glib/gconvert.c:897
#, c-format
msgid "Cannot convert fallback '%s' to codeset '%s'"
msgstr "Gat ekki umbreytt '%s' í stafatöflu '%s'"

#: glib/gconvert.c:1706
#, fuzzy, c-format
msgid "The URI '%s' is not an absolute URI using the \"file\" scheme"
msgstr "URI '%s' er ekki fullt URI sem notar 'file' skemuna"

#: glib/gconvert.c:1716
#, c-format
msgid "The local file URI '%s' may not include a '#'"
msgstr "Skráar-URI '%s' má ekki innihalda '#'"

#: glib/gconvert.c:1733
#, c-format
msgid "The URI '%s' is invalid"
msgstr "URI '%s' er ógilt"

#: glib/gconvert.c:1745
#, c-format
msgid "The hostname of the URI '%s' is invalid"
msgstr "Vélarheitið í URI '%s' er ógilt"

#: glib/gconvert.c:1761
#, c-format
msgid "The URI '%s' contains invalidly escaped characters"
msgstr "URI '%s' inniheldur ógild sértákn"

#: glib/gconvert.c:1855
#, c-format
msgid "The pathname '%s' is not an absolute path"
msgstr "Slóðin '%s' er ekki full slóð"

#: glib/gconvert.c:1865
#, c-format
msgid "Invalid hostname"
msgstr "Ógilt vélarheiti"

#: glib/gdir.c:121 glib/gdir.c:141
#, c-format
msgid "Error opening directory '%s': %s"
msgstr "Villa við að opna möppuna '%s': %s"

#: glib/gfileutils.c:576 glib/gfileutils.c:649
#, c-format
msgid "Could not allocate %lu bytes to read file \"%s\""
msgstr "Gat ekki frátekið %lu bæti til að lesa skrána \"%s\""

#: glib/gfileutils.c:591
#, c-format
msgid "Error reading file '%s': %s"
msgstr "Villa við lestur skráarinnar '%s': %s"

#: glib/gfileutils.c:673
#, c-format
msgid "Failed to read from file '%s': %s"
msgstr "Gat ekki lesið úr skránni '%s': %s"

#: glib/gfileutils.c:724 glib/gfileutils.c:811
#, c-format
msgid "Failed to open file '%s': %s"
msgstr "Gat ekki opnað skrána '%s': %s"

#: glib/gfileutils.c:741 glib/gmappedfile.c:133
#, c-format
msgid "Failed to get attributes of file '%s': fstat() failed: %s"
msgstr "gat ekki lesið eiginleika skráarinnar '%s': fstat() brást: %s"

#: glib/gfileutils.c:775
#, c-format
msgid "Failed to open file '%s': fdopen() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gfileutils.c:909
#, fuzzy, c-format
msgid "Failed to rename file '%s' to '%s': g_rename() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gfileutils.c:950 glib/gfileutils.c:1415
#, c-format
msgid "Failed to create file '%s': %s"
msgstr "gat ekki búið til skrána '%s': %s"

#: glib/gfileutils.c:964
#, fuzzy, c-format
msgid "Failed to open file '%s' for writing: fdopen() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gfileutils.c:989
#, fuzzy, c-format
msgid "Failed to write file '%s': fwrite() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gfileutils.c:1008
#, fuzzy, c-format
msgid "Failed to close file '%s': fclose() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gfileutils.c:1126
#, c-format
msgid "Existing file '%s' could not be removed: g_unlink() failed: %s"
msgstr ""

#: glib/gfileutils.c:1376
#, c-format
msgid "Template '%s' invalid, should not contain a '%s'"
msgstr "Sniðmátið '%s' er ógilt og ætti ekki að innihalda '%s'"

#: glib/gfileutils.c:1390
#, c-format
msgid "Template '%s' doesn't end with XXXXXX"
msgstr "Sniðmátið '%s' endar ekki á XXXXXX"

#: glib/gfileutils.c:1865
#, c-format
msgid "Failed to read the symbolic link '%s': %s"
msgstr "gat ekki lesið tákntengið '%s': %s"

#: glib/gfileutils.c:1886
#, c-format
msgid "Symbolic links not supported"
msgstr "Tákntengi eru ekki studd"

#: glib/giochannel.c:1154
#, fuzzy, c-format
msgid "Could not open converter from '%s' to '%s': %s"
msgstr "Gat ekki opnað umbreyti úr `%s' í `%s': %s"

#: glib/giochannel.c:1499
#, c-format
msgid "Can't do a raw read in g_io_channel_read_line_string"
msgstr "Gat ekki lesið í g_io_channel_read_line_string"

#: glib/giochannel.c:1546 glib/giochannel.c:1803 glib/giochannel.c:1889
#, c-format
msgid "Leftover unconverted data in read buffer"
msgstr "Það eru eftir óumbreytt gögn í lesminninu"

#: glib/giochannel.c:1626 glib/giochannel.c:1703
#, c-format
msgid "Channel terminates in a partial character"
msgstr "Rásin endar á hluta úr tákni"

#: glib/giochannel.c:1689
#, c-format
msgid "Can't do a raw read in g_io_channel_read_to_end"
msgstr "Gat ekki lesið í g_io_channel_read_to_end"

#: glib/gmappedfile.c:116
#, fuzzy, c-format
msgid "Failed to open file '%s': open() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gmappedfile.c:193
#, fuzzy, c-format
msgid "Failed to map file '%s': mmap() failed: %s"
msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#: glib/gmarkup.c:232
#, c-format
msgid "Error on line %d char %d: %s"
msgstr "Villa á línu %d tákn %d: %s"

#: glib/gmarkup.c:330
#, c-format
msgid "Error on line %d: %s"
msgstr "Villa á línu %d: %s"

#: glib/gmarkup.c:434
msgid ""
"Empty entity '&;' seen; valid entities are: &amp; &quot; &lt; &gt; &apos;"
msgstr ""
"Tómt viðfang '&;' fannst; gild viðföng eru: &amp; &quot; &lt; &gt; &apos;"

#: glib/gmarkup.c:444
#, c-format
msgid ""
"Character '%s' is not valid at the start of an entity name; the & character "
"begins an entity; if this ampersand isn't supposed to be an entity, escape "
"it as &amp;"
msgstr ""
"Táknið '%s' er ógilt í upphafi heiti viðfanga; & táknið byrjar viðfang; ef "
"Þetta og-merki á ekki að vera byrjun viðfangs ættir þú að rita það sem &amp;"

#: glib/gmarkup.c:478
#, c-format
msgid "Character '%s' is not valid inside an entity name"
msgstr "Táknið '%s' er ekki gilt í heitum viðfanga"

#: glib/gmarkup.c:515
#, c-format
msgid "Entity name '%s' is not known"
msgstr "Viðfangið '%s' er óþekkt"

#: glib/gmarkup.c:526
msgid ""
"Entity did not end with a semicolon; most likely you used an ampersand "
"character without intending to start an entity - escape ampersand as &amp;"
msgstr ""
"Viðfangið endar ekki á semikommu; líklega notaðir þú og-merkið án þess að "
"ætla að byrja viðfang. Ritaðu það sem &amp;"

#: glib/gmarkup.c:579
#, fuzzy, c-format
msgid ""
"Failed to parse '%-.*s', which should have been a digit inside a character "
"reference (&#234; for example) - perhaps the digit is too large"
msgstr ""
"Gat ekki þáttað '%s' sem ætti að vera tölustafur innan í tilvísun í tákn "
"(til dæmis &#234;). Ef til vill er talan of stór"

#: glib/gmarkup.c:604
#, fuzzy, c-format
msgid "Character reference '%-.*s' does not encode a permitted character"
msgstr "Tákntilvísunin '%s' vísar ekki í leyfilegt tákn"

#: glib/gmarkup.c:619
msgid "Empty character reference; should include a digit such as &#454;"
msgstr "Tóm tákntilvísun; hún ætti að innihalda tölur eins og &#454;"

#: glib/gmarkup.c:629
msgid ""
"Character reference did not end with a semicolon; most likely you used an "
"ampersand character without intending to start an entity - escape ampersand "
"as &amp;"
msgstr ""
"Viðfangið endar ekki á semikommu; líklega notaðir þú og-merkið án þess að "
"ætla að byrja viðfang. Ritaðu það sem &amp;"

#: glib/gmarkup.c:715
msgid "Unfinished entity reference"
msgstr "Hálfkláruð viðfangatilvísun"

#: glib/gmarkup.c:721
msgid "Unfinished character reference"
msgstr "Hálfkláruð tákntilvísun"

#: glib/gmarkup.c:964 glib/gmarkup.c:992 glib/gmarkup.c:1023
msgid "Invalid UTF-8 encoded text"
msgstr "Ógildur UTF-8 þýddur texti"

#: glib/gmarkup.c:1059
msgid "Document must begin with an element (e.g. <book>)"
msgstr "Skjalið verður að byrja á viðfangi (t.d. <book>)"

#: glib/gmarkup.c:1099
#, c-format
msgid ""
"'%s' is not a valid character following a '<' character; it may not begin an "
"element name"
msgstr ""
"'%s' er ekki gilt tákn strax á eftir '<' tákninu; það má ekki byrja á heiti "
"viðfangs"

#: glib/gmarkup.c:1163
#, c-format
msgid ""
"Odd character '%s', expected a '>' character to end the start tag of element "
"'%s'"
msgstr "Undarlegt tákn '%s', átti von á '>' tákninu til að enda viðfangið '%s'"

#: glib/gmarkup.c:1252
#, c-format
msgid ""
"Odd character '%s', expected a '=' after attribute name '%s' of element '%s'"
msgstr ""
"Undarlegt tákn '%s', átti von á '=' eftir heiti eiginleika '%s' af mengi '%s'"

#: glib/gmarkup.c:1294
#, c-format
msgid ""
"Odd character '%s', expected a '>' or '/' character to end the start tag of "
"element '%s', or optionally an attribute; perhaps you used an invalid "
"character in an attribute name"
msgstr ""
"Undarlegt tákn '%s', átti von á '>' eða '/' tákni rtil þess að enda upphafs "
"viðfangi '%s', eða eiginleika; Þú notaðir ef til vill ógilt tákn í heiti "
"eiginleika"

#: glib/gmarkup.c:1383
#, c-format
msgid ""
"Odd character '%s', expected an open quote mark after the equals sign when "
"giving value for attribute '%s' of element '%s'"
msgstr ""
"Undarlegt tákn '%s', átti von á tilvísunarmerki eftir samasem merkinu þegar "
"gildi er gefið með eiginleikanum '%s' af menginu '%s'"

#: glib/gmarkup.c:1528
#, c-format
msgid ""
"'%s' is not a valid character following the characters '</'; '%s' may not "
"begin an element name"
msgstr ""
"'%s' er ekki gilt tákn strax á eftir '</'; '%s' má ekki vera fyrsta tákn í "
"heiti mengis"

#: glib/gmarkup.c:1568
#, c-format
msgid ""
"'%s' is not a valid character following the close element name '%s'; the "
"allowed character is '>'"
msgstr ""
"'%s' er ekki gilt tákn strax á eftir lokun mengis '%s'. Leyfilegt tákn er '>'"

#: glib/gmarkup.c:1579
#, c-format
msgid "Element '%s' was closed, no element is currently open"
msgstr "Mengið '%s' var lokað og engin önnur mengi eru opin"

#: glib/gmarkup.c:1588
#, c-format
msgid "Element '%s' was closed, but the currently open element is '%s'"
msgstr "Mengið '%s' var lokað en mengið sem nú er opið er '%s'"

#: glib/gmarkup.c:1735
msgid "Document was empty or contained only whitespace"
msgstr "Skjalið var tómt eða innihélt einungis orðabil"

#: glib/gmarkup.c:1749
msgid "Document ended unexpectedly just after an open angle bracket '<'"
msgstr "Skjalið endar óvænt rétt eftir opið minna en merki '<'"

#: glib/gmarkup.c:1757 glib/gmarkup.c:1801
#, c-format
msgid ""
"Document ended unexpectedly with elements still open - '%s' was the last "
"element opened"
msgstr ""
"Skjalið endar óvænt með mengi sem enn eru opin. '%s' var mengið sem síðast "
"var opnað"

#: glib/gmarkup.c:1765
#, c-format
msgid ""
"Document ended unexpectedly, expected to see a close angle bracket ending "
"the tag <%s/>"
msgstr ""
"Skjalið endar óvænt. Átti von á að sjá stærraen merki sem lokar taginu <%s/>"

#: glib/gmarkup.c:1771
msgid "Document ended unexpectedly inside an element name"
msgstr "Skjalið endar óvænt inn í heiti mengis"

#: glib/gmarkup.c:1776
msgid "Document ended unexpectedly inside an attribute name"
msgstr "Skjalið endar óvænt inn í heiti eiginleika"

#: glib/gmarkup.c:1781
msgid "Document ended unexpectedly inside an element-opening tag."
msgstr "Skjalið endar óvænt inn í tagi sem opnar mengi."

#: glib/gmarkup.c:1787
msgid ""
"Document ended unexpectedly after the equals sign following an attribute "
"name; no attribute value"
msgstr ""
"Skjalið endar óvænt eftir samasem merkið sem fylgir heiti eiginleika og það "
"er ekkert gildi"

#: glib/gmarkup.c:1794
msgid "Document ended unexpectedly while inside an attribute value"
msgstr "Skjalið endar óvænt inn í gildi eiginleika"

#: glib/gmarkup.c:1809
#, c-format
msgid "Document ended unexpectedly inside the close tag for element '%s'"
msgstr "Skjalið endar óvænt inni í lokunartagi fyrir mengið '%s'"

#: glib/gmarkup.c:1815
msgid "Document ended unexpectedly inside a comment or processing instruction"
msgstr "Skjalið endar óvænt inni í athugasemd eða í miðri skipun"

#: glib/gshell.c:73
#, c-format
msgid "Quoted text doesn't begin with a quotation mark"
msgstr "Tilvísunin byrjar ekki á spurningarmerki"

#: glib/gshell.c:163
#, c-format
msgid "Unmatched quotation mark in command line or other shell-quoted text"
msgstr "Tilvísunarmerki stemma ekki í skipanalínunni eða öðrum texta"

#: glib/gshell.c:541
#, c-format
msgid "Text ended just after a '\\' character. (The text was '%s')"
msgstr "Textinn endaði eftir '\\' tákn. (Textinn var '%s')"

#: glib/gshell.c:548
#, c-format
msgid "Text ended before matching quote was found for %c. (The text was '%s')"
msgstr "Textinn endaði áður en samstaða við %c fannst. (Textinn var '%s')"

#: glib/gshell.c:560
#, c-format
msgid "Text was empty (or contained only whitespace)"
msgstr "Textinn var tómur (eða innihélt eingöngu orðabil)"

#: glib/gspawn-win32.c:276
#, c-format
msgid "Failed to read data from child process"
msgstr "Gat ekki lesið gögn frá undirferli"

#: glib/gspawn-win32.c:291 glib/gspawn.c:1364
#, c-format
msgid "Failed to create pipe for communicating with child process (%s)"
msgstr "Gat ekki búið til pípu til samskipta við undirferli (%s)"

#: glib/gspawn-win32.c:329 glib/gspawn.c:1028
#, c-format
msgid "Failed to read from child pipe (%s)"
msgstr "Gat ekki lesið úr undirferlispípu (%s)"

#: glib/gspawn-win32.c:355 glib/gspawn.c:1233
#, c-format
msgid "Failed to change to directory '%s' (%s)"
msgstr "Gat ekki farið í möppuna '%s' (%s)"

#: glib/gspawn-win32.c:361 glib/gspawn-win32.c:581
#, c-format
msgid "Failed to execute child process (%s)"
msgstr "Gat ekki keyrt undirferli (%s)"

#: glib/gspawn-win32.c:471 glib/gspawn-win32.c:527
#, fuzzy, c-format
msgid "Invalid program name: %s"
msgstr "Ógilt vélarheiti"

#: glib/gspawn-win32.c:481 glib/gspawn-win32.c:537 glib/gspawn-win32.c:780
#: glib/gspawn-win32.c:835 glib/gspawn-win32.c:1370
#, c-format
msgid "Invalid string in argument vector at %d: %s"
msgstr ""

#: glib/gspawn-win32.c:492 glib/gspawn-win32.c:548 glib/gspawn-win32.c:794
#: glib/gspawn-win32.c:848 glib/gspawn-win32.c:1403
#, fuzzy, c-format
msgid "Invalid string in environment: %s"
msgstr "Ógild runa í ílagi umbreytingar"

#: glib/gspawn-win32.c:776 glib/gspawn-win32.c:831 glib/gspawn-win32.c:1351
#, fuzzy, c-format
msgid "Invalid working directory: %s"
msgstr "Villa við að opna möppuna '%s': %s"

#: glib/gspawn-win32.c:890
#, fuzzy, c-format
msgid "Failed to execute helper program (%s)"
msgstr "Gat ekki keyrt hjálparforrit"

#: glib/gspawn-win32.c:1090
#, c-format
msgid ""
"Unexpected error in g_io_channel_win32_poll() reading data from a child "
"process"
msgstr "Óvænt villa í g_io_channel_win32_poll() við lestur úr undirferli"

#: glib/gspawn.c:168
#, c-format
msgid "Failed to read data from child process (%s)"
msgstr "Gat ekki lesið gögn frá undirferli (%s)"

#: glib/gspawn.c:300
#, c-format
msgid "Unexpected error in select() reading data from a child process (%s)"
msgstr "Óvæn villa í select() við lestur gagna frá undirferli (%s)"

#: glib/gspawn.c:383
#, c-format
msgid "Unexpected error in waitpid() (%s)"
msgstr "Óvæn villa í waitpid() (%s)"

#: glib/gspawn.c:1093
#, c-format
msgid "Failed to fork (%s)"
msgstr "Gat ekki ræst (%s)"

#: glib/gspawn.c:1243
#, c-format
msgid "Failed to execute child process \"%s\" (%s)"
msgstr "Gat ekki ræst undirferli \"%s\" (%s)"

#: glib/gspawn.c:1253
#, c-format
msgid "Failed to redirect output or input of child process (%s)"
msgstr "Gat ekki sent frálag eða ílag underferlis annað (%s)"

#: glib/gspawn.c:1262
#, c-format
msgid "Failed to fork child process (%s)"
msgstr "Gat ekki ræst undirferli (%s)"

#: glib/gspawn.c:1270
#, c-format
msgid "Unknown error executing child process \"%s\""
msgstr "Óþekkt villa við keyrslu undirferlis \"%s\""

#: glib/gspawn.c:1292
#, c-format
msgid "Failed to read enough data from child pid pipe (%s)"
msgstr "Gat ekki lesið nægjanleg gögn úr pípunni til undirferlisins (%s)"

#: glib/gutf8.c:1017
#, c-format
msgid "Character out of range for UTF-8"
msgstr "Táknið er utan UTF-8 sviðsins"

#: glib/gutf8.c:1111 glib/gutf8.c:1120 glib/gutf8.c:1252 glib/gutf8.c:1261
#: glib/gutf8.c:1402 glib/gutf8.c:1498
#, c-format
msgid "Invalid sequence in conversion input"
msgstr "Ógild runa í ílagi umbreytingar"

#: glib/gutf8.c:1413 glib/gutf8.c:1509
#, c-format
msgid "Character out of range for UTF-16"
msgstr "Táknið er utan UTF-16 sviðsins"

#: glib/goption.c:468
msgid "Usage:"
msgstr ""

#: glib/goption.c:468
msgid "[OPTION...]"
msgstr ""

#: glib/goption.c:556
msgid "Help Options:"
msgstr ""

#: glib/goption.c:557
msgid "Show help options"
msgstr ""

#: glib/goption.c:562
msgid "Show all help options"
msgstr ""

#: glib/goption.c:612
msgid "Application Options:"
msgstr ""

#: glib/goption.c:653
#, c-format
msgid "Cannot parse integer value '%s' for %s"
msgstr ""

#: glib/goption.c:663
#, c-format
msgid "Integer value '%s' for %s out of range"
msgstr ""

#: glib/goption.c:926
#, fuzzy, c-format
msgid "Error parsing option %s"
msgstr "Villa við umbreytingu: %s"

#: glib/goption.c:959 glib/goption.c:1070
#, c-format
msgid "Missing argument for %s"
msgstr ""

#: glib/goption.c:1474
#, c-format
msgid "Unknown option %s"
msgstr ""

#: glib/gkeyfile.c:339
#, c-format
msgid "Valid key file could not be found in data dirs"
msgstr ""

#: glib/gkeyfile.c:374
#, c-format
msgid "Not a regular file"
msgstr ""

#: glib/gkeyfile.c:382
#, c-format
msgid "File is empty"
msgstr ""

#: glib/gkeyfile.c:697
#, c-format
msgid ""
"Key file contains line '%s' which is not a key-value pair, group, or comment"
msgstr ""

#: glib/gkeyfile.c:765
#, c-format
msgid "Key file does not start with a group"
msgstr ""

#: glib/gkeyfile.c:808
#, c-format
msgid "Key file contains unsupported encoding '%s'"
msgstr ""

#: glib/gkeyfile.c:1017 glib/gkeyfile.c:1176 glib/gkeyfile.c:2177
#: glib/gkeyfile.c:2242 glib/gkeyfile.c:2361 glib/gkeyfile.c:2497
#: glib/gkeyfile.c:2649 glib/gkeyfile.c:2823 glib/gkeyfile.c:2880
#, c-format
msgid "Key file does not have group '%s'"
msgstr ""

#: glib/gkeyfile.c:1188
#, c-format
msgid "Key file does not have key '%s'"
msgstr ""

#: glib/gkeyfile.c:1289 glib/gkeyfile.c:1398
#, c-format
msgid "Key file contains key '%s' with value '%s' which is not UTF-8"
msgstr ""

#: glib/gkeyfile.c:1307 glib/gkeyfile.c:1416 glib/gkeyfile.c:1788
#, c-format
msgid "Key file contains key '%s' which has value that cannot be interpreted."
msgstr ""

#: glib/gkeyfile.c:2004
#, c-format
msgid ""
"Key file contains key '%s' in group '%s' which has value that cannot be "
"interpreted."
msgstr ""

#: glib/gkeyfile.c:2192 glib/gkeyfile.c:2376 glib/gkeyfile.c:2891
#, c-format
msgid "Key file does not have key '%s' in group '%s'"
msgstr ""

#: glib/gkeyfile.c:3067
#, c-format
msgid "Key file contains escape character at end of line"
msgstr ""

#: glib/gkeyfile.c:3089
#, fuzzy, c-format
msgid "Key file contains invalid escape sequence '%s'"
msgstr "URI '%s' inniheldur ógild sértákn"

#: glib/gkeyfile.c:3230
#, c-format
msgid "Value '%s' cannot be interpreted as a number."
msgstr ""

#: glib/gkeyfile.c:3240
#, c-format
msgid "Integer value '%s' out of range"
msgstr ""

#: glib/gkeyfile.c:3270
#, c-format
msgid "Value '%s' cannot be interpreted as a boolean."
msgstr ""

#, fuzzy
#~ msgid "Could not change file mode: fork() failed: %s"
#~ msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#, fuzzy
#~ msgid "Could not change file mode: waitpid() failed: %s"
#~ msgstr "Gat ekki frátekið %lu bæti til að lesa skrána \"%s\""

#, fuzzy
#~ msgid "Could not change file mode: chmod() failed: %s"
#~ msgstr "Gat ekki opnað skrána '%s': fdopen() brást: %s"

#~ msgid "Conversion from character set `%s' to `%s' is not supported"
#~ msgstr "Umbreyting úr stafatöflunni `%s' í `%s' er ekki stutt"
