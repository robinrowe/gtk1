# GTK1 Cross-Platform GUI Toolkit

GTK1 is a cross-platform GUI toolkit maintained by Robin Rowe. GTK1 currently supports 64-bit and 32-bit Windows compilation using Microsoft Visual Studio. Linux version anticipated Q1 2023. MacOS thereafter. GTK1 uses a cmake build system. Has C and C++ source code. Works with Wacom and other tablets. 

![CinePaint 1.4.7](docs/cinepaint-147-darkmode.jpg)
_CinePaint 1.4.7 for Windows with GTK1 Dark Mode 2023/2/20_

## GTK History

GTK was originally developed by Peter Mattis, Spencer Kimball and Josh MacDonald as a replacement for the X11 Motif toolkit in the GNU Image Manipulation Program (GIMP). GTK was first released as part of GIMP 0.60 in July 1996. GTK was rewritten to be object-oriented for GIMP 0.99, released in February 1997. 

CinePaint, originally known as Film Gimp, had been developed by the film industry as an open source alternative for Photoshop on Linux. Hollywood studios hired GIMP developers to add HDR support to GIMP 1.2, forked as Film Gimp in December 2000 and renamed CinePaint in 2002. CinePaint continued using GTK 1.2 and took over the orphaned GtkWindows and GtkOSX (MacOS) ports.  Meanwhile, GIMP continued using mainstream GTK. GTK 2.0 (released March 2004) was adopted by the GNOME Foundation for use in the GNOME desktop and continued onward with GTK3 and GTK4. GTK 1.2 was deprecated by the GTK maintainers since 2002. GTK 2.0 was declared as end-of-life in 2020. 

A widespread criticism of GTK is the lack of backward compatibility between GTK1, GTK2, GTK3 and GTK4 APIs. Another criticism is code bloat, with GTK 1.2 of 1999 being 168,000 lines of code, while GTK2 of 2002 has 460,000. Developers who contributed patches at that time objected that the GTK library maintainers were uncivil and rejected outside contributors. Except for being slower and bigger, many end users noticed no difference using later GTK versions. Due to a lack of backward compatibility and a lack of maintenance for prior versions, some GTK application developers migrated their projects to other toolkits, in particular, to Qt in the motion picture industry. Our GTK1 library enables classic GTK1 applications, including many more than CinePaint, to still work.

GTK1 forked from GTK 1.2 to support CinePaint for Windows and MacOS. CinePaint forked from Film Gimp in 2002. CinePaint and GTK1 have always been independent projects that had adopted orphaned open source code. Not sanctioned by the GTK, GNOME or GIMP maintainers. GTK 1.2 for Linux, Windows and MacOS existed as three separate projects. Robin Rowe led the Windows and MacOS ports for many years. The Windows, Linux and MacOS GTK 1.2 source code is currently being merging into one GTK1 codebase.

https://en.wikipedia.org/wiki/GTK

## Building GTK1

Windows:

* VisualStudio
* cmake 
* gitbash
* libunistd

Linux Ubuntu or Debian:

* apt-get install buildtools cmake

MacOS:

* Xcode
* cmake

After installing the above:

1. git clone https://gitlab.com/robinrowe/gtk1.git
1. cd gtk1
1. mkdir build
1. cd build
1. cmake ..

Windows developers likely prefer 'cmake .. -A x64' to specify 64-bit build instead of 32-bit. Next, open GTK1.sln file in VisualStudio and Build All. Or, at command-line run msbuild.

Linux and MacOS developers, next step is to run make. 

MacOS developers will be prompted to type 'agree' to Xcode command-line tools installation on first use of git. You also need to set the path to cmake with 'sudo vi /etc/paths'. The path to cmake is '/Applications/CMake.app/Contents/bin'. Restart terminal. Use 'echo $PATH' and 'cmake' to check cmake works.


## GTK1 Examples

Included with the GTK1 codebase are the classic code examples that came with GTK, plus a collection of code examples that had been  included as open source with now out-of-print books about GTK 1.2 programming. For starters, here is Hello World...

	// hello_gtk1.c

	#include <gtk/gtk.h>

	void click_event( GtkWidget *widget,gpointer data )
	{    g_print ("Hello again - %s was pressed\n", (char *) data);
	}

	gint delete_event( GtkWidget *widget,GdkEvent  *event,gpointer data )
	{   gtk_main_quit();
		return FALSE;
	}

	GtkWidget *window;
	GtkWidget *button;
	GtkWidget *box;

	int main(int argc,char *argv[])
	{	gtk_init (&argc, &argv);
		window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title(GTK_WINDOW (window), "Hello World!");
		gtk_signal_connect(GTK_OBJECT(window), "delete_event",GTK_SIGNAL_FUNC (delete_event), NULL);
		gtk_container_set_border_width(GTK_CONTAINER (window), 10);
		box = gtk_hbox_new(FALSE, 0);
		gtk_container_add (GTK_CONTAINER(window), box);
		button = gtk_button_new_with_label ("Button 1");
		gtk_signal_connect (GTK_OBJECT (button),"clicked",GTK_SIGNAL_FUNC(click_event),(gpointer)"button 1");
		gtk_box_pack_start(GTK_BOX(box),button,TRUE,TRUE,0);
		gtk_widget_show(button);
		button = gtk_button_new_with_label("Button 2");
		gtk_signal_connect (GTK_OBJECT(button), "clicked",GTK_SIGNAL_FUNC (click_event), (gpointer) "button 2");
		gtk_box_pack_start(GTK_BOX(box),button,TRUE,TRUE,0);
		gtk_widget_show(button);
		gtk_widget_show(box);
		gtk_widget_show (window);
		gtk_main();
		return 0;
	}

## GTK1 Project Manager

Robin.Rowe@cinepaint.org Los Angeles, California 323-535-0952 https://goshrobin.com

rev. 2023/3/9


