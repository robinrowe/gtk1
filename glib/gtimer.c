/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GLib Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/. 
 */

/* 
 * MT safe
 */

#include "glib-config.h"
#include <unistd.h>
#ifndef G_OS_WIN32
//#include <sys/time.h>
#include <time.h>
#include <errno.h>
#endif /* G_OS_WIN32 */
#include "glib.h"


struct GTimer
{
#ifdef G_OS_WIN32
  ULONGLONG start;
  ULONGLONG end;
#else /* !G_OS_WIN32 */
  struct timeval start;
  struct timeval end;
#endif /* !G_OS_WIN32 */

  guint active : 1;
};

#ifdef G_OS_WIN32
#  define GETTIME(v) \
     v = GetTickCount64 ()
#else /* !G_OS_WIN32 */
#  define GETTIME(v) \
     gettimeofday (&v, NULL)
#endif /* !G_OS_WIN32 */

GTimer*
g_timer_new (void)
{
  GTimer *timer;

  timer = g_new (GTimer, 1);
  timer->active = TRUE;

  GETTIME (timer->start);

  return timer;
}

void
g_timer_destroy (GTimer *timer)
{
  g_return_if_fail (timer != NULL);

  g_free (timer);
}

void
g_timer_start (GTimer *timer)
{
  g_return_if_fail (timer != NULL);

  timer->active = TRUE;

  GETTIME (timer->start);
}

void
g_timer_stop (GTimer *timer)
{
  g_return_if_fail (timer != NULL);

  timer->active = FALSE;

  GETTIME(timer->end);
}

void
g_timer_reset (GTimer *timer)
{
  g_return_if_fail (timer != NULL);

  GETTIME (timer->start);
}

gdouble
g_timer_elapsed (GTimer *timer,
		 gulong *microseconds)
{
  gdouble total;
#ifndef G_OS_WIN32
  struct timeval elapsed;
#endif /* G_OS_WIN32 */

  g_return_val_if_fail (timer != NULL, 0);

#ifdef G_OS_WIN32
  if (timer->active)
    timer->end = GetTickCount ();

  /* Check for wraparound, which happens every 49.7 days. */
  if (timer->end < timer->start)
    total = (UINT_MAX - (timer->start - timer->end - 1)) / 1000.0;
  else
    total = (timer->end - timer->start) / 1000.0;

  if (microseconds)
    {
      if (timer->end < timer->start)
	*microseconds =
	  ((UINT_MAX - (timer->start - timer->end - 1)) % 1000) * 1000;
      else
	*microseconds =
	  ((timer->end - timer->start) % 1000) * 1000;
    }
#else /* !G_OS_WIN32 */
  if (timer->active)
    gettimeofday (&timer->end, NULL);

  if (timer->start.tv_usec > timer->end.tv_usec)
    {
      timer->end.tv_usec += G_USEC_PER_SEC;
      timer->end.tv_sec--;
    }

  elapsed.tv_usec = timer->end.tv_usec - timer->start.tv_usec;
  elapsed.tv_sec = timer->end.tv_sec - timer->start.tv_sec;

  total = elapsed.tv_sec + ((gdouble) elapsed.tv_usec / 1e6);
  if (total < 0)
    {
      total = 0;

      if (microseconds)
	*microseconds = 0;
    }
  else if (microseconds)
    *microseconds = elapsed.tv_usec;

#endif /* !G_OS_WIN32 */

  return total;
}

void
g_usleep (gulong microseconds)
{
#ifdef G_OS_WIN32
  Sleep (microseconds / 1000);
#else /* !G_OS_WIN32 */
# ifdef HAVE_NANOSLEEP
  struct timespec request, remaining;
  request.tv_sec = microseconds / G_USEC_PER_SEC;
  request.tv_nsec = 1000 * (microseconds % G_USEC_PER_SEC);
  while (nanosleep (&request, &remaining) == EINTR)
    request = remaining;
# else /* !HAVE_NANOSLEEP */
  if (g_thread_supported ())
    {
      static GStaticMutex mutex = G_STATIC_MUTEX_INIT;
      static GCond* cond = NULL;
      GTimeVal end_time;
      
      g_get_current_time (&end_time);
      if (microseconds > G_MAXLONG)
	{
	  microseconds -= G_MAXLONG;
	  g_time_val_add (&end_time, G_MAXLONG);
	}
      g_time_val_add (&end_time, microseconds);

      g_static_mutex_lock (&mutex);
      
      if (!cond)
	cond = g_cond_new ();
      
      while (g_cond_timed_wait (cond, g_static_mutex_get_mutex (&mutex), 
				&end_time))
	/* do nothing */;
      
      g_static_mutex_unlock (&mutex);
    }
  else
    {
      struct timeval tv;
      tv.tv_sec = microseconds / G_USEC_PER_SEC;
      tv.tv_usec = microseconds % G_USEC_PER_SEC;
      select(0, NULL, NULL, NULL, &tv);
    }
# endif /* !HAVE_NANOSLEEP */
#endif /* !G_OS_WIN32 */
}

/**
 * g_time_val_add:
 * @time_: a #GTimeVal
 * @microseconds: number of microseconds to add to @time
 *
 * Adds the given number of microseconds to @time_. @microseconds can
 * also be negative to decrease the value of @time_.
 **/
void 
g_time_val_add (GTimeVal *time_, glong microseconds)
{
  g_return_if_fail (time_->tv_usec >= 0 && time_->tv_usec < G_USEC_PER_SEC);

  if (microseconds >= 0)
    {
      time_->tv_usec += microseconds % G_USEC_PER_SEC;
      time_->tv_sec += microseconds / G_USEC_PER_SEC;
      if (time_->tv_usec >= G_USEC_PER_SEC)
       {
         time_->tv_usec -= G_USEC_PER_SEC;
         time_->tv_sec++;
       }
    }
  else
    {
      microseconds *= -1;
      time_->tv_usec -= microseconds % G_USEC_PER_SEC;
      time_->tv_sec -= microseconds / G_USEC_PER_SEC;
      if (time_->tv_usec < 0)
       {
         time_->tv_usec += G_USEC_PER_SEC;
         time_->tv_sec--;
       }      
    }
}

/**
 * g_time_val_to_iso8601:
 * @time_: a #GTimeVal
 *
 * Converts @time_ into an RFC 3339 encoded string, relative to the
 * Coordinated Universal Time (UTC). This is one of the many formats
 * allowed by ISO 8601.
 *
 * ISO 8601 allows a large number of date/time formats, with or without
 * punctuation and optional elements. The format returned by this function
 * is a complete date and time, with optional punctuation included, the
 * UTC time zone represented as "Z", and the @tv_usec part included if
 * and only if it is nonzero, i.e. either
 * "YYYY-MM-DDTHH:MM:SSZ" or "YYYY-MM-DDTHH:MM:SS.fffffZ".
 *
 * This corresponds to the Internet date/time format defined by
 * [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt),
 * and to either of the two most-precise formats defined by
 * the W3C Note
 * [Date and Time Formats](http://www.w3.org/TR/NOTE-datetime-19980827).
 * Both of these documents are profiles of ISO 8601.
 *
 * Use g_date_time_format() or g_strdup_printf() if a different
 * variation of ISO 8601 format is required.
 *
 * If @time_ represents a date which is too large to fit into a `struct tm`,
 * %NULL will be returned. This is platform dependent. Note also that since
 * `GTimeVal` stores the number of seconds as a `glong`, on 32-bit systems it
 * is subject to the year 2038 problem. Accordingly, since GLib 2.62, this
 * function has been deprecated. Equivalent functionality is available using:
 * |[
 * GDateTime *dt = g_date_time_new_from_unix_utc (time_val);
 * iso8601_string = g_date_time_format_iso8601 (dt);
 * g_date_time_unref (dt);
 * ]|
 *
 * The return value of g_time_val_to_iso8601() has been nullable since GLib
 * 2.54; before then, GLib would crash under the same conditions.
 *
 * Returns: (nullable): a newly allocated string containing an ISO 8601 date,
 *    or %NULL if @time_ was too large
 *
 * Since: 2.12* Deprecated: 2.62: #GTimeVal is not year-2038-safe. Use
 *    g_date_time_format_iso8601(dt) instead.
 */
//G_GNUC_BEGIN_IGNORE_DEPRECATIONS
gchar*
g_time_val_to_iso8601(GTimeVal* time_)
{
    gchar* retval;
    struct tm* tm;
#ifdef HAVE_GMTIME_R
    struct tm tm_;
#endif
    time_t secs;

    g_return_val_if_fail(time_ != NULL &&
        time_->tv_usec >= 0 &&
        time_->tv_usec < G_USEC_PER_SEC, NULL);

    secs = time_->tv_sec;
#ifdef _WIN32
    tm = gmtime(&secs);
#else
#ifdef HAVE_GMTIME_R
    tm = gmtime_r(&secs, &tm_);
#else
    tm = gmtime(&secs);
#endif
#endif

    /* If the gmtime() call has failed, time_->tv_sec is too big. */
    if (tm == NULL)
        return NULL;

    if (time_->tv_usec != 0)
    {
        /* ISO 8601 date and time format, with fractionary seconds:
         *   YYYY-MM-DDTHH:MM:SS.MMMMMMZ
         */
        retval = g_strdup_printf("%4d-%02d-%02dT%02d:%02d:%02d.%06ldZ",
            tm->tm_year + 1900,
            tm->tm_mon + 1,
            tm->tm_mday,
            tm->tm_hour,
            tm->tm_min,
            tm->tm_sec,
            time_->tv_usec);
    }
    else
    {
        /* ISO 8601 date and time format:
         *   YYYY-MM-DDTHH:MM:SSZ
         */
        retval = g_strdup_printf("%4d-%02d-%02dT%02d:%02d:%02dZ",
            tm->tm_year + 1900,
            tm->tm_mon + 1,
            tm->tm_mday,
            tm->tm_hour,
            tm->tm_min,
            tm->tm_sec);
    }

    return retval;
}

guint
timeout_add_full(gint           priority,
    guint          interval,
    gboolean       seconds,
    gboolean       one_shot,
    GSourceFunc    function,
    gpointer       data,
    GDestroyNotify notify)
{
    GSource* source;
    guint id;

    g_return_val_if_fail(function != NULL, 0);

    source = timeout_source_new(interval, seconds, one_shot);

    if (priority != G_PRIORITY_DEFAULT)
        g_source_set_priority(source, priority);

    g_source_set_callback(source, function, data, notify);
    id = g_source_attach(source, NULL);

    TRACE(GLIB_TIMEOUT_ADD(source, g_main_context_default(), id, priority, interval, function, data));

    g_source_unref(source);

    return id;
}

