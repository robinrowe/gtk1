/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * gmutex.c: MT safety related functions
 * Copyright 1998 Sebastian Wilhelmi; University of Karlsruhe
 *                Owen Taylor
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GLib Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/. 
 */

/* 
 * MT safe
 */

#include "glib-config.h"

#ifdef G_THREAD_USE_PID_SURROGATE
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#endif /* G_THREAD_USE_PID_SURROGATE */

#include <unistd.h>
#include <string.h>
#include "glib.h"
#include "gslist.h"

static GMutex g_once_mutex;

#if GLIB_SIZEOF_SYSTEM_THREAD == SIZEOF_VOID_P
# define g_system_thread_equal_simple(thread1, thread2)			\
   ((thread1).dummy_pointer == (thread2).dummy_pointer)
# define g_system_thread_assign(dest, src)				\
   ((dest).dummy_pointer = (src).dummy_pointer)
#else /* GLIB_SIZEOF_SYSTEM_THREAD != SIZEOF_VOID_P */
# define g_system_thread_equal_simple(thread1, thread2)			\
   (memcmp (&(thread1), &(thread2), GLIB_SIZEOF_SYSTEM_THREAD) == 0)
# define g_system_thread_assign(dest, src)				\
   (memcpy (&(dest), &(src), GLIB_SIZEOF_SYSTEM_THREAD))
#endif /* GLIB_SIZEOF_SYSTEM_THREAD == SIZEOF_VOID_P */

#define g_system_thread_equal(thread1, thread2)				\
  (g_thread_functions_for_glib_use.thread_equal ? 			\
   g_thread_functions_for_glib_use.thread_equal (&(thread1), &(thread2)) :\
   g_system_thread_equal_simple((thread1), (thread2)))

GQuark 
g_thread_error_quark (void)
{
  static GQuark quark;
  if (!quark)
    quark = g_quark_from_static_string ("g_thread_error");
  return quark;
}


#ifdef G_THREAD_USE_PID_SURROGATE
static gint priority_map[4];
static gboolean prio_warned = FALSE;
# define SET_PRIO(pid, prio) G_STMT_START{				\
  gint error = setpriority (PRIO_PROCESS, (pid), priority_map[prio]);	\
  if (error == -1 && errno == EACCES && !prio_warned)			\
    {									\
      prio_warned = TRUE;						\
      g_warning ("Priorities can only be increased by root.");		\
    }									\
  }G_STMT_END
#endif /* G_THREAD_USE_PID_SURROGATE */

typedef struct GStaticPrivateNode GStaticPrivateNode;
struct GStaticPrivateNode
{
  gpointer       data;
  GDestroyNotify destroy;
};

static void g_thread_cleanup (gpointer data);
static void g_thread_fail (void);

/* Global variables */

static GSystemThread zero_thread; /* This is initialized to all zero */
gboolean g_thread_use_default_impl = TRUE;
gboolean g_threads_got_initialized = FALSE;

#if defined(G_PLATFORM_WIN32) && defined(__GNUC__)
__declspec(dllexport)
#endif
GThreadFunctions g_thread_functions_for_glib_use = {
  (GMutex*(*)(void))g_thread_fail,                 /* mutex_new */
  NULL,                                        /* mutex_lock */
  NULL,                                        /* mutex_trylock */
  NULL,                                        /* mutex_unlock */
  NULL,                                        /* mutex_free */
  (GCond*(*)(void))g_thread_fail,                  /* cond_new */
  NULL,                                        /* cond_signal */
  NULL,                                        /* cond_broadcast */
  NULL,                                        /* cond_wait */
  NULL,                                        /* cond_timed_wait  */
  NULL,                                        /* cond_free */
  (GPrivate*(*)(GDestroyNotify))g_thread_fail, /* private_new */
  NULL,                                        /* private_get */
  NULL,                                        /* private_set */
  (void(*)(GThreadFunc, gpointer, gulong, 
	   gboolean, gboolean, GThreadPriority, 
	   gpointer, GError**))g_thread_fail,  /* thread_create */
  NULL,                                        /* thread_yield */
  NULL,                                        /* thread_join */
  NULL,                                        /* thread_exit */
  NULL,                                        /* thread_set_priority */
  NULL                                         /* thread_self */
}; 

/* Local data */

static GMutex   *g_mutex_protect_static_mutex_allocation = NULL;
static GPrivate *g_thread_specific_private = NULL;
static GSList   *g_thread_all_threads = NULL;
static GSList   *g_thread_free_indeces = NULL;

G_LOCK_DEFINE_STATIC (g_thread);

/* This must be called only once, before any threads are created.
 * It will only be called from g_thread_init() in -lgthread.
 */
void
g_mutex_init (void)
{
  GRealThread* main_thread;
 
  /* We let the main thread (the one that calls g_thread_init) inherit
   * the data, that it set before calling g_thread_init
   */
  main_thread = (GRealThread*) g_thread_self ();

  g_thread_specific_private = g_private_new (g_thread_cleanup);
  G_THREAD_UF (private_set, (g_thread_specific_private, main_thread));
  G_THREAD_UF (thread_self, (&main_thread->system_thread));

  g_mutex_protect_static_mutex_allocation = g_mutex_new ();

#ifdef G_THREAD_USE_PID_SURROGATE
  priority_map[G_THREAD_PRIORITY_NORMAL] = 
    getpriority (PRIO_PROCESS, (getpid ()));
  priority_map[G_THREAD_PRIORITY_LOW] = 
    MIN (20, priority_map[G_THREAD_PRIORITY_NORMAL] + 10);
  priority_map[G_THREAD_PRIORITY_HIGH] = 
    MAX (-20, priority_map[G_THREAD_PRIORITY_NORMAL] - 10);
  priority_map[G_THREAD_PRIORITY_URGENT] = 
    MAX (-20, priority_map[G_THREAD_PRIORITY_NORMAL] - 15);
#endif /* G_THREAD_USE_PID_SURROGATE */
}

void 
g_static_mutex_init (GStaticMutex *mutex)
{
  static GStaticMutex init_mutex = G_STATIC_MUTEX_INIT;

  g_return_if_fail (mutex);

  *mutex = init_mutex;
}

GMutex *
g_static_mutex_get_mutex_impl (GMutex** mutex)
{
  if (!g_thread_supported ())
    return NULL;

  g_assert (g_mutex_protect_static_mutex_allocation);

  g_mutex_lock (g_mutex_protect_static_mutex_allocation);

  if (!(*mutex)) 
    *mutex = g_mutex_new (); 

  g_mutex_unlock (g_mutex_protect_static_mutex_allocation);
  
  return *mutex;
}

void
g_static_mutex_free (GStaticMutex* mutex)
{
  GMutex **runtime_mutex;
  
  g_return_if_fail (mutex);

  /* The runtime_mutex is the first (or only) member of GStaticMutex,
   * see both versions (of glib-config.h) in configure.in */
  runtime_mutex = ((GMutex**)mutex);
  
  if (*runtime_mutex)
    g_mutex_free (*runtime_mutex);

  *runtime_mutex = NULL;
}

void     
g_static_rec_mutex_init (GStaticRecMutex *mutex)
{
  static GStaticRecMutex init_mutex = G_STATIC_REC_MUTEX_INIT;
  
  g_return_if_fail (mutex);

  *mutex = init_mutex;
}

void
g_static_rec_mutex_lock (GStaticRecMutex* mutex)
{
  GSystemThread self;

  g_return_if_fail (mutex);

  if (!g_thread_supported ())
    return;

  G_THREAD_UF (thread_self, (&self));

  if (g_system_thread_equal (self, mutex->owner))
    {
      mutex->depth++;
      return;
    }
  g_static_mutex_lock (&mutex->mutex);
  g_system_thread_assign (mutex->owner, self);
  mutex->depth = 1;
}

gboolean
g_static_rec_mutex_trylock (GStaticRecMutex* mutex)
{
  GSystemThread self;

  g_return_val_if_fail (mutex, FALSE);

  if (!g_thread_supported ())
    return TRUE;

  G_THREAD_UF (thread_self, (&self));

  if (g_system_thread_equal (self, mutex->owner))
    {
      mutex->depth++;
      return TRUE;
    }

  if (!g_static_mutex_trylock (&mutex->mutex))
    return FALSE;

  g_system_thread_assign (mutex->owner, self);
  mutex->depth = 1;
  return TRUE;
}

void
g_static_rec_mutex_unlock (GStaticRecMutex* mutex)
{
  g_return_if_fail (mutex);

  if (!g_thread_supported ())
    return;

  if (mutex->depth > 1)
    {
      mutex->depth--;
      return;
    }
  g_system_thread_assign (mutex->owner, zero_thread);
  g_static_mutex_unlock (&mutex->mutex);  
}

void
g_static_rec_mutex_lock_full   (GStaticRecMutex *mutex,
				guint            depth)
{
  GSystemThread self;
  g_return_if_fail (mutex);

  if (!g_thread_supported ())
    return;

  G_THREAD_UF (thread_self, (&self));

  if (g_system_thread_equal (self, mutex->owner))
    {
      mutex->depth += depth;
      return;
    }
  g_static_mutex_lock (&mutex->mutex);
  g_system_thread_assign (mutex->owner, self);
  mutex->depth = depth;
}

guint    
g_static_rec_mutex_unlock_full (GStaticRecMutex *mutex)
{
  guint depth;

  g_return_val_if_fail (mutex, 0);

  if (!g_thread_supported ())
    return 1;

  depth = mutex->depth;

  g_system_thread_assign (mutex->owner, zero_thread);
  mutex->depth = 0;
  g_static_mutex_unlock (&mutex->mutex);

  return depth;
}

void
g_static_rec_mutex_free (GStaticRecMutex *mutex)
{
  g_return_if_fail (mutex);

  g_static_mutex_free (&mutex->mutex);
}

void     
g_static_private_init (GStaticPrivate *private_key)
{
  private_key->index = 0;
}

gpointer
g_static_private_get (GStaticPrivate *private_key)
{
  GRealThread *self = (GRealThread*) g_thread_self ();
  GArray *array;

  array = self->private_data;
  if (!array)
    return NULL;

  if (!private_key->index)
    return NULL;
  else if (private_key->index <= array->len)
    return g_array_index (array, GStaticPrivateNode, 
			  private_key->index - 1).data;
  else
    return NULL;
}

void
g_static_private_set (GStaticPrivate *private_key, 
		      gpointer        data,
		      GDestroyNotify  notify)
{
  GRealThread *self = (GRealThread*) g_thread_self ();
  GArray *array;
  static guint next_index = 0;
  GStaticPrivateNode *node;

  array = self->private_data;
  if (!array)
    {
      array = g_array_new (FALSE, TRUE, sizeof (GStaticPrivateNode));
      self->private_data = array;
    }

  if (!private_key->index)
    {
      G_LOCK (g_thread);

      if (!private_key->index)
	{
	  if (g_thread_free_indeces)
	    {
	      private_key->index = 
		GPOINTER_TO_UINT (g_thread_free_indeces->data);
	      g_thread_free_indeces = 
		g_slist_delete_link (g_thread_free_indeces,
				     g_thread_free_indeces);
	    }
	  else
	    private_key->index = ++next_index;
	}

      G_UNLOCK (g_thread);
    }

  if (private_key->index > array->len)
    g_array_set_size (array, private_key->index);

  node = &g_array_index (array, GStaticPrivateNode, private_key->index - 1);
  if (node->destroy)
    {
      gpointer ddata = node->data;
      GDestroyNotify ddestroy = node->destroy;

      node->data = data;
      node->destroy = notify;

      ddestroy (ddata);
    }
  else
    {
      node->data = data;
      node->destroy = notify;
    }
}

void     
g_static_private_free (GStaticPrivate *private_key)
{
  gsize index = private_key->index;
  GSList *list;

  if (!index)
    return;
  
  private_key->index = 0;

  G_LOCK (g_thread);
  list =  g_thread_all_threads;
  while (list)
    {
      GRealThread *thread = list->data;
      GArray *array = thread->private_data;
      list = list->next;

      if (array && index <= array->len)
	{
	  GStaticPrivateNode *node = &g_array_index (array, 
						     GStaticPrivateNode, 
						     index - 1);
	  gpointer ddata = node->data;
	  GDestroyNotify ddestroy = node->destroy;

	  node->data = NULL;
	  node->destroy = NULL;

	  if (ddestroy) 
	    {
	      G_UNLOCK (g_thread);
	      ddestroy (ddata);
	      G_LOCK (g_thread);
	      }
	}
    }
  g_thread_free_indeces = g_slist_prepend (g_thread_free_indeces, 
					   GUINT_TO_POINTER (index));
  G_UNLOCK (g_thread);
}

static void
g_thread_cleanup (gpointer data)
{
  if (data)
    {
      GRealThread* thread = data;
      if (thread->private_data)
	{
	  GArray* array = thread->private_data;
	  guint i;
	  
	  for (i = 0; i < array->len; i++ )
	    {
	      GStaticPrivateNode *node = 
		&g_array_index (array, GStaticPrivateNode, i);
	      if (node->destroy)
		node->destroy (node->data);
	    }
	  g_array_free (array, TRUE);
	}

      /* We only free the thread structure, if it isn't joinable. If
         it is, the structure is freed in g_thread_join */
      if (!thread->thread.joinable)
	{
	  G_LOCK (g_thread);
	  g_thread_all_threads = g_slist_remove (g_thread_all_threads, data);
	  G_UNLOCK (g_thread);
	  
	  /* Just to make sure, this isn't used any more */
	  g_system_thread_assign (thread->system_thread, zero_thread);
	  g_free (thread);
	}
    }
}

static void
g_thread_fail (void)
{
  g_error ("The thread system is not yet initialized.");
}

static gpointer
g_thread_create_proxy (gpointer data)
{
  GRealThread* thread = data;

  g_assert (data);

#ifdef G_THREAD_USE_PID_SURROGATE
  thread->pid = getpid ();
#endif /* G_THREAD_USE_PID_SURROGATE */

  /* This has to happen before G_LOCK, as that might call g_thread_self */
  g_private_set (g_thread_specific_private, data);

  /* the lock makes sure, that thread->system_thread is written,
     before thread->thread.func is called. See g_thread_create. */
  G_LOCK (g_thread);
  G_UNLOCK (g_thread);
 
#ifdef G_THREAD_USE_PID_SURROGATE
  if (g_thread_use_default_impl)
    SET_PRIO (thread->pid, thread->thread.priority);
#endif /* G_THREAD_USE_PID_SURROGATE */

  thread->retval = thread->thread.func (thread->thread.data);

  return NULL;
}

GThread* 
g_thread_create_full (GThreadFunc 		 func,
		      gpointer 		 data,
		      gulong 		 stack_size,
		      gboolean 		 joinable,
		      gboolean 		 bound,
		      GThreadPriority 	 priority,
		      GError                **error)
{
  GRealThread* result;
  GError *local_error = NULL;
  g_return_val_if_fail (func, NULL);
  g_return_val_if_fail (priority >= G_THREAD_PRIORITY_LOW, NULL);
  g_return_val_if_fail (priority <= G_THREAD_PRIORITY_URGENT, NULL);
  
  result = g_new (GRealThread, 1);

  result->thread.joinable = joinable;
  result->thread.priority = priority;
  result->thread.func = func;
  result->thread.data = data;
  result->private_data = NULL; 
  G_LOCK (g_thread);
  G_THREAD_UF (thread_create, (g_thread_create_proxy, result, 
			       stack_size, joinable, bound, priority,
			       &result->system_thread, &local_error));
  g_thread_all_threads = g_slist_prepend (g_thread_all_threads, result);
  G_UNLOCK (g_thread);

  if (local_error)
    {
      g_propagate_error (error, local_error);
      g_free (result);
      return NULL;
    }

  return (GThread*) result;
}

void
g_thread_exit (gpointer retval)
{
  GRealThread* real = (GRealThread*) g_thread_self ();
  real->retval = retval;
  G_THREAD_CF (thread_exit, (void)0, ());
}

gpointer
g_thread_join (GThread* thread)
{
  GRealThread* real = (GRealThread*) thread;
  gpointer retval;

  g_return_val_if_fail (thread, NULL);
  g_return_val_if_fail (thread->joinable, NULL);
  g_return_val_if_fail (!g_system_thread_equal (real->system_thread, 
						zero_thread), NULL);

  G_THREAD_UF (thread_join, (&real->system_thread));

  retval = real->retval;

  G_LOCK (g_thread);
  g_thread_all_threads = g_slist_remove (g_thread_all_threads, thread);
  G_UNLOCK (g_thread);

  /* Just to make sure, this isn't used any more */
  thread->joinable = 0;
  g_system_thread_assign (real->system_thread, zero_thread);

  /* the thread structure for non-joinable threads is freed upon
     thread end. We free the memory here. This will leave a loose end,
     if a joinable thread is not joined. */

  g_free (thread);

  return retval;
}

void
g_thread_set_priority (GThread* thread, 
		       GThreadPriority priority)
{
  GRealThread* real = (GRealThread*) thread;

  g_return_if_fail (thread);
  g_return_if_fail (!g_system_thread_equal (real->system_thread, zero_thread));
  g_return_if_fail (priority >= G_THREAD_PRIORITY_LOW);
  g_return_if_fail (priority <= G_THREAD_PRIORITY_URGENT);

  thread->priority = priority;

#ifdef G_THREAD_USE_PID_SURROGATE
  if (g_thread_use_default_impl)
    SET_PRIO (real->pid, priority);
  else
#endif /* G_THREAD_USE_PID_SURROGATE */
    G_THREAD_CF (thread_set_priority, (void)0, 
		 (&real->system_thread, priority));
}

GThread*
g_thread_self (void)
{
  GRealThread* thread = g_private_get (g_thread_specific_private);

  if (!thread)
    {  
      /* If no thread data is available, provide and set one.  This
         can happen for the main thread and for threads, that are not
         created by GLib. */
      thread = g_new (GRealThread, 1);
      thread->thread.joinable = FALSE; /* This is a save guess */
      thread->thread.priority = G_THREAD_PRIORITY_NORMAL; /* This is
							     just a guess */
      thread->thread.func = NULL;
      thread->thread.data = NULL;
      thread->private_data = NULL;

      if (g_thread_supported ())
	G_THREAD_UF (thread_self, (&thread->system_thread));

#ifdef G_THREAD_USE_PID_SURROGATE
      thread->pid = getpid ();
#endif /* G_THREAD_USE_PID_SURROGATE */
      
      g_private_set (g_thread_specific_private, thread); 
      
      G_LOCK (g_thread);
      g_thread_all_threads = g_slist_prepend (g_thread_all_threads, thread);
      G_UNLOCK (g_thread);
    }
  
  return (GThread*)thread;
}

void
g_static_rw_lock_init (GStaticRWLock* lock)
{
  static GStaticRWLock init_lock = G_STATIC_RW_LOCK_INIT;

  g_return_if_fail (lock);

  *lock = init_lock;
}

static void inline 
g_static_rw_lock_wait (GCond** cond, GStaticMutex* mutex)
{
  if (!*cond)
      *cond = g_cond_new ();
  g_cond_wait (*cond, g_static_mutex_get_mutex (mutex));
}

static void inline 
g_static_rw_lock_signal (GStaticRWLock* lock)
{
  if (lock->want_to_write && lock->write_cond)
    g_cond_signal (lock->write_cond);
  else if (lock->want_to_read && lock->read_cond)
    g_cond_broadcast (lock->read_cond);
}

void 
g_static_rw_lock_reader_lock (GStaticRWLock* lock)
{
  g_return_if_fail (lock);

  if (!g_threads_got_initialized)
    return;

  g_static_mutex_lock (&lock->mutex);
  lock->want_to_read++;
  while (lock->have_writer || lock->want_to_write) 
    g_static_rw_lock_wait (&lock->read_cond, &lock->mutex);
  lock->want_to_read--;
  lock->read_counter++;
  g_static_mutex_unlock (&lock->mutex);
}

gboolean 
g_static_rw_lock_reader_trylock (GStaticRWLock* lock)
{
  gboolean ret_val = FALSE;

  g_return_val_if_fail (lock, FALSE);

  if (!g_threads_got_initialized)
    return TRUE;

  g_static_mutex_lock (&lock->mutex);
  if (!lock->have_writer && !lock->want_to_write)
    {
      lock->read_counter++;
      ret_val = TRUE;
    }
  g_static_mutex_unlock (&lock->mutex);
  return ret_val;
}

void 
g_static_rw_lock_reader_unlock  (GStaticRWLock* lock)
{
  g_return_if_fail (lock);

  if (!g_threads_got_initialized)
    return;

  g_static_mutex_lock (&lock->mutex);
  lock->read_counter--;
  if (lock->read_counter == 0)
    g_static_rw_lock_signal (lock);
  g_static_mutex_unlock (&lock->mutex);
}

void 
g_static_rw_lock_writer_lock (GStaticRWLock* lock)
{
  g_return_if_fail (lock);

  if (!g_threads_got_initialized)
    return;

  g_static_mutex_lock (&lock->mutex);
  lock->want_to_write++;
  while (lock->have_writer || lock->read_counter)
    g_static_rw_lock_wait (&lock->write_cond, &lock->mutex);
  lock->want_to_write--;
  lock->have_writer = TRUE;
  g_static_mutex_unlock (&lock->mutex);
}

gboolean 
g_static_rw_lock_writer_trylock (GStaticRWLock* lock)
{
  gboolean ret_val = FALSE;

  g_return_val_if_fail (lock, FALSE);
  
  if (!g_threads_got_initialized)
    return TRUE;

  g_static_mutex_lock (&lock->mutex);
  if (!lock->have_writer && !lock->read_counter)
    {
      lock->have_writer = TRUE;
      ret_val = TRUE;
    }
  g_static_mutex_unlock (&lock->mutex);
  return ret_val;
}

void 
g_static_rw_lock_writer_unlock (GStaticRWLock* lock)
{
  g_return_if_fail (lock);
  
  if (!g_threads_got_initialized)
    return;

  g_static_mutex_lock (&lock->mutex);
  lock->have_writer = FALSE; 
  g_static_rw_lock_signal (lock);
  g_static_mutex_unlock (&lock->mutex);
}

void 
g_static_rw_lock_free (GStaticRWLock* lock)
{
  g_return_if_fail (lock);
  
  if (lock->read_cond)
    {
      g_cond_free (lock->read_cond);
      lock->read_cond = NULL;
    }
  if (lock->write_cond)
    {
      g_cond_free (lock->write_cond);
      lock->write_cond = NULL;
    }
  g_static_mutex_free (&lock->mutex);
}

#if 0
void g_private_set(GPrivate* key, gpointer  value)
{
    TlsSetValue(g_private_get_impl(key), value);
}
#endif

#ifdef WIN32

typedef HRESULT(WINAPI* pSetThreadDescription) (HANDLE hThread,
    PCWSTR lpThreadDescription);
static pSetThreadDescription SetThreadDescriptionFunc = NULL;
static HMODULE kernel32_module = NULL;

static gboolean
g_thread_win32_load_library(void)
{
    /* FIXME: Add support for UWP app */
#if !defined(G_WINAPI_ONLY_APP)
    static gsize _init_once = 0;
    if (g_once_init_enter(&_init_once))
    {
        kernel32_module = LoadLibraryW(L"kernel32.dll");
        if (kernel32_module)
        {
            SetThreadDescriptionFunc =
                (pSetThreadDescription)GetProcAddress(kernel32_module,
                    "SetThreadDescription");
            if (!SetThreadDescriptionFunc)
                FreeLibrary(kernel32_module);
        }
        g_once_init_leave(&_init_once, 1);
    }
#endif

    return !!SetThreadDescriptionFunc;
}

static gboolean
g_thread_win32_set_thread_desc(const gchar* name)
{
    HRESULT hr;
    wchar_t* namew;

    if (!g_thread_win32_load_library() || !name)
        return FALSE;

    namew = g_utf8_to_utf16(name, -1, NULL, NULL, NULL);
    if (!namew)
        return FALSE;

    hr = SetThreadDescriptionFunc(GetCurrentThread(), namew);

    g_free(namew);
    return SUCCEEDED(hr);
}

typedef struct _THREADNAME_INFO
{
    DWORD  dwType;	/* must be 0x1000 */
    LPCSTR szName;	/* pointer to name (in user addr space) */
    DWORD  dwThreadID;	/* thread ID (-1=caller thread) */
    DWORD  dwFlags;	/* reserved for future use, must be zero */
} THREADNAME_INFO;

#define EXCEPTION_SET_THREAD_NAME ((DWORD) 0x406D1388)

static void
SetThreadName(DWORD  dwThreadID,
    LPCSTR szThreadName)
{
    THREADNAME_INFO info;
    DWORD infosize;

    info.dwType = 0x1000;
    info.szName = szThreadName;
    info.dwThreadID = dwThreadID;
    info.dwFlags = 0;

    infosize = sizeof(info) / sizeof(ULONG_PTR);

#ifdef _MSC_VER
    __try
    {
        RaiseException(EXCEPTION_SET_THREAD_NAME, 0, infosize,
            (const ULONG_PTR*)&info);
    }
    __except (GetExceptionCode() == EXCEPTION_SET_THREAD_NAME ?
        EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH)
    {
    }
#else
    if ((!IsDebuggerPresent()) || (SetThreadName_VEH_handle == NULL))
        return;

    RaiseException(EXCEPTION_SET_THREAD_NAME, 0, infosize, (const ULONG_PTR*)&info);
#endif
}

void
g_system_thread_set_name(const gchar* name)
{
    /* Prefer SetThreadDescription over exception based way if available,
     * since thread description set by SetThreadDescription will be preserved
     * in dump file */
    if (!g_thread_win32_set_thread_desc(name))
        SetThreadName((DWORD)-1, name);
}
#else
void
g_system_thread_set_name(const gchar* name)
{
#if defined(HAVE_PTHREAD_SETNAME_NP_WITHOUT_TID)
    pthread_setname_np(name); /* on OS X and iOS */
#elif defined(HAVE_PTHREAD_SETNAME_NP_WITH_TID)
    pthread_setname_np(pthread_self(), name); /* on Linux and Solaris */
#elif defined(HAVE_PTHREAD_SETNAME_NP_WITH_TID_AND_ARG)
    pthread_setname_np(pthread_self(), "%s", (gchar*)name); /* on NetBSD */
#elif defined(HAVE_PTHREAD_SET_NAME_NP)
    pthread_set_name_np(pthread_self(), name); /* on FreeBSD, DragonFlyBSD, OpenBSD */
#endif
}
#endif

gpointer
g_thread_proxy(gpointer data)
{
    GRealThread* thread = data;

    g_assert(data);
    g_private_set(g_thread_specific_private, data);
#if 0
    TRACE(GLIB_THREAD_SPAWNED(thread->thread.func, thread->thread.data,
        thread->name));
#endif
    if (thread->name)
    {
        g_system_thread_set_name(thread->name);
        g_free(thread->name);
        thread->name = NULL;
    }

    thread->retval = thread->thread.func(thread->thread.data);

    return NULL;
}

static guint g_thread_n_created_counter = 0;

GThread*
g_thread_new_internal(const gchar* name,
    GThreadFunc proxy,
    GThreadFunc func,
    gpointer data,
    gsize stack_size,
    GError** error)
{
    g_return_val_if_fail(func != NULL, NULL);

    g_atomic_int_inc(&g_thread_n_created_counter);

 //   g_trace_mark(G_TRACE_CURRENT_TIME, 0, "GLib", "GThread created", "%s", name ? name : "(unnamed)");
    return (GThread*)g_system_thread_new(proxy, (gulong) stack_size, name, func, data, error);
}

/**
 * g_thread_new:
 * @name: (nullable): an (optional) name for the new thread
 * @func: (closure data) (scope async): a function to execute in the new thread
 * @data: (nullable): an argument to supply to the new thread
 *
 * This function creates a new thread. The new thread starts by invoking
 * @func with the argument data. The thread will run until @func returns
 * or until g_thread_exit() is called from the new thread. The return value
 * of @func becomes the return value of the thread, which can be obtained
 * with g_thread_join().
 *
 * The @name can be useful for discriminating threads in a debugger.
 * It is not used for other purposes and does not have to be unique.
 * Some systems restrict the length of @name to 16 bytes.
 *
 * If the thread can not be created the program aborts. See
 * g_thread_try_new() if you want to attempt to deal with failures.
 *
 * If you are using threads to offload (potentially many) short-lived tasks,
 * #GThreadPool may be more appropriate than manually spawning and tracking
 * multiple #GThreads.
 *
 * To free the struct returned by this function, use g_thread_unref().
 * Note that g_thread_join() implicitly unrefs the #GThread as well.
 *
 * New threads by default inherit their scheduler policy (POSIX) or thread
 * priority (Windows) of the thread creating the new thread.
 *
 * This behaviour changed in GLib 2.64: before threads on Windows were not
 * inheriting the thread priority but were spawned with the default priority.
 * Starting with GLib 2.64 the behaviour is now consistent between Windows and
 * POSIX and all threads inherit their parent thread's priority.
 *
 * Returns: (transfer full): the new #GThread
 *
 * Since: 2.32
 */
GThread*
g_thread_new(const gchar* name,
    GThreadFunc  func,
    gpointer     data)
{
    GError* error = NULL;
    GThread* thread;

    thread = g_thread_new_internal(name, g_thread_proxy, func, data, 0, &error);

    if G_UNLIKELY(thread == NULL)
        g_error("creating thread '%s': %s", name ? name : "", error->message);

    return thread;
}

/**
 * g_once_init_enter:
 * @location: (inout) (not optional): location of a static initializable variable
 *    containing 0
 *
 * Function to be called when starting a critical initialization
 * section. The argument @location must point to a static
 * 0-initialized variable that will be set to a value other than 0 at
 * the end of the initialization section. In combination with
 * g_once_init_leave() and the unique address @value_location, it can
 * be ensured that an initialization section will be executed only once
 * during a program's life time, and that concurrent threads are
 * blocked until initialization completed. To be used in constructs
 * like this:
 *
 * |[<!-- language="C" -->
 *   static gsize initialization_value = 0;
 *
 *   if (g_once_init_enter (&initialization_value))
 *     {
 *       gsize setup_value = 42; // initialization code here
 *
 *       g_once_init_leave (&initialization_value, setup_value);
 *     }
 *
 *   // use initialization_value here
 * ]|
 *
 * While @location has a `volatile` qualifier, this is a historical artifact and
 * the pointer passed to it should not be `volatile`.
 *
 * Returns: %TRUE if the initialization section should be entered,
 *     %FALSE and blocks otherwise
 *
 * Since: 2.14
 */

static GSList* g_once_init_list = NULL;
static GMutex    g_once_mutex;
static GCond     g_once_cond;

gboolean
(g_once_init_enter) (volatile void* location)
{
    gsize* value_location = (gsize*)location;
    gboolean need_init = FALSE;
    g_mutex_lock(&g_once_mutex);
    if (g_atomic_pointer_get(value_location) == 0)
    {
        if (!g_slist_find(g_once_init_list, (void*)value_location))
        {
            need_init = TRUE;
            g_once_init_list = g_slist_prepend(g_once_init_list, (void*)value_location);
        }
        else
            do
                g_cond_wait(&g_once_cond, &g_once_mutex);
        while (g_slist_find(g_once_init_list, (gconstpointer)value_location));
    }
    g_mutex_unlock(&g_once_mutex);
    return need_init;
}

/**
 * g_atomic_int_inc:
 * @atomic: a pointer to a #gint or #guint
 *
 * Increments the value of @atomic by 1.
 *
 * Think of this operation as an atomic version of `{ *atomic += 1; }`.
 *
 * This call acts as a full compiler and hardware memory barrier.
 *
 * While @atomic has a `volatile` qualifier, this is a historical artifact and
 * the pointer passed to it should not be `volatile`.
 *
 * Since: 2.4
 **/
#if 0
void
(g_atomic_int_inc)(volatile gint* atomic)
{
    g_atomic_int_inc(atomic);
}
#endif

#ifdef _WIN32

typedef struct GThreadWin32
{
    GRealThread thread;
    GThreadFunc proxy;
    HANDLE      handle;
} GThreadWin32;

/* Allow the compiler to inline memset(). Since the size is a constant, this
 * can significantly improve performance. */
#if defined (__GNUC__) && (__GNUC__ >= 2) && defined (__OPTIMIZE__)
#  define g_slice_new0(type)                                    \
  (type *) (G_GNUC_EXTENSION ({                                 \
    gsize __s = sizeof (type);                                  \
    gpointer __p;                                               \
    __p = g_slice_alloc (__s);                                  \
    memset (__p, 0, __s);                                       \
    __p;                                                        \
  }))
#else
#  define g_slice_new0(type)    ((type*) g_slice_alloc0 (sizeof (type)))
#endif

#if 0
#define g_node_alloc0()         g_slice_new0 (GNode)
#define g_node_free(node)       g_slice_free (GNode, node)
#endif

 /* we go through extra hoops to ensure type safety */
#define g_slice_dup(type, mem)                                  \
  (1 ? (type*) g_slice_copy (sizeof (type), (mem))              \
     : ((void) ((type*) 0 == (mem)), (type*) 0))
#define g_slice_free(type, mem)                                 \
G_STMT_START {                                                  \
  if (1) g_slice_free1 (sizeof (type), (mem));			\
  else   (void) ((type*) 0 == (mem)); 				\
} G_STMT_END
#define g_slice_free_chain(type, mem_chain, next)               \
G_STMT_START {                                                  \
  if (1) g_slice_free_chain_with_offset (sizeof (type),		\
                 (mem_chain), G_STRUCT_OFFSET (type, next)); 	\
  else   (void) ((type*) 0 == (mem_chain));			\
} G_STMT_END


GRealThread*
g_system_thread_new(GThreadFunc proxy,
    gulong stack_size,
    const char* name,
    GThreadFunc func,
    gpointer data,
    GError** error)
{
    GThreadWin32* thread;
    GRealThread* base_thread;
    guint ignore;
    const gchar* message = NULL;
    int thread_prio;

    thread = g_slice_new0(GThreadWin32);
    thread->proxy = proxy;
    thread->handle = (HANDLE)NULL;
    base_thread = (GRealThread*)thread;
    base_thread->ref_count = 2;
    base_thread->ours = TRUE;
    base_thread->thread.joinable = TRUE;
    base_thread->thread.func = func;
    base_thread->thread.data = data;
    base_thread->name = g_strdup(name);

    thread->handle = (HANDLE)_beginthreadex(NULL, stack_size, g_thread_win32_proxy, thread,
        CREATE_SUSPENDED, &ignore);

    if (thread->handle == NULL)
    {
        message = "Error creating thread";
        goto error;
    }

    /* For thread priority inheritance we need to manually set the thread
     * priority of the new thread to the priority of the current thread. We
     * also have to start the thread suspended and resume it after actually
     * setting the priority here.
     *
     * On Windows, by default all new threads are created with NORMAL thread
     * priority.
     */
    {
        HANDLE current_thread = GetCurrentThread();
        thread_prio = GetThreadPriority(current_thread);
    }

    if (thread_prio == THREAD_PRIORITY_ERROR_RETURN)
    {
        message = "Error getting current thread priority";
        goto error;
    }

    if (SetThreadPriority(thread->handle, thread_prio) == 0)
    {
        message = "Error setting new thread priority";
        goto error;
    }

    if (ResumeThread(thread->handle) == (DWORD)-1)
    {
        message = "Error resuming new thread";
        goto error;
    }

    return (GRealThread*)thread;

error:
    {
        gchar* win_error = g_win32_error_message(GetLastError());
        g_set_error(error, G_THREAD_ERROR, G_THREAD_ERROR_AGAIN,
            "%s: %s", message, win_error);
        g_free(win_error);
        if (thread->handle)
            CloseHandle(thread->handle);
        g_slice_free(GThreadWin32, thread);
        return NULL;
    }
}

/**
 * g_once_init_leave:
 * @location: (inout) (not optional): location of a static initializable variable
 *    containing 0
 * @result: new non-0 value for *@value_location
 *
 * Counterpart to g_once_init_enter(). Expects a location of a static
 * 0-initialized initialization variable, and an initialization value
 * other than 0. Sets the variable to the initialization value, and
 * releases concurrent threads blocking in g_once_init_enter() on this
 * initialization variable.
 *
 * While @location has a `volatile` qualifier, this is a historical artifact and
 * the pointer passed to it should not be `volatile`.
 *
 * Since: 2.14
 */
void
(g_once_init_leave)(volatile void* location,
    gsize          result)
{
    gsize* value_location = (gsize*)location;
    gsize old_value;

    g_return_if_fail(result != 0);

    old_value = (gsize)g_atomic_pointer_exchange(value_location, result);
    g_return_if_fail(old_value == 0);

    g_mutex_lock(&g_once_mutex);
    g_return_if_fail(g_once_init_list != NULL);
    g_once_init_list = g_slist_remove(g_once_init_list, (void*)value_location);
    g_cond_broadcast(&g_once_cond);
    g_mutex_unlock(&g_once_mutex);
}

#else

void
g_system_thread_free(GRealThread* thread)
{
    GThreadPosix* pt = (GThreadPosix*)thread;

    if (!pt->joined)
        pthread_detach(pt->system_thread);

    g_mutex_clear(&pt->lock);

    g_slice_free(GThreadPosix, pt);
}

GRealThread*
g_system_thread_new(GThreadFunc proxy,
    gulong stack_size,
    const char* name,
    GThreadFunc func,
    gpointer data,
    GError** error)
{
    GThreadPosix* thread;
    GRealThread* base_thread;
    pthread_attr_t attr;
    gint ret;

    thread = g_slice_new0(GThreadPosix);
    base_thread = (GRealThread*)thread;
    base_thread->ref_count = 2;
    base_thread->ours = TRUE;
    base_thread->thread.joinable = TRUE;
    base_thread->thread.func = func;
    base_thread->thread.data = data;
    base_thread->name = g_strdup(name);
    thread->proxy = proxy;

    posix_check_cmd(pthread_attr_init(&attr));

#ifdef HAVE_PTHREAD_ATTR_SETSTACKSIZE
    if (stack_size)
    {
#ifdef _SC_THREAD_STACK_MIN
        long min_stack_size = sysconf(_SC_THREAD_STACK_MIN);
        if (min_stack_size >= 0)
            stack_size = MAX((gulong)min_stack_size, stack_size);
#endif /* _SC_THREAD_STACK_MIN */
        /* No error check here, because some systems can't do it and
         * we simply don't want threads to fail because of that. */
        pthread_attr_setstacksize(&attr, stack_size);
    }
#endif /* HAVE_PTHREAD_ATTR_SETSTACKSIZE */

#ifdef HAVE_PTHREAD_ATTR_SETINHERITSCHED
    {
        /* While this is the default, better be explicit about it */
        pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
    }
#endif /* HAVE_PTHREAD_ATTR_SETINHERITSCHED */

    ret = pthread_create(&thread->system_thread, &attr, (void* (*)(void*))proxy, thread);

    posix_check_cmd(pthread_attr_destroy(&attr));

    if (ret == EAGAIN)
    {
        g_set_error(error, G_THREAD_ERROR, G_THREAD_ERROR_AGAIN,
            "Error creating thread: %s", g_strerror(ret));
        g_free(thread->thread.name);
        g_slice_free(GThreadPosix, thread);
        return NULL;
    }

    posix_check_err(ret, "pthread_create");

    g_mutex_init(&thread->lock);

    return (GRealThread*)thread;
}
#endif

/**
 * g_atomic_pointer_get:
 * @atomic: (not nullable): a pointer to a #gpointer-sized value
 *
 * Gets the current value of @atomic.
 *
 * This call acts as a full compiler and hardware
 * memory barrier (before the get).
 *
 * While @atomic has a `volatile` qualifier, this is a historical artifact and
 * the pointer passed to it should not be `volatile`.
 *
 * Returns: the value of the pointer
 *
 * Since: 2.4
 **/
#if 0
gpointer
(g_atomic_pointer_get) (const volatile void* atomic)
{
    return g_atomic_pointer_get((gpointer*)atomic);
}
#endif

guint __stdcall g_thread_win32_proxy(gpointer data)
{
    GThreadWin32* self = data;
    self->proxy(self);
    g_system_thread_exit();
    g_assert_not_reached();
    return 0;
}


/**
 * g_slice_alloc0:
 * @block_size: the number of bytes to allocate
 *
 * Allocates a block of memory via g_slice_alloc() and initializes
 * the returned memory to 0.
 *
 * Since GLib 2.76 this always uses the system malloc() implementation
 * internally.
 *
 * Returns: (nullable): a pointer to the allocated block, which will be %NULL
 *    if and only if @mem_size is 0
 *
 * Since: 2.10
 */
gpointer g_slice_alloc0(gsize mem_size)
{
    gpointer mem = g_slice_alloc(mem_size);
    if (mem)
        memset(mem, 0, mem_size);
    return mem;
}

/**
 * g_slice_free1:
 * @block_size: the size of the block
 * @mem_block: (nullable): a pointer to the block to free
 *
 * Frees a block of memory.
 *
 * The memory must have been allocated via g_slice_alloc() or
 * g_slice_alloc0() and the @block_size has to match the size
 * specified upon allocation. Note that the exact release behaviour
 * can be changed with the [`G_DEBUG=gc-friendly`][G_DEBUG] environment
 * variable.
 *
 * If @mem_block is %NULL, this function does nothing.
 *
 * Since GLib 2.76 this always uses the system free_sized() implementation
 * internally.
 *
 * Since: 2.10
 */

extern gboolean g_mem_gc_friendly;
#define TRACE(probe)

void g_slice_free1(gsize mem_size,gpointer mem_block)
{
    if (G_UNLIKELY(g_mem_gc_friendly && mem_block))
        memset(mem_block, 0, mem_size);
    g_free_sized(mem_block, mem_size);
    TRACE(GLIB_SLICE_FREE((void*)mem_block, mem_size));
}

/**
 * g_win32_error_message:
 * @error: error code.
 *
 * Translate a Win32 error code (as returned by GetLastError() or
 * WSAGetLastError()) into the corresponding message. The message is
 * either language neutral, or in the thread's language, or the user's
 * language, the system's language, or US English (see docs for
 * FormatMessage()). The returned string is in UTF-8. It should be
 * deallocated with g_free().
 *
 * Returns: newly-allocated error message
 **/
gchar* g_win32_error_message(gint error)
{
    gchar* retval;
    wchar_t* msg = NULL;
    size_t nchars;

    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER
        | FORMAT_MESSAGE_IGNORE_INSERTS
        | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL, error, 0,
        (LPWSTR)&msg, 0, NULL);
    if (msg != NULL)
    {
        nchars = wcslen(msg);

        if (nchars >= 2 && msg[nchars - 1] == L'\n' && msg[nchars - 2] == L'\r')
            msg[nchars - 2] = L'\0';

        retval = g_utf16_to_utf8(msg, -1, NULL, NULL, NULL);

        LocalFree(msg);
    }
    else
        retval = g_strdup("");

    return retval;
}

/**
 * g_atomic_pointer_exchange:
 * @atomic: a pointer to a #gpointer-sized value
 * @newval: the value to replace with
 *
 * Sets the @atomic to @newval and returns the old value from @atomic.
 *
 * This exchange is done atomically.
 *
 * Think of this operation as an atomic version of
 * `{ tmp = *atomic; *atomic = val; return tmp; }`.
 *
 * This call acts as a full compiler and hardware memory barrier.
 *
 * Returns: the value of @atomic before the exchange
 *
 * Since: 2.74
 **/
#if 0
gpointer g_atomic_pointer_exchange(void* atomic,gpointer  newval)
{
    return g_atomic_pointer_exchange((gpointer*)atomic, newval);
}
#endif

void g_system_thread_exit(void)
{
    _endthreadex(0);
}

/**
 * g_mem_gc_friendly:
 *
 * This variable is %TRUE if the `G_DEBUG` environment variable
 * includes the key `gc-friendly`.
 */
gboolean g_mem_gc_friendly = FALSE;

GLogLevelFlags g_log_msg_prefix = G_LOG_LEVEL_ERROR | G_LOG_LEVEL_WARNING |
G_LOG_LEVEL_CRITICAL | G_LOG_LEVEL_DEBUG;
GLogLevelFlags g_log_always_fatal = G_LOG_FATAL_MASK;

gboolean debug_key_matches(const gchar* key,const gchar* token,guint length)
{
    /* may not call GLib functions: see note in g_parse_debug_string() */
    for (; length; length--, key++, token++)
    {
        char k = (*key == '_') ? '-' : tolower(*key);
        char t = (*token == '_') ? '-' : tolower(*token);

        if (k != t)
            return FALSE;
    }

    return *key == '\0';
}

/**
 * g_slice_alloc:
 * @block_size: the number of bytes to allocate
 *
 * Allocates a block of memory from the libc allocator.
 *
 * The block address handed out can be expected to be aligned
 * to at least `1 * sizeof (void*)`.
 *
 * Since GLib 2.76 this always uses the system malloc() implementation
 * internally.
 *
 * Returns: (nullable): a pointer to the allocated memory block, which will
 *   be %NULL if and only if @mem_size is 0
 *
 * Since: 2.10
 */
gpointer g_slice_alloc(gsize mem_size)
{
    gpointer mem;

    mem = g_malloc(mem_size);
    TRACE(GLIB_SLICE_ALLOC((void*)mem, mem_size));

    return mem;
}

/**
 * g_free_sized:
 * @mem: (nullable): the memory to free
 * @size: size of @mem, in bytes
 *
 * Frees the memory pointed to by @mem, assuming it is has the given @size.
 *
 * If @mem is %NULL this is a no-op (and @size is ignored).
 *
 * It is an error if @size doesn�t match the size passed when @mem was
 * allocated. @size is passed to this function to allow optimizations in the
 * allocator. If you don�t know the allocation size, use g_free() instead.
 *
 * In case a GCC compatible compiler is used, this function may be used
 * automatically via g_free() if the allocated size is known at compile time,
 * since GLib 2.78.
 *
 * Since: 2.76
 */
void g_free_sized(void* mem,size_t  size)
{
#ifdef HAVE_FREE_SIZED
    free_sized(mem, size);
#else
    free(mem);
#endif
    TRACE(GLIB_MEM_FREE((void*)mem));
}


/**
 * g_atomic_pointer_exchange:
 * @atomic: a pointer to a #gpointer-sized value
 * @newval: the value to replace with
 *
 * Sets the @atomic to @newval and returns the old value from @atomic.
 *
 * This exchange is done atomically.
 *
 * Think of this operation as an atomic version of
 * `{ tmp = *atomic; *atomic = val; return tmp; }`.
 *
 * This call acts as a full compiler and hardware memory barrier.
 *
 * Returns: the value of @atomic before the exchange
 *
 * Since: 2.74
 **/
#if 0
gpointer g_atomic_pointer_exchange (void* atomic,gpointer  newval)
{
    return g_atomic_pointer_exchange((gpointer*)atomic, newval);
}
#endif

/**
 * g_thread_new:
 * @name: (nullable): an (optional) name for the new thread
 * @func: (closure data) (scope async): a function to execute in the new thread
 * @data: (nullable): an argument to supply to the new thread
 *
 * This function creates a new thread. The new thread starts by invoking
 * @func with the argument data. The thread will run until @func returns
 * or until g_thread_exit() is called from the new thread. The return value
 * of @func becomes the return value of the thread, which can be obtained
 * with g_thread_join().
 *
 * The @name can be useful for discriminating threads in a debugger.
 * It is not used for other purposes and does not have to be unique.
 * Some systems restrict the length of @name to 16 bytes.
 *
 * If the thread can not be created the program aborts. See
 * g_thread_try_new() if you want to attempt to deal with failures.
 *
 * If you are using threads to offload (potentially many) short-lived tasks,
 * #GThreadPool may be more appropriate than manually spawning and tracking
 * multiple #GThreads.
 *
 * To free the struct returned by this function, use g_thread_unref().
 * Note that g_thread_join() implicitly unrefs the #GThread as well.
 *
 * New threads by default inherit their scheduler policy (POSIX) or thread
 * priority (Windows) of the thread creating the new thread.
 *
 * This behaviour changed in GLib 2.64: before threads on Windows were not
 * inheriting the thread priority but were spawned with the default priority.
 * Starting with GLib 2.64 the behaviour is now consistent between Windows and
 * POSIX and all threads inherit their parent thread's priority.
 *
 * Returns: (transfer full): the new #GThread
 *
 * Since: 2.32
 */
#if 0
GThread*
g_thread_new(const gchar* name,
    GThreadFunc  func,
    gpointer     data)
{
    GError* error = NULL;
    GThread* thread;

    thread = g_thread_new_internal(name, g_thread_proxy, func, data, 0, &error);

    if G_UNLIKELY(thread == NULL)
        g_error("creating thread '%s': %s", name ? name : "", error->message);

    return thread;
}
#endif

#ifdef HAVE_SYSPROF
#define G_TRACE_CURRENT_TIME SYSPROF_CAPTURE_CURRENT_TIME
#else
#define G_TRACE_CURRENT_TIME 0
#endif

#if 0
GThread*
g_thread_new_internal(const gchar* name,
    GThreadFunc proxy,
    GThreadFunc func,
    gpointer data,
    gsize stack_size,
    GError** error)
{
    g_return_val_if_fail(func != NULL, NULL);

    g_atomic_int_inc(&g_thread_n_created_counter);

    g_trace_mark(G_TRACE_CURRENT_TIME, 0, "GLib", "GThread created", "%s", name ? name : "(unnamed)");
    return (GThread*)g_system_thread_new(proxy, stack_size, name, func, data, error);
}
#endif

#if 0
gpointer g_thread_proxy(gpointer data)
{
    GRealThread* thread = data;

    g_assert(data);
    g_private_set(&g_thread_specific_private, data);

    TRACE(GLIB_THREAD_SPAWNED(thread->thread.func, thread->thread.data,
        thread->name));

    if (thread->name)
    {
        g_system_thread_set_name(thread->name);
        g_free(thread->name);
        thread->name = NULL;
    }

    thread->retval = thread->thread.func(thread->thread.data);

    return NULL;
}
#endif