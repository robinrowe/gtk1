# glib_test/CMakeLists.txt
# Created by Robin Rowe 2024-08-09
# License Open Source GPL2

cmake_minimum_required(VERSION 3.8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(GlibTest)
message("Configuring ${CMAKE_PROJECT_NAME}...")
file(STRINGS sources.cmake SOURCES)
message("Docs: ${SOURCES}")
link_libraries(glib)

option(UNISTD "Enable libunistd" false)
if(UNISTD)
	if(WIN32)
#		message("CMAKE_GENERATOR_PLATFORM = ${CMAKE_GENERATOR_PLATFORM}")
		set(LIBUNISTD_PATH /code/github/libunistd)
		include_directories(${LIBUNISTD_PATH}/unistd)
		set (UNISTD_LINK_DIRECTORIES ${LIBUNISTD_PATH}/build/${CMAKE_GENERATOR_PLATFORM}/Release)
		message(libunistd = ${UNISTD_LINK_DIRECTORIES}/libunistd.lib)
		link_directories(${UNISTD_LINK_DIRECTORIES})
		link_libraries(libunistd Ws2_32 Imm32)
	endif(WIN32)
endif(UNISTD)

#add_subdirectory(glib_contrainers)  
add_subdirectory(glib_main) 
#add_subdirectory(glib_tests)
