/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997, 2002  Peter Mattis, Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __G_PRINTF_H__
#define __G_PRINTF_H__

#include <glib/gtypes.h>
#include <stdio.h>
#include <stdarg.h>

gint g_printf    (gchar const *format,
                                   ...) G_GNUC_PRINTF (1, 2);            
gint g_fprintf   (FILE        *file,
				   gchar const *format,
				   ...) G_GNUC_PRINTF (2, 3);
gint g_sprintf   (gchar       *string,
				   gchar const *format,
				   ...) G_GNUC_PRINTF (2, 3);
gint g_snprintf  (gchar       *string,
				   gulong       n,
				   gchar const *format,
				   ...) G_GNUC_PRINTF (3, 4);

gint g_vprintf   (gchar const *format,
                                   va_list      args);            
gint g_vfprintf  (FILE        *file,
				   gchar const *format,
				   va_list      args);
gint g_vsprintf  (gchar       *string,
				   gchar const *format,
				   va_list      args);
gint g_vsnprintf (gchar       *string,
				   gulong       n,
				   gchar const *format,
				   va_list      args);

#ifdef _WIN32
//#define HAVE_GOOD_PRINTF
//#ifdef _WIN32

#include "trio/glibtrio.h"
#include "trio/trio.h"
#define g_printf    trio_printf
#define g_fprintf   trio_fprintf
#define g_sprintf   trio_sprintf
#define g_snprintf  trio_snprintf
#define g_vprintf   trio_vprintf
#define g_vfprintf  trio_vfprintf
#define g_vsprintf  trio_vsprintf
#define g_vsnprintf trio_vsnprintf
#define _g_printf    trio_printf
#define _g_fprintf   trio_fprintf
#define _g_sprintf   trio_sprintf
#define _g_snprintf  trio_snprintf
#define _g_vprintf   trio_vprintf
#define _g_vfprintf  trio_vfprintf
#define _g_vsprintf  trio_vsprintf
#define _g_vsnprintf trio_vsnprintf

#if 0
#define _g_printf    _g_trio_printf
#define _g_fprintf   _g_trio_fprintf
#define _g_sprintf   _g_trio_sprintf
#define _g_snprintf  _g_trio_snprintf
#define _g_vprintf   _g_trio_vprintf
#define _g_vfprintf  _g_trio_vfprintf
#define _g_vsprintf  _g_trio_vsprintf
#define _g_vasprintf _g_trio_vasprintf
#define _g_vsnprintf _g_trio_vsnprintf
#endif

#else

#define g_printf    printf
#define g_fprintf   fprintf
#define g_sprintf   sprintf
#define g_snprintf  snprintf
#define g_vprintf   vprintf
#define g_vfprintf  vfprintf
#define g_vsprintf  vsprintf
#define g_vsnprintf vsnprintf
#define g_vasprintf vasprintf
#define _g_printf    printf
#define _g_fprintf   fprintf
#define _g_sprintf   sprintf
#define _g_snprintf  _snprintf
#define _g_vprintf   vprintf
#define _g_vfprintf  vfprintf
#define _g_vsprintf  vsprintf
#define _g_vsnprintf _vsnprintf

#endif

#endif /* __G_PRINTF_H__ */



