/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * gmain.c: Main loop abstraction, timeouts, and idle functions
 * Copyright 1998 Owen Taylor
 *
 * gmain.c: modifications for MacOS X 
 * Copyright 2003 Brian Griffith
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GLib Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/. 
 */

/* 
 * MT safe
 */

/* uncomment the next line to get poll() debugging info */
/* #define G_MAIN_POLL_DEBUG */

#include <sys/types.h>
#include <time.h>
#ifndef _WIN32
#include <sys/time.h>
#endif
#include <unistd.h>
#include <errno.h>
#ifdef GLIB_HAVE_SYS_POLL_H
#  include <sys/poll.h>
#  undef events	 /* AIX 4.1.5 & 4.3.2 define this for SVR3,4 compatibility */
#  undef revents /* AIX 4.1.5 & 4.3.2 define this for SVR3,4 compatibility */
#endif /* GLIB_HAVE_SYS_POLL_H */
#include "glib.h"
#include "gthread.h"
#include "ghash.h"

#ifdef NATIVE_WIN32
#include <windows.h>
//#include "g-win32.h"
#include <fcntl.h>
#include <io.h>
#endif /* _MSC_VER */

#ifdef MAC_CARBON_EVENTS
#include <Carbon/Carbon.h>
#endif /* MAC_CARBON_EVENTS */

/* Types */


/* The following lock is used for both the list of sources
 * and the list of poll records
 */
G_LOCK_DEFINE_STATIC (main_loop);

static GSourceFuncs timeout_funcs =
{
  g_timeout_prepare,
  g_timeout_check,
  g_timeout_dispatch,
  g_free,
};

static GSourceFuncs idle_funcs =
{
  g_idle_prepare,
  g_idle_check,
  g_idle_dispatch,
  NULL
};

static GPollRec *poll_records = NULL;
static GPollRec *poll_free_list = NULL;
static GMemChunk *poll_chunk;
static guint n_poll_records = 0;

#ifdef G_THREADS_ENABLED
#ifndef NATIVE_WIN32
/* this pipe is used to wake up the main loop when a source is added.
 */
static gint wake_up_pipe[2] = { -1, -1 };
#else /* NATIVE_WIN32 */
static SOCKET wake_up_semaphore = 0;
#endif /* NATIVE_WIN32 */
static GPollFD wake_up_rec;
static gboolean poll_waiting = FALSE;

/* Flag indicating whether the set of fd's changed during a poll */
static gboolean poll_changed = FALSE;
#endif /* G_THREADS_ENABLED */

#ifdef HAVE_POLL
/* SunOS has poll, but doesn't provide a prototype. */
#  if defined (sun) && !defined (__SVR4)
extern gint poll (GPollFD *ufds, guint nfsd, gint timeout);
#  endif  /* !sun */
static GPollFunc poll_func = (GPollFunc) poll;
#else	/* !HAVE_POLL */

#if 0

static gint
g_poll (GPollFD *fds, guint nfds, gint timeout)
{
  HANDLE handles[MAXIMUM_WAIT_OBJECTS];
  GPollFD *f;
  DWORD ready;
  MSG msg;
  UINT timer;
  LONG prevcnt;
  gint poll_msgs = -1;
  gint nhandles = 0;

  for (f = fds; f < &fds[nfds]; ++f)
    if (f->fd >= 0)
      {
	if (f->events & G_IO_IN)
	  if (f->fd == G_WIN32_MSG_HANDLE)
	    poll_msgs = f - fds;
	  else
	    {
	      /* g_print ("g_poll: waiting for handle %#x\n", f->fd); */
	      handles[nhandles++] = (HANDLE) f->fd;
	    }
      }

  if (timeout == -1)
    timeout = INFINITE;

  if (poll_msgs >= 0)
    {
      /* Waiting for messages, and maybe events */
      if (nhandles == 0)
	{
	  if (timeout == INFINITE)
	    {
	      /* Waiting just for messages, infinite timeout
	       * -> Use PeekMessage, then WaitMessage
	       */
	      /* g_print ("WaitMessage, PeekMessage\n"); */
	      if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
		ready = WAIT_OBJECT_0;
	      else if (!WaitMessage ())
		g_warning ("g_poll: WaitMessage failed");
	      ready = WAIT_OBJECT_0;
	    }
	  else if (timeout == 0)
	    {
	      /* Waiting just for messages, zero timeout
	       * -> Use PeekMessage
	       */
	      /* g_print ("PeekMessage\n"); */
	      if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
		ready = WAIT_OBJECT_0;
	      else
		ready = WAIT_TIMEOUT;
	    }
	  else
	    {
	      /* Waiting just for messages, some timeout
	       * -> First try PeekMessage, then set a timer, wait for message,
	       * kill timer, use PeekMessage
	       */
	      /* g_print ("PeekMessage\n"); */
	      if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
		ready = WAIT_OBJECT_0;
	      else if ((timer = SetTimer (NULL, 0, timeout, NULL)) == 0)
		g_warning ("g_poll: SetTimer failed");
	      else
		{
		  /* g_print ("WaitMessage\n"); */
		  WaitMessage ();
		  KillTimer (NULL, timer);
		  if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
		    ready = WAIT_OBJECT_0;
		  else
		    ready = WAIT_TIMEOUT;
		}
	    }
	}
      else
	{
	  /* Wait for either message or event
	   * -> Use MsgWaitForMultipleObjects
	   */
	  /* g_print ("MsgWaitForMultipleObjects(%d, %d)\n", nhandles, timeout); */
	  ready = MsgWaitForMultipleObjects (nhandles, handles, FALSE,
					     timeout, QS_ALLINPUT);
	  /* g_print("=%d\n", ready); */
	  if (ready == WAIT_FAILED)
	    g_warning ("g_poll: MsgWaitForMultipleObjects failed");
	}
    }
  else if (nhandles == 0)
    {
      /* Wait for nothing (huh?) */
      return 0;
    }
  else
    {
      /* Wait for just events
       * -> Use WaitForMultipleObjects
       */
      /* g_print ("WaitForMultipleObjects(%d, %d)\n", nhandles, timeout); */
      ready = WaitForMultipleObjects (nhandles, handles, FALSE, timeout);
      /* g_print("=%d\n", ready); */
      if (ready == WAIT_FAILED)
	g_warning ("g_poll: WaitForMultipleObjects failed");
    }

  for (f = fds; f < &fds[nfds]; ++f)
    f->revents = 0;

  if (ready == WAIT_FAILED)
    return -1;
  else if (poll_msgs >= 0 && ready == WAIT_OBJECT_0 + nhandles)
    {
      fds[poll_msgs].revents |= G_IO_IN;
    }
  else if (ready >= WAIT_OBJECT_0 && ready < WAIT_OBJECT_0 + nhandles)
    for (f = fds; f < &fds[nfds]; ++f)
      {
	if ((f->events & G_IO_IN)
	    && f->fd == (gint) handles[ready - WAIT_OBJECT_0])
	  {
	    f->revents |= G_IO_IN;
	    /* g_print ("event %#x\n", f->fd); */
	    ResetEvent ((HANDLE) f->fd);
	  }
      }
    
  if (ready == WAIT_TIMEOUT)
    return 0;
  else
    return 1;
}
#endif
#ifdef G_OS_WIN32

gint
g_poll (GPollFD *fds,
	guint    nfds,
	guint     timeout)
{
  SOCKET handles[MAXIMUM_WAIT_OBJECTS];
  gboolean poll_msgs = FALSE;
  GPollFD *f;
  DWORD ready;
  MSG msg;
  UINT_PTR timer;
  gint nhandles = 0;

  for (f = fds; f < &fds[nfds]; ++f)
    if (f->fd >= 0)
      {
	if (f->fd == G_WIN32_MSG_HANDLE)
	  poll_msgs = TRUE;
	else
	  {
#ifdef G_MAIN_POLL_DEBUG
	    g_print ("g_poll: waiting for %#x\n", f->fd);
#endif
	    handles[nhandles++] = f->fd;
	  }
      }

  if (timeout == -1)
    timeout = INFINITE;

  if (poll_msgs)
    {
      /* Waiting for messages, and maybe events
       * -> First PeekMessage
       */
#ifdef G_MAIN_POLL_DEBUG
      g_print ("PeekMessage\n");
#endif
      if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
	ready = WAIT_OBJECT_0 + nhandles;
      else
	{
	  if (nhandles == 0)
	    {
	      /* Waiting just for messages */
	      if (timeout == INFINITE)
		{
		  /* Infinite timeout
		   * -> WaitMessage
		   */
#ifdef G_MAIN_POLL_DEBUG
		  g_print ("WaitMessage\n");
#endif
		  if (!WaitMessage ())
		    g_warning (G_STRLOC ": WaitMessage() failed");
		  ready = WAIT_OBJECT_0 + nhandles;
		}
	      else if (timeout == 0)
		{
		  /* Waiting just for messages, zero timeout.
		   * If we got here, there was no message
		   */
		  ready = WAIT_TIMEOUT;
		}
	      else
		{
		  /* Waiting just for messages, some timeout
		   * -> Set a timer, wait for message,
		   * kill timer, use PeekMessage
		   */
		  timer = SetTimer (NULL, 0, timeout, NULL);
		  if (timer == 0)
		    {
		      g_warning (G_STRLOC ": SetTimer() failed");
		      ready = WAIT_TIMEOUT;
		    }
		  else
		    {
#ifdef G_MAIN_POLL_DEBUG
		      g_print ("WaitMessage\n");
#endif
		      WaitMessage ();
		      KillTimer (NULL, timer);
#ifdef G_MAIN_POLL_DEBUG
		      g_print ("PeekMessage\n");
#endif
		      if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)
			  && msg.message != WM_TIMER)
			ready = WAIT_OBJECT_0;
		      else
			ready = WAIT_TIMEOUT;
		    }
		}
	    }
	  else
	    {
	      /* Wait for either message or event
	       * -> Use MsgWaitForMultipleObjects
	       */
#ifdef G_MAIN_POLL_DEBUG
	      g_print ("MsgWaitForMultipleObjects(%d, %d)\n", nhandles, timeout);
#endif
	      ready = MsgWaitForMultipleObjects (nhandles, (HANDLE*) handles, FALSE,
						 timeout, QS_ALLINPUT);

	      if (ready == WAIT_FAILED)
		g_warning (G_STRLOC ": MsgWaitForMultipleObjects() failed");
	    }
	}
    }
  else if (nhandles == 0)
    {
      /* Wait for nothing (huh?) */
      return 0;
    }
  else
    {
      /* Wait for just events
       * -> Use WaitForMultipleObjects
       */
#ifdef G_MAIN_POLL_DEBUG
      g_print ("WaitForMultipleObjects(%d, %d)\n", nhandles, timeout);
#endif
      ready = WaitForMultipleObjects (nhandles, (HANDLE*) handles, FALSE, timeout);
      if (ready == WAIT_FAILED)
	g_warning (G_STRLOC ": WaitForMultipleObjects() failed");
    }

#ifdef G_MAIN_POLL_DEBUG
  g_print ("wait returns %ld%s\n",
	   ready,
	   (ready == WAIT_FAILED ? " (WAIT_FAILED)" :
	    (ready == WAIT_TIMEOUT ? " (WAIT_TIMEOUT)" :
	     (poll_msgs && ready == WAIT_OBJECT_0 + nhandles ? " (msg)" : ""))));
#endif
  for (f = fds; f < &fds[nfds]; ++f)
    f->revents = 0;

  if (ready == WAIT_FAILED)
    return -1;
  else if (ready == WAIT_TIMEOUT)
    return 0;
  else if (poll_msgs && ready == WAIT_OBJECT_0 + nhandles)
    {
      for (f = fds; f < &fds[nfds]; ++f)
	if (f->fd >= 0)
	  {
	    if (f->events & G_IO_IN)
	      if (f->fd == G_WIN32_MSG_HANDLE)
		f->revents |= G_IO_IN;
	  }
    }
#if 1 /* TEST_WITHOUT_THIS */
  else if (ready >= WAIT_OBJECT_0 && ready < WAIT_OBJECT_0 + nhandles)
    for (f = fds; f < &fds[nfds]; ++f)
      {
	if ((f->events & (G_IO_IN | G_IO_OUT))
	    && f->fd == handles[ready - WAIT_OBJECT_0])
	  {
	    if (f->events & G_IO_IN)
	      f->revents |= G_IO_IN;
	    else
	      f->revents |= G_IO_OUT;
#ifdef G_MAIN_POLL_DEBUG
	    g_print ("g_poll: got event %#x\n", f->fd);
#endif
#if 0
	    ResetEvent ((HANDLE) f->fd);
#endif
	  }
      }
#endif
    
  return 1;
}

#else  /* !G_OS_WIN32 */

//#else  /* !NATIVE_WIN32 */

/* The following implementation of poll() comes from the GNU C Library.
 * Copyright (C) 1994, 1996, 1997 Free Software Foundation, Inc.
 */

#include <string.h> /* for bzero on BSD systems */

#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif /* HAVE_SYS_SELECT_H_ */

#ifndef NO_FD_SET
#  define SELECT_MASK fd_set
#else /* !NO_FD_SET */
#  ifndef _AIX
typedef long fd_mask;
#  endif /* _AIX */
#  ifdef _IBMR2
#    define SELECT_MASK void
#  else /* !_IBMR2 */
#    define SELECT_MASK int
#  endif /* !_IBMR2 */
#endif /* !NO_FD_SET */

gint 
g_poll (GPollFD *fds,
	guint    nfds,
	gint     timeout)
{
  struct timeval tv;
  SELECT_MASK rset, wset, xset;
  GPollFD *f;
  int ready;
  int maxfd = 0;

  FD_ZERO (&rset);
  FD_ZERO (&wset);
  FD_ZERO (&xset);

  for (f = fds; f < &fds[nfds]; ++f)
    if (f->fd >= 0)
      {
	if (f->events & G_IO_IN)
	  FD_SET (f->fd, &rset);
	if (f->events & G_IO_OUT)
	  FD_SET (f->fd, &wset);
	if (f->events & G_IO_PRI)
	  FD_SET (f->fd, &xset);
	if (f->fd > maxfd && (f->events & (G_IO_IN|G_IO_OUT|G_IO_PRI)))
	  maxfd = f->fd;
      }

  tv.tv_sec = timeout / 1000;
  tv.tv_usec = (timeout % 1000) * 1000;

  ready = select (maxfd + 1, &rset, &wset, &xset,
		  timeout == -1 ? NULL : &tv);
  if (ready > 0)
    for (f = fds; f < &fds[nfds]; ++f)
      {
	f->revents = 0;
	if (f->fd >= 0)
	  {
	    if (FD_ISSET (f->fd, &rset))
	      f->revents |= G_IO_IN;
	    if (FD_ISSET (f->fd, &wset))
	      f->revents |= G_IO_OUT;
	    if (FD_ISSET (f->fd, &xset))
	      f->revents |= G_IO_PRI;
	  }
      }

  return ready;
}

#endif /* !NATIVE_WIN32 */

static GPollFunc poll_func = g_poll;
#endif	/* !HAVE_POLL */

/* Hooks for adding to the main loop */

/* Use knowledge of insert_sorted algorithm here to make
 * sure we insert at the end of equal priority items
 */
static gint
g_source_compare (GHook *a,
		  GHook *b)
{
  GSource *source_a = (GSource *)a;
  GSource *source_b = (GSource *)b;

  return (source_a->priority < source_b->priority) ? -1 : 1;
}

/* HOLDS: main_loop lock */
static void
g_source_destroy_func (GHookList *hook_list,
		       GHook     *hook)
{
  GSource *source = (GSource*) hook;
  GDestroyNotify destroy;

  G_UNLOCK (main_loop);

  destroy = hook->destroy;
  if (destroy)
    destroy (hook->data);

  destroy = (hook->source_funcs)->destroy;
  if (destroy)
    destroy (source);

  G_LOCK (main_loop);
}

guint 
g_source_add (gint           priority,
	      gboolean       can_recurse,
	      GSourceFuncs  *funcs,
	      gpointer       source_data, 
	      gpointer       user_data,
	      GDestroyNotify notify)
{
  guint return_val;
  GSource *source;

  G_LOCK (main_loop);

  if (!source_list.is_setup)
    {
      g_hook_list_init (&source_list, sizeof (GSource));

      source_list.hook_destroy = G_HOOK_DEFERRED_DESTROY;
      source_list.hook_free = g_source_destroy_func;
    }

  source = (GSource*) g_hook_alloc (&source_list);
  source->priority = priority;
 // source->source_data = source_data;
  source->hook.source_funcs = funcs;
  source->hook.data = user_data;
  source->hook.destroy = notify;
  
  g_hook_insert_sorted (&source_list, 
			(GHook *)source, 
			g_source_compare);

  if (can_recurse)
    source->hook.flags |= G_SOURCE_CAN_RECURSE;

  return_val = source->hook.hook_id;
  
#ifdef MAC_CARBON_EVENTS  
  /* low-level glib source events (not idle or timeout type), handle with a single Mac timer */
  if (g_main_iteration_timer_ref != NULL) 
  {
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB incrementing timer for source\n");
#endif
    g_main_iteration_timer_use_count++;
  }
  else
  {
    /* install a native Mac idle timer to handle these low-level glib sources */
    if (g_main_iteration_timer_upp == NULL)
    {
      g_main_iteration_timer_upp = NewEventLoopTimerUPP (mac_handle_g_main_iteration_action);
    }

#ifdef	G_MAIN_MAC_DEBUG    
    printf ("GLIB Creating timer for source\n");
#endif
    InstallEventLoopTimer (GetMainEventLoop(), g_mac_main_iteration_interval,
                            g_mac_main_iteration_interval, g_main_iteration_timer_upp,
                            NULL, &g_main_iteration_timer_ref);
    
    g_main_iteration_timer_use_count++;
  }
#endif

#ifdef G_THREADS_ENABLED
  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_wakeup ();
#endif

  G_UNLOCK (main_loop);
  
  return return_val;
}

gboolean
g_source_remove (guint tag)
{
  GHook *hook;

  g_return_val_if_fail (tag > 0, FALSE);

  G_LOCK (main_loop);

  hook = g_hook_get (&source_list, tag);
  
  if (hook)
  {
#ifdef MAC_CARBON_EVENTS
    GSource *source = (GSource*) hook;
    if (source->mac_timer_ref != NULL)
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB removing timer for source (source)\n");
#endif
      RemoveEventLoopTimer (source->mac_timer_ref);
      source->mac_timer_ref = NULL;
    }
    else
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB decrementing main_iteration timer (source)\n");
#endif
      /* source is using idle timer to handle events */
      g_main_iteration_timer_use_count--;
      
      if (g_main_iteration_timer_use_count == 0)
      {
#ifdef	G_MAIN_MAC_DEBUG
        printf ("GLIB removing main_iteration timer (source)\n");
#endif
        RemoveEventLoopTimer (g_main_iteration_timer_ref);
        g_main_iteration_timer_ref = NULL;
      }
    }
#endif

    g_hook_destroy_link (&source_list, hook);
  }

  G_UNLOCK (main_loop);
  
  return hook != NULL;
}

gboolean
g_source_remove_by_user_data (gpointer user_data)
{
  GHook *hook;
  
  G_LOCK (main_loop);
  
  hook = g_hook_find_data (&source_list, TRUE, user_data);
  if (hook)
  {
#ifdef MAC_CARBON_EVENTS
    GSource *source = (GSource*) hook; 
    if (source->mac_timer_ref != NULL)
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB removing timer for source (by user data)\n");
#endif
      RemoveEventLoopTimer (source->mac_timer_ref);
      source->mac_timer_ref = NULL;
    }
    else
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB decrementing timer for main_iteration (by user data)\n");
#endif
      /* source is using idle timer to handle events */
      g_main_iteration_timer_use_count--;
      
      if (g_main_iteration_timer_use_count == 0)
      {
#ifdef	G_MAIN_MAC_DEBUG
        printf ("GLIB removing timer for main_iteration (by user data)\n");
#endif
        RemoveEventLoopTimer (g_main_iteration_timer_ref);
        g_main_iteration_timer_ref = NULL;
      }
    }
#endif

    g_hook_destroy_link (&source_list, hook);
  }

  G_UNLOCK (main_loop);

  return hook != NULL;
}

static gboolean
g_source_find_source_data (GHook	*hook,
			   gpointer	 data)
{
  GSource *source = (GSource *)hook;

  return (source == data);
}

gboolean
g_source_remove_by_source_data (gpointer source_data)
{
  GHook *hook;

  G_LOCK (main_loop);

  hook = g_hook_find (&source_list, TRUE, 
		      g_source_find_source_data, source_data);
  if (hook)
  {
#ifdef MAC_CARBON_EVENTS
    GSource *source = (GSource*) hook;
    if (source->mac_timer_ref != NULL)
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB removing timer for source (by source data)\n");
#endif
      RemoveEventLoopTimer (source->mac_timer_ref);
      source->mac_timer_ref = NULL;
    }
    else
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB decrementing timer for main_iteration (by source data)\n");
#endif
      /* source is using idle timer to handle events */
      g_main_iteration_timer_use_count--;
      
      if (g_main_iteration_timer_use_count == 0)
      {
#ifdef	G_MAIN_MAC_DEBUG
        printf ("GLIB removing timer for main_iteration (by source data)\n");
#endif
        RemoveEventLoopTimer (g_main_iteration_timer_ref);
        g_main_iteration_timer_ref = NULL;
      }
    }
#endif

    g_hook_destroy_link (&source_list, hook);
  }

  G_UNLOCK (main_loop);

  return hook != NULL;
}

static gboolean
g_source_find_funcs_user_data (GHook   *hook,
			       gpointer data)
{
  gpointer *d = data;
  return hook->hook_func == d[0] && hook->data == d[1];
}

gboolean
g_source_remove_by_funcs_user_data (GSourceFuncs *funcs,
				    gpointer      user_data)
{
  gpointer d[2];
  GHook *hook;

  g_return_val_if_fail (funcs != NULL, FALSE);

  G_LOCK (main_loop);

  d[0] = funcs;
  d[1] = user_data;

  hook = g_hook_find (&source_list, TRUE,
		      g_source_find_funcs_user_data, d);
  if (hook)
  {
#ifdef MAC_CARBON_EVENTS
    GSource *source = (GSource*) hook;
    if (source->mac_timer_ref != NULL)
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB removing timer for source (by funcs)\n");
#endif
      RemoveEventLoopTimer (source->mac_timer_ref);
      source->mac_timer_ref = NULL;
    }
    else
    {
#ifdef	G_MAIN_MAC_DEBUG
      printf ("GLIB decrementing timer for main_iteration (by funcs)\n");
#endif
      /* source is using idle timer to handle events */
      g_main_iteration_timer_use_count--;
      
      if (g_main_iteration_timer_use_count == 0)
      {
#ifdef	G_MAIN_MAC_DEBUG
        printf ("GLIB removing timer for main_iteration (by funcs)\n");
#endif
        RemoveEventLoopTimer (g_main_iteration_timer_ref);
        g_main_iteration_timer_ref = NULL;
      }
    }
#endif

    g_hook_destroy_link (&source_list, hook);
  }

  G_UNLOCK (main_loop);

  return hook != NULL;
}

void
g_get_current_time (GTimeVal *result)
{
#ifndef _MSC_VER
  struct timeval r;
  g_return_if_fail (result != NULL);

  /*this is required on alpha, there the timeval structs are int's
    not longs and a cast only would fail horribly*/
  gettimeofday (&r, NULL);
  result->tv_sec = r.tv_sec;
  result->tv_usec = r.tv_usec;
#else
  /* Avoid calling time() except for the first time.
   * GetTickCount() should be pretty fast and low-level?
   * I could also use ftime() but it seems unnecessarily overheady.
   */
  static ULONGLONG start_tick = 0;
  static time_t start_time;
  ULONGLONG tick;
//  time_t t;

  g_return_if_fail (result != NULL);
 
  if (start_tick == 0)
    {
      start_tick = GetTickCount64 ();
      time (&start_time);
    }

  tick = GetTickCount64 ();
#if 1
    *result = tick;
#else
  result->tv_sec = (glong)((tick - start_tick) / 1000 + start_time);
  result->tv_usec = (glong)(((tick - start_tick) % 1000) * 1000);
#endif
#endif
}

/* Running the main loop */

/* HOLDS: main_loop_lock */
static void
g_main_dispatch (GTimeVal dispatch_time)
{
  while (pending_dispatches != NULL)
    {
      gboolean need_destroy;
      GSource *source = pending_dispatches->data;
      GSList *tmp_list;

      tmp_list = pending_dispatches;
      pending_dispatches = g_slist_remove_link (pending_dispatches, pending_dispatches);
      g_slist_free_1 (tmp_list);

      if (G_HOOK_IS_VALID (source))
	{
	  gboolean was_in_call;
	  gpointer hook_data = source->hook.data;
//	  gpointer source_data = source->source_data;
#if 0
	  gboolean (*dispatch) (gpointer,
				GTimeVal *,
				gpointer);
#endif
      GSourceFunc dispatch = (source->hook.source_funcs)->dispatch;
	  was_in_call = G_HOOK_IN_CALL (source);
	  source->hook.flags |= G_HOOK_FLAG_IN_CALL;
      source->dispatch_time = *dispatch_time;
      source->hook.data = hook_data;
	  G_UNLOCK (main_loop);

	  need_destroy = ! dispatch (source);
	  G_LOCK (main_loop);

	  if (!was_in_call)
	    source->hook.flags &= ~G_HOOK_FLAG_IN_CALL;
	  
	  if (need_destroy && G_HOOK_IS_VALID (source))
	    g_hook_destroy_link (&source_list, (GHook *) source);
	}

      g_hook_unref (&source_list, (GHook*) source);
    }
}

/* g_main_iterate () runs a single iteration of the mainloop, or,
 * if !dispatch checks to see if any sources need dispatching.
 * basic algorithm for dispatch=TRUE:
 *
 * 1) while the list of currently pending sources is non-empty,
 *    we call (*dispatch) on those that are !IN_CALL or can_recurse,
 *    removing sources from the list after each returns.
 *    the return value of (*dispatch) determines whether the source
 *    itself is kept alive.
 *
 * 2) call (*prepare) for sources that are not yet SOURCE_READY and
 *    are !IN_CALL or can_recurse. a return value of TRUE determines
 *    that the source would like to be dispatched immediatedly, it
 *    is then flagged as SOURCE_READY.
 *
 * 3) poll with the pollfds from all sources at the priority of the
 *    first source flagged as SOURCE_READY. if there are any sources
 *    flagged as SOURCE_READY, we use a timeout of 0 or the minimum
 *    of all timouts otherwise.
 *
 * 4) for each source !IN_CALL or can_recurse, if SOURCE_READY or
 *    (*check) returns true, add the source to the pending list.
 *    once one source returns true, stop after checking all sources
 *    at that priority.
 *
 * 5) while the list of currently pending sources is non-empty,
 *    call (*dispatch) on each source, removing the source
 *    after the call.
 *
 */
static gboolean
g_main_iterate (gboolean block,
		gboolean dispatch)
{
  GHook *hook;
  GTimeVal current_time  = { 0, 0 };
  gint n_ready = 0;
  gint current_priority = 0;
  gint timeout;
  gboolean retval = FALSE;

  g_return_val_if_fail (!block || dispatch, FALSE);

  g_get_current_time (&current_time);

  G_LOCK (main_loop);

#ifdef G_THREADS_ENABLED
  if (poll_waiting)
    {
      g_warning("g_main_iterate(): main loop already active in another thread");
      G_UNLOCK (main_loop);
      return FALSE;
    }
#endif
  
  /* If recursing, finish up current dispatch, before starting over */
  if (pending_dispatches)
    {
      if (dispatch)
	g_main_dispatch (&current_time);
      
      G_UNLOCK (main_loop);

      return TRUE;
    }

  /* Prepare all sources */

  timeout = block ? -1 : 0;
  
  hook = g_hook_first_valid (&source_list, TRUE);
  while (hook)
    {
      GSource *source = (GSource*) hook;
      gint source_timeout = -1;

      if ((n_ready > 0) && (source->priority > current_priority))
	{
	  g_hook_unref (&source_list, hook);
	  break;
	}
      if (G_HOOK_IN_CALL (hook) && !(hook->flags & G_SOURCE_CAN_RECURSE))
	{
	  hook = g_hook_next_valid (&source_list, hook, TRUE);
	  continue;
	}

      if (!(hook->flags & G_SOURCE_READY))
	{
#if 0
	  gboolean (*prepare)  (gpointer  source_data, 
				GTimeVal *current_time,
				gint     *timeout,
				gpointer  user_data);
#endif
	  GSourceFunc prepare = (hook->source_funcs)->prepare;
	  in_check_or_prepare++;
	  G_UNLOCK (main_loop);
      source->dispatch_time = current_time;
//bug?      source->timeout_data = source_timeout; 
	  if ((*prepare)(source))
        {	    source->hook.flags |= G_SOURCE_READY;
	    }
	  G_LOCK (main_loop);
	  in_check_or_prepare--;
	}

      if (hook->flags & G_SOURCE_READY)
	{
	  if (!dispatch)
	    {
	      g_hook_unref (&source_list, hook);
	      G_UNLOCK (main_loop);

	      return TRUE;
	    }
	  else
	    {
	      n_ready++;
	      current_priority = source->priority;
	      timeout = 0;
	    }
	}
      
      if (source_timeout >= 0)
	{
	  if (timeout < 0)
	    timeout = source_timeout;
	  else
	    timeout = MIN (timeout, source_timeout);
	}

      hook = g_hook_next_valid (&source_list, hook, TRUE);
    }

  /* poll(), if necessary */

  g_main_poll (timeout, n_ready > 0, current_priority);

  if (timeout != 0)
    g_get_current_time (&current_time);
  
  /* Check to see what sources need to be dispatched */

  n_ready = 0;
  
  hook = g_hook_first_valid (&source_list, TRUE);
  while (hook)
    {
      GSource *source = (GSource *)hook;

      if ((n_ready > 0) && (source->priority > current_priority))
	{
	  g_hook_unref (&source_list, hook);
	  break;
	}
      if (G_HOOK_IN_CALL (hook) && !(hook->flags & G_SOURCE_CAN_RECURSE))
	{
	  hook = g_hook_next_valid (&source_list, hook, TRUE);
	  continue;
	}

      if (!(hook->flags & G_SOURCE_READY))
	{
#if 0
	  gboolean (*check) (gpointer  source_data,
			     GTimeVal *current_time,
			     gpointer  user_data);
#endif
	  GSourceFunc check = (hook->source_funcs)->check;
	  in_check_or_prepare++;
	  G_UNLOCK (main_loop);
      source->dispatch_time = current_time;
        //, source->hook.data
	  if ((*check)(source))
	    hook->flags |= G_SOURCE_READY;

	  G_LOCK (main_loop);
	  in_check_or_prepare--;
	}

      if (hook->flags & G_SOURCE_READY)
	{
	  if (dispatch)
	    {
	      hook->flags &= ~G_SOURCE_READY;
	      g_hook_ref (&source_list, hook);
	      pending_dispatches = g_slist_prepend (pending_dispatches, source);
	      current_priority = source->priority;
	      n_ready++;
	    }
	  else
	    {
	      g_hook_unref (&source_list, hook);
	      G_UNLOCK (main_loop);

	      return TRUE;
	    }
	}
      
      hook = g_hook_next_valid (&source_list, hook, TRUE);
    }
 
  /* Now invoke the callbacks */

  if (pending_dispatches)
    {
      pending_dispatches = g_slist_reverse (pending_dispatches);
      g_main_dispatch (&current_time);
      retval = TRUE;
    }

  G_UNLOCK (main_loop);

  return retval;
}

/* See if any events are pending
 */
gboolean 
g_main_pending (void)
{
  return in_check_or_prepare ? FALSE : g_main_iterate(FALSE, FALSE);
}

/* Run a single iteration of the mainloop. If block is FALSE,
 * will never block
 */
gboolean
g_main_iteration (gboolean block)
{
  if (in_check_or_prepare)
    {
      g_warning ("g_main_iteration(): called recursively from within a source's check() or "
		 "prepare() member or from a second thread, iteration not possible");
      return FALSE;
    }
  else
    return g_main_iterate (block, TRUE);
}

GMainLoop*
g_main_new (gboolean is_running)
{
  GMainLoop *loop;

  loop = g_new0 (GMainLoop, 1);
  loop->is_running = is_running != FALSE;

  return loop;
}

void 
g_main_run (GMainLoop *loop)
{
  g_return_if_fail (loop != NULL);

  if (in_check_or_prepare)
    {
      g_warning ("g_main_run(): called recursively from within a source's check() or "
		 "prepare() member or from a second thread, iteration not possible");
      return;
    }
  
  loop->is_running = TRUE;

#ifdef MAC_CARBON_EVENTS
  RunApplicationEventLoop ();
#else
  while (loop->is_running)
    g_main_iterate (TRUE, TRUE);
#endif
}

void 
g_main_quit (GMainLoop *loop)
{
  g_return_if_fail (loop != NULL);

  loop->is_running = FALSE;

#ifdef MAC_CARBON_EVENTS
  QuitApplicationEventLoop ();
  
  if (g_main_iteration_timer_upp != NULL)
  {
    DisposeEventLoopTimerUPP(g_main_iteration_timer_upp);
    g_main_iteration_timer_upp = NULL;
  }
  
  if (g_idle_timer_upp != NULL)
  {
    DisposeEventLoopIdleTimerUPP(g_idle_timer_upp);
    g_idle_timer_upp = NULL;
  }
  
  if (g_timeout_timer_upp != NULL)
  {
    DisposeEventLoopTimerUPP(g_timeout_timer_upp);
    g_timeout_timer_upp = NULL;
  }
  
#endif
}

void 
g_main_destroy (GMainLoop *loop)
{
  g_return_if_fail (loop != NULL);

  g_free (loop);
}

gboolean
g_main_is_running (GMainLoop *loop)
{
  g_return_val_if_fail (loop != NULL, FALSE);

  return loop->is_running;
}

/* HOLDS: main_loop_lock */
void
g_main_poll (gint     timeout,
	     gboolean use_priority,
	     gint     priority)
{
#ifdef  G_MAIN_POLL_DEBUG
  GTimer *poll_timer;
#endif
  GPollFD *fd_array;
  GPollRec *pollrec;
  gint i;
  gint npoll;

#ifdef G_THREADS_ENABLED
#ifndef NATIVE_WIN32
  if (wake_up_pipe[0] < 0)
    {
      if (pipe (wake_up_pipe) < 0)
	g_error ("Cannot create pipe main loop wake-up: %s\n",
		 g_strerror (errno));

      wake_up_rec.fd = wake_up_pipe[0];
      wake_up_rec.events = G_IO_IN;
      g_main_add_poll_unlocked (0, &wake_up_rec);
    }
#else
  if (wake_up_semaphore == 0)
    {
      if ((wake_up_semaphore = (SOCKET) CreateSemaphore (NULL, 0, 100, NULL)) == 0)
	g_error ("Cannot create wake-up semaphore: %d", GetLastError ());
      wake_up_rec.fd = wake_up_semaphore;
      wake_up_rec.events = G_IO_IN;
      g_main_add_poll_unlocked (0, &wake_up_rec);
    }
#endif
#endif
  fd_array = g_new (GPollFD, n_poll_records);
 
  pollrec = poll_records;
  i = 0;
  while (pollrec && (!use_priority || priority >= pollrec->priority))
    {
      if (pollrec->fd->events)
	{
	  fd_array[i].fd = pollrec->fd->fd;
	  /* In direct contradiction to the Unix98 spec, IRIX runs into
	   * difficulty if you pass in POLLERR, POLLHUP or POLLNVAL
	   * flags in the events field of the pollfd while it should
	   * just ignoring them. So we mask them out here.
	   */
	  fd_array[i].events = pollrec->fd->events & ~(G_IO_ERR|G_IO_HUP|G_IO_NVAL);
	  fd_array[i].revents = 0;
	  i++;
	}
      
      pollrec = pollrec->next;
    }
#ifdef G_THREADS_ENABLED
  poll_waiting = TRUE;
  poll_changed = FALSE;
#endif
  
  npoll = i;
  if (npoll || timeout != 0)
    {
#ifdef	G_MAIN_POLL_DEBUG
      g_print ("g_main_poll(%d) timeout: %d\r", npoll, timeout);
      poll_timer = g_timer_new ();
#endif
      
      G_UNLOCK (main_loop);
      (*poll_func) (fd_array, npoll, timeout);
      G_LOCK (main_loop);
      
#ifdef	G_MAIN_POLL_DEBUG
      g_print ("g_main_poll(%d) timeout: %d - elapsed %12.10f seconds",
	       npoll,
	       timeout,
	       g_timer_elapsed (poll_timer, NULL));
      g_timer_destroy (poll_timer);
      pollrec = poll_records;
      i = 0;
      while (i < npoll)
	{
	  if (pollrec->fd->events)
	    {
	      if (fd_array[i].revents)
		{
		  g_print (" [%d:", fd_array[i].fd);
		  if (fd_array[i].revents & G_IO_IN)
		    g_print ("i");
		  if (fd_array[i].revents & G_IO_OUT)
		    g_print ("o");
		  if (fd_array[i].revents & G_IO_PRI)
		    g_print ("p");
		  if (fd_array[i].revents & G_IO_ERR)
		    g_print ("e");
		  if (fd_array[i].revents & G_IO_HUP)
		    g_print ("h");
		  if (fd_array[i].revents & G_IO_NVAL)
		    g_print ("n");
		  g_print ("]");
		}
	      i++;
	    }
	  pollrec = pollrec->next;
	}
      g_print ("\n");
#endif
    } /* if (npoll || timeout != 0) */
  
#ifdef G_THREADS_ENABLED
  if (!poll_waiting)
    {
#ifndef NATIVE_WIN32
      gchar c;
      read (wake_up_pipe[0], &c, 1);
#endif
    }
  else
    poll_waiting = FALSE;

  /* If the set of poll file descriptors changed, bail out
   * and let the main loop rerun
   */
  if (poll_changed)
    {
      g_free (fd_array);
      return;
    }
#endif

  pollrec = poll_records;
  i = 0;
  while (i < npoll)
    {
      if (pollrec->fd->events)
	{
	  pollrec->fd->revents = fd_array[i].revents;
	  i++;
	}
      pollrec = pollrec->next;
    }

  g_free (fd_array);
}

void 
g_main_add_poll (GPollFD *fd,
		 gint     priority)
{
  G_LOCK (main_loop);
  g_main_add_poll_unlocked (priority, fd);
  G_UNLOCK (main_loop);
}

/* HOLDS: main_loop_lock */
void 
g_main_add_poll_unlocked (gint     priority,
			  GPollFD *fd)
{
  GPollRec *lastrec, *pollrec, *newrec;

  if (!poll_chunk)
    poll_chunk = g_mem_chunk_create (GPollRec, 32, G_ALLOC_ONLY);

  if (poll_free_list)
    {
      newrec = poll_free_list;
      poll_free_list = newrec->next;
    }
  else
    newrec = g_chunk_new (GPollRec, poll_chunk);

  /* This file descriptor may be checked before we ever poll */
  fd->revents = 0;
  newrec->fd = fd;
  newrec->priority = priority;

  lastrec = NULL;
  pollrec = poll_records;
  while (pollrec && priority >= pollrec->priority)
    {
      lastrec = pollrec;
      pollrec = pollrec->next;
    }
  
  if (lastrec)
    lastrec->next = newrec;
  else
    poll_records = newrec;

  newrec->next = pollrec;

  n_poll_records++;
  
#ifdef MAC_CARBON_EVENTS  
  if (g_main_iteration_timer_ref != NULL) 
  {
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB incrementing timer for poll\n");
#endif
    g_main_iteration_timer_use_count++;
  }
  else
  {
    /* install a native Mac idle timer to handle these low-level glib sources */
    if (g_main_iteration_timer_upp == NULL)
    {
      g_main_iteration_timer_upp = NewEventLoopTimerUPP (mac_handle_g_main_iteration_action);
    }

#ifdef	G_MAIN_MAC_DEBUG    
    printf ("GLIB Creating timer for poll\n");
#endif
    InstallEventLoopTimer (GetMainEventLoop(), g_mac_main_iteration_interval,
                            g_mac_main_iteration_interval, g_main_iteration_timer_upp,
                            NULL, &g_main_iteration_timer_ref);
    
    g_main_iteration_timer_use_count++;
  }
#endif

#ifdef G_THREADS_ENABLED
  poll_changed = TRUE;

  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_wakeup ();
#endif
}

void 
g_main_remove_poll (GPollFD *fd)
{
  GPollRec *pollrec, *lastrec;

  G_LOCK (main_loop);
  
  lastrec = NULL;
  pollrec = poll_records;

  while (pollrec)
    {
      if (pollrec->fd == fd)
	{
	  if (lastrec != NULL)
	    lastrec->next = pollrec->next;
	  else
	    poll_records = pollrec->next;

	  pollrec->next = poll_free_list;
	  poll_free_list = pollrec;

	  n_poll_records--;
	  break;
	}
      lastrec = pollrec;
      pollrec = pollrec->next;
    }

#ifdef MAC_CARBON_EVENTS
#ifdef	G_MAIN_MAC_DEBUG
  printf ("GLIB decrementing timer for main_iteration (poll)\n");
#endif
  g_main_iteration_timer_use_count--;
    
  if (g_main_iteration_timer_use_count == 0)
  {
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB removing timer for main_iteration (poll)\n");
#endif
    RemoveEventLoopTimer (g_main_iteration_timer_ref);
    g_main_iteration_timer_ref = NULL;
  }
#endif

#ifdef G_THREADS_ENABLED
  poll_changed = TRUE;
  
  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_wakeup ();
#endif

  G_UNLOCK (main_loop);
}

void 
g_main_set_poll_func (GPollFunc func)
{
  if (func)
    poll_func = func;
  else
#ifdef HAVE_POLL
    poll_func = (GPollFunc) poll;
#else
    poll_func = (GPollFunc) g_poll;
#endif
}

/* Wake the main loop up from a poll() */
static void
g_main_wakeup (void)
{
#ifdef G_THREADS_ENABLED
  /* on MAC_CARBON_EVENTS poll() should never be waiting */
  if (poll_waiting)
    {
      poll_waiting = FALSE;
      
#ifndef NATIVE_WIN32
      write (wake_up_pipe[1], "A", 1);
#else
      ReleaseSemaphore ((HANDLE) wake_up_semaphore, 1, NULL);
#endif
    }
#endif
}

/* Timeouts */

#if 0
static void
g_timeout_set_expiration (GTimeoutData *data,
			  GTimeVal     *current_time)
{
  guint seconds = data->interval / 1000;
  guint msecs = data->interval - seconds * 1000;

  data->expiration.tv_sec = current_time->tv_sec + seconds;
  data->expiration.tv_usec = current_time->tv_usec + msecs * 1000;
  if (data->expiration.tv_usec >= 1000000)
    {
      data->expiration.tv_usec -= 1000000;
      data->expiration.tv_sec++;
    }
}
#else
static void
g_timeout_set_expiration(GTimeoutSource* timeout_source,
    gint64          current_time)
{
    gint64 expiration;

    if (timeout_source->seconds)
    {
        gint64 remainder;
        static gint timer_perturb = -1;

        if (timer_perturb == -1)
        {
            /*
             * we want a per machine/session unique 'random' value; try the dbus
             * address first, that has a UUID in it. If there is no dbus, use the
             * hostname for hashing.
             */
            const char* session_bus_address = g_getenv("DBUS_SESSION_BUS_ADDRESS");
            if (!session_bus_address)
                session_bus_address = g_getenv("HOSTNAME");
            if (session_bus_address)
                timer_perturb = ABS((gint)g_str_hash(session_bus_address)) % 1000000;
            else
                timer_perturb = 0;
        }

        expiration = current_time + (guint64)timeout_source->interval * 1000 * 1000;

        /* We want the microseconds part of the timeout to land on the
         * 'timer_perturb' mark, but we need to make sure we don't try to
         * set the timeout in the past.  We do this by ensuring that we
         * always only *increase* the expiration time by adding a full
         * second in the case that the microsecond portion decreases.
         */
        expiration -= timer_perturb;

        remainder = expiration % 1000000;
        if (remainder >= 1000000 / 4)
            expiration += 1000000;

        expiration -= remainder;
        expiration += timer_perturb;
    }
    else
    {
        expiration = current_time + (guint64)timeout_source->interval * 1000;
    }

    g_source_set_ready_time((GSource*)timeout_source, expiration);
}
#endif
#ifndef GTK_OLD_VERSION
static gboolean 
g_timeout_prepare  (gpointer  source_data, 
		    GTimeVal *current_time,
		    gint     *timeout,
		    gpointer  user_data)
#else
gboolean g_timeout_prepare(GSource  *source)
//,	    gint     *timeout)
#endif
{
#ifdef MAC_CARBON_EVENTS
  *timeout = 0;
  return FALSE;
#else
  glong msec;
  GTimeoutSource *data = &source->timeout_data;
  GTimeVal current_time;
  g_get_current_time(&current_time);
  msec = (data->expiration.tv_sec  - current_time.tv_sec) * 1000 +
         (data->expiration.tv_usec - current_time.tv_usec) / 1000;

  if (msec < 0)
    msec = 0;
  else if ((guint) msec > data->interval)
    {
      /* The system time has been set backwards, so we 
       * reset the expiration time to now + data->interval;
       * this at least avoids hanging for long periods of time.
       */
      GTimeVal current_time;
      g_get_current_time(&current_time);
    gint64 current_time = GetTickCount64();
      g_timeout_set_expiration (data, current_time);
      msec = data->interval;
    }
  source->timeout_data.expiration.tv_sec = 0;
  source->timeout_data.expiration.tv_usec = msec;
  return (msec == 0);
#endif
}

#ifndef GTK_OLD_VERSION
static gboolean 
g_timeout_check (gpointer  source_data,
		 GTimeVal *current_time,
		 gpointer  user_data)
#else
static gboolean 
g_timeout_check (GSource  *source)
#endif
{
#ifdef MAC_CARBON_EVENTS
  return FALSE;
#else
  GTimeoutData *data = &source->timeout_data;
  GTimeVal current_time;
  g_get_current_time(&current_time);
  return (data->expiration.tv_sec < current_time.tv_sec) ||
         ((data->expiration.tv_sec == current_time.tv_sec) &&
	  (data->expiration.tv_usec <= current_time.tv_usec));
#endif
}

static gboolean g_timeout_dispatch (GSource* source)
//,		    GTimeVal *dispatch_time,
//		    gpointer user_data)
{
  GTimeoutData* data = &source->timeout_data;
  if (data->callback (source->callback_data))//user_data))
    {
      g_timeout_set_expiration (data, &source->dispatch_time);
      return TRUE;
    }
  else
    return FALSE;
}

guint 
g_timeout_add_full (gint           priority,
		    guint          interval, 
		    GSourceFunc    function,
		    gpointer       data,
		    GDestroyNotify notify)
{
  GTimeoutData *timeout_data = g_new (GTimeoutData, 1);
  GTimeVal current_time;
  guint return_val;
  GSource *source;
  
#ifdef MAC_CARBON_EVENTS
  EventTimerInterval timer_interval;
#endif

  timeout_data->interval = interval;
  timeout_data->callback = function;
  g_get_current_time (&current_time);
  
  g_timeout_set_expiration (timeout_data, &current_time);
  
  G_LOCK (main_loop);

  if (!source_list.is_setup)
    {
      g_hook_list_init (&source_list, sizeof (GSource));

      source_list.hook_destroy = G_HOOK_DEFERRED_DESTROY;
      source_list.hook_free = g_source_destroy_func;
    }

  source = (GSource*) g_hook_alloc (&source_list);
  source->priority = priority;
  source->timeout_data = *timeout_data;
  source->hook.source_funcs = &timeout_funcs;
  source->hook.data = data;
  source->hook.destroy = notify;
  
  g_hook_insert_sorted (&source_list, 
			(GHook *)source, 
			g_source_compare);

  return_val = source->hook.hook_id;
  
#ifdef MAC_CARBON_EVENTS  
    /* install mac timer to handle */
    if (g_timeout_timer_upp == NULL)
    {
      g_timeout_timer_upp = NewEventLoopTimerUPP (mac_handle_timeout_action);
    }
    
    timer_interval = (interval * kEventDurationMillisecond);
    if (timer_interval == 0)
    {
      /* 
       * on Mac a timer_interval of 0 means a one shot timer
       * I believe glib expects a timer_interval of 0 to fire "constantly"
       */
#ifdef	G_MAIN_MAC_DEBUG
      printf("WARNING: timeout timer_interval is 0\n");
#endif
      
      /* microsecond I think should be plenty fast, but could go even shorter */
      timer_interval = kEventDurationMicrosecond;
    }
    
    /* 
     * priority is basically ignored here... No native Mac support for priority with
     * timers, they fire in order they were installed I believe
     */
    
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB creating Carbon event loop timer, interval: %f\n", timer_interval);
#endif
    InstallEventLoopTimer (GetMainEventLoop(), timer_interval,
                            timer_interval, g_timeout_timer_upp,
                            source, &source->mac_timer_ref);
#endif

#ifdef G_THREADS_ENABLED
  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_wakeup ();
#endif

  G_UNLOCK (main_loop);
  
  return return_val;
}

guint 
g_timeout_add (guint32        interval,
	       GSourceFunc    function,
	       gpointer       data)
{
  return g_timeout_add_full (G_PRIORITY_DEFAULT, 
			     interval, function, data, NULL);
}

/* Idle functions */

static gboolean g_idle_prepare  (GSource* source)
//,		 GTimeVal *current_time,
//		 gint     *timeout,
//		 gpointer  user_data)
{
#ifdef MAC_CARBON_EVENTS
  *timeout = 0;
  
  return FALSE;
#else
// timeout = 0;
  source->timeout_data.expiration.tv_sec = 0;
  source->timeout_data.expiration.tv_usec = 0;

  return TRUE;
#endif
}

static gboolean g_idle_check(gpointer  source_data)
//		 GTimeVal *current_time,
//		 gpointer  user_data)
{
#ifdef MAC_CARBON_EVENTS
  return FALSE;
#else
  return TRUE;
#endif
}

static gboolean g_idle_dispatch (GSource* source)
//		 GTimeVal *dispatch_time,
//		 gpointer user_data)
{
  GSourceFunc dispatch = source->source_funcs->dispatch;
  return dispatch(source->callback_data);//user_data);
}

guint 
g_idle_add_full (gint           priority,
		 GSourceFunc    function,
		 gpointer       data,
		 GDestroyNotify notify)
{
  guint return_val;
  GSource *source;
  
  g_return_val_if_fail (function != NULL, 0);  

  G_LOCK (main_loop);

  if (!source_list.is_setup)
    {
      g_hook_list_init (&source_list, sizeof (GSource));

      source_list.hook_destroy = G_HOOK_DEFERRED_DESTROY;
      source_list.hook_free = g_source_destroy_func;
    }

  source = (GSource*) g_hook_alloc (&source_list);
  source->priority = priority;
//  source->source_data = (gpointer) function;
  source->hook.source_funcs = &idle_funcs;
  source->hook.data = data;
  source->hook.destroy = notify;
  
  g_hook_insert_sorted (&source_list, 
			(GHook *)source, 
			g_source_compare);

  return_val = source->hook.hook_id;
  
#ifdef MAC_CARBON_EVENTS  
  if ( (UInt32)InstallEventLoopIdleTimer != (UInt32)kUnresolvedCFragSymbolAddress )
  { 
    if (g_idle_timer_upp == NULL)
    {
      g_idle_timer_upp = NewEventLoopIdleTimerUPP (mac_handle_idle_action);
    }

#ifdef	G_MAIN_MAC_DEBUG
  printf ("GLIB creating Carbon event loop idle timer\n");
#endif    
    /* priority is basically ignored here... */
    
    /* only fires if there is no user activity for 1 millisecond..
     * and then repeats at 1/100 second intervals as long as user activity doesn't
     * start again, lower "priority" than standar Mac EventLoopTimer...
     * could increase the idle delay here? (the amount of time user must be idle
     * before timer begins firing)  or increase the interval this may be agressive
     * but depends on how frequently gtk apps expect idle timers to fire
     */
    InstallEventLoopIdleTimer (GetMainEventLoop(), kEventDurationMillisecond,
                                (1.0 / 100.0), g_idle_timer_upp,
                                source, &source->mac_timer_ref);
  }
  else
  {
    /*
     * idle timers not supported prior to 10.2
     * use standard event loop timer to process with interval of 10 millis (1/100 second)
     */
    g_timeout_add_full (priority, 10, function, data, notify);
  }
#endif

#ifdef G_THREADS_ENABLED
  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_wakeup ();
#endif

  G_UNLOCK (main_loop);
  
  return return_val;
}

guint 
g_idle_add (GSourceFunc    function,
	    gpointer       data)
{
  return g_idle_add_full (G_PRIORITY_DEFAULT_IDLE, function, data, NULL);
}

gboolean
g_idle_remove_by_data (gpointer data)
{
  return g_source_remove_by_funcs_user_data (&idle_funcs, data);
}

#ifdef MAC_CARBON_EVENTS

static void
mac_handle_idle_action (EventLoopTimerRef timer_ref, EventLoopIdleTimerMessage state, void* user_data)
{
  GSource *source = user_data;
  GSourceFunc func = (GSourceFunc) source->source_data;
  OSStatus err;
  gboolean need_destroy;
  
  need_destroy = !func (source->hook.data);
  
  if (need_destroy)
  {
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB removing timer for idle\n");
#endif
    err = RemoveEventLoopTimer (timer_ref);
    source->mac_timer_ref = NULL;
    
    if (G_HOOK_IS_VALID (source))
    {
	    g_hook_destroy_link (&source_list, (GHook *) source);
    }
  }
}

static void 
mac_handle_timeout_action (EventLoopTimerRef timer_ref, void* user_data)
{
  GSource *source = user_data;
  GTimeoutData *data = source->source_data;
  OSStatus err;
  gboolean need_destroy;
  
  need_destroy = !data->callback (source->hook.data);
  
  if (need_destroy)
  {
#ifdef	G_MAIN_MAC_DEBUG
    printf ("GLIB removing timer for timeout\n");
#endif
    err = RemoveEventLoopTimer (timer_ref);
    source->mac_timer_ref = NULL;
    
    if (G_HOOK_IS_VALID (source))
    {
	    g_hook_destroy_link (&source_list, (GHook *) source);
    }
  }
}

static void 
mac_handle_g_main_iteration_action (EventLoopTimerRef timer_ref, void* user_data)
{
  /* could loop to allow this to handle more events per action? */
  gboolean events_pending = g_main_iteration (false);    

#ifdef	G_MAIN_MAC_DEBUG
  if (events_pending)
  {
    printf ("GLIB events still pending after g_main_iteration\n");
  }
#endif
}

#endif /* MAC_CARBON_EVENTS */
#ifdef G_THREADS_ENABLED
#if 0
const char* g_win32_error_message()
{   DWORD errorMessageID = GetLastError();
    if (errorMessageID == 0) {
        return ""; //No error message has been recorded
    }
    LPSTR messageBuffer = 0;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), messageBuffer, 0, NULL);
    enum { bufsize = 40 };
    static char msg[bufsize];
    strncpy(msg,messageBuffer,bufsize-1);
    msg[bufsize-1] = 0;
    return msg;
}
#endif
void 
g_main_context_init_pipe (GMainContext *context)
{
# ifndef G_OS_WIN32
  if (pipe (context->wake_up_pipe) < 0)
    g_error ("Cannot create pipe main loop wake-up: %s\n",
	     g_strerror (errno));
  
  context->wake_up_rec.fd = context->wake_up_pipe[0];
  context->wake_up_rec.events = G_IO_IN;
# else
  context->wake_up_semaphore = (SOCKET) CreateSemaphore (NULL, 0, 100, NULL);
  if (context->wake_up_semaphore == 0)
    g_error ("Cannot create wake-up semaphore: %s",
	     g_win32_error_message (GetLastError ()));
  context->wake_up_rec.fd = context->wake_up_semaphore;
  context->wake_up_rec.events = G_IO_IN;
#  ifdef G_MAIN_POLL_DEBUG
  g_print ("wake-up semaphore: %#x\n", (guint) context->wake_up_semaphore);
#  endif
# endif
  g_main_context_add_poll_unlocked (context, 0, &context->wake_up_rec);
}
#endif



/* HOLDS: main_loop_lock */
void 
g_main_context_add_poll_unlocked (GMainContext *context,
				  gint          priority,
				  GPollFD      *fd)
{
  GPollRec *lastrec, *pollrec, *newrec;

  if (!context->poll_chunk)
    context->poll_chunk = g_mem_chunk_create (GPollRec, 32, G_ALLOC_ONLY);

  if (context->poll_free_list)
    {
      newrec = context->poll_free_list;
      context->poll_free_list = newrec->next;
    }
  else
    newrec = g_chunk_new (GPollRec, context->poll_chunk);

  /* This file descriptor may be checked before we ever poll */
  fd->revents = 0;
  newrec->fd = fd;
  newrec->priority = priority;

  lastrec = NULL;
  pollrec = context->poll_records;
  while (pollrec && priority >= pollrec->priority)
    {
      lastrec = pollrec;
      pollrec = pollrec->next;
    }
  
  if (lastrec)
    lastrec->next = newrec;
  else
    context->poll_records = newrec;

  newrec->next = pollrec;

  context->n_poll_records++;

#ifdef G_THREADS_ENABLED
  context->poll_changed = TRUE;

  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_context_wakeup_unlocked (context);
#endif
}


/* HOLDS: context's lock */
/* Wake the main loop up from a poll() */
void
g_main_context_wakeup_unlocked (GMainContext *context)
{
#ifdef G_THREADS_ENABLED
  if (g_thread_supported() && context->poll_waiting)
    {
      context->poll_waiting = FALSE;
#ifndef G_OS_WIN32
      write (context->wake_up_pipe[1], "A", 1);
#else
      ReleaseSemaphore ((HANDLE) context->wake_up_semaphore, 1, NULL);
#endif
    }
#endif
}

/* Holds context's lock
 */
void
g_source_list_remove (GSource      *source,
		      GMainContext *context)
{
  if (source->prev)
    source->prev->next = source->next;
  else
    context->source_list = source->next;

  if (source->next)
    source->next->prev = source->prev;

  source->prev = NULL;
  source->next = NULL;
}


void
g_main_context_remove_poll_unlocked (GMainContext *context,
				     GPollFD      *fd)
{
  GPollRec *pollrec, *lastrec;

  lastrec = NULL;
  pollrec = context->poll_records;

  while (pollrec)
    {
      if (pollrec->fd == fd)
	{
	  if (lastrec != NULL)
	    lastrec->next = pollrec->next;
	  else
	    context->poll_records = pollrec->next;

#ifdef ENABLE_GC_FRIENDLY
	  pollrec->fd = NULL;  
#endif /* ENABLE_GC_FRIENDLY */

	  pollrec->next = context->poll_free_list;
	  context->poll_free_list = pollrec;

	  context->n_poll_records--;
	  break;
	}
      lastrec = pollrec;
      pollrec = pollrec->next;
    }

#ifdef G_THREADS_ENABLED
  context->poll_changed = TRUE;
  
  /* Now wake up the main loop if it is waiting in the poll() */
  g_main_context_wakeup_unlocked (context);
#endif
}


/**
 * g_main_context_default:
 * 
 * Returns the default main context. This is the main context used
 * for main loop functions when a main loop is not explicitly
 * specified.
 * 
 * Return value: the default main context.
 **/

static GMainContext *default_main_context;
static GSList *main_contexts_without_pipe = NULL;

GMainContext *
g_main_context_default (void)
{
  /* Slow, but safe */
  
  G_LOCK (main_loop);

  if (!default_main_context)
    default_main_context = g_main_context_new ();

  G_UNLOCK (main_loop);

  return default_main_context;
}


/**
 * g_source_set_callback_indirect:
 * @source: the source
 * @callback_data: pointer to callback data "object"
 * @callback_funcs: functions for reference counting @callback_data
 *                  and getting the callback and data
 * 
 * Sets the callback function storing the data as a refcounted callback
 * "object". This is used internally. Note that calling 
 * g_source_set_callback_indirect() assumes
 * an initial reference count on @callback_data, and thus
 * @callback_funcs->unref will eventually be called once more
 * than @callback_funcs->ref.
 **/
void
g_source_set_callback_indirect (GSource              *source,
				gpointer              callback_data,
				GSourceCallbackFuncs *callback_funcs)
{
  GMainContext *context;
  gpointer old_cb_data;
  GSourceCallbackFuncs *old_cb_funcs;
  
  g_return_if_fail (source != NULL);
  g_return_if_fail (callback_funcs != NULL || callback_data == NULL);

  context = source->context;

  if (context)
    LOCK_CONTEXT (context);

  old_cb_data = source->callback_data;
  old_cb_funcs = source->callback_funcs;

  source->callback_data = callback_data;
  source->callback_funcs = callback_funcs;
  
  if (context)
    UNLOCK_CONTEXT (context);
  
  if (old_cb_funcs)
    old_cb_funcs->unref (old_cb_data);
}


/**
 * g_main_context_new:
 * 
 * Creates a new #GMainContext strcuture
 * 
 * Return value: the new #GMainContext
 **/
GMainContext *
g_main_context_new ()
{
  GMainContext *context = g_new0 (GMainContext, 1);

#ifdef G_THREADS_ENABLED
  g_static_mutex_init (&context->mutex);

  context->owner = NULL;
  context->waiters = NULL;
#endif
      
  context->ref_count = 1;

  context->next_id = 1;
  
  context->source_list = NULL;
  
#if HAVE_POLL
  context->poll_func = (GPollFunc)poll;
#else
  context->poll_func = g_poll;
#endif
  
  context->cached_poll_array = NULL;
  context->cached_poll_array_size = 0;
  
  context->pending_dispatches = g_ptr_array_new ();
  
  context->time_is_current = FALSE;
  
#ifdef G_THREADS_ENABLED
  if (g_thread_supported ())
    g_main_context_init_pipe (context);
  else
    main_contexts_without_pipe = g_slist_prepend (main_contexts_without_pipe, 
						  context);
#endif

  return context;
}

/**
 * g_main_loop_new:
 * @context: a #GMainContext  (if %NULL, the default context will be used).
 * @is_running: set to %TRUE to indicate that the loop is running. This
 * is not very important since calling g_main_loop_run() will set this to
 * %TRUE anyway.
 *
 * Creates a new #GMainLoop structure.
 *
 * Return value: a new #GMainLoop.
 **/
GMainLoop*
g_main_loop_new(GMainContext* context,
    gboolean      is_running)
{
    GMainLoop* loop;

    if (!context)
        context = g_main_context_default();

    g_main_context_ref(context);

    loop = g_new0(GMainLoop, 1);
    loop->context = context;
    loop->is_running = is_running != FALSE;
    loop->ref_count = 1;

    return loop;
}

/**
 * g_main_context_wakeup:
 * @context: a #GMainContext
 *
 * If @context is currently waiting in a <function>poll()</function>, interrupt
 * the <function>poll()</function>, and continue the iteration process.
 **/
void
g_main_context_wakeup(GMainContext* context)
{
    if (!context)
        context = g_main_context_default();

    g_return_if_fail(context->ref_count > 0);

    LOCK_CONTEXT(context);
    g_main_context_wakeup_unlocked(context);
    UNLOCK_CONTEXT(context);
}


/**
 * g_source_ref:
 * @source: a #GSource
 *
 * Increases the reference count on a source by one.
 *
 * Return value: @source
 **/
GSource*
g_source_ref(GSource* source)
{
    GMainContext* context;

    g_return_val_if_fail(source != NULL, NULL);

    context = source->context;

    if (context)
        LOCK_CONTEXT(context);

    source->ref_count++;

    if (context)
        UNLOCK_CONTEXT(context);

    return source;
}

/* g_source_unref() but possible to call within context lock
 */
static void
g_source_unref_internal(GSource* source,
    GMainContext* context,
    gboolean      have_lock)
{
    gpointer old_cb_data = NULL;
    GSourceCallbackFuncs* old_cb_funcs = NULL;

    g_return_if_fail(source != NULL);

    if (!have_lock && context)
        LOCK_CONTEXT(context);

    source->ref_count--;
    if (source->ref_count == 0)
    {
        old_cb_data = source->callback_data;
        old_cb_funcs = source->callback_funcs;

        source->callback_data = NULL;
        source->callback_funcs = NULL;

        if (context && !SOURCE_DESTROYED(source))
        {
            g_warning(G_STRLOC ": ref_count == 0, but source is still attached to a context!");
            source->ref_count++;
        }
        else if (context)
            g_source_list_remove(source, context);

        if (source->source_funcs->destroy);//finalize)
            source->source_funcs->destroy(source);//finalize(source);

        g_slist_free(source->poll_fds);
        source->poll_fds = NULL;
        g_free(source);
    }

    if (!have_lock && context)
        UNLOCK_CONTEXT(context);

    if (old_cb_funcs)
    {
        if (have_lock)
            UNLOCK_CONTEXT(context);

        old_cb_funcs->unref(old_cb_data);

        if (have_lock)
            LOCK_CONTEXT(context);
    }
}

/**
 * g_source_unref:
 * @source: a #GSource
 *
 * Decreases the reference count of a source by one. If the
 * resulting reference count is zero the source and associated
 * memory will be destroyed.
 **/
void
g_source_unref(GSource* source)
{
    g_return_if_fail(source != NULL);

    g_source_unref_internal(source, source->context, FALSE);
}

/**
 * g_source_set_priority:
 * @source: a #GSource
 * @priority: the new priority.
 *
 * Sets the priority of a source. While the main loop is being
 * run, a source will be dispatched if it is ready to be dispatched and no sources
 * at a higher (numerically smaller) priority are ready to be dispatched.
 **/
void
g_source_set_priority(GSource* source,
    gint      priority)
{
    GSList* tmp_list;
    GMainContext* context;

    g_return_if_fail(source != NULL);

    context = source->context;

    if (context)
        LOCK_CONTEXT(context);

    source->priority = priority;

    if (context)
    {
        source->next = NULL;
        source->prev = NULL;

        tmp_list = source->poll_fds;
        while (tmp_list)
        {
            g_main_context_remove_poll_unlocked(context, tmp_list->data);
            g_main_context_add_poll_unlocked(context, priority, tmp_list->data);

            tmp_list = tmp_list->next;
        }

        UNLOCK_CONTEXT(source->context);
    }
}

/**
 * g_source_get_priority:
 * @source: a #GSource
 *
 * Gets the priority of a source.
 *
 * Return value: the priority of the source
 **/
gint
g_source_get_priority(GSource* source)
{
    g_return_val_if_fail(source != NULL, 0);

    return source->priority;
}


static void
g_source_callback_ref(gpointer cb_data)
{
    GSourceCallback* callback = cb_data;

    callback->ref_count++;
}


static void
g_source_callback_unref(gpointer cb_data)
{
    GSourceCallback* callback = cb_data;

    callback->ref_count--;
    if (callback->ref_count == 0)
    {
        if (callback->notify)
            callback->notify(callback->data);
        g_free(callback);
    }
}

static void
g_source_callback_get(gpointer     cb_data,
    GSource* source,
    GSourceFunc* func,
    gpointer* data)
{
    GSourceCallback* callback = cb_data;

    *func = callback->func;
    *data = callback->data;
}

static GSourceCallbackFuncs g_source_callback_funcs = {
  g_source_callback_ref,
  g_source_callback_unref,
  g_source_callback_get,
};

/**
 * g_source_set_callback:
 * @source: the source
 * @func: a callback function
 * @data: the data to pass to callback function
 * @notify: a function to call when @data is no longer in use, or %NULL.
 *
 * Sets the callback function for a source. The callback for a source is
 * called from the source's dispatch function.
 *
 * The exact type of @func depends on the type of source; ie. you
 * should not count on @func being called with @data as its first
 * parameter.
 *
 * Typically, you won't use this function. Instead use functions specific
 * to the type of source you are using.
 **/
void
g_source_set_callback(GSource* source,
    GSourceFunc     func,
    gpointer        data,
    GDestroyNotify  notify)
{
    GSourceCallback* new_callback;

    g_return_if_fail(source != NULL);

    new_callback = g_new(GSourceCallback, 1);

    new_callback->ref_count = 1;
    new_callback->func = func;
    new_callback->data = data;
    new_callback->notify = notify;

    g_source_set_callback_indirect(source, new_callback, &g_source_callback_funcs);
}

/**
 * g_source_add_poll:
 * @source:a #GSource
 * @fd: a #GPollFD structure holding information about a file
 *      descriptor to watch.
 *
 * Adds a file descriptor to the set of file descriptors polled for
 * this source. This is usually combined with g_source_new() to add an
 * event source. The event source's check function will typically test
 * the @revents field in the #GPollFD struct and return %TRUE if events need
 * to be processed.
 **/
void
g_source_add_poll(GSource* source,
    GPollFD* fd)
{
    GMainContext* context;

    g_return_if_fail(source != NULL);
    g_return_if_fail(fd != NULL);
    g_return_if_fail(!SOURCE_DESTROYED(source));

    context = source->context;

    if (context)
        LOCK_CONTEXT(context);

    source->poll_fds = g_slist_prepend(source->poll_fds, fd);

    if (context)
    {
        g_main_context_add_poll_unlocked(context, source->priority, fd);
        UNLOCK_CONTEXT(context);
    }
}

/**
 * g_source_attach:
 * @source: a #GSource
 * @context: a #GMainContext (if %NULL, the default context will be used)
 *
 * Adds a #GSource to a @context so that it will be executed within
 * that context.
 *
 * Return value: the ID for the source within the #GMainContext
 **/
guint
g_source_attach(GSource* source,
    GMainContext* context)
{
    guint result = 0;
    GSList* tmp_list;

    g_return_val_if_fail(source->context == NULL, 0);
    g_return_val_if_fail(!SOURCE_DESTROYED(source), 0);

    if (!context)
        context = g_main_context_default();

    LOCK_CONTEXT(context);

    source->context = context;
    result = source->source_id = context->next_id++;

    source->ref_count++;
    g_source_list_add(source, context);

    tmp_list = source->poll_fds;
    while (tmp_list)
    {
        g_main_context_add_poll_unlocked(context, source->priority, tmp_list->data);
        tmp_list = tmp_list->next;
    }

#ifdef G_THREADS_ENABLED
    /* Now wake up the main loop if it is waiting in the poll() */
    g_main_context_wakeup_unlocked(context);
#endif

    UNLOCK_CONTEXT(context);

    return result;
}

/**
 * g_main_context_ref:
 * @context: a #GMainContext
 *
 * Increases the reference count on a #GMainContext object by one.
 **/
void
g_main_context_ref(GMainContext* context)
{
    g_return_if_fail(context != NULL);
    g_return_if_fail(context->ref_count > 0);

    LOCK_CONTEXT(context);

    context->ref_count++;

    UNLOCK_CONTEXT(context);
}

/**
 * g_source_new:
 * @source_funcs: structure containing functions that implement
 *                the sources behavior.
 * @struct_size: size of the #GSource structure to create.
 *
 * Creates a new #GSource structure. The size is specified to
 * allow creating structures derived from #GSource that contain
 * additional data. The size passed in must be at least
 * <literal>sizeof (GSource)</literal>.
 *
 * The source will not initially be associated with any #GMainContext
 * and must be added to one with g_source_attach() before it will be
 * executed.
 *
 * Return value: the newly-created #GSource.
 **/
GSource*
g_source_new(GSourceFuncs* source_funcs,
    guint         struct_size)
{
    GSource* source;

    g_return_val_if_fail(source_funcs != NULL, NULL);
    g_return_val_if_fail(struct_size >= sizeof(GSource), NULL);

    source = (GSource*)g_malloc0(struct_size);

    source->source_funcs = source_funcs;
    source->ref_count = 1;

    source->priority = G_PRIORITY_DEFAULT;

    source->flags = G_HOOK_FLAG_ACTIVE;

    /* NULL/0 initialization for all other fields */

    return source;
}

// From glib 2.5.6:

void g_thread_init(GThreadFunctions* vtable)
{}


typedef struct GIdleSource
{
    GSource  source;
    gboolean one_shot;
} GIdleSource;


static void
g_source_set_name_full(GSource* source,
    const char* name,
    gboolean    is_static)
{
    GMainContext* context;

    g_return_if_fail(source != NULL);
    g_return_if_fail(g_atomic_int_get(&source->ref_count) > 0);

    context = source->context;

    if (context)
        LOCK_CONTEXT(context);

    //    TRACE(GLIB_SOURCE_SET_NAME(source, name));

        /* setting back to NULL is allowed, just because it's
         * weird if get_name can return NULL but you can't
         * set that.
         */

    if (!source->is_static) //priv->static_name)
        g_free(source->name);

    if (is_static)
        source->name = (char*)name;
    else
        source->name = g_strdup(name);

    source->is_static = is_static; //priv->static_name = is_static;

    if (context)
        UNLOCK_CONTEXT(context);
}

/**
 * g_source_set_name:
 * @source: a #GSource
 * @name: debug name for the source
 *
 * Sets a name for the source, used in debugging and profiling.
 * The name defaults to #NULL.
 *
 * The source name should describe in a human-readable way
 * what the source does. For example, "X11 event queue"
 * or "GTK repaint idle handler" or whatever it is.
 *
 * It is permitted to call this function multiple times, but is not
 * recommended due to the potential performance impact.  For example,
 * one could change the name in the "check" function of a #GSourceFuncs
 * to include details like the event type in the source name.
 *
 * Use caution if changing the name while another thread may be
 * accessing it with g_source_get_name(); that function does not copy
 * the value, and changing the value will free it while the other thread
 * may be attempting to use it.
 *
 * Also see g_source_set_static_name().
 *
 * Since: 2.26
 **/
void
g_source_set_name(GSource* source,
    const char* name)
{
    g_source_set_name_full(source, name, FALSE);
}

/**
 * g_source_set_static_name:
 * @source: a #GSource
 * @name: debug name for the source
 *
 * A variant of g_source_set_name() that does not
 * duplicate the @name, and can only be used with
 * string literals.
 *
 * Since: 2.70
 */
void
g_source_set_static_name(GSource* source,
    const char* name)
{
    g_source_set_name_full(source, name, TRUE);
}

static GSource*
idle_source_new(gboolean one_shot)
{
    GSource* source;
    GIdleSource* idle_source;

    source = g_source_new(&g_idle_funcs, sizeof(GIdleSource));
    idle_source = (GIdleSource*)source;

    idle_source->one_shot = one_shot;

    g_source_set_priority(source, G_PRIORITY_DEFAULT_IDLE);

    /* Set a default name on the source, just in case the caller does not. */
    g_source_set_static_name(source, "GIdleSource");

    return source;
}


/**
 * g_get_monotonic_time:
 *
 * Queries the system monotonic time.
 *
 * The monotonic clock will always increase and doesn't suffer
 * discontinuities when the user (or NTP) changes the system time.  It
 * may or may not continue to tick during times where the machine is
 * suspended.
 *
 * We try to use the clock that corresponds as closely as possible to
 * the passage of time as measured by system calls such as poll() but it
 * may not always be possible to do this.
 *
 * Returns: the monotonic time, in microseconds
 *
 * Since: 2.28
 **/
#if defined (G_OS_WIN32)
 /* NOTE:
  * time_usec = ticks_since_boot * usec_per_sec / ticks_per_sec
  *
  * Doing (ticks_since_boot * usec_per_sec) before the division can overflow 64 bits
  * (ticks_since_boot  / ticks_per_sec) and then multiply would not be accurate enough.
  * So for now we calculate (usec_per_sec / ticks_per_sec) and use floating point
  */
static gdouble g_monotonic_usec_per_tick = 0;

void
g_clock_win32_init(void)
{
    LARGE_INTEGER freq;

    if (!QueryPerformanceFrequency(&freq) || freq.QuadPart == 0)
    {
        /* The documentation says that this should never happen */
        g_assert_not_reached();
        return;
    }

    g_monotonic_usec_per_tick = (gdouble)G_USEC_PER_SEC / freq.QuadPart;
}

gint64
g_get_monotonic_time(void)
{
    if (G_LIKELY(g_monotonic_usec_per_tick != 0))
    {
        LARGE_INTEGER ticks;

        if (QueryPerformanceCounter(&ticks))
            return (gint64)(ticks.QuadPart * g_monotonic_usec_per_tick);

        g_warning("QueryPerformanceCounter Failed (%lu)", GetLastError());
        g_monotonic_usec_per_tick = 0;
    }

    return 0;
}
#elif defined(HAVE_MACH_MACH_TIME_H) /* Mac OS */
gint64
g_get_monotonic_time(void)
{
    mach_timebase_info_data_t timebase_info;
    guint64 val;

    /* we get nanoseconds from mach_absolute_time() using timebase_info */
    mach_timebase_info(&timebase_info);
    val = mach_absolute_time();

    if (timebase_info.numer != timebase_info.denom)
    {
#ifdef HAVE_UINT128_T
        val = ((__uint128_t)val * (__uint128_t)timebase_info.numer) / timebase_info.denom / 1000;
#else
        guint64 t_high, t_low;
        guint64 result_high, result_low;

        /* 64 bit x 32 bit / 32 bit with 96-bit intermediate
         * algorithm lifted from qemu */
        t_low = (val & 0xffffffffLL) * (guint64)timebase_info.numer;
        t_high = (val >> 32) * (guint64)timebase_info.numer;
        t_high += (t_low >> 32);
        result_high = t_high / (guint64)timebase_info.denom;
        result_low = (((t_high % (guint64)timebase_info.denom) << 32) +
            (t_low & 0xffffffff)) /
            (guint64)timebase_info.denom;
        val = ((result_high << 32) | result_low) / 1000;
#endif
    }
    else
    {
        /* nanoseconds to microseconds */
        val = val / 1000;
    }

    return val;
}
#else
gint64
g_get_monotonic_time(void)
{
    struct timespec ts;
    gint result;

    result = clock_gettime(CLOCK_MONOTONIC, &ts);

    if G_UNLIKELY(result != 0)
        g_error("GLib requires working CLOCK_MONOTONIC");

    return (((gint64)ts.tv_sec) * 1000000) + (ts.tv_nsec / 1000);
}
#endif

/**
 * g_idle_source_new:
 *
 * Creates a new idle source.
 *
 * The source will not initially be associated with any #GMainContext
 * and must be added to one with g_source_attach() before it will be
 * executed. Note that the default priority for idle sources is
 * %G_PRIORITY_DEFAULT_IDLE, as compared to other sources which
 * have a default priority of %G_PRIORITY_DEFAULT.
 *
 * Returns: the newly-created idle source
 **/
GSource*
g_idle_source_new(void)
{
    return idle_source_new(FALSE);
}

static GSource*
timeout_source_new(guint    interval,
    gboolean seconds,
    gboolean one_shot)
{
    GSource* source = g_source_new(&g_timeout_funcs, sizeof(GTimeoutSource));
    GTimeoutSource* timeout_source = (GTimeoutSource*)source;

    timeout_source->interval = interval;
    timeout_source->seconds = seconds;
    timeout_source->one_shot = one_shot;

    g_timeout_set_expiration(timeout_source, g_get_monotonic_time());

    return source;
}

/**
 * g_timeout_source_new:
 * @interval: the timeout interval in milliseconds.
 *
 * Creates a new timeout source.
 *
 * The source will not initially be associated with any #GMainContext
 * and must be added to one with g_source_attach() before it will be
 * executed.
 *
 * The interval given is in terms of monotonic time, not wall clock
 * time.  See g_get_monotonic_time().
 *
 * Returns: the newly-created timeout source
 **/
GSource*
g_timeout_source_new(guint interval)
{
    return timeout_source_new(interval, FALSE, FALSE);
}


/**
 * g_timeout_source_new_seconds:
 * @interval: the timeout interval in seconds
 *
 * Creates a new timeout source.
 *
 * The source will not initially be associated with any #GMainContext
 * and must be added to one with g_source_attach() before it will be
 * executed.
 *
 * The scheduling granularity/accuracy of this timeout source will be
 * in seconds.
 *
 * The interval given is in terms of monotonic time, not wall clock time.
 * See g_get_monotonic_time().
 *
 * Returns: the newly-created timeout source
 *
 * Since: 2.14
 **/
GSource*
g_timeout_source_new_seconds(guint interval)
{
    return timeout_source_new(interval, TRUE, FALSE);
}

#if 0
gchar* g_time_val_to_iso8601(GTimeVal* time_) 
{   return "Error";
}
#endif

/**
 * g_timeout_add_seconds:
 * @interval: the time between calls to the function, in seconds
 * @function: function to call
 * @data: data to pass to @function
 *
 * Sets a function to be called at regular intervals with the default
 * priority, %G_PRIORITY_DEFAULT.
 *
 * The function is called repeatedly until it returns %G_SOURCE_REMOVE
 * or %FALSE, at which point the timeout is automatically destroyed
 * and the function will not be called again.
 *
 * This internally creates a main loop source using
 * g_timeout_source_new_seconds() and attaches it to the main loop context
 * using g_source_attach(). You can do these steps manually if you need
 * greater control. Also see g_timeout_add_seconds_full().
 *
 * It is safe to call this function from any thread.
 *
 * Note that the first call of the timer may not be precise for timeouts
 * of one second. If you need finer precision and have such a timeout,
 * you may want to use g_timeout_add() instead.
 *
 * See [mainloop memory management](main-loop.html#memory-management-of-sources) for details
 * on how to handle the return value and memory management of @data.
 *
 * The interval given is in terms of monotonic time, not wall clock
 * time.  See g_get_monotonic_time().
 *
 * Returns: the ID (greater than 0) of the event source.
 *
 * Since: 2.14
 **/
guint
g_timeout_add_seconds(guint       interval,
    GSourceFunc function,
    gpointer    data)
{
    g_return_val_if_fail(function != NULL, 0);

    return g_timeout_add_seconds_full(G_PRIORITY_DEFAULT, interval, function, data, NULL);
}

// https://www.manpagez.com/html/glib/glib-2.42.1/glib-Hash-Tables.php

#if 0
gboolean g_hash_table_add(GHashTable* hash_table,gpointer key)
{   return 0;//g_hash_table_replace(key,key);
}

gpointer g_hash_table_find(GHashTable* hash_table,
    GHRFunc predicate,
    gpointer user_data)
{   return 0;
}

#endif
/* Holds context's lock
 */
static void
g_source_list_add(GSource* source,
    GMainContext* context)
{
    GSource* tmp_source, * last_source;

    last_source = NULL;
    tmp_source = context->source_list;
    while (tmp_source && tmp_source->priority <= source->priority)
    {
        last_source = tmp_source;
        tmp_source = tmp_source->next;
    }

    source->next = tmp_source;
    if (tmp_source)
        tmp_source->prev = source;

    source->prev = last_source;
    if (last_source)
        last_source->next = source;
    else
        context->source_list = source;
}

#if 0
/* Holds context's lock
 */
static void
g_source_list_remove(GSource* source,
    GMainContext* context)
{
    if (source->prev)
        source->prev->next = source->next;
    else
        context->source_list = source->next;

    if (source->next)
        source->next->prev = source->prev;

    source->prev = NULL;
    source->next = NULL;
}
#endif

/**
 * g_source_set_name:
 * @source: a #GSource
 * @name: debug name for the source
 *
 * Sets a name for the source, used in debugging and profiling.
 * The name defaults to #NULL.
 *
 * The source name should describe in a human-readable way
 * what the source does. For example, "X11 event queue"
 * or "GTK repaint idle handler" or whatever it is.
 *
 * It is permitted to call this function multiple times, but is not
 * recommended due to the potential performance impact.  For example,
 * one could change the name in the "check" function of a #GSourceFuncs
 * to include details like the event type in the source name.
 *
 * Use caution if changing the name while another thread may be
 * accessing it with g_source_get_name(); that function does not copy
 * the value, and changing the value will free it while the other thread
 * may be attempting to use it.
 *
 * Also see g_source_set_static_name().
 *
 * Since: 2.26
 **/
#if 0
void
g_source_set_name(GSource* source,
    const char* name)
{
    g_source_set_name_full(source, name, FALSE);
}
#endif

/**
 * g_timeout_add_seconds:
 * @interval: the time between calls to the function, in seconds
 * @function: function to call
 * @data: data to pass to @function
 *
 * Sets a function to be called at regular intervals with the default
 * priority, %G_PRIORITY_DEFAULT.
 *
 * The function is called repeatedly until it returns %G_SOURCE_REMOVE
 * or %FALSE, at which point the timeout is automatically destroyed
 * and the function will not be called again.
 *
 * This internally creates a main loop source using
 * g_timeout_source_new_seconds() and attaches it to the main loop context
 * using g_source_attach(). You can do these steps manually if you need
 * greater control. Also see g_timeout_add_seconds_full().
 *
 * It is safe to call this function from any thread.
 *
 * Note that the first call of the timer may not be precise for timeouts
 * of one second. If you need finer precision and have such a timeout,
 * you may want to use g_timeout_add() instead.
 *
 * See [mainloop memory management](main-loop.html#memory-management-of-sources) for details
 * on how to handle the return value and memory management of @data.
 *
 * The interval given is in terms of monotonic time, not wall clock
 * time.  See g_get_monotonic_time().
 *
 * Returns: the ID (greater than 0) of the event source.
 *
 * Since: 2.14
 **/
#if 0
guint
g_timeout_add_seconds(guint       interval,
    GSourceFunc function,
    gpointer    data)
{
    g_return_val_if_fail(function != NULL, 0);

    return g_timeout_add_seconds_full(G_PRIORITY_DEFAULT, interval, function, data, NULL);
}
#endif

/**
 * g_source_set_name:
 * @source: a #GSource
 * @name: debug name for the source
 *
 * Sets a name for the source, used in debugging and profiling.
 * The name defaults to #NULL.
 *
 * The source name should describe in a human-readable way
 * what the source does. For example, "X11 event queue"
 * or "GTK repaint idle handler" or whatever it is.
 *
 * It is permitted to call this function multiple times, but is not
 * recommended due to the potential performance impact.  For example,
 * one could change the name in the "check" function of a #GSourceFuncs
 * to include details like the event type in the source name.
 *
 * Use caution if changing the name while another thread may be
 * accessing it with g_source_get_name(); that function does not copy
 * the value, and changing the value will free it while the other thread
 * may be attempting to use it.
 *
 * Also see g_source_set_static_name().
 *
 * Since: 2.26
 **/
#if 0
void
g_source_set_name(GSource* source,
    const char* name)
{
    g_source_set_name_full(source, name, FALSE);
}
#endif


/**
 * g_timeout_add_seconds_full: (rename-to g_timeout_add_seconds)
 * @priority: the priority of the timeout source. Typically this will be in
 *   the range between %G_PRIORITY_DEFAULT and %G_PRIORITY_HIGH.
 * @interval: the time between calls to the function, in seconds
 * @function: function to call
 * @data: data to pass to @function
 * @notify: (nullable): function to call when the timeout is removed, or %NULL
 *
 * Sets a function to be called at regular intervals, with @priority.
 *
 * The function is called repeatedly until it returns %G_SOURCE_REMOVE
 * or %FALSE, at which point the timeout is automatically destroyed and
 * the function will not be called again.
 *
 * Unlike g_timeout_add(), this function operates at whole second granularity.
 * The initial starting point of the timer is determined by the implementation
 * and the implementation is expected to group multiple timers together so that
 * they fire all at the same time. To allow this grouping, the @interval to the
 * first timer is rounded and can deviate up to one second from the specified
 * interval. Subsequent timer iterations will generally run at the specified
 * interval.
 *
 * Note that timeout functions may be delayed, due to the processing of other
 * event sources. Thus they should not be relied on for precise timing.
 * After each call to the timeout function, the time of the next
 * timeout is recalculated based on the current time and the given @interval
 *
 * See [mainloop memory management](main-loop.html#memory-management-of-sources) for details
 * on how to handle the return value and memory management of @data.
 *
 * If you want timing more precise than whole seconds, use g_timeout_add()
 * instead.
 *
 * The grouping of timers to fire at the same time results in a more power
 * and CPU efficient behavior so if your timer is in multiples of seconds
 * and you don't require the first timer exactly one second from now, the
 * use of g_timeout_add_seconds() is preferred over g_timeout_add().
 *
 * This internally creates a main loop source using
 * g_timeout_source_new_seconds() and attaches it to the main loop context
 * using g_source_attach(). You can do these steps manually if you need
 * greater control.
 *
 * It is safe to call this function from any thread.
 *
 * The interval given is in terms of monotonic time, not wall clock
 * time.  See g_get_monotonic_time().
 *
 * Returns: the ID (greater than 0) of the event source.
 *
 * Since: 2.14
 **/
guint
g_timeout_add_seconds_full(gint           priority,
    guint32        interval,
    GSourceFunc    function,
    gpointer       data,
    GDestroyNotify notify)
{
    return timeout_add_full(priority, interval, TRUE, FALSE, function, data, notify);
}


/**
 * g_timeout_add_seconds:
 * @interval: the time between calls to the function, in seconds
 * @function: function to call
 * @data: data to pass to @function
 *
 * Sets a function to be called at regular intervals with the default
 * priority, %G_PRIORITY_DEFAULT.
 *
 * The function is called repeatedly until it returns %G_SOURCE_REMOVE
 * or %FALSE, at which point the timeout is automatically destroyed
 * and the function will not be called again.
 *
 * This internally creates a main loop source using
 * g_timeout_source_new_seconds() and attaches it to the main loop context
 * using g_source_attach(). You can do these steps manually if you need
 * greater control. Also see g_timeout_add_seconds_full().
 *
 * It is safe to call this function from any thread.
 *
 * Note that the first call of the timer may not be precise for timeouts
 * of one second. If you need finer precision and have such a timeout,
 * you may want to use g_timeout_add() instead.
 *
 * See [mainloop memory management](main-loop.html#memory-management-of-sources) for details
 * on how to handle the return value and memory management of @data.
 *
 * The interval given is in terms of monotonic time, not wall clock
 * time.  See g_get_monotonic_time().
 *
 * Returns: the ID (greater than 0) of the event source.
 *
 * Since: 2.14
 **/
#if 0
guint
g_timeout_add_seconds(guint       interval,
    GSourceFunc function,
    gpointer    data)
{
    g_return_val_if_fail(function != NULL, 0);

    return g_timeout_add_seconds_full(G_PRIORITY_DEFAULT, interval, function, data, NULL);
}

#endif

/**
 * g_source_set_name:
 * @source: a #GSource
 * @name: debug name for the source
 *
 * Sets a name for the source, used in debugging and profiling.
 * The name defaults to #NULL.
 *
 * The source name should describe in a human-readable way
 * what the source does. For example, "X11 event queue"
 * or "GTK repaint idle handler" or whatever it is.
 *
 * It is permitted to call this function multiple times, but is not
 * recommended due to the potential performance impact.  For example,
 * one could change the name in the "check" function of a #GSourceFuncs
 * to include details like the event type in the source name.
 *
 * Use caution if changing the name while another thread may be
 * accessing it with g_source_get_name(); that function does not copy
 * the value, and changing the value will free it while the other thread
 * may be attempting to use it.
 *
 * Also see g_source_set_static_name().
 *
 * Since: 2.26
 **/
#if 0
void
g_source_set_name(GSource* source,
    const char* name)
{
    g_source_set_name_full(source, name, FALSE);
}
#endif


/**
 * g_source_set_ready_time:
 * @source: a #GSource
 * @ready_time: the monotonic time at which the source will be ready,
 *              0 for "immediately", -1 for "never"
 *
 * Sets a #GSource to be dispatched when the given monotonic time is
 * reached (or passed).  If the monotonic time is in the past (as it
 * always will be if @ready_time is 0) then the source will be
 * dispatched immediately.
 *
 * If @ready_time is -1 then the source is never woken up on the basis
 * of the passage of time.
 *
 * Dispatching the source does not reset the ready time.  You should do
 * so yourself, from the source dispatch function.
 *
 * Note that if you have a pair of sources where the ready time of one
 * suggests that it will be delivered first but the priority for the
 * other suggests that it would be delivered first, and the ready time
 * for both sources is reached during the same main context iteration,
 * then the order of dispatch is undefined.
 *
 * It is a no-op to call this function on a #GSource which has already been
 * destroyed with g_source_destroy().
 *
 * This API is only intended to be used by implementations of #GSource.
 * Do not call this API on a #GSource that you did not create.
 *
 * Since: 2.36
 **/
void
g_source_set_ready_time(GSource* source,
    gint64   ready_time)
{
    GMainContext* context;

    g_return_if_fail(source != NULL);
    g_return_if_fail(g_atomic_int_get(&source->ref_count) > 0);

    context = source->context;

    if (context)
        LOCK_CONTEXT(context);

    if (source->priv->ready_time == ready_time)
    {
        if (context)
            UNLOCK_CONTEXT(context);

        return;
    }

    source->priv->ready_time = ready_time;

    TRACE(GLIB_SOURCE_SET_READY_TIME(source, ready_time));

    if (context)
    {
        /* Quite likely that we need to change the timeout on the poll */
        if (!SOURCE_BLOCKED(source))
            g_wakeup_signal(context->wakeup);
        UNLOCK_CONTEXT(context);
    }
}

/**
 * g_source_get_ready_time:
 * @source: a #GSource
 *
 * Gets the "ready time" of @source, as set by
 * g_source_set_ready_time().
 *
 * Any time before or equal to the current monotonic time (including 0)
 * is an indication that the source will fire immediately.
 *
 * Returns: the monotonic ready time, -1 for "never"
 **/
gint64
g_source_get_ready_time(GSource* source)
{
    g_return_val_if_fail(source != NULL, -1);
    g_return_val_if_fail(g_atomic_int_get(&source->ref_count) > 0, -1);

    return source->priv->ready_time;
}

