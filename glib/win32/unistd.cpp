// unistd.c stub for Windows
// Copyright 2024/9/24 Robin.Rowe@CinePaint.org
// License open source MIT

#include "unistd.h"
#include <chrono>
#include <thread>
#include <string>
#include <stdio.h>

int fcntl(int fd, int op, ...)
{	u_long argp = 0;
	return ioctlsocket(fd,op,&argp);
}

int gettimeofday(struct timeval* tv, struct timezone* tz)
{
	(void)tz;
	FILETIME ft;
	ULARGE_INTEGER t;
	ULONGLONG x;
	ULONGLONG m = 1000000;
	GetSystemTimeAsFileTime(&ft);
	t.LowPart = ft.dwLowDateTime;
	t.HighPart = ft.dwHighDateTime;
	x = t.QuadPart / m;
	tv->tv_sec = (long)x;
	x = t.QuadPart % m;
	tv->tv_usec = (long)x;
	return 0;
}


int sleep(useconds_t delay)
{	std::this_thread::sleep_for(std::chrono::seconds(delay));
	return 0;
}

int usleep(useconds_t delay)
{	std::this_thread::sleep_for(std::chrono::nanoseconds(delay));
	return 0;
}