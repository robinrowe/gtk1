// unistd.h stub for Windows
// Copyright 2024/8/3 Robin.Rowe@CinePaint.org
// License open source MIT

#ifndef unistd_h
#define unistd_h

#ifdef _WIN32
#  define STRICT		/* Strict typing, gives warning if Windows.h already included*/
#  define _WIN32_WINDOWS 0x0401 /* to get IsDebuggerPresent */
#  include <WinSock2.h>
#  include <windows.h>
#  include <io.h>
#  undef STRICT
#endif

#include <signal.h>
#include <time.h>
#include <memory.h>
#include <process.h>
#include <fcntl.h> 
#include <direct.h>
#include <stdint.h>

#ifdef __cplusplus
#define CFUNC extern "C"
#else
#define CFUNC extern
#endif

#define SIGRTMIN 32
#define SIGRTMAX 64
#define SIGALRM NSIG+1
#define SIGHUP NSIG+2
#define SIGQUIT NSIG+3
#define SIGBUS NSIG+4
#define SIGPIPE NSIG+5
#define SIGKILL NSIG+9
#define SIGCHLD NSIG+6
#define SIGUSR1 NSIG+7
#define SIGUSR2 NSIG+8

#define S_IXUSR _S_IEXEC
#define S_IRUSR _S_IREAD
#define S_IWUSR _S_IWRITE
#define S_IXOTH S_IEXEC
#define S_IXGRP S_IEXEC
#define S_IRWXU S_IRUSR|S_IWUSR|S_IXUSR
#define S_IRWXG S_IRGRP|S_IWGRP|S_IXGRP
#define S_IRWXO S_IROTH|S_IWOTH|S_IXOTH
//#define S_IRWXU _S_IEXEC|_S_IREAD|_S_IWRITE
//#define S_IRWXO _S_IEXEC|_S_IREAD|_S_IWRITE
//#define S_IRWXG _S_IEXEC|_S_IREAD|_S_IWRITE
#define S_IROTH S_IREAD
#define S_IRGRP S_IREAD
#define S_IWGRP S_IWRITE
#define S_IWOTH S_IWRITE
#define O_CLOEXEC 0
#define O_DIRECTORY _O_OBTAIN_DIR

#define R_OK 4
#define W_OK 2
#define X_OK 0
#define F_OK 0
#define S_ISDIR(x) (_S_IFDIR & x)
#define mkdir(name,flags) _mkdir(name)

typedef int pid_t;
#define PATH_MAX MAX_PATH 
#define getpid _getpid
#define isatty _isatty

#define pipe(pipes) _pipe((pipes),8*1024,_O_BINARY)
#define open _open
#define close _close
#define getcwd _getcwd
#define access _access
#define fileno _fileno
#define strdup _strdup
#define read _read
#define write _write
#define lseek _lseek
#define bzero(address,size) memset((address),0,size)
#define bzero1(x) memset((x),0,sizeof(x))
#define bzero2(x) memset((&x),0,sizeof(x))

typedef int mode_t;
typedef unsigned int ssize_t;
#define SSIZE_MAX UINT_MAX
#define O_NDELAY (_O_RANDOM+1)
#define F_GETFL 0
#define F_SETFL 0
#define S_ISREG(x) (_S_IFREG & x)
#define S_ISCHR(x) (_S_IFCHR & x)
#define S_ISBLK(x) (0)

typedef long long useconds_t;
CFUNC int sleep(useconds_t delay);
CFUNC int usleep(useconds_t delay);
CFUNC int fcntl(int fd, int op, ...);
CFUNC int gettimeofday(struct timeval* tv, struct timezone* tz);

#endif